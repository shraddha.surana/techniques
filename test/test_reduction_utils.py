from unittest import TestCase

from src.embeddings.reduction_utils import reduce_sequence, kd_hydrophobicitya


# TODO : Move to test dir
class AlphabetReductionTest(TestCase):

    def test_reduce_sequence_when_mapping_is_not_present(self):
        actual = reduce_sequence("ACSKMAA")
        expected = "ACSKMAA"

        self.assertEqual(expected, actual, 'Does not return same sequence when mapping is empty')

    def test_reduce_sequence_when_mapping_is_present(self):
        actual = reduce_sequence("ACSKMAA", kd_hydrophobicitya)
        expected = "cdbaccc"

        self.assertEqual(expected, actual, 'Does not correct mapping for kd_hydrophobicitya')
