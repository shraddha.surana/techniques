from unittest import TestCase

import pandas as pd

from src.files.utils import trim_all


class Test(TestCase):
    def test_trim_leading_trailing_spaces_and_commas(self):
        df = pd.DataFrame()
        df.loc[0, 'Activity'] = ' antibacterial, antimicrobial, mammaliancells, '
        actual = trim_all(df.Activity)
        self.assertCountEqual(['antibacterial, antimicrobial, mammaliancells'], actual)
