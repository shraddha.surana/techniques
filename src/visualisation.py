import argparse

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn import preprocessing

from src.files.amp import filter_data_for_selected_activities, get_positive_amp_data, \
    get_negative_amp_data, amp_positive_data_activities
from src.files.camp import massage_camp_data
from src.files.utils import contains, OTHER_ALPHABETS


def get_variables(args):
    return args.root, args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores


def arg_parser():
    root = "./"
    # output_dir = "data/results/basline_binary_clipped_data_rfc" if method == 'binary' else "data/results/basline_clipped_new_data_rfc"
    pos_data_type = 'filtered' if positive_filtered else 'unfiltered'
    curated_type = 'curated' if is_curated else 'CBBio'

    if is_clipped:
        if method == 'binary':
            output_dir = "data/results/dnn_basline_binary_{}_positive_clipped_data".format(pos_data_type)
        elif method == 'multi':
            if positive_only:
                output_dir = "data/results/dnn_basline_multi_{}_positive_only_clipped_data".format(pos_data_type)
            else:
                output_dir = "data/results/dnn_basline_multi_unfiltered_positive_clipped_data".format(pos_data_type)

        negative_data_file = 'data/raw/amp_curated_negative_sequence_length_10_to_60.csv'  # 0.4 similarity threshold with positive data
        positive_data_file = 'data/raw/amp_curated_positive_{}_sequence_length_10_to_60.csv'.format(
            pos_data_type)  # filtered 70% similarity threshold

    else:
        if method == 'binary':
            output_dir = "data/results/dnn_basline_binary_{}_positive".format(pos_data_type)
        elif method == 'multi':
            if positive_only:
                output_dir = "data/results/dnn_basline_multi_{}_positive_only".format(pos_data_type)
            else:
                output_dir = "data/results/dnn_basline_multi_unfiltered_positive".format(pos_data_type)

        negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
        positive_data_file = 'data/raw/AMP/positive_data_{}.csv'.format(
            pos_data_type)  # filtered 70% similarity threshold

    # output_dir = "data/results/basline_curate_binary_clipped_data_rfc" if method == 'binary' else "data/results/basline_curate_multi_clipped_data_rfc"

    cores = 10

    # negative_data_file = 'data/raw/amp_CBBio_negative_binary_sequence_length_10_to_60.csv'  # 0.4 similarity threshold with positive data
    # positive_data_file = 'data/raw/amp_CBBio_positive_binary_sequence_length_10_to_60.csv'  # filtered 70% similarity threshold

    # negative_data_file = 'data/raw/amp_curated_negative_sequence_length_10_to_60.csv'  # 0.4 similarity threshold with positive data
    # positive_data_file = 'data/raw/amp_curated_positive_{}_sequence_length_10_to_60.csv'.format(pos_data_type)  # filtered 70% similarity threshold

    # negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    # positive_data_file = 'data/raw/AMP/positive_data_{}.csv'.format(pos_data_type)  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


def additional_filtering_of_sequences(data):
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())
    indexNames = sequences[sequences['Sequence'] == 'GWLDVAKKIGKAAFNVAKNFLFNKAVNFAAKGIKKAVDLWG '].index
    sequences.drop(indexNames, inplace=True)
    sequences = sequences[sequences.Sequence.apply(lambda x: x.isalpha())]

    return sequences


if __name__ == "__main__":
    method = 'multi'
    positive_only = True
    positive_filtered = True
    is_curated = True
    is_clipped = False

    encoding_scores = []
    root, output_dir, negative_data_file, positive_data_file, cores = get_variables(arg_parser())
    output_dir = 'data/results/basline_binary_curated_unfiltered_positive_CBBio_clipped_data_rfc'

    random_states = list(range(1, 100, 1))

    raw_negative_data = get_negative_amp_data(root + negative_data_file)
    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
    # AMP_data = pd.concat([raw_negative_data, raw_positive_data])     # for new data
    AMP_data = pd.concat([negative_data, positive_data_filtered])  # for old data
    AMP_data_filtered = additional_filtering_of_sequences(AMP_data)
    AMP_data_filtered = AMP_data_filtered.reset_index(drop=True)

    if method == 'multi' and positive_only:
        AMP_data_filtered = additional_filtering_of_sequences(positive_data_filtered)
        AMP_data_filtered = AMP_data_filtered.reset_index(drop=True)

    if method == 'binary':
        target = AMP_data_filtered.Activity.apply(lambda x: 0 if x == ['none'] else 1)
        # target = AMP_data_filtered.Activity.apply(lambda x: 0 if x == 'None' else 1)      # for new data
    else:
        mlb = preprocessing.MultiLabelBinarizer()
        target = mlb.fit_transform(AMP_data_filtered['Activity'])

    tar_list = [''.join([str(label) for label in row]) for row in target.tolist()]
    label_comb, count = np.unique(tar_list, return_counts=True)
    multi_label_count = dict(zip(label_comb, count))

    plt.figure(figsize=(12, 8))
    sns.barplot(list(multi_label_count.keys()), np.cbrt(list(multi_label_count.values())))

    plt.ylabel(r'$\sqrt[3]{Sequence count}$')

    plt.xlabel('Multilabel combination')
    plt.xticks(rotation=45)

    plt.suptitle('Number of sequences for each label combination')

    plt.title('0: Absence, 1: Presence; in order of {}'.format(
        ['antibacterial', 'anticancer', 'antifungal', 'antiparasitic', 'antiviral']))

    for i, (k, v) in enumerate(multi_label_count.items()):
        plt.annotate(v, (i, np.cbrt(v)), ha='center', textcoords="offset points", xytext=(0, 1))

    # plt.show()
    plt.savefig("../multi_seq_count_curated_filtered_positive_only.png")
