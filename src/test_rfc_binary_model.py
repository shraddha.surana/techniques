import pickle

import biovec
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split

from src.classification.utils import calculate_scores
from src.dnn_classification_combinations import get_target_data
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import filter_data_for_selected_activities, get_positive_amp_data, get_negative_amp_data, \
    amp_positive_data_activities, reduce_amp
from src.files.camp import massage_camp_data

if __name__ == "__main__":

    root = "./"
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/dnn/dnn"
    cores = 10
    only_positive = False
    method = 'binary'

    positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'  # 0.4 similarity threshold with positive data
    negative_data_file = 'data/raw/negative_data_removed_cdhit.csv'  # 0.4 similarity threshold with positive data

    embeddings = {}
    all_vectors = {}
    all_errors = {}
    random_states = list(range(1, 2, 1))

    if (only_positive):
        raw_positive_data = get_positive_amp_data(root + positive_data_file)
        positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
        positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
        AMP_data = positive_data_filtered
    else:
        test_data_file = get_negative_amp_data(root + negative_data_file)
        data = massage_camp_data(test_data_file).reset_index().drop('index', axis=1)
        AMP_data = pd.concat([data])

    if (method == 'multiclass'):
        activity_column = list(map(sorted, AMP_data.Activity))
        unique, counts = np.unique(list(map(lambda x: " ".join(x), activity_column)), return_counts=True)
        classes_counts = dict(zip(unique, counts))
        valid_classes = list(filter(lambda k: classes_counts[k] >= 90, classes_counts))
        multiclass_filter = AMP_data.apply(lambda row: " ".join(row[1]) in valid_classes, axis=1)
        AMP_data = AMP_data[multiclass_filter].reset_index().drop('index', axis=1)

    # keys = list(np.unique(list(itertools.chain(*all_reductions_combinations))))
    keys = ['hydrophobicitya', 'blossom_matrix_4', 'contact_energies', 'None']  # dnn & rfc binary
    keys = ['conformation_similarity', 'kd_hydrophobicitya', 'blossom_matrix_4', 'contact_energies']  # divye
    keys = ['conformation_similarity', 'kd_hydrophobicitya', 'mj_4', 'None']
    reduced_data = reduce_amp(AMP_data, keys)
    target = get_target_data(method, reduced_data.classification)

    mlb = preprocessing.MultiLabelBinarizer()
    a = mlb.fit_transform(reduced_data.classification)

    for red in keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    for red in keys:
        if not all_vectors.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors[red] = inputs
            all_errors[red] = errors

    all_inputs = pd.DataFrame()
    for red in keys:
        all_inputs = pd.concat([all_inputs, all_vectors[red]], axis=1)

    loaded_model = pickle.load(open('./data/rfc_binary_tuned_smote', 'rb'))

    x_train, x_test, y_train, y_test = train_test_split(all_inputs, target, random_state=1)

    predicted = loaded_model.predict(x_test)

    scores = calculate_scores(y_test, predicted, method=method)
    print(scores)
