import numpy as np
from dask_ml.datasets import make_classification
from dask_ml.model_selection import HyperbandSearchCV
from distributed import Client
from sklearn.linear_model import SGDClassifier


client = Client(processes=True, threads_per_worker=1, n_workers=10, memory_limit='4GB')

X, y = make_classification(chunks=20)
est = SGDClassifier(tol=1e-3)
param_dist = {'alpha': np.logspace(-4, 0, num=1000),
              'loss': ['hinge', 'log'],
              'average': [True, False]}

search = HyperbandSearchCV(est, param_dist)
search.fit(X, y, classes=np.unique(y))
search.best_params_
