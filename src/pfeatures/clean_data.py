import pandas as pd

from src.embeddings.reduce_dataset import reduce_with_gap_tripeptide_frequency
from src.files.amp import amp_positive_data_activities, filter_data_for_selected_activities
from src.files.utils import trim_all, replace_brackets

if __name__ == "__main__":
    train = pd.read_csv("data/raw/AMP/positive_data_filtered2807.csv", index_col='index').reindex()
    test = pd.read_csv("data/multilabel/test_positive.csv", index_col='index').reindex()

    train['Activity'] = train['Activity'].apply(lambda x: trim_all(replace_brackets(x).split(",")))
    test['Activity'] = test['Activity'].apply(lambda x: trim_all(replace_brackets(x).split(",")))

    a = filter_data_for_selected_activities(train, amp_positive_data_activities)
    b = filter_data_for_selected_activities(test, amp_positive_data_activities)

    a['Activity'] = a['Activity'].apply(lambda x: ",".join(list(filter(lambda y: len(y) > 0, x))))
    b['Activity'] = b['Activity'].apply(lambda x: ",".join(list(filter(lambda y: len(y) > 0, x))))

    a.to_csv("data/multilabel/train.csv")
    b.to_csv("data/multilabel/test.csv")

    pd.read_csv('data/multilabel/train.csv')['Sequence'].to_csv("data/multilabel/train_sequences", index=False,
                                                                header=False)
    pd.read_csv('data/multilabel/test.csv')['Sequence'].to_csv("data/multilabel/test_sequences", index=False,
                                                               header=False)

    x = pd.read_csv('data/multilabel/test_sequences')
    y = pd.read_csv('data/multilabel/test_sequences_new')


    # create tripeptide sequences
    train_data = pd.read_csv('data/multilabel/train_sequences')
    test_data = pd.read_csv('data/multilabel/test_sequences')
    train_data.columns = ['Sequence']
    test_data.columns = ['Sequence']
    gaps = [1, 2]
    functions = {
        'tripeptide': reduce_with_gap_tripeptide_frequency
    }

    for gap in gaps:
        for peptide_type in list(functions.keys()):
            aa = functions[peptide_type](pd.concat([train_data, test_data]), gap)
            aa_train = aa[:train_data.shape[0]]
            aa_test = aa[train_data.shape[0]:].reset_index()
            aa_train.to_csv(f"./data/multilabel/pfeatures/{peptide_type}_{gap}_train.csv", index=False)
            aa_test.to_csv(f"./data/multilabel/pfeatures/{peptide_type}_{gap}_test.csv", index=False)
