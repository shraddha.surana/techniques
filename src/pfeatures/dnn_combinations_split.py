import argparse
from itertools import combinations

import pandas as pd
from imblearn import keras
from sklearn.model_selection import train_test_split

from src.classification.dnn import dnn_train_and_predict
from src.classification.utils import calculate_scores
from src.combined_reduction_all_combinations import multilabel
from src.dnn_classification_combinations import model3
from src.files.utils import trim_all, replace_brackets, save_json
import numpy as np
from sklearn import preprocessing

def get_variables(args):
    return args.root, args.cores


def arg_parser():
    root = "./"
    cores = 10

    # negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    # positive_data_file = 'data/raw/AMP/positive_data_unfiltered.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


def unique_combinations(keys):
    comb = []
    for n in range(1, len(keys) + 1):
        comb = comb + list(map(list, combinations(keys, n)))
    return comb


if __name__ == "__main__":
    root, cores = get_variables(arg_parser())

    available_p_features = {
        'amino_acid_composition': {
            'test': 'amino_acid_composition_test.csv',
            'train': 'amino_acid_composition_train.csv'
        },
        'DDOR': {
            'test': 'DDOR_test.csv',
            'train': 'DDOR_train.csv'
        },
        'dipeptide_1': {
            'test': 'dipeptide_1_test.csv',
            'train': 'dipeptide_1_train.csv'
        },
        'physico': {
            'test': 'physico_test.csv',
            'train': 'physico_train.csv'
        },
        'repetitive_residue_information': {
            'test': 'repetitive_residue_information_test.csv',
            'train': 'repetitive_residue_information_train.csv'
        },
        'secondary_structure': {
            'test': 'secondary_structure_test.csv',
            'train': 'secondary_structure_train.csv'
        },
        'shannon_entropy': {
            'test': 'shannon_entropy_test.csv',
            'train': 'shannon_entropy_train.csv'
        },
        'autocorrelation': {
            'test': 'autocorr_BEGF750101_test.csv',
            'train': 'autocorr_BEGF750101_train.csv'
        }
    }

    to_remove = ['ID', 'Sequence', 'aaindex']


    def read_data(file):
        data = pd.read_csv("data/intermediate/p-features/" + file, index_col=False)
        columns = list(filter(lambda x: to_remove.__contains__(x), list(data.columns)))
        return data.drop(columns=columns)


    p_features = {}
    all_p_features = list(available_p_features.keys())
    for key in all_p_features:
        p_features[key] = {}
        p_features[key]['test'] = read_data(available_p_features[key]['test'])
        p_features[key]['train'] = read_data(available_p_features[key]['train'])

    y_raw = pd.read_csv("data/intermediate/postive_unfiltered2807_4_activities.csv")['Activity']
    y_raw = y_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))

    mlb = preprocessing.MultiLabelBinarizer()

    scores = {}
    for keys in unique_combinations(all_p_features):
        print(keys)
        x = pd.DataFrame()
        for key in keys:
            x = pd.concat([x, p_features[key]['train']], axis=1)

        X_train, X_test, y_train, y_test = train_test_split(x, y_raw, random_state=1)

        y = mlb.fit_transform(pd.concat([y_train, y_test]))
        y_train = y[:y_train.shape[0]]
        y_test = y[y_train.shape[0]:]

        model = model3(x.shape[1], y_train.shape[1])

        try:
            predicted, classifier = dnn_train_and_predict(model, X_train, X_test, y_train)
        except:
            print("Combination Failed - " + ",".join(keys))
        else:
            combination = "-".join(keys)
            scores[combination] = calculate_scores(y_test, predicted, method='multilabel')
            print(scores[combination])

    print(scores)
    save_json("data/results/dnn_p_features_split.json", scores)
