from itertools import combinations

import biovec
import pandas as pd
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier

from src.classification.utils import calculate_scores, classify_and_predict
from src.embeddings.utils import convert_sequences_to_vectors, words_to_vec, create_file_name_using_parameters
from src.files.amp import reduce_amp, amp_positive_data_activities
from src.files.utils import trim_all, replace_brackets, save_json


def get_variables(args):
    return args.root, args.cores


def unique_combinations(keys):
    comb = []
    for n in range(1, len(keys) + 1):
        comb = comb + list(map(list, combinations(keys, n)))
    return comb


def rows(data):
    return data.shape[0]


if __name__ == "__main__":
    root = "./"
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"

    available_p_features = {
        'amino_acid_composition': {
            'test': 'amino_acid_compositions_test.csv',
            'train': 'amino_acid_compositions_train.csv'
        },
        'DDOR': {
            'test': 'DDOR_test.csv',
            'train': 'DDOR_train.csv'
        },
        'dipeptide_1': {
            'test': 'dipeptide_1_test.csv',
            'train': 'dipeptide_1_train.csv'
        },
        'dipeptide_2': {
            'test': 'dipeptide_2_test.csv',
            'train': 'dipeptide_2_train.csv'
        },
        'physico': {
            'test': 'physico-chemical_test.csv',
            'train': 'physico-chemical_train.csv'
        },
        'residue_repeats': {
            'test': 'residue_repeats_test.csv',
            'train': 'residue_repeats_train.csv'
        },
        'secondary_structure': {
            'test': 'secondary_structure_test.csv',
            'train': 'secondary_structure_train.csv'
        },
        'shannon_entropy': {
            'test': 'Shannon_Entropy_residue_test.csv',
            'train': 'Shannon_Entropy_residue_train.csv'
        },
        'cetd': {
            'test': 'cetd_test.csv',
            'train': 'cetd_train.csv'
        },
        # 'tripeptide': {
        #     'test': 'tripep_test.csv',
        #     'train': 'tripep_train.csv'
        # }
        # ,
        # 'autocorrelation': {
        #     'test': 'autocorr_BEGF750101_test.csv',
        #     'train': 'autocorr_BEGF750101_train.csv'
        # }
    }

    to_remove = ['ID', 'Sequence', 'aaindex']


    def read_data(file):
        data = pd.read_csv("data/multilabel/pfeatures/" + file, index_col=False)
        columns = list(filter(lambda x: to_remove.__contains__(x), list(data.columns)))
        return data.drop(columns=columns)


    train_raw = pd.read_csv("data/multilabel/train.csv", index_col="index")
    test_raw = pd.read_csv("data/multilabel/test.csv", index_col="index")

    train_y_raw = train_raw['Activity']
    test_y_raw = test_raw['Activity']

    embeddings = {}
    all_vectors_X_train = {}
    all_vectors_X_test = {}
    all_errors = {}

    reduction_keys = ['blossom_matrix_8', 'contact_energies']
    print("reducing train data")
    reduced_train_data = reduce_amp(train_raw, reduction_keys)
    print("reducing test data")
    reduced_test_data = reduce_amp(test_raw, reduction_keys)

    print("Reading embeddings")
    for red in reduction_keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    print("Creating train vectors")
    for red in reduction_keys:
        if not all_vectors_X_train.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_train_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors_X_train[red] = inputs
            all_errors[red] = errors

    print("Creating Test vectors")
    for red in reduction_keys:
        if not all_vectors_X_test.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_test_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors_X_test[red] = inputs
            all_errors[red] = errors

    print("Reading Features")
    p_features = {}
    all_p_features = list(available_p_features.keys())

    all_keys = all_p_features + reduction_keys

    for key in all_p_features:
        p_features[key] = {}
        p_features[key]['test'] = read_data(available_p_features[key]['test'])
        p_features[key]['train'] = read_data(available_p_features[key]['train'])

    for key in reduction_keys:
        p_features[key] = {}
        p_features[key]['test'] = all_vectors_X_test[key].reset_index().drop(['index'], axis='columns')
        p_features[key]['train'] = all_vectors_X_train[key].reset_index().drop(['index'], axis='columns')

    train_y_raw = pd.read_csv("data/multilabel/train.csv")['Activity']
    test_y_raw = pd.read_csv("data/multilabel/test.csv")['Activity']

    train_y_raw = train_y_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))
    test_y_raw = test_y_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))

    mlb = preprocessing.MultiLabelBinarizer()
    y = mlb.fit_transform(pd.concat([train_y_raw, test_y_raw]))

    train_y = y[:train_y_raw.shape[0]]
    test_y = y[train_y_raw.shape[0]:]

    scores = {}
    for keys in unique_combinations(all_keys):
        print(keys)
        train_x = pd.DataFrame()
        test_x = pd.DataFrame()
        for key in keys:
            train_x = pd.concat([train_x, p_features[key]['train']], axis=1)
            test_x = pd.concat([test_x, p_features[key]['test']], axis=1)

        train_data = pd.concat([train_x, pd.DataFrame(train_y, columns=mlb.classes_)], axis=1)
        test_data = pd.concat([test_x, pd.DataFrame(test_y, columns=mlb.classes_)], axis=1)

        for label in list(mlb.classes_):
            train_label_positive = train_data[train_data[label] == 1]
            train_label_negative = train_data[train_data[label] == 0][:rows(train_label_positive)]
            test_label_positive = test_data[test_data[label] == 1]
            test_label_negative = test_data[test_data[label] == 0][:rows(test_label_positive)]

            train_label = pd.concat([train_label_positive, train_label_negative])
            train_label_x = train_label.iloc[:, :-4]
            train_label_y = train_label.iloc[:, -4:]

            test_label = pd.concat([test_label_positive, test_label_negative])
            test_label_x = test_label.iloc[:, :-4]
            test_label_y = test_label.iloc[:, -4:]

            print("For label: ", label)
            classifier = RandomForestClassifier(n_jobs=10)

            predicted, model = classify_and_predict(train_label_x, test_label_x, train_label_y, classifier)
            combination = "-".join(keys)
            scores[combination] = {}
            scores[combination][label] = calculate_scores(test_label_y, predicted, method='multilabel')
            print(scores[combination][label])

    print(scores)
    save_json("data/results/rfc_multilabel_p_features_cleaned_5.json", scores)
