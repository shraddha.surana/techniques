import dask_ml.model_selection as dcv
import pandas as pd
from dask.diagnostics import ProgressBar
from sklearn.ensemble import RandomForestClassifier

from src.classification.utils import calculate_scores
from src.combined_reduction_all_combinations import multilabel
from src.files.utils import trim_all, replace_brackets, save_json
from src.pfeatures.dnn_combinations import unique_combinations
from src.random_search.random_forest_search_cv import create_random_search_grid

if __name__ == "__main__":

    available_p_features = {
        'amino_acid_composition': {
            'test': 'amino_acid_compositions_test.csv',
            'train': 'amino_acid_compositions_train.csv'
        },
        # 'DDOR': {
        #     'test': 'DDOR_test.csv',
        #     'train': 'DDOR_train.csv'
        # },
        # 'dipeptide_1': {
        #     'test': 'dipeptide_1_test.csv',
        #     'train': 'dipeptide_1_train.csv'
        # },
        # 'dipeptide_2': {
        #     'test': 'dipeptide_2_test.csv',
        #     'train': 'dipeptide_2_train.csv'
        # },
        # 'physico': {
        #     'test': 'physico-chemical_test.csv',
        #     'train': 'physico-chemical_train.csv'
        # },
        # 'residue_repeats': {
        #     'test': 'residue_repeats_test.csv',
        #     'train': 'residue_repeats_train.csv'
        # },
        # 'secondary_structure': {
        #     'test': 'secondary_structure_test.csv',
        #     'train': 'secondary_structure_train.csv'
        # },
        # 'shannon_entropy': {
        #     'test': 'Shannon_Entropy_residue_test.csv',
        #     'train': 'Shannon_Entropy_residue_train.csv'
        # },
        # 'cetd': {
        #     'test': 'cetd_test.csv',
        #     'train': 'cetd_train.csv'
        # },
        # 'autocorrelation': {
        #     'test': 'auto_correlation_test.csv',
        #     'train': 'auto_correlation_train.csv'
        # },
        # 'tripeptide_1': {
        #     'test': 'tripeptide_1_test.csv',
        #     'train': 'tripeptide_1_train.csv'
        # },
        # 'tripeptide_2': {
        #     'test': 'tripeptide_2_test.csv',
        #     'train': 'tripeptide_2_train.csv'
        # }
    }

    to_remove = ['ID', 'Sequence', 'aaindex', 'index']


    def read_data(file):
        data = pd.read_csv("data/multilabel/pfeatures/" + file, index_col=False)
        columns = list(filter(lambda x: to_remove.__contains__(x), list(data.columns)))
        return data.drop(columns=columns)


    train = pd.read_csv("data/multilabel/train.csv", index_col="index")
    test = pd.read_csv("data/multilabel/test.csv", index_col="index")

    train_y_raw = train['Activity']
    test_y_raw = test['Activity']

    print("Reading Features")
    p_features = {}
    all_p_features = list(available_p_features.keys())
    all_keys = all_p_features

    print("Total keys", len(all_keys))

    for key in all_p_features:
        p_features[key] = {}
        p_features[key]['test'] = read_data(available_p_features[key]['test'])
        p_features[key]['train'] = read_data(available_p_features[key]['train'])

    train_y_raw = pd.read_csv("data/multilabel/train.csv")['Activity']
    test_y_raw = pd.read_csv("data/multilabel/test.csv")['Activity']

    train_y_raw = train_y_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))
    test_y_raw = test_y_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))

    y = multilabel(pd.concat([train_y_raw, test_y_raw]))
    train_y = y[:train_y_raw.shape[0]]
    test_y = y[train_y_raw.shape[0]:]

    scores = {}
    all_combinations = unique_combinations(all_keys)
    print("Total combinations : ", str(len(all_combinations)))
    for keys in all_combinations:
        print(keys)
        train_x = pd.DataFrame()
        test_x = pd.DataFrame()
        for key in keys:
            train_x = pd.concat([train_x, p_features[key]['train']], axis=1)
            train_x = train_x.fillna(0)
            test_x = pd.concat([test_x, p_features[key]['test']], axis=1)
            test_x = test_x.fillna(0)

        classifier = RandomForestClassifier(n_jobs=10)

        parameters = create_random_search_grid()
        print(parameters)
        clf = dcv.GridSearchCV(
            classifier,
            parameters,
            n_jobs=10,
            cv=5
        )

        pbar = ProgressBar()
        pbar.register()
        clf.fit(pd.concat([train_x, test_x]), pd.concat([pd.DataFrame(train_y), pd.DataFrame(test_y)]))
        pbar.unregister()

        sorted(clf.cv_results_.keys())
        search_cv = clf
        results = search_cv.cv_results_
        print(results)
        print("======================= Print the best params ==========================")
        print(clf.best_params_)
        best_model = clf.best_estimator_
        predicted = best_model.predict(test_x)
        scores = calculate_scores(test_y, predicted, method='multilabel')

        save_json("./data/results/dask_RandomizedSearchCV_best", clf.best_params_)
        save_json("./data/results/dask_RandomizedSearchCV_all", results)
