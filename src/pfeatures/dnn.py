import argparse

import pandas as pd

from src.classification.dnn import dnn_train_and_predict
from src.classification.utils import calculate_scores
from src.combined_reduction_all_combinations import multilabel
from src.dnn_classification_combinations import model3
from src.files.utils import trim_all, replace_brackets


def get_variables(args):
    return args.root, args.cores


def arg_parser():
    root = "./"
    cores = 10

    # negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    # positive_data_file = 'data/raw/AMP/positive_data_unfiltered.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


if __name__ == "__main__":
    root, cores = get_variables(arg_parser())
    shannon_data_train_x = pd.read_csv(
        "data/intermediate/p-features/repetitive_residue_information_train.csv", index_col=False)
    shannon_data_test_x = pd.read_csv(
        "data/intermediate/p-features/repetitive_residue_information_test.csv", index_col=False)
    to_remove = ['ID', 'Sequence']
    columns = list(filter(lambda x: to_remove.__contains__(x), list(shannon_data_test_x.columns)))
    shannon_data_train_x = shannon_data_train_x.drop(columns=columns)
    shannon_data_test_x = shannon_data_test_x.drop(columns=columns)

    train_y_raw = pd.read_csv("data/intermediate/postive_unfiltered2807_4_activities.csv")['Activity']
    test_y_raw = pd.read_csv("data/intermediate/positive_data_removed_cdhit_4_activities.csv")['Activity']

    train_y_raw = train_y_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))
    test_y_raw = test_y_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))

    y = multilabel(pd.concat([train_y_raw, test_y_raw]))
    train_y = y[:train_y_raw.shape[0]]
    test_y = y[train_y_raw.shape[0]:]

    model = model3(shannon_data_train_x.shape[1], train_y.shape[1])

    predicted, classifier = dnn_train_and_predict(model, shannon_data_train_x, shannon_data_test_x, train_y)

    scores = calculate_scores(test_y, predicted, method='multilabel')

    print(scores)
