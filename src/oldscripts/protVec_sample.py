# from deepchem import load

import numpy as np
# import tensorflow as tf
import tensorflow.compat.v1 as tf

tf.disable_v2_behavior()
# the above 2 lines because of - AttributeError: module 'tensorflow' has no attribute 'placeholder'`.

# GETTING CORPUS
corpus_raw = list()
sequences = ['MDDKFTTLPCELEDYPGSITCPHCSAQITTAVDHVVGKMSWVVCTAITLACLPCCCIPFLCNSTKDVRHTCPKCKQAVFVYKIL',
             'MLVIFLGILGLLANQVSSQLVGQLHPTENPSENELEYWCTYMECCQFCWDCQNGLCVNKLGNTTILENEYVHPCIVSRWLNK']
for sequence in sequences:
    corpus_raw.append(' '.join(map(''.join, zip(*[iter(sequence)] * 3))))
corpus_raw = ' . '.join(corpus_raw).lower()
print(corpus_raw)

# creating word index
words = []
for word in corpus_raw.split():
    if word != '.':
        words.append(word)
words = set(words)
word2int = {}
int2word = {}
vocab_size = len(words)
for i, word in enumerate(words):
    word2int[word] = i
    int2word[i] = word

# list the sentences as a list of words¶
raw_sentences = corpus_raw.split('.')
sentences = []
for sentence in raw_sentences:
    sentences.append(sentence.split())

# creating i/p o/p pairs


data = []
WINDOW_SIZE = 2
for sentence in sentences:
    for word_index, word in enumerate(sentence):
        for nb_word in sentence[max(word_index - WINDOW_SIZE, 0): min(word_index + WINDOW_SIZE, len(sentence)) + 1]:
            if nb_word != word:
                data.append([word, nb_word])

print(data)


# creating one hot encoding vectors


def to_one_hot(data_point_index, vocab_size):
    temp = np.zeros(vocab_size)
    temp[data_point_index] = 1
    return temp


x_train = []
y_train = []
for data_word in data:
    x_train.append(to_one_hot(word2int[data_word[0]], vocab_size))
    y_train.append(to_one_hot(word2int[data_word[1]], vocab_size))
x_train = np.asarray(x_train)
y_train = np.asarray(y_train)
print(y_train)

# fixing i/p o/p vector size through place holder
x = tf.placeholder(tf.float32, shape=(None, vocab_size))
y_label = tf.placeholder(tf.float32, shape=(None, vocab_size))

# fixing size of w1 (weight matrix) through place holder
EMBEDDING_DIM = 5
W1 = tf.Variable(tf.random_normal([vocab_size, EMBEDDING_DIM]))
b1 = tf.Variable(tf.random_normal([EMBEDDING_DIM]))
hidden_representation = tf.add(tf.matmul(x, W1), b1)

# fixing size of the w2 (weight matrix) through place holder
W2 = tf.Variable(tf.random_normal([EMBEDDING_DIM, vocab_size]))
b2 = tf.Variable(tf.random_normal([vocab_size]))

# fixing output softmax function
prediction = tf.nn.softmax(tf.add(tf.matmul(hidden_representation, W2), b2))

# loss function
cross_entropy_loss = tf.reduce_mean(-tf.reduce_sum(y_label * tf.log(prediction), reduction_indices=[1]))

# training
train_step = tf.train.GradientDescentOptimizer(0.1).minimize(cross_entropy_loss)
sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)
n_iters = 10000
for i in range(n_iters):
    sess.run(train_step, feed_dict={x: x_train, y_label: y_train})
    print(sess.run(cross_entropy_loss, feed_dict={x: x_train, y_label: y_train}))

# getting word vectors
vectors = sess.run(W1 + b1)
print(vectors[word2int['kft']])


# finding closest words
def euclidean_dist(vec1, vec2):
    return np.sqrt(np.sum((vec1 - vec2) ** 2))


def find_closest(word_index, vectors):
    min_dist = 10000  # to act like positive infinity
    min_index = -1
    query_vector = vectors[word_index]
    for index, vector in enumerate(vectors):
        if euclidean_dist(vector, query_vector) < min_dist and not np.array_equal(vector, query_vector):
            min_dist = euclidean_dist(vector, query_vector)
            min_index = index
    return min_index


print(int2word[find_closest(word2int['kft'], vectors)])

# Visualization
from sklearn.manifold import TSNE

model = TSNE(n_components=2, random_state=0)
np.set_printoptions(suppress=True)
vectors = model.fit_transform(vectors)

from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler(feature_range=(0.2, 0.8))
scaler.fit(vectors)
vectors = scaler.transform(vectors)
import matplotlib.pyplot as plt

fig_size = plt.rcParams["figure.figsize"]
fig_size[0] = 10
fig_size[1] = 10
plt.rcParams["figure.figsize"] = fig_size
fig, ax = plt.subplots()
for word in words:
    # print(word, vectors[word2int[word]][1])
    ax.annotate(word, (vectors[word2int[word]][0], vectors[word2int[word]][1]))
plt.show()
