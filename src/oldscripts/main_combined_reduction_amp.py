import biovec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import sklearn.metrics as skm
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split

from src.amp_binary_classification import run_random_forest_variation
from src.oldscripts.classify_alphabet_reduced_kd_hydrophobicitya import convert_sequences_to_vectors
from src.embeddings.reduce_dataset import reduce
from src.embeddings.reduction_utils import conformation_similarity, kd_hydrophobicitya
from src.embeddings.utils import words_to_vec
from src.files.camp import read_camp_data
from src.oldscripts.main import get_negative_amp_data, get_positive_amp_data
from src.utils import print_confusion_matrix

if __name__ == "__main__":
    ROOT_DIR = ""
    negative_data_filtered_path = ROOT_DIR + 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_filtered_path = ROOT_DIR + 'data/raw/AMP/positive_data_filtered.csv'  # filtered 70% similarity threshold
    positive_data_unfiltered_path = ROOT_DIR + 'positive_data_unfiltered.csv'

    # ---- Reading the negative dataaset ----
    ndata = get_negative_amp_data(negative_data_filtered_path)
    print(ndata.columns)

    # ---- Read the positive dataset ----
    pdata = get_positive_amp_data(positive_data_filtered_path)
    print(pdata.columns)
    # ---- Binary classification ----

    pdata['Activity'] = 'antimicrobial'

    # ---- ----

    # ---- join the data  + cleanse ----
    data = pd.concat([ndata, pdata])
    basic_sequences = read_camp_data(data).reset_index().drop('index', axis=1)
    basic_sequences.shape

    indexNames = basic_sequences[basic_sequences['Sequence'] == 'GWLDVAKKIGKAAFNVAKNFLFNKAVNFAAKGIKKAVDLWG '].index
    # Delete these row indexes from dataFrame
    basic_sequences.drop(indexNames, inplace=True)

    # ---- reduce data ----

    conformation_embedding_file = ROOT_DIR + 'data/embeddings/uniprot__kmer_5_contextWindow_5_vector_300_reduction_conformation_similarity'
    kd_hydrophobicitya_embedding_file = ROOT_DIR + 'data/embeddings/uniprot__kmer_3_contextWindow_5_vector_300_reduction_kd_hydrophobicitya'

    conformation_reduced_data = pd.DataFrame(reduce(pd.DataFrame(basic_sequences), conformation_similarity))
    kd_hydrophobicitya_reduced_data = pd.DataFrame(reduce(pd.DataFrame(basic_sequences), kd_hydrophobicitya))
    reduced_data = pd.DataFrame()
    reduced_data['conformation'] = conformation_reduced_data[0]
    reduced_data['kd_hydrophobicitya'] = kd_hydrophobicitya_reduced_data[0]
    reduced_data['classification'] = kd_hydrophobicitya_reduced_data[1]
    # then work on output to do multi label classification
    mlb = preprocessing.MultiLabelBinarizer()
    target = mlb.fit_transform(reduced_data['classification'])
    mlb.classes_

    # --- Prot vec ----
    # Use above to classify dataset.
    conformation_pv = biovec.models.load_protvec(conformation_embedding_file)
    kd_hydrophobicitya_pv = biovec.models.load_protvec(kd_hydrophobicitya_embedding_file)

    # convert to protein embeddings
    inputs_conformation, errors_conformation = convert_sequences_to_vectors(reduced_data['conformation'],
                                                                            conformation_pv, words_to_vec)
    inputs_kd_hydrophobicitya, errors_kd_hydrophobicitya = convert_sequences_to_vectors(
        reduced_data['kd_hydrophobicitya'], kd_hydrophobicitya_pv, words_to_vec)
    inputs = np.append(inputs_conformation.values, inputs_kd_hydrophobicitya.values, axis=1)
    # sequences = data[data.apply(lambda r: not contains(other_alphabets, r['Sequence']), axis=1)]

    # reduced_data_without_error_seq = reduced_data[reduced_data.apply(lambda r: r[0] not in errors, axis=1)]

    # ---- Classification ----

    # split train test
    X_train, X_test, Y_train, Y_test = train_test_split(inputs, target, random_state=11)

    # ---- Random Forest ----
    rfc1 = RandomForestClassifier()
    rfc100 = RandomForestClassifier(n_estimators=100, max_depth=100, n_jobs=10, verbose=True)
    model, y_predict = run_random_forest_variation(rfc100, X_train, X_test, Y_train, Y_test)
    # model, y_predict = run_random_forest_variation(rfc1, X_train, X_test, Y_train, Y_test)

    # ---- DNN  ----

    # TODO: Implement DNN for multilabel classification

    # ---- Evaluation ----
    # --- Confusion Matrix ----
    labels = ['anitifungal', 'anti-diabetes', 'anti-hsv', 'anti-protists', 'anti-sepsis', 'anti-tb', 'anti-toxin',
              'antiallodynic', 'antibacterial', 'anticancer', 'antifungal', 'antihiv', 'antiinflammatory',
              'antilisterial', 'antimalarial', 'antimicrobial', 'antimrsa', 'antinociceptive', 'antioxidant',
              'antiparasitic', 'antiplasmodial', 'antiprotozoal', 'antitumour', 'antiviral', 'antiyeast', 'cancercells',
              'enzymeinhibitor', 'hemolysis', 'immunomodulatory', 'insecticidal', 'mammaliancells', 'nan', 'none',
              'spermicidal', 'toxin', 'woundhealing']

    print_confusion_matrix(Y_test, y_predict, labels)

    # ---- Classification report ----
    print(skm.classification_report(Y_test, y_predict))  # , labels=labels

    # ---- Trying multilabel confusion matrix ---- the following should not be used ----
    cm = confusion_matrix(Y_test.argmax(axis=1), y_predict.argmax(
        axis=1))  # For each prediction takes the max prediction value and assumes that to be the class for the row.. so essentially this is a multiclass classification confusion matrix.. but incorrect as it takes the first value encountered as the class of the row.
    cm
    fig, ax = plt.subplots(figsize=(10, 10))
    sns.heatmap(cm, ax=ax, square=True, annot=True, cmap="RdPu", cbar=False, xticklabels=mlb.classes_)
    # ---- ----

    # roc_auc_score(Y_test, y_predict)
    # roc_curve(Y_test, y_predict)
    # auc(Y_test, y_predict)
