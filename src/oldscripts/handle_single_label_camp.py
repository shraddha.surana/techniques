import biovec
import pandas as pd
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC

from src.oldscripts.classify_alphabet_reduced_kd_hydrophobicitya import convert_sequences_to_vectors, \
    classify_model_with_results
from src.embeddings.reduce_dataset import reduce
from src.embeddings.reduction_utils import kd_hydrophobicitya
from src.files.utils import trim_all, contains, OTHER_ALPHABETS


def massage_CAMP_single_label_data(data):
    sequences = data[['Sequence', 'Activity']]
    sequences = sequences[sequences.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())
    sequences['Activity'] = sequences['Activity'].apply(lambda x: str(x).lower())
    return sequences


if __name__ == "__main__":
    ROOT_DIR = ""
    raw_data = ROOT_DIR + 'data/raw/CAMP.txt'
    CAMP_single_label = ROOT_DIR + 'data/intermediate/CAMP_single_label.txt'
    CAMP_multi_label = ROOT_DIR + 'data/intermediate/CAMP_multi_label.txt'
    embedding_file = ROOT_DIR + 'data/embeddings/uniprot__kmer_5_contextWindow_5_vector_300_reduction_kd_hydrophobicitya'

    data = pd.read_csv(raw_data, sep="\t", header=0)
    single_label_records = pd.DataFrame()
    multi_label_records = pd.DataFrame()

    for i, row in data.iterrows():
        activities = trim_all(str(row[10]).split(','))
        if len(activities) > 1:
            multi_label_records = multi_label_records.append(row)
        else:
            single_label_records = single_label_records.append(row)

    multi_label_records.to_csv(CAMP_multi_label, index=False, sep="\t")
    single_label_records.to_csv(CAMP_single_label, index=False, sep="\t")

    basic_sequences = massage_CAMP_single_label_data(single_label_records)

    reduced_data = reduce(pd.DataFrame(basic_sequences), kd_hydrophobicitya)

    # then work on output to do multi label classification
    le = preprocessing.LabelEncoder()
    reduced_data[1] = le.fit_transform(reduced_data[1])

    # Use above to classify dataset.
    pv = biovec.models.load_protvec(embedding_file)

    # convert to protein embeddings
    inputs, errors = convert_sequences_to_vectors(reduced_data, pv)

    # sequences = data[data.apply(lambda r: not contains(other_alphabets, r['Sequence']), axis=1)]

    reduced_data_without_error_seq = reduced_data[reduced_data.apply(lambda r: r[0] not in errors, axis=1)]
    # split train test
    X_train, X_test, Y_train, Y_test = train_test_split(inputs, reduced_data_without_error_seq.iloc[:, 1],
                                                        random_state=3)

    print("Classify SVM : ")
    classify_model_with_results(X_train, X_test, Y_train, Y_test, SVC(gamma='auto'))
    # Classify SVM :
    # Time Taken :  9.23466400000001
    # confusion_matrix :
    # [[ 303    0    0  201    0    0    0]
    #  [   0    0    0    1    0    0    0]
    #  [  18    0   14   22    0    0    0]
    #  [ 111    0    0 1098    0    0    0]
    #  [   1    0    0    1    0    0    0]
    #  [   2    0    0    3    0    2    0]
    #  [   1    0    0    1    0    0    0]]
    # accuracy_score :
    # 0.7965148960089938

    print("RandomForestClassifier : ")
    classify_model_with_results(X_train, X_test, Y_train, Y_test, RandomForestClassifier())
    # RandomForestClassifier :
    # Time Taken :  6.507203000000004
    # confusion_matrix :
    # [[ 312    0    0  192    0    0    0]
    #  [   1    0    0    0    0    0    0]
    #  [  16    0   13   25    0    0    0]
    #  [  92    0    2 1115    0    0    0]
    #  [   0    0    0    2    0    0    0]
    #  [   1    0    0    4    0    2    0]
    #  [   1    0    0    1    0    0    0]]
    # accuracy_score :
    # 0.8105677346824058
