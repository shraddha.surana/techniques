import argparse

import biovec
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split

from src.classification.random_forest import rm_classifier_for_various_states
from src.classification.utils import get_best_result, add_properties_to_result
from src.embeddings.utils import convert_sequences_to_vectors, words_to_vec, create_file_name_using_parameters
from src.files.amp import get_negative_amp_data, get_positive_amp_data, amp_positive_data_activities, amp_activities, \
    reduce_amp, filter_data_for_selected_activities
from src.files.camp import massage_camp_data
from src.files.utils import save_json
from src.utils import print_confusion_matrix


def get_variables(args):
    return args.root, args.kmer, args.vector_size, args.context_size, args.embedding_file, args.reduction, \
           args.output_dir, args.negative_data_file, args.positive_data_file, args.cores


def arg_parser():
    root = "./"
    kmer = 5
    context_size = 5
    vector_size = 300
    reduction = 'mj_4'
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/rfc"
    cores = 10

    negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/AMP/positive_data_filtered2107.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--reduction', help='Which reduction to use', default=reduction)
    parser.add_argument('--embedding-file', help='Embedding file path', default=embedding_file)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


if __name__ == "__main__":
    root, kmer, vector_size, context_size, embedding_file, reduction, output_dir, negative_data_file, positive_data_file, cores = \
        get_variables(arg_parser())

    raw_negative_data = get_negative_amp_data(root + negative_data_file)
    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)

    # top_activities = get_top_activities(raw_positive_data, 11)

    positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)

    AMP_data = pd.concat([negative_data, positive_data_filtered])

    reduced_data = reduce_amp(AMP_data, list(reduction))

    # reduced_data.to_csv("data/intermediate/reduced_mj_4_negative_data_amp.csv", index=False, header=True)

    mlb = preprocessing.MultiLabelBinarizer()
    target = mlb.fit_transform(reduced_data['classification'])

    embedding = biovec.models.load_protvec(
        create_file_name_using_parameters(root + embedding_file, reduction, kmer, context_size, vector_size)
    )

    # convert to protein embeddings
    inputs, errors = convert_sequences_to_vectors(reduced_data['sequence'], embedding, words_to_vec, kmer)

    # reduced_data_with_error_seq = reduced_data[~reduced_data.apply(lambda r: r[0] not in errors, axis=1)]
    # reduced_data_without_error_seq = reduced_data[reduced_data.apply(lambda r: r[0] not in errors, axis=1)]
    # reduced_data_with_error_seq.to_csv("data/intermediate/camp_data_errors.csv", index=False, header=False)

    results = rm_classifier_for_various_states(
        states=list(range(1, 100, 1)),
        values=inputs.values,
        to_predict=target,
        classifier=None,  # classifier = RandomForestClassifier()
        cores=cores
    )

    result = get_best_result(results)

    scores = add_properties_to_result(kmer, context_size, vector_size, reduction, result[0], result[2].get_params())
    file_name = create_file_name_using_parameters(root + output_dir, reduction, kmer, context_size, vector_size)
    save_json(file_name + ".json", scores)

    X_train, X_test, Y_train, Y_test = train_test_split(inputs.values, target, random_state=result[0]['random_state'])

    predicted = result[1]
    print_confusion_matrix(Y_test, predicted, amp_activities)

# print("Classify SVM : ")
# svc = SVC(gamma='auto', kernel='linear')
# classify_model(X_train, X_test, Y_train, Y_test, svc)

# Best Feature for Random Search
# {'n_estimators': 300,
#  'min_samples_split': 2,
#  'min_samples_leaf': 1,
#  'max_features': 'sqrt',
#  'max_depth': 40,
#  'bootstrap': False
#  }

# run_random_search_cv(X_train, X_test, Y_train, Y_test, amp_positive_data_activities + amp_negative_data_activities)

# print("=======================feature Importance==========================")
# plot_feature_importance(inputs.columns, model.feature_importances_)
