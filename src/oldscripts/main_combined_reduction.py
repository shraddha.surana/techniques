import argparse

import biovec
from sklearn import preprocessing

from src.classification.random_forest import classify_combination
from src.classification.utils import add_properties_to_result
from src.embeddings.utils import convert_sequences_to_vectors, words_to_vec, create_file_name_using_parameters
from src.files.amp import reduce_amp, load_amp_data
from src.files.utils import save_json, trim_all


def get_variables(args):
    return args.root, args.kmer, args.vector_size, args.context_size, args.embedding_file, \
           args.reductions, args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores


def arg_parser():
    root = "./"
    kmer = 5
    context_size = 5
    vector_size = 300
    reductions = 'mj_4,mj_5,conformation_similarity'
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/rfc"
    cores = 10

    negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/AMP/positive_data_filtered.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--reductions', help='Which reduction to use', default=reductions)
    parser.add_argument('--embedding-file', help='Embedding file path', default=embedding_file)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


if __name__ == "__main__":
    root, kmer, vector_size, context_size, embedding_file, reductions, output_dir, negative_data_file, positive_data_file, cores = \
        get_variables(arg_parser())

    all_reductions = trim_all(reductions.split(","))
    embeddings = {}
    all_vectors = {}
    all_errors = {}
    random_states = list(range(1, 100, 1))

    AMP_data = load_amp_data(root, positive_data_file, negative_data_file)

    reduced_data = reduce_amp(AMP_data, all_reductions)

    mlb = preprocessing.MultiLabelBinarizer()
    target = mlb.fit_transform(reduced_data['classification'])

    for red in all_reductions:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    for red in all_reductions:
        if not all_vectors.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors[red] = inputs
            all_errors[red] = errors

    result = classify_combination(all_reductions, all_vectors, random_states, target, cores)

    scores = add_properties_to_result(
        kmer,
        context_size,
        vector_size,
        all_reductions,
        result[0],
        result[2].get_params()
    )

    file_name = create_file_name_using_parameters(
        root + output_dir,
        '_'.join(all_reductions),
        kmer,
        context_size,
        vector_size
    )

    save_json(file_name + ".json", scores)
