# Use pip env for this
import biovec

"""
https://github.com/kyu999/biovec
This package includes already trained model in '/trained_models/swissprot-reviewed.model'.
It is a protvec model fed all Swiss-Prot reviewed proteins(551,754 proteins as of 2018/09/13) as the training data.
"""
import os

os.getcwd()

# From fasta corpus_fname is generated. corpus_fname is like a doc which now consistes of words.
# The corpus_fname file acts as input to word2vec
pv = biovec.models.ProtVec("data/uniprot/uniprot_sprot.fasta", corpus_fname="protein_seq_corpus_3gram.txt", n=3)
# save trained model into file
pv.save('protein_embeddings_3gram')
# The n-gram "QAT" should be trained in advance
print(pv["QAT"])

# convert whole amino acid sequence into vector
a = pv.to_vecs("ATATQSQSMTEEL")
b = pv.to_vecs("ATATQSQSMT")
c = pv.to_vecs("ATATQS")

pv.to_vecs("ILYQVPFSV")  # -> numeric -> ML model for classification.
