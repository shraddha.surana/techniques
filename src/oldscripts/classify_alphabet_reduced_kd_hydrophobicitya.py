import argparse
import time

import biovec
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, multilabel_confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC

from src.embeddings.utils import convert_sequences_to_vectors


def arg_parser(ROOT_DIR=""):
    embedding_file = ROOT_DIR + 'data/embeddings/uniprot__kmer_5_contextWindow_5_vector_300_reduction_kd_hydrophobicitya'
    data_file = ROOT_DIR + 'data/intermediate/reduced_data_kd_hydrophobicitya_tab.csv'
    separator = ","

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--embedding-file', help='Embedding file', default=embedding_file)
    parser.add_argument('--data-file', help='Data file', default=data_file)
    parser.add_argument('--separator', help='separator for data file', default=separator)
    return parser.parse_args()


def classify_model_with_results(x_train, x_test, y_train, y_test, model):
    tic = time.process_time()
    model.fit(x_train, y_train)
    toc = time.process_time()
    print("Time Taken : ", toc - tic)
    y_predicted = model.predict(x_test)
    # print("confusion_matrix : ")
    matrix = multilabel_confusion_matrix(y_test, y_predicted)
    print(matrix)
    print("accuracy_score : ")
    score = accuracy_score(y_test, y_predicted)
    print(score)
    return model, y_predicted, score, matrix


def classify_and_predict(x_train, x_test, y_train, model):
    model.fit(x_train, y_train)
    y_predicted = model.predict(x_test)
    return y_predicted, model


def get_data_to_classify():
    return pd.read_csv(args.data_file, sep=args.separator, header=None)


if __name__ == "__main__":
    ROOT_DIR = "../../"
    args = arg_parser(ROOT_DIR)

    data = get_data_to_classify()

    # load trained model from file
    pv = biovec.models.load_protvec(args.embedding_file)

    # convert to protein embeddings
    inputs = convert_sequences_to_vectors(data.iloc[:, 0], pv)

    # split train test
    X_train, X_test, Y_train, Y_test = train_test_split(inputs, data.iloc[:, 1], random_state=2)

    print("Classify SVM : ")
    classify_model_with_results(X_train, X_test, Y_train, Y_test, SVC(gamma='auto'))
    # accuracy_score svm_classifier
    # 0.5238095238095238

    print("RandomForestClassifier : ")
    classify_model_with_results(X_train, X_test, Y_train, Y_test, RandomForestClassifier())
    # accuracy_score RandomForestClassifier
    # 0.5238095238095238
