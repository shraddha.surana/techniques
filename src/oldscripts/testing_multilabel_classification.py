import argparse

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

from src.oldscripts.classify_alphabet_reduced_kd_hydrophobicitya import classify_and_predict
from src.classification.utils import calculate_scores
from src.classification.utils import get_best_result
from src.embeddings.reduce_dataset import reduce_by_alphabet_frequency, reduce_with_dipeptide_frequency
from src.files.amp import filter_data_for_selected_activities, get_positive_amp_data, \
    get_negative_amp_data, amp_positive_data_activities
from src.files.camp import massage_camp_data
from src.files.utils import save_json, contains, OTHER_ALPHABETS

encoding_mapping = {
    "aa_freq": reduce_by_alphabet_frequency,
    "dipeptide_freq": reduce_with_dipeptide_frequency,
    "aa_dipeptide_combined": lambda x: pd.concat([reduce_by_alphabet_frequency(x), reduce_with_dipeptide_frequency(x)],
                                                 axis=1)
}


def generate_random_data(df_size, tar_size, proportion):
    new_df = np.random.rand(*df.shape)
    new_tar = np.round(np.random.rand(*tar.shape))
    return new_df, new_tar


def classify(random_states, val, target, encoding_name, method=None):
    result = rm_classifier_for_various_states(random_states, val, target, method=method)
    result = get_best_result(result)

    scores = result[0]
    scores['encoding'] = encoding_name
    scores['model'] = result[1].get_params()

    return (scores)


def get_variables(args):
    return args.root, args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores


def arg_parser():
    root = "./"
    # output_dir = "data/results/basline_binary_clipped_data_rfc" if method == 'binary' else "data/results/basline_clipped_new_data_rfc"
    output_dir = "data/results/basline_testing_multi_unfiltered_positive_rfc"
    cores = 10

    # negative_data_file = 'data/raw/amp_CBBio_negative_binary_sequence_length_10_to_60.csv'  # 0.4 similarity threshold with positive data
    # positive_data_file = 'data/raw/amp_CBBio_positive_binary_sequence_length_10_to_60.csv'  # filtered 70% similarity threshold

    negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


def additional_filtering_of_sequences(data):
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())
    indexNames = sequences[sequences['Sequence'] == 'GWLDVAKKIGKAAFNVAKNFLFNKAVNFAAKGIKKAVDLWG '].index
    sequences.drop(indexNames, inplace=True)
    sequences = sequences[sequences.Sequence.apply(lambda x: x.isalpha())]

    return sequences


if __name__ == "__main__":
    method = 'multi'

    encoding_scores = []
    root, output_dir, negative_data_file, positive_data_file, cores = get_variables(arg_parser())

    random_states = [29]  # list(range(1, 100, 1))

    raw_negative_data = get_negative_amp_data(root + negative_data_file)
    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
    # AMP_data = pd.concat([raw_negative_data, raw_positive_data])     # for new data
    # AMP_data = pd.concat([negative_data, positive_data_filtered])   # for old data
    # AMP_data_filtered = additional_filtering_of_sequences(AMP_data)
    AMP_data_filtered = additional_filtering_of_sequences(positive_data_filtered)
    AMP_data_filtered = AMP_data_filtered.reset_index(drop=True)

    mlb = preprocessing.MultiLabelBinarizer()
    target = mlb.fit_transform(AMP_data_filtered['Activity'])
    target = pd.DataFrame(target, columns=[mlb.classes_])
    reduced_data = reduce_by_alphabet_frequency(AMP_data_filtered)
    x_train, x_test, y_train, y_test = train_test_split(reduced_data, target, random_state=29)

    print("--------- AA FREQ -----------")

    val = x_train.values
    tar = y_train.values
    classifier = RandomForestClassifier(n_jobs=cores)
    predicted, model = classify_and_predict(val, x_test.values, tar, classifier)
    scores = calculate_scores(y_test.values, predicted, method=method)
    scores["data"] = "AA FREQ"
    scores["model"] = model.get_params()
    print(scores)
    encoding_scores.append(scores)

    print("--------- RANDOM + AA FREQ -----------")

    new_df = pd.DataFrame(np.random.rand(*x_train.shape), columns=x_train.columns)
    new_tar = pd.DataFrame(np.round(np.random.rand(*y_train.shape)), columns=y_train.columns)

    val = pd.concat([x_train, new_df]).values
    tar = pd.concat([y_train, new_tar]).values
    classifier = RandomForestClassifier(n_jobs=cores)
    predicted, model = classify_and_predict(val, x_test.values, tar, classifier)
    scores = calculate_scores(y_test.values, predicted, method=method)
    scores["data"] = "RANDOM + AA FREQ"
    scores["model"] = model.get_params()
    print(scores)
    encoding_scores.append(scores)

    print("--------- RANDOM -----------")

    new_df = pd.DataFrame(np.random.rand(*x_train.shape), columns=x_train.columns)
    new_tar = pd.DataFrame(np.round(np.random.rand(*y_train.shape)), columns=y_train.columns)

    val = new_df.values
    tar = new_tar.values
    classifier = RandomForestClassifier(n_jobs=cores)
    predicted, model = classify_and_predict(val, x_test.values, tar, classifier)
    scores = calculate_scores(y_test.values, predicted, method=method)
    scores["data"] = "RANDOM"
    scores["model"] = model.get_params()
    print(scores)
    encoding_scores.append(scores)

    file_name = root + output_dir + "_reduction_none"

    save_json(file_name + ".json", encoding_scores)
