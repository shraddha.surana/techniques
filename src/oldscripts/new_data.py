import pandas as pd
from Bio import SeqIO

root = "/Users/in-digvijay.gunjal/e4r/life_sciences/techniques/"

file_in_positive = "/Users/in-digvijay.gunjal/e4r/data/life_sciences/M_model_train_AMP_sequence.fasta"
file_in_negative = "/Users/in-digvijay.gunjal/e4r/data/life_sciences/M_model_train_nonAMP_sequence.fasta"
file_out_positive = "/data/raw/AMP/new_amp_positive.csv"
file_out_negative = "/data/raw/AMP/new_amp_negative.csv"

if __name__ == "__main__":

    total_count = 0
    valid_records = 0
    for seq_record in SeqIO.parse(open(file_in_positive, mode='r'), 'fasta'):
        print(seq_record.seq)
    positive = pd.DataFrame(
        list(map(lambda s: ["".join(s.seq), 1], SeqIO.parse(open(file_in_positive, mode='r'), 'fasta'))))
    negative = pd.DataFrame(
        list(map(lambda s: ["".join(s.seq), 0], SeqIO.parse(open(file_in_negative, mode='r'), 'fasta'))))

    positive = positive.rename(columns={0: 'Sequence', 1: 'Amp'})
    negative = negative.rename(columns={0: 'Sequence', 1: 'Amp'})

    positive.to_csv(file_out_positive, index=False)
    negative.to_csv(file_out_negative, index=False)
