import pandas as pd

from src.files.camp import massage_camp_data

ROOT_DIR = ""
positive_data_filtered_path = ROOT_DIR + 'data/raw/AMP/positive_data_filter_70_complete.csv'  # filtered 70% similarity threshold
positive_data_unfiltered_path = ROOT_DIR + 'data/raw/AMP/positive_data_unfiltered_temp.csv'

# pdata_temp = pd.read_csv(positive_data_filtered_path, sep=",", header=0)
pdata_temp = pd.read_csv(positive_data_unfiltered_path, sep=",", header=0)
pdata_temp.columns
pdata = pd.DataFrame()
pdata['Sequence'] = pdata_temp['Sequence']
pdata['Activity'] = pdata_temp['Bioactivity']
pdata.shape
# def clean_activity_data(pdata):
data = massage_camp_data(pdata)

for i in range(data.__len__()):
    while 'in' in data.loc[i, 'Activity']:
        data.loc[i, 'Activity'].remove('in')
    while 'ity' in data.loc[i, 'Activity']:
        data.loc[i, 'Activity'].remove('ity')

# data.to_csv('data/raw/AMP/positive_data_filtered.csv')
data.to_csv('data/raw/AMP/positive_data_unfiltered.csv')

# check:
for i in range(0, data.__len__()):
    for item in data.loc[i, 'Activity']:
        # print(item)
        if (item == 'ity' or item == 'in' or item == ''):
            print("--", i)
            print(data.loc[i, :])
