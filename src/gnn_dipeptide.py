import numpy as np
import pandas as pd
import torch
import torch.nn.functional as F
from sklearn.metrics import roc_auc_score
from torch_geometric.data import DataLoader
from torch_geometric.nn import MessagePassing
from torch_geometric.nn import TopKPooling, SAGEConv
from torch_geometric.nn import global_mean_pool as gap, global_max_pool as gmp
from torch_geometric.utils import remove_self_loops, add_self_loops

from src.combined_reduction_all_combinations_binary_whole_data import get_train_data
from src.embeddings.reduce_dataset import reduce_with_gap_dipeptide_frequency
from src.files.amp import get_positive_amp_data, get_negative_amp_data
from src.files.camp import massage_camp_data

embed_dim = 128
num_epochs = 2


class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()

        self.conv1 = SAGEConv(embed_dim, 128)
        self.pool1 = TopKPooling(128, ratio=0.8)
        self.conv2 = SAGEConv(128, 128)
        self.pool2 = TopKPooling(128, ratio=0.8)
        self.conv3 = SAGEConv(128, 128)
        self.pool3 = TopKPooling(128, ratio=0.8)
        self.item_embedding = torch.nn.Embedding(num_embeddings=num_items, embedding_dim=embed_dim)
        self.lin1 = torch.nn.Linear(256, 128)
        self.lin2 = torch.nn.Linear(128, 64)
        self.lin3 = torch.nn.Linear(64, 1)
        self.bn1 = torch.nn.BatchNorm1d(128)
        self.bn2 = torch.nn.BatchNorm1d(64)
        self.act1 = torch.nn.ReLU()
        self.act2 = torch.nn.ReLU()

    def forward(self, data):
        x, edge_index, batch = data.x, data.edge_index, data.batch
        x = self.item_embedding(x)
        x = x.squeeze(1)

        x = F.relu(self.conv1(x, edge_index))

        x, edge_index, _, batch, _ = self.pool1(x, edge_index, None, batch)
        x1 = torch.cat([gmp(x, batch), gap(x, batch)], dim=1)

        x = F.relu(self.conv2(x, edge_index))

        x, edge_index, _, batch, _ = self.pool2(x, edge_index, None, batch)
        x2 = torch.cat([gmp(x, batch), gap(x, batch)], dim=1)

        x = F.relu(self.conv3(x, edge_index))

        x, edge_index, _, batch, _ = self.pool3(x, edge_index, None, batch)
        x3 = torch.cat([gmp(x, batch), gap(x, batch)], dim=1)

        x = x1 + x2 + x3

        x = self.lin1(x)
        x = self.act1(x)
        x = self.lin2(x)
        x = self.act2(x)
        x = F.dropout(x, p=0.5, training=self.training)

        x = torch.sigmoid(self.lin3(x)).squeeze(1)

        return x


class SAGEConv(MessagePassing):
    def __init__(self, in_channels, out_channels):
        super(SAGEConv, self).__init__(aggr='max')  # "Max" aggregation.
        self.lin = torch.nn.Linear(in_channels, out_channels)
        self.act = torch.nn.ReLU()
        self.update_lin = torch.nn.Linear(in_channels + out_channels, in_channels, bias=False)
        self.update_act = torch.nn.ReLU()

    def forward(self, x, edge_index):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]

        edge_index, _ = remove_self_loops(edge_index)
        edge_index, _ = add_self_loops(edge_index, num_nodes=x.size(0))

        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)

    def message(self, x_j):
        # x_j has shape [E, in_channels]

        x_j = self.lin(x_j)
        x_j = self.act(x_j)

        return x_j

    def update(self, aggr_out, x):
        # aggr_out has shape [N, out_channels]

        new_embedding = torch.cat([aggr_out, x], dim=1)

        new_embedding = self.update_lin(new_embedding)
        new_embedding = self.update_act(new_embedding)

        return new_embedding


def get_test_data(path):
    negative_test_data_file = 'data/negative_data_cdhit_filter_removed.csv'
    positive_test_data_file = 'data/raw/positive_data_removed_cdhit.csv'
    # negative_test_data_file = 'data/raw/benchmark_data_non_amp.csv'
    # positive_test_data_file = 'data/raw/benchmark_data_amp.csv'
    # negative_test_data_file = 'data/raw/amp_scanner/ampscanner_test_non_amp.csv'
    # positive_test_data_file = 'data/raw/amp_scanner/ampscanner_test_amp.csv'
    test_positive_data = get_positive_amp_data(path + positive_test_data_file)
    test_positive_data = massage_camp_data(test_positive_data).reset_index().drop('index', axis=1)
    test_negative_data = get_negative_amp_data(path + negative_test_data_file)
    test_negative_data = massage_camp_data(test_negative_data).reset_index().drop('index', axis=1)
    return pd.concat([test_positive_data, test_negative_data])


if __name__ == '__main__':
    root = "./"
    # negative_data_file = 'data/raw/M_model_train_nonAMP_sequence.csv'
    # positive_data_file = 'data/raw/M_model_train_AMP_sequence.csv'
    negative_data_file = 'data/raw/negative_data_unfiltered.csv'
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'

    train_data = get_train_data(root, positive_data_file, negative_data_file)
    test_data = get_test_data(root)

    aa = reduce_with_gap_dipeptide_frequency(pd.concat([train_data, test_data]), 2)
    train_dataset = aa[:train_data.shape[0]]
    test_dataset = aa[train_data.shape[0]:]

    batch_size = 1024
    train_loader = DataLoader(train_dataset, batch_size=batch_size, pin_memory=False)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, pin_memory=False)

    num_items = train_loader.dataset.shape[0] + 1

    device = torch.device('cpu')
    model = Net().to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.005)
    crit = torch.nn.BCELoss()

    def train():
        model.train()

        loss_all = 0
        for data in train_loader:
            print(data)
            print("chalui zala")
            data = data.to(device)
            print("chalui zala 1")
            optimizer.zero_grad()
            print("chalui zala 2")
            output = model(data)
            print("chalui zala 3")
            label = data.y.to(device)
            print("chalui zala 4")
            loss = crit(output, label)
            loss.backward()
            print("chalui zala 5")
            loss_all += data.num_graphs * loss.item()
            print("chalui zala 6")
            optimizer.step()
            print("chalui zala 7")
        return loss_all / len(train_dataset)


    def evaluate(loader):
        model.eval()

        predictions = []
        labels = []

        with torch.no_grad():
            for data in loader:
                data = data.to(device)
                pred = model(data).detach().cpu().numpy()

                label = data.y.detach().cpu().numpy()
                predictions.append(pred)
                labels.append(label)

        predictions = np.hstack(predictions)
        labels = np.hstack(labels)

        return roc_auc_score(labels, predictions)


    for epoch in range(num_epochs):
        print(epoch)
        train()
