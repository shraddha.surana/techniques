from functools import reduce

import pandas as pd

from src.embeddings.reduction_utils import amino_acids

if __name__ == "__main__":

    data = pd.read_table('./data/raw/pfeatures/z_aaindex.csv', sep=',', index_col='INDEX')

    all_prop = list((pd.read_csv('./data/raw/pfeatures/multilabel_aaindices.csv', sep=',', header=None)).iloc[0, :])


    def p_aa(prop, residue):
        return data.loc[prop][residue.upper()]


    def NMB(prop, seq, d):
        sum = 0
        for i in range(len(seq) - d):
            sum += p_aa(prop, seq[i]) * p_aa(prop, seq[i + d])
        return sum / (len(seq) - d)


    def pav(prop, seq):
        return reduce(lambda av, residue: av + p_aa(prop, residue), seq, 0) / len(seq)


    def moran(prop, seq, d):
        s_1 = 0
        s_2 = 0
        p_bar = pav(prop, seq)
        for i in range(len(seq) - d):
            s_1 += (p_aa(prop, seq[i]) - p_bar) * (p_aa(prop, seq[i + d]) - p_bar)
        for i, residue in enumerate(seq):
            s_2 += (p_aa(prop, residue) - p_bar) * (p_aa(prop, residue) - p_bar)
        return (s_1 / (len(seq) - d)) / (s_2 / len(seq))


    def geary(prop, seq, d):
        s_1 = 0
        s_2 = 0
        p_bar = pav(prop, seq)
        seq_len = len(seq)
        for i in range(seq_len - d):
            paa = p_aa(prop, seq[i]) - p_aa(prop, seq[i + d])
            s_1 += paa * paa
        for residue in seq:
            paa_2 = p_aa(prop, residue) - p_bar
            s_2 += paa_2 * paa_2
        return (s_1 / (2 * (seq_len - d))) / (s_2 / (seq_len - 1))


    def validate_sequences(sequences):
        for sequence in sequences:
            for residue in sequence:
                if not amino_acids.__contains__(residue.upper()):
                    return sequence
        return None


    def for_prop(prop, seq, d):
        return list(map(lambda f: round(f(prop, seq, d), 3), [NMB, moran, geary]))


    def autocorr_full_aa(filename, d):
        seq_data = list((pd.read_csv(filename, sep=',', header=None)).iloc[:, 0])

        invalid_sequence = validate_sequences(seq_data)
        if invalid_sequence:
            print("Error!: Invalid sequence. Special character(s)/invalid amino acid letter(s) present in input.")
            print(invalid_sequence)
            return
        print("All sequences are valid")

        output = []
        for i, seq in enumerate(seq_data):
            print(f"Sequence_{i} : {seq}")
            output.append(list(reduce(lambda c, prop: c + for_prop(prop, seq, d), all_prop, [])))

        return output


    LG = 1  # also called as d
    INPUT_FILE = "./data/raw/apd/plain_sequences_top_4"
    OUTPUT_FILE = "./data/multilabel/apd_pfeatures_4/auto_correlation.csv"

    print('Value of lag:', LG, '\n')
    print("Read Prop : ", str(all_prop))

    columns = list(reduce(lambda c, p: c + [f"ACR{LG}_MB_{p}", f"ACR{LG}_MO_{p}", f"ACR{LG}_GE_{p}"], all_prop, []))
    features = pd.DataFrame(autocorr_full_aa(INPUT_FILE, LG))
    features.columns = columns
    features.to_csv(OUTPUT_FILE, index=False)
