import argparse

import pandas as pd
from keras.initializers import GlorotNormal
from keras.layers import Dense, Dropout
from keras.models import Sequential

from src.classification.dnn import dnn_classifier_for_various_states
from src.classification.utils import get_best_result
from src.files.utils import contains, OTHER_ALPHABETS


def get_model(inp_shape):
    model = Sequential()
    model.add(Dense(500, activation="relu", input_shape=(inp_shape,), kernel_initializer=GlorotNormal(),
                    bias_initializer=GlorotNormal()))
    model.add(Dropout(0.4))
    model.add(Dense(800, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.4))
    model.add(Dense(300, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.3))
    model.add(Dense(70, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation="sigmoid", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    return model


def get_model2(inp_shape):
    model = Sequential()
    model.add(Dense(1000, activation="relu", input_shape=(inp_shape,), kernel_initializer=GlorotNormal(),
                    bias_initializer=GlorotNormal()))
    model.add(Dropout(0.5))
    model.add(Dense(800, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.3))
    model.add(Dense(500, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.3))
    model.add(Dense(300, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.3))
    model.add(Dense(150, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.3))
    model.add(Dense(70, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation="sigmoid", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    return model


def get_model3(inp_shape):
    model = Sequential()
    model.add(Dense(128, activation="relu", input_shape=(inp_shape,), kernel_initializer=GlorotNormal(),
                    bias_initializer=GlorotNormal()))
    model.add(Dropout(0.4))
    model.add(Dense(64, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.4))
    model.add(Dense(32, activation="relu", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation="sigmoid", kernel_initializer=GlorotNormal(), bias_initializer=GlorotNormal()))
    return model


def classify(random_states, val, target, reduction, method=None):
    result = dnn_classifier_for_various_states(random_states, val, target, model, method=method)
    result = get_best_result(result)

    scores = result[0]
    scores['reduction'] = reduction
    config = result[1].get_config()
    config['build_input_shape'] = config['build_input_shape'].as_list()
    scores['model'] = config
    # result[1].save("best_dnn_model.h5")

    return (scores)


def get_variables(args):
    return args.root, args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores


def arg_parser():
    root = "./"
    output_dir = "data/results/binary_dnn_unfiltered_positive"
    cores = 10

    negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


def additional_filtering_of_sequences(data):
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())
    indexNames = sequences[sequences['Sequence'] == 'GWLDVAKKIGKAAFNVAKNFLFNKAVNFAAKGIKKAVDLWG '].index
    sequences.drop(indexNames, inplace=True)
    sequences = sequences[sequences.Sequence.apply(lambda x: x.isalpha())]

    return sequences


if __name__ == "__main__":
    root, output_dir, negative_data_file, positive_data_file, cores = get_variables(arg_parser())
    red = "conformation_similarity"
    data = pd.read_csv("../filtered_embedded_data_kmer_5_context_5_vector_300_reduction_conformation_similarity.csv")
    val = data[data.columns[:-1]]
    target = data.Activity

    model = get_model3(val.shape[1])

    random_states = list(range(1, 100, 1))
    res = classify(random_states, val, target, red, method='binary')

    file_name = root + output_dir + "_reduction_conformation_similarity"

    # save_json(file_name + ".json", res)
