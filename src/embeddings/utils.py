from collections import Counter

import biovec
import numpy as np
import pandas as pd
from tqdm import tqdm

from src.embeddings.corpus import split_n_grams


def prot_vec_to_vecs(pv, x, k):
    return pv.to_vecs(x)


def words_to_vec(pv, seq, n=5):
    ngram_patterns = split_n_grams(seq, n)

    vectors = []
    for ngrams in ngram_patterns:
        ngram_vecs = []
        for ngram in ngrams:
            try:
                ngram_vecs.append(pv[ngram])
            except:
                print(ngram)
                raise Exception("Model has never trained this n-gram: " + ngram)
        vectors.append(sum(ngram_vecs))
    return vectors


def dipeptide_encoding(seq, n):
    """
    Returns n-Gram Motif frequency
    https://www.biorxiv.org/content/10.1101/170407v1.full.pdf
    """
    aa_list = list(seq)
    return {''.join(aa_list): n for aa_list, n in Counter(zip(*[aa_list[i:] for i in range(n)])).items() if
            not aa_list[0][-1] == (',')}


def gap_dipeptide_encoding(seq, n, g=1):
    """
    Returns n-Gram Motif frequency
    https://www.biorxiv.org/content/10.1101/170407v1.full.pdf
    """
    aa_list = list(seq)

    return {''.join(aa_list): n for aa_list, n in Counter(zip(*[aa_list[i:] for i in range(0, n+g, g+1)])).items() if
            not aa_list[0][-1] == (',')}


def convert_sequences_to_vectors(data, embedding, to_vec=prot_vec_to_vecs, kmer=5):
    output = pd.DataFrame()
    errors = list()
    for row in tqdm(data, desc="Creating vectors", unit="sequence"):
        try:
            output = output.append(pd.DataFrame(sum(to_vec(embedding, row, kmer))).T)
        except:
            output = output.append(pd.DataFrame(np.zeros((1, embedding.vector_size))))
            errors.append(row)
    return output, errors


def create_file_name_using_parameters(path, reduction, kmer, context_size, vector_size, separator="_"):
    values = [path, "kmer", str(kmer), "contextWindow", str(context_size), "vector", str(vector_size), "reduction",
              str(reduction)]

    return separator.join(values)


def create_vectors(data, embeddings, kmer, all_reductions):
    all_vectors = {}
    all_errors = {}
    for reduction in all_reductions:
        if not all_vectors.__contains__(reduction):
            inputs, errors = convert_sequences_to_vectors(
                data[reduction],
                embeddings[reduction],
                words_to_vec,
                kmer
            )
            all_vectors[reduction] = inputs
            all_errors[reduction] = errors

    return all_vectors, all_errors


def join_all_vectors(all_reductions, all_vectors):
    all_inputs = pd.DataFrame()
    for red in all_reductions:
        all_inputs = pd.concat([all_inputs, all_vectors[red]], axis=1)

    return all_inputs


def load_all_embeddings(reductions, root, embedding_file, kmer, context_size, vector_size):
    data = {}
    for reduction in reductions:
        if not data.__contains__(reduction):
            data[reduction] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, reduction, kmer, context_size, vector_size))

    return data
