from biovec.utils import *

from src.embeddings.reduction_utils import reduce_sequence


def split_n_grams(seq, n):
    """
    'AGAMQSASM' => [['AGA', 'MQS', 'ASM'], ['GAM','QSA'], ['AMQ', 'SAS']]
    In case of n = 3
    """
    grams = []
    for i in range(n):
        grams.append(zip(*[iter(seq[i:])] * n))

    str_ngrams = []
    for ngrams in grams:
        x = []
        for ngram in ngrams:
            x.append("".join(ngram))
        str_ngrams.append(x)
    return str_ngrams


def generate_corpusfile(fasta_fname, kmer, corpus_fname, reduction_mapping=None):
    """
    Args:
        reduction_mapping: mapping used for alphabet reduction
        fasta_fname: corpus file name
        kmer: the number of chunks to split. In other words, "n" for "n-gram"
        corpus_fname: corpus_fname put corpus file path
    Description:
        Protvec uses word2vec inside, and it requires to load corpus file
        to generate corpus.
    Flow: load fasta file -> read the sequence -> reduce alphabet -> put in the .txt file which is the corpus file now.
    """
    file_out = open(corpus_fname, "w+")
    fasta = Fasta(fasta_fname)
    corpus = []
    for record_id in tqdm(fasta.keys(), desc='corpus generation progress'):
        record = fasta[record_id]
        seq = str(record)
        reduced_protein = reduce_sequence(seq, reduction_mapping)

        ngram_patterns = split_n_grams(reduced_protein._data, kmer)
        for ngram_pattern in ngram_patterns:
            corpus.append(ngram_pattern)
            file_out.write(" ".join(ngram_pattern) + "\n")
    file_out.close()
    return corpus
