import argparse

import pandas as pd
# from Bio.Alphabet.Reduced import hp_model_tab  # pip install biopython - removed due to deprecation

from src.embeddings.reduction_utils import reduce_sequence, kd_hydrophobicitya, hp_model_tab
from src.embeddings.utils import dipeptide_encoding, gap_dipeptide_encoding


def arg_parser(ROOT_DIR=""):
    file_in = ROOT_DIR + 'data/raw/CoEPrA-2006_Classification_001_Calibration_Peptides.csv'
    file_out = ROOT_DIR + "data/intermediate/reduced_data.csv"
    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--input', help='CSV file to reduce', default=file_in)
    parser.add_argument('--output', help='path to output CSV file', default=file_out)
    parser.add_argument('--mapping', help='Mapping for reduction', default="kd_hydrophobicitya")
    return parser.parse_args()


def reduce_data(input_file, output_file, mapping):
    print("Reading CSV : ", input_file)
    data = pd.read_csv(input_file, sep=',', header=None)
    result = reduce(data, mapping)
    print("Creating CSV : ", output_file)
    result.to_csv(output_file, index=False, header=False)


def reduce_by_alphabet_frequency(data):
    seq_vec = data.Sequence.apply(lambda x: dipeptide_encoding(x, 1)).to_list()
    df = pd.DataFrame(seq_vec)
    df = df.fillna(0)
    return df.div(df.sum(axis=1), axis=0)


def reduce_with_dipeptide_frequency(data):
    seq_vec = data.Sequence.apply(lambda x: dipeptide_encoding(x, 2)).to_list()
    df = pd.DataFrame(seq_vec)
    df = df.fillna(0)
    return df.div(df.sum(axis=1), axis=0)


def reduce_with_tripeptide_frequency(data):
    seq_vec = data.Sequence.apply(lambda x: dipeptide_encoding(x, 3)).to_list()
    df = pd.DataFrame(seq_vec)
    df = df.fillna(0)
    return df.div(df.sum(axis=1), axis=0)


def reduce_with_gap_dipeptide_frequency(data, g):
    seq_vec = data.Sequence.apply(lambda x: gap_dipeptide_encoding(x, 2, g)).to_list()
    df = pd.DataFrame(seq_vec)
    df = df.fillna(0)
    return df.div(df.sum(axis=1), axis=0)


def reduce_with_gap_tripeptide_frequency(data, g):
    seq_vec = data.Sequence.apply(lambda x: gap_dipeptide_encoding(x, 3, g)).to_list()
    df = pd.DataFrame(seq_vec)
    df = df.fillna(0)
    return df.div(df.sum(axis=1), axis=0)


def reduce(data, mapping=None):
    return data.apply(lambda row: [reduce_sequence(row[0], mapping)._data, row[1]], axis=1, result_type='expand')


mappings = {
    "hp_model_tab": hp_model_tab,
    "kd_hydrophobicitya": kd_hydrophobicitya,
}

if __name__ == "__main__":
    ROOT_DIR = "../../"
    args = arg_parser(ROOT_DIR)
    reduce_data(args.input, args.output, mappings[args.mapping])
