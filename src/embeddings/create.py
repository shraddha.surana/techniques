import argparse
import os

from gensim.models import word2vec

from src.embeddings.corpus import generate_corpusfile
from src.embeddings.reduction_utils import reduction_mappings
from src.embeddings.utils import create_file_name_using_parameters


def arg_parser():
    root = ""
    data_path = ""
    file_in = "data/uniprot/uniprot_sprot_standard_alphabets.fasta"
    kmer = 5
    context_size = 5
    vector_size = 300
    reduction = "None"
    cores = 50

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--data-path', help='Path to data directory', default=data_path)
    parser.add_argument('--file-in', help='Fasta file', default=file_in)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--reduction', help='Which reduction to use', default=reduction)
    parser.add_argument('--cores', help='Cores used for creation of embedding', default=cores, type=int)

    return parser.parse_args()


def get_variables(args):
    return args.root, args.data_path, args.file_in, args.kmer, args.vector_size, args.context_size, args.reduction, args.cores


if __name__ == "__main__":
    root, data_path, file_in, kmer, vector_size, context_size, reduction, cores = get_variables(arg_parser())
    corpus_file = data_path + "data/processed/kmer" + str(kmer) + "_reduction_" + reduction + ".txt"
    embedding_file = create_file_name_using_parameters(data_path + "data/embeddings/uniprot_", reduction, kmer,
                                                       context_size, vector_size)

    print("These are varaibles : ")
    print(root, data_path, file_in, kmer, vector_size, context_size, reduction)
    print(corpus_file, embedding_file)

    if (os.path.isfile(corpus_file)):
        print("=== Corpus file present. Not regenerating ===")
    else:
        print("START: Generate corpus file")
        corpus_data = generate_corpusfile(str(root) + str(file_in),
                                          kmer,
                                          corpus_file,
                                          reduction_mappings[reduction])
        print("DONE: Generate corpus file")

    if os.path.isfile(embedding_file):
        print("=== Embedding file already present. Not regenerating ===")
    else:
        print("START: load corpus")
        corpus = word2vec.Text8Corpus(corpus_file)
        print("Done: load corpus")

        print("START: create embedding")
        sg = 1
        epochs = 5
        negative = 0
        hs = 1
        pv = word2vec.Word2Vec(
            corpus,
            size=vector_size,
            sg=sg,
            hs=hs,
            window=context_size,
            min_count=1,
            iter=epochs,
            negative=negative,
            compute_loss=True,
            workers=cores
        )
        # pv = biovec.models.ProtVec(corpus=corpus, n=kmer, size=vector_size, window=context_size, workers=10)
        print("DONE: create embedding")

        print("START: save embedding")
        pv.save(embedding_file)
        print("DONE: saved embedding at - ", embedding_file)
