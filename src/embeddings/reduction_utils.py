from Bio.Seq import Seq
from propy import PyPro


def reduce_sequence(seq_string, reduction_mapping=None):
    if reduction_mapping is not None:
        try:
            return Seq(''.join(map(lambda a: reduction_mapping[a], seq_string)))
        except:
            print("====================")
            print(seq_string)
    return Seq(seq_string)


# Miyazawa Jernigan (MJ) matrix 5 - MFILV, AWC, YQHPGTSN, RK, DE
mj_5 = {
    "M": "a",
    "F": "a",
    "I": "a",
    "L": "a",
    "V": "a",
    "A": "b",
    "W": "b",
    "C": "b",
    "Y": "c",
    "Q": "c",
    "H": "c",
    "P": "c",
    "G": "c",
    "T": "c",
    "S": "c",
    "N": "c",
    "R": "d",
    "K": "d",
    "D": "e",
    "E": "e"
}

# Miyazawa Jernigan (MJ) matrix 4 - MFILV, AWC, YQHPGTSNRK, DE
mj_4 = {
    "M": "a",
    "F": "a",
    "I": "a",
    "L": "a",
    "V": "a",
    "A": "b",
    "W": "b",
    "C": "b",
    "Y": "c",
    "Q": "c",
    "H": "c",
    "P": "c",
    "G": "c",
    "T": "c",
    "S": "c",
    "N": "c",
    "R": "c",
    "K": "c",
    "D": "d",
    "E": "d"
}

# 5 (LVIMC) (AGSTP) (FYW) (EDNQ) (KRH)
murphy_5 = {
    "L": "a",
    "V": "a",
    "I": "a",
    "M": "a",
    "C": "a",
    "A": "b",
    "G": "b",
    "S": "b",
    "T": "b",
    "P": "b",
    "F": "c",
    "Y": "c",
    "W": "c",
    "E": "d",
    "D": "d",
    "N": "d",
    "Q": "d",
    "K": "e",
    "R": "e",
    "H": "e"
}

# [LIVFMWAY]
# [C]
# [ED]
# [HQPTSGN]
# [KR]

# LIVFMWAY, ED, C, HQPTSG, KR
contact_energies = {
    "L": "a",
    "I": "a",
    "V": "a",
    "F": "a",
    "M": "a",
    "W": "a",
    "A": "a",
    "Y": "a",
    "E": "b",
    "D": "b",
    "C": "c",
    "H": "d",
    "Q": "d",
    "P": "d",
    "T": "d",
    "S": "d",
    "G": "d",
    "N": "d",
    "K": "e",
    "R": "e"
}

# 4.5 to 2.7 - [IVLF]
#  2.7 to 0.9 - [CMA]
# 0.9 to -0.9 - [GTSW]
#  -0.9  to  -2.7 - [YP]
#  -2.7 to -4.5 - [HEQDNKR]
hydrophobicitya = {
    "I": "a",
    "V": "a",
    "L": "a",
    "F": "a",
    "C": "b",
    "M": "b",
    "A": "b",
    "G": "c",
    "T": "c",
    "S": "c",
    "W": "c",
    "Y": "d",
    "P": "d",
    "H": "e",
    "E": "e",
    "Q": "e",
    "D": "e",
    "N": "e",
    "K": "e",
    "R": "e"
}

# hydrophobic - F, M, W, I, V, L, P, A
# hydropholic - N, C, Q, G, S, T, Y
# charged - R, D, E, H, K
amino_acid_residues = {
    "F": "a",
    "M": "a",
    "W": "a",
    "I": "a",
    "V": "a",
    "L": "a",
    "P": "a",
    "A": "a",
    "N": "b",
    "C": "b",
    "Q": "b",
    "G": "b",
    "S": "b",
    "T": "b",
    "Y": "b",
    "R": "c",
    "D": "c",
    "E": "c",
    "H": "c",
    "K": "c"
}
# from Bio.Alphabet.Reduced import hp_model_tab
hp_model_tab = {"A": "P",  # Hydrophilic
                "G": "P",
                "T": "P",
                "S": "P",
                "N": "P",
                "Q": "P",
                "D": "P",
                "E": "P",
                "H": "P",
                "R": "P",
                "K": "P",
                "P": "P",
                "C": "H",  # Hydrophobic
                "M": "H",
                "F": "H",
                "I": "H",
                "L": "H",
                "V": "H",
                "W": "H",
                "Y": "H"}

# RKNDQEH, PYWSTG, AM, CFLVI

kd_hydrophobicitya = {
    "R": "a",
    "K": "a",
    "N": "a",
    "D": "a",
    "Q": "a",
    "E": "a",
    "H": "a",
    "P": "b",
    "Y": "b",
    "W": "b",
    "S": "b",
    "T": "b",
    "G": "b",
    "A": "c",
    "M": "c",
    "C": "d",
    "F": "d",
    "L": "d",
    "V": "d",
    "I": "d"
}
#     "U": "e",  # other 6
#     "O": "e",
#     "X": "e",
#     "B": "a",
#     "Z": "a",
#     "J": "d",

# Conformation Similarity
# [CMQLEKRA], [P], [ND], [G], [HWFY], [S], [TIV]

# CMQLEKRA, P, ND, G, HWFY, S, TIV
conformation_similarity = {
    "C": "a",
    "M": "a",
    "Q": "a",
    "L": "a",
    "E": "a",
    "K": "a",
    "R": "a",
    "A": "a",
    "P": "b",
    "N": "c",
    "D": "c",
    "G": "d",
    "H": "e",
    "W": "e",
    "F": "e",
    "Y": "e",
    "S": "f",
    "T": "g",
    "I": "g",
    "V": "g",
}

# LVIMC, AG, ST, FYW, EDNQ, KR, P, H


blossom_matrix_8 = {
    "L": "a",
    "V": "a",
    "I": "a",
    "M": "a",
    "C": "a",
    "A": "b",
    "G": "b",
    "S": "c",
    "T": "c",
    "F": "d",
    "Y": "d",
    "W": "d",
    "E": "e",
    "D": "e",
    "N": "e",
    "Q": "e",
    "K": "f",
    "R": "f",
    "P": "g",
    "H": "h",
}

# LVIMC, AGSTP, FYW, EDNQKRH

blossom_matrix_4 = {
    "L": "a",
    "V": "a",
    "I": "a",
    "M": "a",
    "C": "a",
    "A": "b",
    "G": "b",
    "S": "b",
    "T": "b",
    "P": "b",
    "F": "c",
    "Y": "c",
    "W": "c",
    "E": "d",
    "D": "d",
    "N": "d",
    "Q": "d",
    "K": "d",
    "R": "d",
    "H": "d",
}

# Hydropathy 5
# blossom_matrix_4
# contact energy
# new paper reduction

reduction_mappings = {
    'conformation_similarity': conformation_similarity,
    'kd_hydrophobicitya': kd_hydrophobicitya,
    'blossom_matrix_8': blossom_matrix_8,
    'blossom_matrix_4': blossom_matrix_4,
    'hydrophobicitya': hydrophobicitya,
    'contact_energies': contact_energies,
    'amino_acid_residues': amino_acid_residues,
    'murphy_5': murphy_5,
    'mj_4': mj_4,
    'mj_5': mj_5,
    'None': None,
}
from itertools import combinations

keys = list(reduction_mappings.keys())


def get_combinations(elements, group=None):
    if group == 0:
        return 0
    if group is None:
        group = len(elements)
    return len(list(combinations(elements, group))) + get_combinations(elements, group - 1)


all_combs = get_combinations(keys, 11)

list(map(lambda x: len(x), keys))
# ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']


# Propy paac keys mapping for Amino acids
propy_paac_map = {
    'A': 'PAAC1',
    'C': 'PAAC5',
    'D': 'PAAC4',
    'E': 'PAAC6',
    'F': 'PAAC14',
    'G': 'PAAC8',
    'H': 'PAAC9',
    'I': 'PAAC10',
    'K': 'PAAC12',
    'L': 'PAAC11',
    'M': 'PAAC13',
    'N': 'PAAC3',
    'P': 'PAAC15',
    'Q': 'PAAC7',
    'R': 'PAAC2',
    'S': 'PAAC16',
    'T': 'PAAC17',
    'V': 'PAAC20',
    'W': 'PAAC18',
    'Y': 'PAAC19'
}

amino_acids = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']
propy_keys = ['PAAC1', 'PAAC5', 'PAAC4', 'PAAC6', 'PAAC14', 'PAAC8', 'PAAC9', 'PAAC10', 'PAAC12', 'PAAC11', 'PAAC13',
              'PAAC3',
              'PAAC15', 'PAAC7', 'PAAC2', 'PAAC16', 'PAAC17', 'PAAC20', 'PAAC18', 'PAAC19']

result = {}
def get_PAAC_key(aa):
    paac = PyPro.GetProDes(aa + aa + aa).GetPAAC(lamda=2)
    key = list(filter(lambda k: paac[k] == 100, paac))[0]
    result[aa] = key
    return key

# Life Sciences in whole or just regarding Dr. Jayaraman's work?
#
# What are Amino's Acids. What are protein Sequences?
# Properties of Protein sequences?

# What will the model predict? Why we are predicting.
# Why we needed word2vec.
# Approach. - Word2Vec Embeddings(n-grams), vectors for classification.
#
# Predicting - properties (anti-microbial or not / Binary), Multilabel, (anti-bacterial, anti-viral, anti-fungal) (36 label)
#
# Results. non reduced techniques. Then Reduced. (Accuracy, hammings loss, fi score (Macro Micro))
#
# Reduction techniques. (Alphabet reduction for embeddings and )
# Performance impact of using reductions.
#
# Combinations of reductions.
#
# Server/Hosted site.
