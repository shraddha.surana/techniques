from Bio import SeqIO

from src.files.utils import contains, OTHER_ALPHABETS

root = "/Users/in-digvijay.gunjal/e4r/life_sciences/techniques/"

file_in = "/Users/in-digvijay.gunjal/e4r/life_sciences/data/uniprot/uniprot_sprot.fasta"
file_out = "/Users/in-digvijay.gunjal/e4r/life_sciences/data/uniprot/uniprot_sprot_standard_alphabets.fasta"

if __name__ == "__main__":

    total_count = 0
    valid_records = 0
    with open(file_out, 'w') as f_out:
        for seq_record in SeqIO.parse(open(file_in, mode='r'), 'fasta'):
            total_count += 1
            if not contains(OTHER_ALPHABETS, seq_record.seq):
                valid_records += 1
                r = SeqIO.write(seq_record, f_out, 'fasta')
                if r != 1: print('Error while writing sequence:  ' + seq_record.id)

    print("Total records: " + str(total_count))
    print("Valid records: " + str(valid_records))


    def get_discarded_seq_ids():
        seq_ids = []
        for seq_record in SeqIO.parse(open(file_in, mode='r'), 'fasta'):
            if contains(OTHER_ALPHABETS, seq_record.seq):
                seq_ids.append(seq_record.id)


    ids = get_discarded_seq_ids()
