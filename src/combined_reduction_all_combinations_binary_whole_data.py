import argparse
import itertools

import biovec
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder

from src.classification.random_forest import rm_classifier_for_various_states_binary
from src.classification.utils import add_properties_to_result
from src.combined_reduction_all_combinations import get_all_combinations, reduction_groups
from src.embeddings.reduce_dataset import reduce_with_gap_tripeptide_frequency, reduce_with_gap_dipeptide_frequency
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import get_positive_amp_data, get_negative_amp_data, \
    reduce_amp
from src.files.camp import massage_camp_data
from src.files.utils import save_json


def get_variables(args):
    return args.root, args.kmer, args.vector_size, args.context_size, args.embedding_file, \
           args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores, args.only_positive, args.method, args.smote


def get_result_props(all_scores):
    a = []
    for key in all_scores.keys():
        current = all_scores[key][0]
        current['random_state'] = key
        a.append((current, all_scores[key][1]))
    return a


def classify_combination(combination, random_states, all_vectors_X_train, all_vectors_X_test, y_train, y_test, cores,
                         handleDataImbalance=False, method=None):
    X_train = pd.DataFrame()
    for red in combination:
        X_train = pd.concat([pd.DataFrame(X_train.values), pd.DataFrame(all_vectors_X_train[red].values)], axis=1)

    X_test = pd.DataFrame()
    for red in combination:
        X_test = pd.concat([pd.DataFrame(X_test.values), pd.DataFrame(all_vectors_X_test[red].values)], axis=1)

    print(X_train.shape)
    print(X_test.shape)
    return rm_classifier_for_various_states_binary(
        random_states,
        X_train, X_test, y_train, y_test,
        cores=cores,
        method=method,
        handleDataImbalance=handleDataImbalance
    )


def run_combination_of_reductions(combination, random_states, all_vectors_X_train, all_vectors_X_test, y_train, y_test,
                                  cores, kmer, context_size,
                                  vector_size, method=None, handleDataImbalance=False):
    results = classify_combination(combination, random_states, all_vectors_X_train, all_vectors_X_test, y_train, y_test,
                                   cores, handleDataImbalance, method)
    return list(
        map(
            lambda x: add_properties_to_result(kmer, context_size, vector_size, combination, x[0], x[1].get_params()),
            get_result_props(results)
        )
    )


# def multiclass_for_all_activitites(data):
#     r = list(map(lambda x: "".join(map(str, x)), data))
#     encoder = LabelEncoder()
#     encoder.fit(r)
#     encoded_Y = encoder.transform(r)
#     # convert integers to dummy variables (i.e. one hot encoded)
#     return np_utils.to_categorical(encoded_Y)
#     # mlb = preprocessing.MultiLabelBinarizer()
#     # r = list(map(lambda x: "".join(map(str, x)), mlb.fit_transform(data)))
#     # tk = Tokenizer()
#     # tk.fit_on_texts(r)
#     # index_list = tk.texts_to_sequences(r)
#     # return pd.DataFrame(list(itertools.chain(*pad_sequences(index_list, maxlen=1))))
#

def multilabel(data):
    mlb = preprocessing.MultiLabelBinarizer()
    return mlb.fit_transform(data)


def get_target_data(classification_type, data):
    if classification_type == 'binary':
        return data.apply(lambda x: 0 if x == ['none'] else 1)
    if classification_type == 'multilabel':
        return multilabel(data)
    # if classification_type == 'multiclass':
    #     return multiclass_for_all_activitites(data)


def arg_parser():
    root = "./"
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/rfc/rfc_whole"
    cores = 10
    only_positive = False
    method = 'binary'
    smote = False

    negative_data_file = 'data/raw/M_model_train_nonAMP_sequence.csv'
    positive_data_file = 'data/raw/M_model_train_AMP_sequence.csv'
    # negative_data_file = 'data/raw/negative_data_unfiltered.csv'
    # positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--embedding-file', help='Embedding file path', default=embedding_file)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    parser.add_argument('--only-positive', help='Run only for positive data set', default=only_positive)
    parser.add_argument('--method', help='Binary / multiclass / multilabel', default=method)
    parser.add_argument('--smote', help='True or False', default=smote)
    return parser.parse_args()


def get_test_data(path):
    # negative_test_data_file = 'data/negative_data_cdhit_filter_removed.csv'
    # positive_test_data_file = 'data/raw/positive_data_removed_cdhit.csv'
    # negative_test_data_file = 'data/raw/benchmark_data_non_amp.csv'
    # positive_test_data_file = 'data/raw/benchmark_data_amp.csv'
    negative_test_data_file = 'data/raw/amp_scanner/ampscanner_test_non_amp.csv'
    positive_test_data_file = 'data/raw/amp_scanner/ampscanner_test_amp.csv'
    test_positive_data = get_positive_amp_data(path + positive_test_data_file)
    test_positive_data = massage_camp_data(test_positive_data).reset_index().drop('index', axis=1)
    test_negative_data = get_negative_amp_data(path + negative_test_data_file)
    test_negative_data = massage_camp_data(test_negative_data).reset_index().drop('index', axis=1)
    return pd.concat([test_positive_data, test_negative_data])


def get_train_data(root, positive_data_file, negative_data_file):
    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    raw_negative_data = get_negative_amp_data(root + negative_data_file)
    negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
    return pd.concat([positive_data, negative_data])


if __name__ == "__main__":
    root, kmer, vector_size, context_size, embedding_file, output_dir, negative_data_file, positive_data_file, cores, only_positive, method, smote = get_variables(
        arg_parser())

    all_reductions_combinations = get_all_combinations(reduction_groups)
    keys = list(np.unique(list(itertools.chain(*all_reductions_combinations))))
    # keys = ['blossom_matrix_8', 'contact_energies', 'None']
    # keys = ['conformation_similarity', 'blossom_matrix_4', 'mj_5', 'None']

    embeddings = {}
    all_vectors_X_train = {}
    all_vectors_X_test = {}
    all_errors = {}

    train_data = get_train_data(root, positive_data_file, negative_data_file)
    reduced_train_data = reduce_amp(train_data, keys)
    test_data = get_test_data(root)
    # test_data = pd.read_csv("data/raw/benchmark_whole_non_duplicate.csv", sep=",", header=0).reset_index().drop('index', axis=1)[
    #     ['Sequence', 'Activity']]
    reduced_test_data = reduce_amp(test_data, keys)

    y_train = get_target_data(method, reduced_train_data.classification)
    y_test = get_target_data(method, reduced_test_data.classification)

    for red in keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    random_states = [1]

    for red in keys:
        if not all_vectors_X_train.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_train_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors_X_train[red] = inputs
            inputs.to_csv(root + "data/vector_" + red + "_cbbio_whole.csv")
            all_errors[red] = errors

    for red in keys:
        if not all_vectors_X_test.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_test_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors_X_test[red] = inputs
            all_errors[red] = errors

    gaps = [1, 2]
    functions = {
        'dipeptide': reduce_with_gap_dipeptide_frequency,
        'tripeptide': reduce_with_gap_tripeptide_frequency
    }
    for gap in gaps:
        for peptide_type in list(functions.keys()):
            aa = functions[peptide_type](pd.concat([train_data, test_data]), gap)
            aa_train = aa[:train_data.shape[0]]
            aa_test = aa[train_data.shape[0]:]
            all_vectors_X_train['aa'] = aa_train.fillna(0.0)
            all_vectors_X_test['aa'] = aa_test.fillna(0.0)

            coombinations = list(map(lambda x: x + ['aa'], all_reductions_combinations))

            result = list(map(
                lambda combination: run_combination_of_reductions(
                    combination, random_states,
                    all_vectors_X_train, all_vectors_X_test, y_train, y_test, cores, kmer, context_size, vector_size,
                    method,
                    smote
                ),
                coombinations
            ))

            print(result)
            file_name = create_file_name_using_parameters(
                root + output_dir + method,
                "all_combinations_gap_" + str(gap) + "_" + peptide_type + "_cbbio_train_amp_test",
                kmer,
                context_size,
                vector_size
            )

            save_json(file_name + ".json", list(itertools.chain(*result)))

    # train_negative_test_data_file = 'data/raw/benchmark_data_train_non_amp.csv'
    # train_positive_test_data_file = 'data/raw/benchmark_data_train_amp.csv'
    # test_negative_test_data_file = 'data/raw/benchmark_data_non_amp.csv'
    # test_positive_test_data_file = 'data/raw/benchmark_data_amp.csv'
    #
    # test_positive_data = massage_camp_data(get_positive_amp_data(test_positive_test_data_file)).reset_index().drop('index', axis=1)
    # test_negative_data = massage_camp_data(get_negative_amp_data(test_negative_test_data_file)).reset_index().drop('index', axis=1)
    # train_positive_data = massage_camp_data(get_positive_amp_data(train_positive_test_data_file)).reset_index().drop('index', axis=1)
    # train_negative_data = massage_camp_data(get_negative_amp_data(train_negative_test_data_file)).reset_index().drop('index', axis=1)
    #
    # benchmark = pd.concat([test_positive_data, test_negative_data, train_positive_data, train_negative_data])
    #
    # positive_data = massage_camp_data(get_positive_amp_data(positive_data_file)).reset_index().drop('index', axis=1)
    # negative_data = massage_camp_data(get_negative_amp_data(negative_data_file)).reset_index().drop('index', axis=1)
    # train = pd.concat([positive_data, negative_data])
    #
    # pd.concat([benchmark, train]).drop_duplicates(subset=['Sequence'], keep=False)
    # values = train['Sequence'].values
    # a = benchmark['Sequence'].apply(lambda x: x not in values)
    #
    # non_duplicate_benchmark = benchmark[a]
    # non_duplicate_benchmark.to_csv("data/raw/benchmark_whole_non_duplicate.csv")
    # amp_scanner_predicted = pd.read_csv('data/results/1605009955345_Prediction_Summary.csv')
    # whole_test = pd.read_csv('data/raw/whole_test.csv')
    # whole_test['Activity'] = whole_test['Activity'].apply(lambda x: 0 if x.__contains__('none') else 1)
    #
    # r = pd.DataFrame()
    # r['prediction'] = amp_scanner_predicted['Prediction_Class'].apply(lambda x: 0 if x.__contains__('Non-AMP') else 1)
    # r['Sequence'] = amp_scanner_predicted['Sequence']
    #
    # prediction = r['prediction']
    # actual = whole_test['Activity']
    #
    # calculate_scores(actual, prediction, 'binary')

    # f = open("/Users/in-digvijay.gunjal/Downloads/antibacterial.fasta")
    # data = f.read().splitlines()
    #
    # content = []
    # seq = []
    # for line in data:
    #     if (line.startswith('>')):
    #         content.append("".join(seq))
    #         seq = []
    #         content.append(line)
    #     else:
    #         seq.append(line)
    #
    # f.close()
    #
    # nonamp = "\n".join(content)
    #
    # with open("/Users/in-digvijay.gunjal/Downloads/amp-final.fasta", 'w') as f:
    #     f.write(nonamp)

    # bencmark test accuracy
    # 21 non_amp in 920 amp
    # 74 amp in 920 non-amp
    # Accuracy - 94.8369565217

    # bencmark train accuracy
    # 98 non_amp in 590 amp B1
    # 70 non_amp in 896 amp B2
    # 189 amp in 1000 non-amp B1
    # 161 amp in 1405 non-amp B2

    # Accuracy - 86.59420289855072

    # (94.8369565217 + 86.59420289855072)/2
    #
    # 100-((98+70+189+161)/(590+869+2405)*100)
