import argparse
import itertools

import biovec
import numpy as np
import pandas as pd

from src.classification.random_forest import classify_combination
from src.classification.utils import add_properties_to_result
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import reduce_amp, get_positive_amp_data, \
    get_negative_amp_data
from src.files.camp import massage_camp_data
from src.files.utils import save_json, contains, OTHER_ALPHABETS


def get_variables(args):
    return args.root, args.kmer, args.vector_size, args.context_size, args.embedding_file, \
           args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores, args.only_positive


reduction_groups = [
    ['conformation_similarity', None],
    ['kd_hydrophobicitya', 'hydrophobicitya', 'amino_acid_residues', None],
    ['blossom_matrix_8', 'blossom_matrix_4', 'murphy_5', None],
    ['contact_energies', 'mj_4', 'mj_5', None],
    ['None', None]
]


def get_all_combinations(groups):
    # return [
    #     ['hydrophobicitya', 'blossom_matrix_4', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'amino_acid_residues', 'blossom_matrix_8', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'kd_hydrophobicitya', 'blossom_matrix_4', 'contact_energies'],
    #     ['conformation_similarity', 'amino_acid_residues', 'blossom_matrix_4', 'contact_energies'],
    #     ['amino_acid_residues', 'blossom_matrix_4', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'murphy_5', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'hydrophobicitya', 'blossom_matrix_4', 'contact_energies'],
    #     ['conformation_similarity', 'amino_acid_residues', 'blossom_matrix_4', 'contact_energies'],
    #     ['hydrophobicitya', 'murphy_5', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'amino_acid_residues', 'blossom_matrix_8', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'blossom_matrix_4', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'amino_acid_residues', 'blossom_matrix_4', 'contact_energies'],
    #     ['conformation_similarity', 'hydrophobicitya', 'murphy_5', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'amino_acid_residues', 'blossom_matrix_8', 'contact_energies'],
    #     ['hydrophobicitya', 'murphy_5', 'contact_energies', 'None']
    # ]
    return list(
        filter(
            lambda c: len(c) != 0,
            map(
                lambda c: list(filter(lambda r: r is not None, c)),
                itertools.product(*groups)
            )
        )
    )


def arg_parser():
    root = "./"
    kmer = 5
    context_size = 5
    vector_size = 300
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/binary_rfc"
    cores = 10
    only_positive = False

    negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/AMP/data/positive_data_filtered2807.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--embedding-file', help='Embedding file path', default=embedding_file)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    parser.add_argument('--only-positive', help='Cores used by classifier', default=only_positive, type=bool)

    return parser.parse_args()


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def run_combination_of_reductions(all_reductions, all_vectors, random_states, target, cores, kmer, context_size,
                                  vector_size):
    result = classify_combination(all_reductions, all_vectors, random_states, target, cores)
    return add_properties_to_result(kmer, context_size, vector_size, all_reductions, result[0],
                                    result[1].get_params())


def additional_filtering_of_sequences(data):
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())
    indexNames = sequences[sequences['Sequence'] == 'GWLDVAKKIGKAAFNVAKNFLFNKAVNFAAKGIKKAVDLWG '].index
    sequences.drop(indexNames, inplace=True)
    sequences = sequences[sequences.Sequence.apply(lambda x: x.isalpha())]

    return sequences


if __name__ == "__main__":
    root, kmer, vector_size, context_size, embedding_file, output_dir, negative_data_file, positive_data_file, cores, only_positive = \
        get_variables(arg_parser())

    all_reductions_combinations = get_all_combinations(reduction_groups)

    embeddings = {}
    all_vectors = {}
    all_errors = {}
    random_states = list(range(1, 2, 1))

    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    AMP_data = positive_data

    if not only_positive:
        raw_negative_data = get_negative_amp_data(root + negative_data_file)
        negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
        AMP_data = pd.concat([positive_data, negative_data])

    AMP_data_filtered = additional_filtering_of_sequences(AMP_data)

    keys = list(np.unique(list(itertools.chain(*all_reductions_combinations))))
    scores = []
    reduced_data = reduce_amp(AMP_data_filtered, keys)
    reduced_data = reduced_data.reset_index(drop=True)
    reduced_data['classification'] = reduced_data.classification.apply(lambda x: 0 if x == ['none'] else 1)

    target = reduced_data['classification']
    for red in keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    for red in keys:
        if not all_vectors.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors[red] = inputs
            all_errors[red] = errors

    result = list(map(
        lambda all_reductions: run_combination_of_reductions(
            all_reductions, all_vectors, random_states, target, cores, kmer,
            context_size, vector_size),
        all_reductions_combinations
    ))

    file_name = create_file_name_using_parameters(
        root + output_dir,
        "all_combinations",
        kmer,
        context_size,
        vector_size
    )

    save_json(file_name + ".json", result)
