import itertools

import biovec
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from tensorflow import keras

from src.classification.utils import calculate_scores
from src.dnn_classification_combinations import get_target_data
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import get_positive_amp_data, get_negative_amp_data, \
    reduce_amp
from src.files.camp import massage_camp_data

if __name__ == "__main__":

    root = "./"
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/dnn/dnn"
    cores = 10
    only_positive = False
    method = 'binary'

    negative_data_file = 'data/negative_data_cdhit_filter_removed.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/positive_data_removed_cdhit.csv'  # 0.4 similarity threshold with positive data

    embeddings = {}
    all_vectors = {}
    all_errors = {}
    random_states = list(range(1, 2, 1))

    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    # AMP_data = positive_data
    test_data_file = get_negative_amp_data(root + negative_data_file)
    data = massage_camp_data(test_data_file).reset_index().drop('index', axis=1)
    AMP_data = pd.concat([positive_data, data])
    # AMP_data = data

    # keys = list(np.unique(list(itertools.chain(*all_reductions_combinations))))
    # keys = ['hydrophobicitya', 'blossom_matrix_4', 'contact_energies', 'None'] # dnn binary
    # keys = ['blossom_matrix_8', 'None']

    reduced_data = reduce_amp(AMP_data, keys)

    target = get_target_data(method, reduced_data.classification)

    for red in keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    for red in keys:
        if not all_vectors.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors[red] = inputs
            all_errors[red] = errors

    all_inputs = pd.DataFrame()
    for red in keys:
        all_inputs = pd.concat([all_inputs, all_vectors[red]], axis=1)

    model = keras.models.load_model('./data/dnn_binary_unfiltered_1')

    predicted = model.predict(all_inputs)

    predicted = list(itertools.chain(*np.round(predicted)))
    predicted = list(map(int, predicted))

    print(confusion_matrix(target, predicted))

    scores = calculate_scores(target, predicted, method=method)
    print(scores)
