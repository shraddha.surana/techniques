import argparse

import pandas as pd
from sklearn import preprocessing

from src.classification.dnn import dnn_classifier_for_various_states
from src.classification.utils import get_best_result
from src.dnn_classification import get_model3
from src.embeddings.reduce_dataset import reduce_by_alphabet_frequency, reduce_with_dipeptide_frequency
from src.files.amp import filter_data_for_selected_activities, get_positive_amp_data, \
    get_negative_amp_data, amp_positive_data_activities
from src.files.camp import massage_camp_data
from src.files.utils import save_json, contains, OTHER_ALPHABETS

encoding_mapping = {
    "aa_freq": reduce_by_alphabet_frequency,
    "dipeptide_freq": reduce_with_dipeptide_frequency,
    "aa_dipeptide_combined": lambda x: pd.concat([reduce_by_alphabet_frequency(x), reduce_with_dipeptide_frequency(x)],
                                                 axis=1)
}


def classify(random_states, val, target, reduction, method=None):
    result = dnn_classifier_for_various_states(random_states, val, target, model, method=method)
    result = get_best_result(result)

    scores = result[0]
    scores['reduction'] = reduction
    config = result[1].get_config()
    config['build_input_shape'] = config['build_input_shape'].as_list()
    scores['model'] = config
    result[1].save("best_dnn_model.h5")

    return (scores)


def get_variables(args):
    return args.root, args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores


def arg_parser():
    root = "./"
    # output_dir = "data/results/basline_binary_clipped_data_rfc" if method == 'binary' else "data/results/basline_clipped_new_data_rfc"
    pos_data_type = 'filtered' if positive_filtered else 'unfiltered'
    curated_type = 'curated' if is_curated else 'CBBio'

    if is_clipped:
        if method == 'binary':
            output_dir = "data/results/dnn_basline_binary_{}_positive_clipped_data".format(pos_data_type)
        elif method == 'multi':
            if positive_only:
                output_dir = "data/results/dnn_basline_multi_{}_positive_only_clipped_data".format(pos_data_type)
            else:
                output_dir = "data/results/dnn_basline_multi_unfiltered_positive_clipped_data".format(pos_data_type)

        negative_data_file = 'data/raw/amp_curated_negative_sequence_length_10_to_60.csv'  # 0.4 similarity threshold with positive data
        positive_data_file = 'data/raw/amp_curated_positive_{}_sequence_length_10_to_60.csv'.format(
            pos_data_type)  # filtered 70% similarity threshold

    else:
        if method == 'binary':
            output_dir = "data/results/dnn_basline_binary_{}_positive".format(pos_data_type)
        elif method == 'multi':
            if positive_only:
                output_dir = "data/results/dnn_basline_multi_{}_positive_only".format(pos_data_type)
            else:
                output_dir = "data/results/dnn_basline_multi_unfiltered_positive".format(pos_data_type)

        negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
        positive_data_file = 'data/raw/AMP/positive_data_{}.csv'.format(
            pos_data_type)  # filtered 70% similarity threshold

    # output_dir = "data/results/basline_curate_binary_clipped_data" if method == 'binary' else "data/results/basline_curate_multi_clipped_data"

    cores = 10

    # negative_data_file = 'data/raw/amp_CBBio_negative_binary_sequence_length_10_to_60.csv'  # 0.4 similarity threshold with positive data
    # positive_data_file = 'data/raw/amp_CBBio_positive_binary_sequence_length_10_to_60.csv'  # filtered 70% similarity threshold

    # negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    # positive_data_file = 'data/raw/AMP/positive_data_{}.csv'.format(pos_data_type)  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


def additional_filtering_of_sequences(data):
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())
    indexNames = sequences[sequences['Sequence'] == 'GWLDVAKKIGKAAFNVAKNFLFNKAVNFAAKGIKKAVDLWG '].index
    sequences.drop(indexNames, inplace=True)
    sequences = sequences[sequences.Sequence.apply(lambda x: x.isalpha())]

    return sequences


if __name__ == "__main__":
    method = 'binary'
    positive_only = True
    positive_filtered = False
    is_curated = True
    is_clipped = True

    encoding_scores = []
    root, output_dir, negative_data_file, positive_data_file, cores = get_variables(arg_parser())
    # output_dir = 'data/results/dnn_basline_binary_curated_unfiltered_positive_CBBio_clipped_data'

    random_states = list(range(1, 100, 1))

    raw_negative_data = get_negative_amp_data(root + negative_data_file)
    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
    # AMP_data = pd.concat([raw_negative_data, raw_positive_data])     # for new data
    AMP_data = pd.concat([negative_data, positive_data_filtered])  # for old data
    AMP_data_filtered = additional_filtering_of_sequences(AMP_data)
    AMP_data_filtered = AMP_data_filtered.reset_index(drop=True)

    if method == 'multi' and positive_only:
        AMP_data_filtered = additional_filtering_of_sequences(positive_data_filtered)
        AMP_data_filtered = AMP_data_filtered.reset_index(drop=True)

    if method == 'binary':
        target = AMP_data_filtered.Activity.apply(lambda x: 0 if x == ['none'] else 1)
        # target = AMP_data_filtered.Activity.apply(lambda x: 0 if x == 'None' else 1)      # for new data
    else:
        mlb = preprocessing.MultiLabelBinarizer()
        target = mlb.fit_transform(AMP_data_filtered['Activity'])

    print(f"Output file: {output_dir}")
    print(f"Negative file: {negative_data_file}")
    print(f"Positive file: {positive_data_file}")

    for encoding_name, encoding_stretagy in encoding_mapping.items():
        print("--------- Encoding with {} ---------".format(encoding_name.upper()))
        reduced_data = encoding_stretagy(AMP_data_filtered)
        val = reduced_data.values

        model = get_model3(val.shape[1])

        res = classify(random_states, val, target, encoding_name.upper(), method=method)
        print(res)
        encoding_scores.append(res)

    file_name = root + output_dir + "_reduction_none"

    save_json(file_name + ".json", encoding_scores)
