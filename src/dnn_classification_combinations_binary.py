import argparse
import itertools

import biovec
import keras
import numpy as np
import pandas as pd
from keras.initializers import glorot_normal
from keras.layers import Dense, Dropout
from keras.models import Sequential
from keras.utils import np_utils
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder

from src.classification.dnn import dnn_classifier_for_various_states_binary
from src.classification.utils import add_properties_to_result
from src.combined_reduction_all_combinations import get_all_combinations, reduction_groups
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import get_positive_amp_data, get_negative_amp_data, \
    reduce_amp
from src.files.camp import massage_camp_data
from src.files.utils import save_json


def get_model(input_shape, output_size):
    model = Sequential()
    model.add(Dense(500, activation="relu", input_shape=(input_shape,), kernel_initializer=glorot_normal(),
                    bias_initializer=glorot_normal()))
    model.add(Dropout(0.4))
    model.add(Dense(800, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.4))
    model.add(Dense(300, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.3))
    model.add(Dense(70, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.2))
    model.add(
        Dense(output_size, activation="sigmoid", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    return model


def get_model2(input_shape, output_size):
    model = Sequential()
    model.add(Dense(1000, activation="relu", input_shape=(input_shape,), kernel_initializer=glorot_normal(),
                    bias_initializer=glorot_normal()))
    model.add(Dropout(0.5))
    model.add(Dense(800, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.3))
    model.add(Dense(500, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.3))
    model.add(Dense(300, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.3))
    model.add(Dense(150, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.3))
    model.add(Dense(70, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.2))
    model.add(
        Dense(output_size, activation="sigmoid", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    return model


def model3(input_shape, output_size):
    model = Sequential()
    model.add(Dense(128, activation="relu", input_shape=(input_shape,), kernel_initializer=glorot_normal(),
                    bias_initializer=glorot_normal()))
    model.add(Dropout(0.4))
    model.add(Dense(64, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.4))
    model.add(Dense(32, activation="relu", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    model.add(Dropout(0.2))
    model.add(
        Dense(output_size, activation="sigmoid", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    return keras.models.clone_model(model)


def get_variables(args):
    return args.root, args.kmer, args.vector_size, args.context_size, args.embedding_file, \
           args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores, args.only_positive, args.method, args.smote


def get_result_props(all_scores):
    a = []
    for key in all_scores.keys():
        current = all_scores[key][0]
        current['random_state'] = key
        a.append((current, all_scores[key][1]))
    return a


def run_combination_of_reductions(combination, random_states, all_vectors_X_train, all_vectors_X_test, y_train, y_test,
                                  cores, kmer, context_size,
                                  vector_size, model, method, handleDataImbalance):
    results = classify_combination(combination, random_states, all_vectors_X_train, all_vectors_X_test, y_train, y_test,
                                   model, cores, method,
                                   handleDataImbalance)
    return list(
        map(
            lambda x: add_properties_to_result(kmer, context_size, vector_size, combination, x[0], x[1].get_config()),
            get_result_props(results))
    )


def classify_combination(combination, random_states, all_vectors_X_train, all_vectors_X_test, y_train, y_test, model,
                         cores, method, handleDataImbalance):
    X_train = pd.DataFrame()
    for red in combination:
        X_train = pd.concat([X_train, all_vectors_X_train[red]], axis=1)

    X_test = pd.DataFrame()
    for red in combination:
        X_test = pd.concat([X_test, all_vectors_X_test[red]], axis=1)

    return dnn_classifier_for_various_states_binary(random_states, X_train, X_test, y_train, y_test,
                                                    model(X_train.shape[1], 1),
                                                    method, handleDataImbalance)


def multiclass_for_all_activitites(data):
    r = list(map(lambda x: "".join(map(str, x)), data))
    encoder = LabelEncoder()
    encoder.fit(r)
    encoded_Y = encoder.transform(r)
    # convert integers to dummy variables (i.e. one hot encoded)
    return np_utils.to_categorical(encoded_Y)
    # mlb = preprocessing.MultiLabelBinarizer()
    # r = list(map(lambda x: "".join(map(str, x)), mlb.fit_transform(data)))
    # tk = Tokenizer()
    # tk.fit_on_texts(r)
    # index_list = tk.texts_to_sequences(r)
    # return pd.DataFrame(list(itertools.chain(*pad_sequences(index_list, maxlen=1))))


def multilabel(data):
    mlb = preprocessing.MultiLabelBinarizer()
    return mlb.fit_transform(data)


def get_target_data(classification_type, data):
    if classification_type == 'binary':
        return data.apply(lambda x: 0 if x == ['none'] else 1)
    if classification_type == 'multilabel':
        return multilabel(data)
    if classification_type == 'multiclass':
        return multiclass_for_all_activitites(data)


def arg_parser():
    root = "./"
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/dnn/dnn"
    cores = 10
    only_positive = False
    method = 'binary'
    smote = False

    negative_data_file = 'data/raw/negative_data_unfiltered.csv'
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--embedding-file', help='Embedding file path', default=embedding_file)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    parser.add_argument('--only-positive', help='Run only for positive data set', default=only_positive)
    parser.add_argument('--method', help='Binary / multiclass / multilabel', default=method)
    parser.add_argument('--smote', help='True or False', default=smote)
    return parser.parse_args()


def get_test_data(path):
    negative_test_data_file = 'data/negative_data_cdhit_filter_removed.csv'
    positive_test_data_file = 'data/raw/positive_data_removed_cdhit.csv'
    test_positive_data = get_positive_amp_data(path + positive_test_data_file)
    test_positive_data = massage_camp_data(test_positive_data).reset_index().drop('index', axis=1)
    test_negative_data = get_negative_amp_data(path + negative_test_data_file)
    test_negative_data = massage_camp_data(test_negative_data).reset_index().drop('index', axis=1)
    return pd.concat([test_positive_data, test_negative_data])


def get_train_data(root, positive_data_file, negative_data_file):
    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    raw_negative_data = get_negative_amp_data(root + negative_data_file)
    negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
    return pd.concat([positive_data, negative_data])


if __name__ == "__main__":
    root, kmer, vector_size, context_size, embedding_file, output_dir, negative_data_file, positive_data_file, cores, only_positive, method, smote = get_variables(
        arg_parser())

    all_reductions_combinations = get_all_combinations(reduction_groups)
    # keys = list(np.unique(list(itertools.chain(*all_reductions_combinations))))
    keys = ['conformation_similarity', 'blossom_matrix_4', 'mj_5', 'None']

    # keys = ['blossom_matrix_8', 'contact_energies', 'None']
    # keys = ['hydrophobicitya', 'blossom_matrix_4', 'conformation_similarity']

    embeddings = {}
    all_vectors_X_train = {}
    all_vectors_X_test = {}
    all_errors = {}

    train_data = get_train_data(root, positive_data_file, negative_data_file)
    reduced_train_data = reduce_amp(train_data, keys)
    test_data = get_test_data(root)
    reduced_test_data = reduce_amp(test_data, keys)

    y_train = get_target_data(method, reduced_train_data.classification)
    y_test = get_target_data(method, reduced_test_data.classification)

    for red in keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    random_states = [1]

    for red in keys:
        if not all_vectors_X_train.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_train_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors_X_train[red] = inputs
            all_errors[red] = errors

    for red in keys:
        if not all_vectors_X_test.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_test_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors_X_test[red] = inputs
            all_errors[red] = errors

    result = list(map(
        lambda combination: run_combination_of_reductions(
            combination, random_states,
            all_vectors_X_train, all_vectors_X_test, y_train, y_test, cores, kmer, context_size, vector_size,
            model3, method, smote
        ),
        [keys]
    ))

    file_name = create_file_name_using_parameters(
        root + output_dir + method,
        "benchmark_all_combinations",
        kmer,
        context_size,
        vector_size
    )

    save_json(file_name + ".json", list(itertools.chain(*result)))
