import biovec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras import Sequential
from keras.layers import Dense
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

from src.oldscripts.classify_alphabet_reduced_kd_hydrophobicitya import convert_sequences_to_vectors, \
    classify_model_with_results
from src.embeddings.reduce_dataset import reduce
from src.embeddings.reduction_utils import conformation_similarity, kd_hydrophobicitya
from src.embeddings.utils import words_to_vec
from src.files.utils import contains, OTHER_ALPHABETS
from src.oldscripts.main import get_negative_amp_data, get_positive_amp_data


def run_random_forest_variation(rfc, x_train, x_test, y_train, y_test):
    print("RandomForestClassifier : ")
    return classify_model_with_results(x_train, x_test, y_train, y_test, rfc)
    # print("RFECV : ")
    # rfecv = RFECV(estimator=rfc, step=1, cv=10, scoring=None, verbose=2, n_jobs=10)
    # classify_model(x_train, x_test, y_train, y_test, rfecv)
    # print("RFE : ")
    # rfe = RFE(estimator=rfc, n_features_to_select=200, step=1, verbose=0)
    # classify_model(X_train, X_test, Y_train, Y_test, rfe)


def plot_loss(keras_history):
    fig2, ax2 = plt.subplots()
    ax2.plot(keras_history.history['loss'], color='gray', lw=1)
    ax2.plot(keras_history.history['val_loss'], color='k', lw=0.9)
    plt.yscale('log')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper right')
    plt.tight_layout()
    return plt


if __name__ == "__main__":
    ROOT_DIR = ""
    negative_data_filtered_path = ROOT_DIR + 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_filtered_path = ROOT_DIR + 'data/raw/AMP/positive_data_filtered.csv'  # filtered 70% similarity threshold
    positive_data_unfiltered_path = ROOT_DIR + 'data/raw/AMP/positive_data_unfiltered.csv'

    # ---- Reading the negative dataaset ----
    ndata = get_negative_amp_data(negative_data_filtered_path)
    print(ndata.columns)
    ndata['Activity'] = 0

    # ---- Read the positive dataset ----
    # pdata = get_positive_amp_data(positive_data_filtered_path)
    pdata = get_positive_amp_data(positive_data_unfiltered_path)
    # pdata['Activity'] = 'antimicrobial'
    pdata['Activity'] = 1
    print(pdata.columns)

    # ---- join the data  + cleanse ----
    data = pd.concat([ndata, pdata])
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())

    basic_sequences = sequences

    indexNames = basic_sequences[basic_sequences['Sequence'] == 'GWLDVAKKIGKAAFNVAKNFLFNKAVNFAAKGIKKAVDLWG '].index
    # Delete these row indexes from dataFrame
    basic_sequences.drop(indexNames, inplace=True)

    # ----Non reduced data----
    uniprot_embedding_file = ROOT_DIR + 'data/embeddings/kmer_5_contextWindow_25_vector_300_reduction_None'
    uniprot_embedding = biovec.models.load_protvec(uniprot_embedding_file)
    # non_reduced_pv = biovec.models.load_protvec(uniprot_embedding)

    inputs, errors = convert_sequences_to_vectors(basic_sequences['Sequence'], uniprot_embedding, words_to_vec, kmer=5)

    X_train, X_test, Y_train, Y_test = train_test_split(inputs, basic_sequences['Activity'], random_state=11)

    # ---- reduce data ----

    conformation_embedding_file = ROOT_DIR + 'data/embeddings/uniprot__kmer_5_contextWindow_5_vector_300_reduction_conformation_similarity'
    kd_hydrophobicitya_embedding_file = ROOT_DIR + 'data/embeddings/uniprot__kmer_3_contextWindow_5_vector_300_reduction_kd_hydrophobicitya'

    conformation_reduced_data = pd.DataFrame(reduce(pd.DataFrame(basic_sequences), conformation_similarity))
    kd_hydrophobicitya_reduced_data = pd.DataFrame(reduce(pd.DataFrame(basic_sequences), kd_hydrophobicitya))
    reduced_data = pd.DataFrame()
    reduced_data['conformation'] = conformation_reduced_data[0]
    reduced_data['kd_hydrophobicitya'] = kd_hydrophobicitya_reduced_data[0]
    reduced_data['classification'] = kd_hydrophobicitya_reduced_data[1]

    # --- Prot vec ----
    # Use above to classify dataset.
    conformation_pv = biovec.models.load_protvec(conformation_embedding_file)
    kd_hydrophobicitya_pv = biovec.models.load_protvec(kd_hydrophobicitya_embedding_file)

    # convert to protein embeddings
    inputs_conformation, errors_conformation = convert_sequences_to_vectors(reduced_data['conformation'],
                                                                            conformation_pv, words_to_vec, kmer=5)
    inputs_kd_hydrophobicitya, errors_kd_hydrophobicitya = convert_sequences_to_vectors(
        reduced_data['kd_hydrophobicitya'], kd_hydrophobicitya_pv, words_to_vec, kmer=5)
    inputs = np.append(inputs_conformation.values, inputs_kd_hydrophobicitya.values, axis=1)
    target = reduced_data['classification']

    # ---- Classification ----

    # split train test
    X_train, X_test, Y_train, Y_test = train_test_split(inputs, target, random_state=11)

    # ---- Random Forest ----
    rfc1 = RandomForestClassifier()
    rfc100 = RandomForestClassifier(n_estimators=100, max_depth=100, n_jobs=10, verbose=True)
    model, y_predict, score, matrix = run_random_forest_variation(rfc100, X_train, X_test, Y_train, Y_test)


    # ---- DNN  ----

    def prepare_targets(y_train, y_test):
        le = LabelEncoder()
        le.fit(y_train)
        y_train_enc = le.transform(y_train)
        y_test_enc = le.transform(y_test)
        return y_train_enc, y_test_enc


    y_train, y_test = prepare_targets(Y_train, Y_test)

    # define the model
    model = Sequential()
    model.add(Dense(10, input_dim=X_train.shape[1], activation='relu', kernel_initializer='he_normal'))
    model.add(Dense(100, activation='relu', kernel_initializer='he_normal'))
    model.add(Dense(100, activation='relu', kernel_initializer='he_normal'))
    model.add(Dense(100, activation='relu', kernel_initializer='he_normal'))
    model.add(Dense(1, activation='sigmoid'))
    # compile the keras model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])  # optimizer='rmsprop'
    # fit the keras model on the dataset
    history = model.fit(X_train, y_train, epochs=3, batch_size=16, verbose=2, validation_split=0.2)
    # evaluate the keras model
    _, accuracy = model.evaluate(X_test, y_test, verbose=0)
    print('Accuracy: %.2f' % (accuracy * 100))
    print(history.history.keys())

    # -----------------------------------
    model.model.save('first' + '.h5')
    print(history.history.keys())
    # np.save(history, name + '_history')
    # can save the history here
    loss_plot = plot_loss(history)
    loss_plot.show()
    # loss_plot.savefig(name + '_loss.pdf')

# When there is categorical input data
# prepare input data
# def prepare_inputs(X_train, X_test):
# 	ohe = OneHotEncoder()
# 	ohe.fit(X_train)
# 	X_train_enc = ohe.transform(X_train)
# 	X_test_enc = ohe.transform(X_test)
# 	return X_train_enc, X_test_enc
