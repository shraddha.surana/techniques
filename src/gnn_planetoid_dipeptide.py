import matplotlib.pyplot as plt
import pandas as pd
import torch
import torch.nn.functional as F
from sklearn.manifold import TSNE
from torch.nn import Linear
from torch_geometric.data import Data, InMemoryDataset, DataLoader
from torch_geometric.nn import GCNConv, global_mean_pool

from src.combined_reduction_all_combinations_binary_whole_data import get_train_data, get_target_data
from src.embeddings.reduce_dataset import reduce_with_gap_dipeptide_frequency
from src.files.amp import get_negative_amp_data, get_positive_amp_data
from src.files.amp import massage_camp_data

if __name__ == '__main__':
    # dataset = Planetoid(root='data/Planetoid', name='Cora', transform=NormalizeFeatures())
    #
    # print()
    # print(f'Dataset: {dataset}:')
    # print('======================')
    # print(f'Number of graphs: {len(dataset)}')
    # print(f'Number of features: {dataset.num_features}')
    # print(f'Number of classes: {dataset.num_classes}')
    #
    # data = dataset[0]  # Get the first graph object.
    #
    # print()
    # print(data)
    # print('===========================================================================================================')
    #
    # # Gather some statistics about the graph.
    # print(f'Number of nodes: {data.num_nodes}')
    # print(f'Number of edges: {data.num_edges}')
    # print(f'Average node degree: {data.num_edges / data.num_nodes:.2f}')
    # print(f'Number of training nodes: {data.train_mask.sum()}')
    # print(f'Training node label rate: {int(data.train_mask.sum()) / data.num_nodes:.2f}')
    # print(f'Contains isolated nodes: {data.contains_isolated_nodes()}')
    # print(f'Contains self-loops: {data.contains_self_loops()}')
    # print(f'Is undirected: {data.is_undirected()}')

    root = "./"
    # negative_data_file = 'data/raw/M_model_train_nonAMP_sequence.csv'
    # positive_data_file = 'data/raw/M_model_train_AMP_sequence.csv'
    negative_data_file = 'data/raw/negative_data_unfiltered.csv'
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'


    def get_test_data(path):
        negative_test_data_file = 'data/negative_data_cdhit_filter_removed.csv'
        positive_test_data_file = 'data/raw/positive_data_removed_cdhit.csv'
        # negative_test_data_file = 'data/raw/benchmark_data_non_amp.csv'
        # positive_test_data_file = 'data/raw/benchmark_data_amp.csv'
        # negative_test_data_file = 'data/raw/amp_scanner/ampscanner_test_non_amp.csv'
        # positive_test_data_file = 'data/raw/amp_scanner/ampscanner_test_amp.csv'
        test_positive_data = get_positive_amp_data(path + positive_test_data_file)
        test_positive_data = massage_camp_data(test_positive_data).reset_index().drop('index', axis=1)
        test_negative_data = get_negative_amp_data(path + negative_test_data_file)
        test_negative_data = massage_camp_data(test_negative_data).reset_index().drop('index', axis=1)
        return pd.concat([test_positive_data, test_negative_data])


    print("loading data")
    train_data = get_train_data(root, positive_data_file, negative_data_file)
    test_data = get_test_data(root)


    print("gap dipeptide data")
    aa = reduce_with_gap_dipeptide_frequency(pd.concat([train_data, test_data]), 2)
    train_dataset = aa[:train_data.shape[0]]
    test_dataset = aa[train_data.shape[0]:]

    print("get target data")
    y_train = get_target_data("binary", train_data.Activity)
    y_test = get_target_data("binary", test_data.Activity)

    num_features = train_dataset.shape[1]
    num_classes = 2

    class TrainDataset(InMemoryDataset):
        def __init__(self, root, transform=None, pre_transform=None):
            super(TrainDataset, self).__init__(root, transform, pre_transform)
            self.data, self.slices = torch.load(self.processed_paths[0])

        @property
        def raw_file_names(self):
            return []

        @property
        def processed_file_names(self):
            return ['gnn_dipeptide_train.dataset']

        def download(self):
            pass

        def process(self):
            data_list = []
            train_dataset_values = train_dataset.values
            for i in range(0, len(train_dataset)):
                group = train_dataset_values[i]
                x = torch.tensor(group, dtype=torch.float).unsqueeze(1)
                y = torch.tensor([y_train.values[i]])
                nodes = list(range(0, len(group)))

                target_nodes = nodes[1:]
                source_nodes = nodes[:-1]

                edge_index = torch.tensor([source_nodes, target_nodes], dtype=torch.long)

                data = Data(x=x, edge_index=edge_index, y=y)
                data_list.append(data)

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])


    class TestDataset(InMemoryDataset):
        def __init__(self, root, transform=None, pre_transform=None):
            super(TestDataset, self).__init__(root, transform, pre_transform)
            self.data, self.slices = torch.load(self.processed_paths[0])

        @property
        def raw_file_names(self):
            return []

        @property
        def processed_file_names(self):
            return ['gnn_dipeptide_test.dataset']

        def download(self):
            pass

        def process(self):
            data_list = []
            test_dataset_values = test_dataset.values
            for i in range(0, len(test_dataset)):
                group = test_dataset_values[i]
                x = torch.tensor(group, dtype=torch.float).unsqueeze(1)
                y = torch.tensor([y_test.values[i]])
                nodes = list(range(0, len(group)))

                target_nodes = nodes[1:]
                source_nodes = nodes[:-1]

                edge_index = torch.tensor([source_nodes, target_nodes], dtype=torch.long)

                data = Data(x=x, edge_index=edge_index, y=y)
                data_list.append(data)

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])


    print("Create Train Dataset")
    train_dataset = TrainDataset(root='./data/')
    print("Create Test Dataset")
    test_dataset = TestDataset(root='./data/')


    class GCN(torch.nn.Module):
        def __init__(self, hidden_channels):
            super(GCN, self).__init__()
            torch.manual_seed(12345)
            self.conv1 = GCNConv(train_dataset.num_node_features, hidden_channels)
            self.conv2 = GCNConv(hidden_channels, hidden_channels)
            self.conv3 = GCNConv(hidden_channels, hidden_channels)
            self.lin = Linear(hidden_channels, train_dataset.num_classes)

        def forward(self, x, edge_index, batch):
            # 1. Obtain node embeddings
            x = self.conv1(x, edge_index)
            x = x.relu()
            x = self.conv2(x, edge_index)
            x = x.relu()
            x = self.conv3(x, edge_index)

            # 2. Readout layer
            x = global_mean_pool(x, batch)  # [batch_size, hidden_channels]

            # 3. Apply a final classifier
            x = F.dropout(x, p=0.5, training=self.training)
            x = self.lin(x)

            return x

    print("Create model")
    model = GCN(hidden_channels=64)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
    criterion = torch.nn.CrossEntropyLoss()

    print("Train Data loader")
    train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)
    print("Test Data loader")
    test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False)


    def train():
        model.train()

        for data in train_loader:  # Iterate in batches over the training dataset.
            out = model(data.x, data.edge_index, data.batch)  # Perform a single forward pass.
            loss = criterion(out, data.y)  # Compute the loss.
            loss.backward()  # Derive gradients.
            optimizer.step()  # Update parameters based on gradients.
            optimizer.zero_grad()  # Clear gradients.


    def test(loader):
        model.eval()

        correct = 0
        for data in loader:
            out = model(data.x, data.edge_index, data.batch)
            pred = out.argmax(dim=1)
            correct += int((pred == data.y).sum())
        return correct / len(loader.dataset)

    print("Start Epoch")
    for epoch in range(1, 3):
        print(epoch)
        train()
        train_acc = test(train_loader)
        test_acc = test(test_loader)
        print(f'Epoch: {epoch:03d}, Train Acc: {train_acc:.4f}, Test Acc: {test_acc:.4f}')
