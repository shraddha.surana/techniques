kmers="3 5"
context_sizes="5 10 25"
vector_sizes="100 200 300"

helpFunction() {
  echo ""
  echo "Usage: $0 -m <binary/multilabel/multiclass>"
  echo "Parameter"
  echo "\t-m\t Binary classification(binary) or Multilabel classification(multilabel) or Multiclass classification(multiclass)."
  exit 1
}

while getopts ":m:" opt; do
  case $opt in
  m)
    method="$OPTARG"
    ;;
  \?)
    echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

if [ -z $method ]; then
  echo "method missing"
  helpFunction
fi

echo "Running $method classification - DNN"
for kmer in $kmers; do
  for context_size in $context_sizes; do
    for vector_size in $vector_sizes; do
      echo "kmer: $kmer"
      echo "context size: $context_size"
      echo "vector size: $vector_size"
      echo "method: $method"

      python -m src.dnn_classification_combinations --root="/nvme/life_science/techniques/" --kmer="$kmer" --context-size="$context_size" --vector-size="$vector_size" --cores=10 --method="$method" &
    done
  done
done
