kmers="3 5"
context_sizes="5 10 25"
vector_sizes="100 200 300"
reductions="conformation_similarity kd_hydrophobicitya blossom_matrix_8 blossom_matrix_4 hydrophobicitya contact_energies amino_acid_residues murphy_5 mj_4 mj_5 None"

for kmer in $kmers; do
  for context_size in $context_sizes; do
    for vector_size in $vector_sizes; do
      for reduction in $reductions; do
        echo "kmer: $kmer"
        echo "context size: $context_size"
        echo "vector size: $vector_size"

        echo "Embedding: $reduction"
        python -m src.embeddings.create --root="" --kmer="$kmer" --context-size="$context_size" --vector-size="$vector_size" --reduction="$reduction"
      done
    done
  done
done
