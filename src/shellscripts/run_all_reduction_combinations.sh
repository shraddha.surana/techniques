kmers="3 5"
context_sizes="5 10 25"
vector_sizes="100 200 300"

helpFunction() {
  echo ""
  echo "Usage: $0 -m <binary/multi>"
  echo "Parameter"
  echo "\t-m\t Binary classification(binary) or Multilabel classification(multi)."
  exit 1
}

while getopts ":m:" opt; do
  case $opt in
  m)
    method="$OPTARG"
    ;;
  \?)
    echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

if [ -z $method ]; then
  echo "method missing"
  helpFunction
fi

if [ "$method" == "binary" ]; then
  echo "Running binary classification"
  for kmer in $kmers; do
    for context_size in $context_sizes; do
      for vector_size in $vector_sizes; do
        echo "kmer: $kmer"
        echo "context size: $context_size"
        echo "vector size: $vector_size"

        python -m src.combined_reduction_all_combinations_binary --root="/nvme/life_science/techniques/" --kmer="$kmer" --context-size="$context_size" --vector-size="$vector_size" --cores=10 &
      done
    done
  done
elif [ "$method" == "multi" ]; then
  echo "Running multilabel classification"
  for kmer in $kmers; do
    for context_size in $context_sizes; do
      for vector_size in $vector_sizes; do
        echo "kmer: $kmer"
        echo "context size: $context_size"
        echo "vector size: $vector_size"

        python -m src.combined_reduction_all_combinations --root="/nvme/life_science/techniques/" --kmer="$kmer" --context-size="$context_size" --vector-size="$vector_size" --cores=10 &
      done
    done
  done
else
  echo "Invalid argument $method" >&2
  helpFunction
fi
