kmers="3"
context_sizes="10 25"
vector_sizes="100"

echo "Running clasifier chain classification - RFC"
for kmer in $kmers; do
  for context_size in $context_sizes; do
    for vector_size in $vector_sizes; do
      echo "kmer: $kmer"
      echo "context size: $context_size"
      echo "vector size: $vector_size"

      python -m src.classifier_chain --root="/nvme/life_science/techniques/" --data="data/raw/AMP/" --output-dir="data/results/classifier_chain/" --kmer="$kmer" --context-size="$context_size" --vector-size="$vector_size" --cores=50 &
    done
  done
done
