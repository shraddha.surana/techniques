kmer=3
context_size=10
vector_size=100

echo $kmer
echo $context_size
echo $vector_size

echo "Embedding: Non reduced" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --cores=3 &
echo "Embedding: hydrophobicitya" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="hydrophobicitya" --cores=3 &
echo "Embedding: kd hydrophobicitya" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="kd_hydrophobicitya" --cores=3 &
echo "Embedding: conformation_similarity" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="conformation_similarity" --cores=3 &
echo "Embedding: blossom_matrix_8" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="blossom_matrix_8" --cores=3 &
echo "Embedding: blossom_matrix_4" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="blossom_matrix_4" --cores=3 &
echo "Embedding: contact_energies" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="contact_energies" --cores=3 &
echo "Embedding: amino_acid_residues" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="amino_acid_residues" --cores=3 &
echo "Embedding: murphy_5" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="murphy_5" --cores=3 &
echo "Embedding: mj_4" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="mj_4" --cores=3 &
echo "Embedding: mj_5" &
python -m src.embeddings.create --root="" --data-path="/nvme/life_science/techniques/" --file-in="data/uniprot/uniprot_sprot_standard_alphabets.fasta" --kmer=$kmer --context-size=$context_size --vector-size=$vector_size --reduction="mj_5" --cores=3 &
