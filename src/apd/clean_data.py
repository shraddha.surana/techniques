import itertools

import pandas as pd

from src.embeddings.reduce_dataset import reduce_with_gap_tripeptide_frequency, reduce_with_gap_dipeptide_frequency


def all_to_str(a):
    return list(map(str, a))


def replace_anti_viral(activities):
    anti_bacterial_activities = ["anti-hiv", "antiviral"]

    if any(item in activities for item in anti_bacterial_activities):
        for activity_to_remove in list(set(activities).intersection(anti_bacterial_activities)):
            activities.remove(activity_to_remove)
        activities.append("antiviral")

    return activities


def replace_anti_bacterial(activities):
    anti_bacterial_activities = ["anti-gram+ & gram-", "anti-gram+", "anti-gram-", "antibiofilm", "anti-mrsa", "anti-bacterial"]

    if any(item in activities for item in anti_bacterial_activities):
        for activity_to_remove in list(set(activities).intersection(anti_bacterial_activities)):
            activities.remove(activity_to_remove)
        activities.append("anti-bacterial")

    return activities


if __name__ == "__main__":
    unfiltered = pd.read_csv("data/raw/AMP/positive_data_unfiltered2807.csv").reindex()
    apd = pd.read_csv("data/raw/apd/APD.csv").reindex()

    result = pd.DataFrame()

    result['Sequences'] = apd['Sequence']
    result['apd_activity'] = apd['Bioactivity']

    sequences = list(result['Sequences'].values)

    # unfiltered_sequences = []
    # for sequence in sequences:
    #     print(sequence)
    #     values = list(unfiltered.loc[unfiltered['Sequence'] == sequence]['Activity'].values)
    #     unfiltered_sequences.append("".join(values))

    unfiltered_sequences = list(
        map(lambda seq: list(unfiltered.loc[unfiltered['Sequence'] == seq]['Activity'].values), sequences))

    unfiltered_sequences = list(map(lambda x: "".join(all_to_str(x)), unfiltered_sequences))

    result["Unfiltered_data_activity"] = pd.DataFrame(unfiltered_sequences)[0]

    result.to_csv("data/raw/apd/mapped_with_unfiltered.csv")

    data = pd.read_csv("data/raw/apd/mapped_with_unfiltered.csv", index_col='index')

    data['apd_activity'] = data['apd_activity'].apply(lambda x: x.lower().strip() if type(x) == type("") else '')

    data['apd_activity'] = data['apd_activity'].apply(lambda x: map(lambda y: y.strip(), x.split(",")))

    data['apd_activity'] = data['apd_activity'].apply(lambda x: list(filter(lambda y: len(y) > 0, x)))

    data[['Sequences', 'apd_activity']].to_csv("data/raw/apd/APD_cleaned_activites.csv", index=False)

    cleaned_data = data[['Sequences', 'apd_activity']]

    cleaned_data = cleaned_data[cleaned_data['Sequences'].apply(lambda x: 10 < len(x) < 60)].reindex()

    cleaned_data['apd_activity'].apply(replace_anti_bacterial)
    cleaned_data['apd_activity'].apply(replace_anti_viral)

    activity__values = cleaned_data['apd_activity'].values

    unique_apd_activities = set(map(lambda x: x.strip(), itertools.chain(*activity__values)))

    counts = {}
    for activity in unique_apd_activities:
        count = len(cleaned_data['apd_activity'][
                        cleaned_data['apd_activity'].apply(lambda x: x.__contains__(activity)) == True].index)
        counts[activity] = count

    # After sequence length filter
    # 'anti-mrsa': 158, 'antiparasitic': 105, 'anti-toxin': 15, 'spermicidal': 12, 'antiviral': 163, 'insecticidal': 34,
    #  'anti-sepsis': 60, 'antimalarial': 28, 'enzyme inhibitor': 24, 'wound healing': 24, 'channel inhibitors': 5,
    #  'anti-tb': 10, 'anti-protists': 3, 'anti-diabetes': 15, 'anti-hiv': 100, 'anticancer': 219, 'antioxidant': 21,
    #  'antifungal': 1032, 'anti-inflammatory': 17, 'hemolytic': 311, 'chemotactic': 26, 'anti-bacterial': 2331

    to_remove = []
    all_activities = list(counts.keys())
    for activity in all_activities:
        if counts[activity] < 50:
            to_remove.append(activity)

    valid_activities = [item for item in all_activities if item not in to_remove]

    cleaned_data['apd_activity'] = cleaned_data['apd_activity'].apply(
        lambda x: list(filter(lambda y: y in valid_activities, x)))

    cleaned_data = cleaned_data[cleaned_data['apd_activity'].apply(lambda x: len(x) > 0)]

    counts = {}
    for activity in unique_apd_activities:
        count = len(cleaned_data['apd_activity'][
                        cleaned_data['apd_activity'].apply(lambda x: x.__contains__(activity)) == True].index)
        counts[activity] = count

    filtered_data = cleaned_data.copy()
    filtered_data['apd_activity'] = filtered_data['apd_activity'].apply(lambda x: ", ".join(x))
    filtered_data.to_csv("data/raw/apd/apd_filtered_7_activities.csv", index=False)

    selected_activites = ['antiparasitic', 'antiviral', 'anticancer', 'antifungal', 'anti-bacterial']

    top_5_activites = cleaned_data.copy()
    top_5_activites['apd_activity'] = top_5_activites['apd_activity'].apply(
        lambda x: list(filter(lambda y: y in selected_activites, x)))
    top_5_activites = top_5_activites[top_5_activites['apd_activity'].apply(lambda x: len(x) > 0)]

    top_5_activites['apd_activity'] = top_5_activites['apd_activity'].apply(lambda x: ", ".join(x))
    top_5_activites.to_csv("data/raw/apd/apd_filtered_5_activities.csv", index=False)

    activities = pd.read_csv("data/raw/apd/apd_filtered_5_activities.csv")

    activities['Sequences'].to_csv("data/raw/apd/plain_sequences", index=False, header=False)

    #top 4 activities
    selected_activites = ['antiviral', 'anticancer', 'antifungal', 'anti-bacterial']

    top_4_activites = cleaned_data.copy()
    top_4_activites['apd_activity'] = top_4_activites['apd_activity'].apply(
        lambda x: list(filter(lambda y: y in selected_activites, x)))
    top_4_activites = top_4_activites[top_4_activites['apd_activity'].apply(lambda x: len(x) > 0)]

    top_4_activites['apd_activity'] = top_4_activites['apd_activity'].apply(lambda x: ", ".join(x))
    top_4_activites.to_csv("data/raw/apd/apd_filtered_4_activities.csv", index=False)

    activities = pd.read_csv("data/raw/apd/apd_filtered_4_activities.csv")

    activities['Sequences'].to_csv("data/raw/apd/plain_sequences_top_4", index=False, header=False)

    # top 3 activities
    # selected_activites = ['anti-bacterial', 'antifungal', 'anticancer']
    #
    # top_3_activites = cleaned_data.copy()
    # top_3_activites['apd_activity'] = top_3_activites['apd_activity'].apply(
    #     lambda x: list(filter(lambda y: y in selected_activites, x)))
    # top_3_activites = top_3_activites[top_3_activites['apd_activity'].apply(lambda x: len(x) > 0)]
    #
    # top_3_activites['apd_activity'] = top_3_activites['apd_activity'].apply(lambda x: ", ".join(x))
    # top_3_activites.to_csv("data/raw/apd/apd_filtered_3_activities.csv", index=False)

    # create tripeptide sequences
    train_data = pd.read_csv('data/raw/apd/plain_sequences_top_4', header=None)
    train_data.columns = ['Sequence']

    gaps = [1, 2]
    functions = {
        'tripeptide': reduce_with_gap_tripeptide_frequency,
        'dipeptide': reduce_with_gap_dipeptide_frequency
    }

    for gap in gaps:
        for peptide_type in list(functions.keys()):
            aa = functions[peptide_type](train_data, gap)
            aa.to_csv(f"./data/multilabel/apd_pfeatures_4/{peptide_type}_{gap}.csv", index=False)

# 'anti-sepsis': 75, 'anti-protists': 4, 'anti-mrsa': 167, 'anti-gram+': 532, 'wound healing': 24, 'antimalarial': 33,
# 'spermicidal': 13, 'antiparasitic': 121, 'anti-tb': 13, 'anti-toxin': 15, 'chemotactic': 61, 'anti-inflammatory': 20,
# 'channel inhibitors': 7, 'anticancer': 237, 'antioxidant': 23, 'antibiofilm': 58, 'enzyme inhibitor': 30,
# 'anti-gram-': 307, 'anti-hiv': 109, 'anti-gram+ & gram-': 1833, 'insecticidal': 39, 'hemolytic': 317,
# 'antiviral': 189, 'anti-diabetes': 15, 'antifungal': 1141

# 'anti-mrsa': 167, 'antiparasitic': 121, 'anti-toxin': 15, 'spermicidal': 13, 'antiviral': 189, 'insecticidal': 39,
# 'anti-sepsis': 75, 'antimalarial': 33, 'enzyme inhibitor': 30, 'wound healing': 24, 'channel inhibitors': 7,
# 'anti-tb': 13, 'anti-protists': 4, 'anti-diabetes': 15, 'antibiofilm': 58, 'anti-hiv': 109, 'anticancer': 237,
# 'antioxidant': 23, 'antifungal': 1141, 'anti-inflammatory': 20, 'hemolytic': 317, 'chemotactic': 61,
# 'anti-bacterial': 2672


# 'anti-mrsa': 167, 'antiparasitic': 121, 'anti-toxin': 15, 'spermicidal': 13, 'antiviral': 189, 'insecticidal': 39,
# 'anti-sepsis': 75, 'antimalarial': 33, 'enzyme inhibitor': 30, 'wound healing': 24, 'channel inhibitors': 7,
# 'anti-tb': 13, 'anti-protists': 4, 'anti-diabetes': 15, 'anti-hiv': 109, 'anticancer': 237, 'antioxidant': 23,
# 'antifungal': 1141, 'anti-inflammatory': 20, 'hemolytic': 317, 'chemotactic': 61, 'anti-bacterial': 2677
