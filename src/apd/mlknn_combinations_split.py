import argparse
from itertools import combinations
from warnings import simplefilter

import biovec
import dask_ml.model_selection as dcv
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from skmultilearn.adapt import MLkNN

from src.classification.utils import calculate_scores
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import reduce_amp
from src.files.utils import trim_all, replace_brackets, save_json
from src.random_search.random_forest_search_cv import create_mlknn_grid

# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)


def unique_combinations(keys):
    comb = []
    for n in range(1, len(keys) + 1):
        comb = comb + list(map(list, combinations(keys, n)))
    return comb


def get_variables(args):
    return args.root, args.cores, args.scheduler


def arg_parser():
    root = "./"
    cores = 10
    scheduler = "multiprocessing"

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    parser.add_argument('--scheduler',
                        help="either 'threading', 'multiprocessing', or 'synchronous'", default=scheduler)
    return parser.parse_args()


if __name__ == "__main__":
    root, cores, scheduler = get_variables(arg_parser())
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"

    available_p_features = {
        "amino_acid_compositions": "amino_acid_compositions.csv",
        "auto_correlations": "auto_correlations.csv",
        "cetd": "cetd.csv",
        "DDOR": "DDOR.csv",
        "dipeptide_1": "dipeptide_1.csv",
        "dipeptide_2": "dipeptide_2.csv",
        "physico": "physico.csv",
        "residue_repeats": "residue_repeats.csv",
        "secondary_structure": "secondary_structure.csv",
        "shannon_entropy": "shannon_entropy.csv",
        "tripeptide_1": "tripeptide_1.csv",
        "tripeptide_2": "tripeptide_2.csv"
    }

    to_remove = ['ID', 'Sequence', 'aaindex', 'index']


    def read_data(file):
        data = pd.read_csv(root + "data/multilabel/apd_pfeatures/" + file, index_col=False)
        columns = list(filter(lambda x: to_remove.__contains__(x), list(data.columns)))
        return data.drop(columns=columns).fillna(0)


    p_features = {}
    all_p_features = list(available_p_features.keys())
    for key in all_p_features:
        p_features[key] = {}
        p_features[key] = read_data(available_p_features[key])

    embeddings = {}
    all_vectors = {}
    all_errors = {}

    data = pd.read_csv(root + "data/raw/apd/apd_filtered_5_activities.csv")
    y_raw = data['apd_activity']
    y_raw = y_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))

    reduction_keys = ['blossom_matrix_8', 'contact_energies', 'None']
    print("reducing data")
    reduced_train_data = reduce_amp(data, reduction_keys)

    print("Reading embeddings")
    for red in reduction_keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    print("Creating vectors")
    for red in reduction_keys:
        if not all_vectors.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_train_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors[red] = inputs
            all_errors[red] = errors

    for key in reduction_keys:
        p_features[key] = {}
        p_features[key] = all_vectors[key].reset_index().drop(['index'], axis='columns')

    all_keys = all_p_features + reduction_keys

    print("total keys", len(all_keys))

    mlb = preprocessing.MultiLabelBinarizer()

    scores = {}
    # top_combinations = [
    #     ["dipeptide_2", "secondary_structure"],
    #     ["dipeptide_1", "dipeptide_2", "physico", "secondary_structure"],
    #     ["dipeptide_1", "dipeptide_2", "physico", "residue_repeats", "secondary_structure"],
    #     ["dipeptide_1", "dipeptide_2"],
    #     ["DDOR", "dipeptide_1", "dipeptide_2"],
    #     ["amino_acid_compositions", "DDOR", "dipeptide_1", "dipeptide_2", "residue_repeats"],
    #     ["DDOR", "dipeptide_1", "dipeptide_2", "physico", "secondary_structure"],
    #     ["dipeptide_1", "dipeptide_2", "residue_repeats", "secondary_structure"],
    #     ["dipeptide_1", "dipeptide_2", "secondary_structure"],
    #     ["amino_acid_compositions", "DDOR", "dipeptide_1", "dipeptide_2", "residue_repeats", "secondary_structure"]
    # ]

    combs = unique_combinations(all_keys)
    # combs = top_combinations
    print("Total combinations")
    print(len(combs))

    for i, keys in enumerate(combs):
        print(f"{i} : {keys}")
        x = pd.DataFrame()
        for key in keys:
            x = pd.concat([x, p_features[key]], axis=1)

        y = mlb.fit_transform(y_raw)

        for state in range(1, 2):
            X_train, X_test, y_train, y_test = train_test_split(x, y, random_state=1, test_size=1 / 3)

            classifier = MLkNN()
            parameters = create_mlknn_grid()
            clf = dcv.GridSearchCV(
                classifier,
                parameters,
                scoring='f1_macro',
                n_jobs=cores,
                scheduler=scheduler
            )

            clf.fit(X_train, y_train)

            best_model = clf.best_estimator_
            predicted = best_model.predict(X_test)

            keys_join = '-'.join(keys)
            combination = f"{keys_join}-{state}"
            scores[combination] = calculate_scores(y_test, predicted, method='multilabel')
            scores[combination]["random_state"] = state
            scores[combination]["best_params"] = clf.best_params_
            print(scores[combination])

    print(scores)
    save_json(root + "data/results/mlkn_p_features_grid_search.json", scores)
