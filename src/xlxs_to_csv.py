import os

import pandas as pd

from src.files.amp import amp_positive_data_activities, filter_unwanted_activities
from src.files.utils import trim_all


def filter_data_for_selected_activities(data, activities):
    positive_data_filter = data.apply(lambda row: any(activity in activities for activity in row[2]), axis=1)
    positive_data_filtered = data[positive_data_filter].reset_index().drop('index', axis=1)
    positive_data_filtered.loc[:, 'Activity'] = positive_data_filtered['Activity'].apply(
        lambda x: filter_unwanted_activities(x, amp_positive_data_activities)
    )
    return positive_data_filtered


if __name__ == "__main__":

    ROOT_DIR = "./"
    directory = ROOT_DIR + "data/raw/camp3/"
    file_patterns = ['CAMP_2021-01']
    for file_pattern in file_patterns:
        file_out = directory + file_pattern + ".txt"
        data = pd.DataFrame()
        for root, subdirs, files in os.walk(directory):
            print(len(list(filter(lambda x: file_pattern in x, files))))
            for file in files:
                if file_pattern in file:
                    data = pd.concat([data, pd.read_csv(directory + file, sep='\t')])

        data.to_csv(directory + "camp.csv", index=False)

    camp3 = data[data['Validation'].apply(lambda r: r == 'Experimentally Validated')].reset_index()

    camp3 = camp3.drop_duplicates('Seqence').reset_index()
    camp3.to_csv(directory + "camp_experimentally_vallidated.csv", index=False)

    common_camp3 = pd.DataFrame()
    common_camp3['id'] = camp3['  Camp_ID']
    common_camp3['Sequence'] = camp3['Seqence']
    common_camp3['Activity'] = camp3['Activity']
    common_camp3.to_csv(directory + "camp3_experimentally_vallidated_common.csv", index=False)

    campold_file = directory + "CAMp_old.csv"
    campold = pd.read_csv(campold_file, sep=',')
    campold = campold[campold['Validation'].apply(lambda r: r == 'Experimentally Validated')].reset_index()
    campold = campold.drop_duplicates('Seqence').reset_index()

    dbamp_file = ROOT_DIR + "data/raw/dbampvalidated.csv"
    dbamp = pd.read_csv(dbamp_file, sep=',')

    dbamp = dbamp.drop_duplicates('Sequence').reset_index()

    common_dbamp = pd.DataFrame()
    common_dbamp['id'] = dbamp['dbAMP_ID']
    common_dbamp['Sequence'] = dbamp['Sequence']
    common_dbamp['Activity'] = dbamp['Activity']
    common_dbamp.to_csv(ROOT_DIR + "data/raw/dbampvalidated_common.csv", index=False)

    drampold_file = ROOT_DIR + "data/raw/dramp/DRAMP_old.csv"
    drampold = pd.read_csv(drampold_file, sep=',')
    drampold = drampold.drop_duplicates('Sequence').reset_index()
    drampold.columns
    drampold = drampold[drampold['Source_Organism'].apply(lambda r: r == 'Synthetic construct')].reset_index()


    common_drampold = pd.DataFrame()
    common_drampold['id'] = drampold['DRAMP_ID']
    common_drampold['Sequence'] = drampold['Sequence']
    common_drampold['Activity'] = drampold['Bioactivity']
    common_drampold['Activity'] = common_drampold['Activity'].apply(lambda x: trim_all(str(x).split(',')))
    common_drampold_selected_activities = filter_data_for_selected_activities(common_drampold, amp_positive_data_activities)

    directory = ROOT_DIR + "data/raw/dramptsvs/"
    file_patterns = ['DRAMP_']
    for file_pattern in file_patterns:
        file_out = directory + file_pattern + ".tsv"
        dramp = pd.DataFrame()
        for root, subdirs, files in os.walk(directory):
            print(len(list(filter(lambda x: file_pattern in x, files))))
            for file in files:
                if file_pattern in file:
                    dramp = pd.concat([dramp, pd.read_csv(directory + file, sep='\t')])

        dramp.to_csv(directory + "camp.csv", index=False)

    dramp = dramp.drop_duplicates('Sequence').reset_index()

    dramp.to_csv('./data/tmp.csv')
    common_dramp = pd.DataFrame()
    common_dramp['id'] = dramp['DRAMP_ID']
    common_dramp['Sequence'] = dramp['Sequence']
    common_dramp['Activity'] = dramp['Activity']
    common_dramp['Activity'] = common_dramp['Activity'].apply(lambda x: trim_all(str(x).split(',')))

    dramp_selected_activities = filter_data_for_selected_activities(common_dramp, amp_positive_data_activities)

    dramp_file = ROOT_DIR + "data/raw/DRAMP_Antimicrobial_amps.tsv"
    dramp = pd.read_csv(dramp_file, sep='\t')

    dramp = dramp.drop_duplicates('Sequence').reset_index()

    common_dramp = pd.DataFrame()
    common_dramp['id'] = dramp['DRAMP_ID']
    common_dramp['Sequence'] = dramp['Sequence']
    common_dramp['Activity'] = dramp['Activity']
    common_dramp.to_csv(ROOT_DIR + "data/raw/dramp_common.csv", index=False)

    apd_file = ROOT_DIR + "data/raw/apd/APD.tsv"
    apd = pd.read_csv(dramp_file, sep='\t')

    common = pd.concat([common_dramp, common_dbamp, common_camp3, apd])

    common = common.drop_duplicates('Sequence').reindex()
    common.to_csv('./data/raw/common.csv')

    common['Activity'] = common['Activity'].apply(lambda x: trim_all(str(x).split(',')))

    common_data_limited_activites = filter_data_for_selected_activities(common, amp_positive_data_activities)

    common_data_limited_activites.to_csv("./data/raw/common_data_limited_activites.csv")
