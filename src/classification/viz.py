import itertools
import os

import numpy as np
import pandas as pd

from src.files.utils import load_json


def plot_feature_importance(columns, feature_importances):
    feats = {}
    for feature, importance in zip(columns, feature_importances):
        feats[feature] = importance  # add the name/value pair
    all_feature_importance = pd.DataFrame.from_dict(feats, orient='index').rename(columns={0: 'Gini-importance'})
    all_feature_importance.plot(kind='bar', rot=45)


if __name__ == "__main__":
    ROOT_DIR = "/Users/in-digvijay.gunjal/e4r/"
    directory = ROOT_DIR + "life_sciences/data/results/"
    file_pattern = "rfc"
    file_out = directory + "/rfc_amp_tw_both_binary_kmer.csv"

    data = pd.DataFrame()
    for root, subdirs, files in os.walk(directory):
        print(len(list(filter(lambda x: file_pattern in x, files))))
        print(files)
        for file in files:
            if file_pattern in file:
                data = pd.concat([data, (pd.json_normalize(load_json(root + file)))])

    # data[:15].plot(kind='bar', rot=45)

    reductions = np.unique(list(itertools.chain(*data['reduction'].values)))
    result = {}
    for r in reductions:
        result[r] = []

    for row in data.values:
        for red in row[8]:
            result[red].append(row[0])

    for key in result.keys():
        scores = list(map(lambda x: x * 100, result[key]))
        result[key] = sum(scores) / len(scores)

    pic = pd.DataFrame.from_dict(result, orient='index')
    pic.plot(kind='bar', rot=45)
    data = data.sort_values('hamming_loss')
    data.to_csv(file_out, index=False)
