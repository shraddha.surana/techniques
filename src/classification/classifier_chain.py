from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.multioutput import ClassifierChain
from tqdm import tqdm

from src.classification.utils import calculate_scores, get_best_result, add_properties_to_result
from src.embeddings.utils import join_all_vectors


def classifier_chain(state, all_inputs, target, cores):
    x_train, x_test, y_train, y_test = train_test_split(all_inputs, target, random_state=state)

    rfc = RandomForestClassifier(n_jobs=cores)
    model = ClassifierChain(rfc, random_state=state)
    model.fit(x_train, y_train)
    y_pred = model.predict(x_test)

    return calculate_scores(y_test, y_pred, "multilabel"), model


def classifier_chain_for_various_states(states, values, to_predict, cores=-1):
    mapping = {}
    for state in tqdm(states, desc="Random Forest Classifiers", unit="model"):
        scores, classifier = classifier_chain(state, values, to_predict, cores)
        mapping[state] = (scores, classifier)

    return mapping


def classify_combination(all_reductions, all_vectors, random_states, target, cores):
    all_inputs = join_all_vectors(all_reductions, all_vectors)

    results = classifier_chain_for_various_states(
        states=random_states,
        values=all_inputs.values,
        to_predict=target,
        cores=cores
    )
    return get_best_result(results)


def run_classifier_chain_combination_of_reductions(all_reductions, all_vectors, random_states, target, cores, kmer,
                                                   context_size, vector_size):
    result = classify_combination(all_reductions, all_vectors, random_states, target, cores)
    params = result[1].get_params()
    params['base_estimator'] = params['base_estimator'].get_params()
    return add_properties_to_result(kmer, context_size, vector_size, all_reductions, result[0], params)
