import itertools
from collections import Counter

import numpy as np
import pandas as pd
from imblearn.over_sampling import SMOTE
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from tqdm import tqdm
import pickle
from src.classification.utils import classify_and_predict
from src.classification.utils import calculate_scores, add_properties_to_result, get_best_result


def rm_classifier_for_various_states(states, values, to_predict, classifier=None, cores=-1, method=None,
                                     handleDataImbalance=False):
    mapping = {}
    for state in tqdm(states, desc="Random Forest Classifiers", unit="model"):
        x_train, x_test, y_train, y_test = train_test_split(values, to_predict, random_state=state)
        classifier = RandomForestClassifier(n_jobs=cores)

        if handleDataImbalance and method == 'multiclass':
            print('Original dataset shape %s' % Counter(y_train))
            sm = SMOTE(random_state=1)
            x_train, y_train = sm.fit_resample(np.array(x_train), y_train)
            print('Resampled dataset shape %s' % Counter(y_train))

        if handleDataImbalance and method == 'multilabel':
            print('Original dataset shape %s' % Counter(list(itertools.chain(*y_train))))
            sm = SMOTE(random_state=1)
            x_train, y_train = sm.fit_resample(np.array(x_train), list(itertools.chain(*y_train)))

        if handleDataImbalance and method == 'binary':
            print('Original dataset shape %s' % Counter(y_train))
            sm = SMOTE(random_state=1)
            x_train, y_train = sm.fit_resample(np.array(x_train), y_train)
            print('Resampled dataset shape %s' % Counter(y_train))

        predicted, model = classify_and_predict(x_train, x_test, y_train, classifier)
        scores = calculate_scores(y_test, predicted, method=method)
        mapping[state] = (scores, classifier)

    return mapping


def rm_classifier_for_various_states_binary(states, x_train, x_test, y_train, y_test, cores=-1, method=None,
                                            handleDataImbalance=False):
    mapping = {}
    for state in tqdm(states, desc="Random Forest Classifiers", unit="model"):
        classifier = RandomForestClassifier(n_jobs=cores)

        if handleDataImbalance and method == 'binary':
            print('Original dataset shape %s' % Counter(y_train))
            sm = SMOTE(random_state=1)
            x_train, y_train = sm.fit_resample(np.array(x_train), y_train)
            print('Resampled dataset shape %s' % Counter(y_train))

        predicted, model = classify_and_predict(x_train, x_test, y_train, classifier)
        pickle.dump(model, open('./data/rfc_model', 'wb'))
        scores = calculate_scores(y_test, predicted, method=method)
        mapping[state] = (scores, classifier)

    return mapping


def classify_combination(all_reductions, all_vectors, random_states, target, cores, handleDataImbalance=False,
                         method=None):
    all_inputs = pd.DataFrame()
    for red in all_reductions:
        all_inputs = pd.concat([all_inputs, all_vectors[red]], axis=1)

    results = rm_classifier_for_various_states(
        states=random_states,
        values=all_inputs.values,
        to_predict=target,
        classifier=None,  # classifier = RandomForestClassifier()
        cores=cores,
        method=method,
        handleDataImbalance=handleDataImbalance
    )
    return get_best_result(results)


def run_combination_of_reductions(all_reductions, all_vectors, random_states, target, cores, kmer, context_size,
                                  vector_size, handleDataImbalance=False, method=None):
    result = classify_combination(all_reductions, all_vectors, random_states, target, cores, handleDataImbalance,
                                  method)
    return add_properties_to_result(kmer, context_size, vector_size, all_reductions, result[0], result[1].get_params())
