import pandas as pd
from sklearn.metrics import accuracy_score, hamming_loss, f1_score, matthews_corrcoef, roc_auc_score


def calculate_scores(actual, predicted, method=None):
    if method == 'binary':
        return {
            'accuracy_score': (accuracy_score(actual, predicted)),
            'hamming_loss': (hamming_loss(actual, predicted)),
            'mcc': (matthews_corrcoef(actual, predicted)),
            'f1_score': (f1_score(actual, predicted)),
            'roc_auc': (roc_auc_score(actual, predicted))
        }
    else:
        return {
            'accuracy_score': (accuracy_score(actual, predicted)),
            'hamming_loss': (hamming_loss(actual, predicted)),
            'f1_score_micro': (f1_score(actual, predicted, average='micro')),
            'f1_score_macro': (f1_score(actual, predicted, average='macro'))
        }


def get_best_result(all_scores):
    best_score = None
    best_score_key = None
    for key in all_scores.keys():
        current_score = all_scores[key][0]['hamming_loss']
        if best_score is None or current_score <= best_score:
            best_score = current_score
            best_score_key = key
    best_score = all_scores[best_score_key]
    best_score[0]['random_state'] = best_score_key
    return best_score


def add_properties_to_result(kmer, context_size, vector_size, reduction, score, model_config):
    score['kmer'] = kmer
    score['context_size'] = context_size
    score['vector_size'] = vector_size
    score['reduction'] = reduction
    score['model'] = model_config
    return score


def plot_feature_importance(columns, feature_importances):
    feats = {}
    for feature, importance in zip(columns, feature_importances):
        feats[feature] = importance  # add the name/value pair
    all_feature_importance = pd.DataFrame.from_dict(feats, orient='index').rename(columns={0: 'Gini-importance'})
    all_feature_importance.plot(kind='bar', rot=45)


def classify_and_predict(x_train, x_test, y_train, model):
    model.fit(x_train, y_train)
    y_predicted = model.predict(x_test)
    return y_predicted, model
