import itertools
from collections import Counter

import keras
import numpy as np
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split
from tqdm import tqdm

from src.classification.utils import calculate_scores

from keras.utils.vis_utils import plot_model

def dnn_train_and_predict(classifier, X_train, X_test, y_train, batch_size=64, epochs=50, verbose=0,
                          validation_split=0.2):
    classifier.compile(
        optimizer=keras.optimizers.Adam(1e-3),
        loss="binary_crossentropy",
        metrics=["accuracy"],
    )
    classifier.fit(x=X_train, y=y_train, batch_size=batch_size, epochs=epochs, verbose=verbose,
                   validation_split=validation_split)
    predicted = classifier.predict(X_test)
    predicted = list(np.round(predicted))
    predicted = list(map(lambda row: list(map(int, row)), predicted))
    # classifier.save("./data/dnn_binary_filtered")
    return predicted, classifier


def dnn_classifier_for_various_states(states, values, to_predict, classfier, method=None, handleDataImbalance=False):
    mapping = {}
    print(f"states: {states}")
    for state in tqdm(states, desc="DNN Classifiers", unit="model"):
        X_train, X_test, y_train, y_test = train_test_split(values, to_predict, random_state=state)

        if handleDataImbalance and method == 'multiclass':
            print('Original dataset shape %s' % Counter(list(itertools.chain(*y_train))))
            sm = SMOTE(random_state=1)
            X_train, y_train = sm.fit_resample(np.array(X_train), list(itertools.chain(*y_train)))
            print('Resampled dataset shape %s' % Counter(y_train))

        if handleDataImbalance and method == 'multilabel':
            print('Original dataset shape %s' % Counter(list(itertools.chain(*y_train))))
            sm = SMOTE(random_state=1)
            x_train, y_train = sm.fit_resample(np.array(X_train), list(itertools.chain(*y_train)))
            print('Resampled dataset shape %s' % Counter(y_train))

        if handleDataImbalance and method == 'binary':
            print('Original dataset shape %s' % Counter(y_train))
            sm = SMOTE(random_state=1)
            x_train, y_train = sm.fit_resample(np.array(X_train), y_train)
            print('Resampled dataset shape %s' % Counter(y_train))

        predicted, classfier = dnn_train_and_predict(classfier, X_train, X_test, y_train, batch_size=64, epochs=10,
                                                     verbose=0,
                                                     validation_split=0.2)
        scores = calculate_scores(y_test, predicted, method=method)

        mapping[state] = (scores, classfier)

    return mapping


def dnn_classifier_for_various_states_binary(states, X_train, X_test, y_train, y_test, classfier, method=None,
                                             handleDataImbalance=False):
    mapping = {}
    print(f"states: {states}")
    for state in tqdm(states, desc="DNN Classifiers", unit="model"):
        if handleDataImbalance and method == 'binary':
            print('Original dataset shape %s' % Counter(y_train))
            sm = SMOTE(random_state=1)
            X_train, y_train = sm.fit_resample(np.array(X_train), y_train)
            print('Resampled dataset shape %s' % Counter(y_train))

        predicted, classfier = dnn_train_and_predict(classfier, X_train, X_test, y_train, batch_size=64, epochs=10,
                                                     verbose=0,
                                                     validation_split=0.2)
        scores = calculate_scores(y_test, predicted, method=method)
        plot_model(classfier, to_file='dnn.png', show_shapes=True, show_layer_names=True)
        # classfier.save('dnn_binary_unfiltered_' + str(state))
        mapping[state] = (scores, classfier)

    return mapping
