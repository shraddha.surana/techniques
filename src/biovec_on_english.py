import biovec
from gensim.models import word2vec

corpus = word2vec.Text8Corpus('eng_sample.txt')
eng = biovec.models.ProtVec(corpus=corpus, corpus_fname="eng_corpous_output.txt", n=5)

print(eng['the'].shape)
print(eng['the'])

eng.to_vecs("the")
eng.to_vecs("is")
eng.to_vecs("Sherlock")
eng.to_vecs("Holmes")

# why is it trimming it to 5? - because it is doing split grams first.. had trained it with n=5.
# Although this n=5 was not used in training, it is used in the 'prediction'.
# It is cutting the incoming sequence into n-mers.
# SO maybe I can set n arbitarily large for the english set vtraining. Nothing works when it is set to 50 :|


eng.to_vecs("woman")
eng.to_vecs("Baker")
eng.to_vecs("dark")
eng.to_vecs("most")
eng.to_vecs("precise")
