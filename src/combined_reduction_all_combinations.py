import argparse
import itertools
from itertools import product

import biovec
import numpy as np
import pandas as pd
from sklearn import preprocessing

from src.classification.random_forest import run_combination_of_reductions
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import reduce_amp, filter_data_for_selected_activities, get_positive_amp_data, \
    get_negative_amp_data, amp_positive_data_activities
from src.files.camp import massage_camp_data
from src.files.utils import save_json


def get_variables(args):
    return args.root, args.kmer, args.vector_size, args.context_size, args.embedding_file, \
           args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores, args.only_positive, args.method, args.smote


reduction_groups = [
    ['conformation_similarity', None],
    ['kd_hydrophobicitya', 'hydrophobicitya', 'amino_acid_residues', None],
    ['blossom_matrix_8', 'blossom_matrix_4', 'murphy_5', None],
    ['contact_energies', 'mj_4', 'mj_5', None],
    ['None', None]
]


def get_all_combinations(groups):
    # top 5 combinations
    # return [
    #     ['amino_acid_residues', 'blossom_matrix_4', 'contact_energies', 'None'],
    #     ['blossom_matrix_8', 'contact_energies', 'None'],
    #     ['blossom_matrix_4', 'contact_energies'],
    #     ['blossom_matrix_8', 'contact_energies'],
    #     ['blossom_matrix_4', 'contact_energies', 'None'],
    #     ['conformation_similarity', 'contact_energies', 'None', 'blossom_matrix_4', 'hydrophobicitya'],
    #     ['conformation_similarity'],
    #     ['kd_hydrophobicitya'],
    #     ['blossom_matrix_8'],
    #     ['blossom_matrix_4'],
    #     ['hydrophobicitya'],
    #     ['contact_energies'],
    #     ['amino_acid_residues'],
    #     ['murphy_5'],
    #     ['mj_4'],
    #     ['mj_5'],
    #     ['None']
    # ]

    return list(
        filter(
            lambda c: len(c) != 0,
            map(
                lambda c: list(filter(lambda r: r is not None, c)),
                product(*groups)
            )
        )
    )


def chunks(reductions, size):
    for i in range(0, len(reductions), size):
        yield reductions[i:i + size]


def multiclass_for_all_activitites(data):
    mlb = preprocessing.MultiLabelBinarizer()
    return pd.DataFrame(list(map(lambda x: "".join(map(str, x)), mlb.fit_transform(data))))


def multilabel(data):
    mlb = preprocessing.MultiLabelBinarizer()
    return mlb.fit_transform(data)


def get_target_data(classification_type, data):
    if classification_type == 'binary':
        return data.apply(lambda x: 0 if x == ['none'] else 1)
    if classification_type == 'multilabel':
        return multilabel(data)
    if classification_type == 'multiclass':
        return multiclass_for_all_activitites(data)


def arg_parser():
    root = "./"
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/rfc"
    cores = 10
    only_positive = False
    method = 'binary'
    smote = False

    negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--embedding-file', help='Embedding file path', default=embedding_file)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    parser.add_argument('--only-positive', help='consider only positive data. Default False', default=only_positive)
    parser.add_argument('--method', help='method (Binary / multiclass / Multilabel, Default Multilabel)',
                        default=method)
    parser.add_argument('--smote', help='Use smote for data imbalance', default=smote)
    return parser.parse_args()


if __name__ == "__main__":
    root, kmer, vector_size, context_size, embedding_file, output_dir, negative_data_file, positive_data_file, cores, only_positive, method, smote = get_variables(
        arg_parser())

    all_reductions_combinations = get_all_combinations(reduction_groups)

    embeddings = {}
    all_vectors = {}
    all_errors = {}
    random_states = list(range(1, 2, 1))

    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    AMP_data = positive_data
    if method == 'multilabel':
        positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
        AMP_data = positive_data_filtered

    a = list(itertools.chain(*positive_data.Activity.values))
    unique, counts = np.unique(a, return_counts=True)
    b = dict(zip(unique, counts))
    if not only_positive:
        raw_negative_data = get_negative_amp_data(root + negative_data_file)
        negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
        AMP_data = pd.concat([AMP_data, negative_data])

    if method == "multiclass":
        activity_column = list(map(sorted, AMP_data.Activity))
        unique, counts = np.unique(list(map(lambda x: " ".join(x), activity_column)), return_counts=True)
        classes_counts = dict(zip(unique, counts))
        valid_classes = list(filter(lambda k: classes_counts[k] >= 99, classes_counts))
        multiclass_filter = AMP_data.apply(lambda row: " ".join(row[1]) in valid_classes, axis=1)
        AMP_data = AMP_data[multiclass_filter].reset_index().drop('index', axis=1)

    scores = []
    keys = list(np.unique(list(itertools.chain(*all_reductions_combinations))))
    # keys = ['hydrophobicitya', 'blossom_matrix_4', 'contact_energies', 'None']
    # keys = ['conformation_similarity', 'kd_hydrophobicitya', 'mj_4', 'None']
    # keys = ['blossom_matrix_8', 'contact_energies', 'None']
    reduced_data = reduce_amp(AMP_data, keys)

    target = get_target_data(method, reduced_data.classification)

    for red in keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    for red in keys:
        if not all_vectors.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors[red] = inputs
            all_errors[red] = errors

    # all_inputs = pd.DataFrame()
    # for red in keys:
    #     all_inputs = pd.concat([all_inputs, all_vectors[red]], axis=1)
    #
    # x_train, x_test, y_train, y_test = train_test_split(all_inputs, target, random_state=1)
    # print('Original dataset shape %s' % Counter(y_train))
    # sm = SMOTE(random_state=1)
    # x_train, y_train = sm.fit_resample(np.array(x_train), y_train)
    # print('Resampled dataset shape %s' % Counter(y_train))

    result = list(map(
        lambda all_reductions: run_combination_of_reductions(
            all_reductions, all_vectors, random_states,
            target, cores, kmer,
            context_size, vector_size, smote, method=method
        ),
        all_reductions_combinations
    ))

    # for reduction_chunks in chunks(all_reductions_combinations, 5):
    #     print(reduction_chunks)
    #     result = delayed(list)(map(
    #         lambda all_reductions: delayed(run_combination_of_reductions)(
    #             all_reductions, all_vectors, random_states,
    #             target, cores, kmer,
    #             context_size, vector_size
    #         ),
    #         reduction_chunks
    #     )).compute()
    #     print(result)
    #     scores.append(result)

    file_name = create_file_name_using_parameters(
        root + output_dir,
        "all_combinations_" + method,
        kmer,
        context_size,
        vector_size
    )

    save_json(file_name + ".json", result)
