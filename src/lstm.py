import argparse
import itertools
from collections import Counter

import biovec
import numpy as np
import pandas as pd
from imblearn.over_sampling import SMOTE
from keras.callbacks import EarlyStopping
from keras.initializers import glorot_normal
from keras.layers import Dense, Embedding, SpatialDropout1D, LSTM, Bidirectional
from keras.models import Sequential
from keras.utils import np_utils
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tqdm import tqdm

from src.classification.utils import calculate_scores
from src.classification.utils import get_best_result, add_properties_to_result
from src.combined_reduction_all_combinations import get_all_combinations, reduction_groups
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import filter_data_for_selected_activities, get_positive_amp_data, get_negative_amp_data, \
    amp_positive_data_activities, reduce_amp
from src.files.camp import massage_camp_data
from src.files.utils import save_json

# The maximum number of words to be used. (most frequent)
MAX_NB_WORDS = 400
# Max number of words in each complaint.
MAX_SEQUENCE_LENGTH = 3
# This is fixed.
EMBEDDING_DIM = 100


vocab_size = 5000
embedding_dim = 64
max_length = 200
trunc_type = 'post'
padding_type = 'post'
oov_tok = '<OOV>'
training_portion = .8


def get_model(input_shape, output_size):
    model = Sequential()
    model.add(Embedding(MAX_NB_WORDS, EMBEDDING_DIM, input_length=input_shape))
    model.add(LSTM(800, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(13, activation='softmax'))
    model.add(
        Dense(output_size, activation="tanh", kernel_initializer=glorot_normal(), bias_initializer=glorot_normal()))
    return model


def get_variables(args):
    return args.root, args.kmer, args.vector_size, args.context_size, args.embedding_file, \
           args.output_dir, args.negative_data_file, \
           args.positive_data_file, args.cores, args.only_positive, args.method, args.smote


def run_combination_of_reductions(all_reductions, all_vectors, random_states, target, cores, kmer, context_size,
                                  vector_size, model, method, handleDataImbalance):
    result = classify_combination(all_reductions, all_vectors, random_states, target, model, cores, method,
                                  handleDataImbalance)
    config = result[1].get_config()

    return add_properties_to_result(kmer, context_size, vector_size, all_reductions, result[0], config)


def classify_combination(all_reductions, all_vectors, random_states, target, model, cores, method, handleDataImbalance):
    all_inputs = pd.DataFrame()
    for red in all_reductions:
        all_inputs = pd.concat([all_inputs, all_vectors[red]], axis=1)

    target_size = pd.DataFrame(target).shape[1]
    results = dnn_classifier_for_various_states(random_states, all_inputs, target,
                                                model(all_inputs.shape[1], target_size),
                                                method, handleDataImbalance)
    return get_best_result(results)


def arg_parser():
    root = "./"
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/dnn/dnn"
    cores = 10
    only_positive = False
    method = 'binary'
    smote = False

    negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'  # filtered 70% similarity threshold

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--embedding-file', help='Embedding file path', default=embedding_file)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--negative-data-file', help='AMP data, negative filtered', default=negative_data_file)
    parser.add_argument('--positive-data-file', help='AMP data, positive filtered', default=positive_data_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    parser.add_argument('--only-positive', help='Run only for positive data set', default=only_positive)
    parser.add_argument('--method', help='Binary / multiclass / multilabel', default=method)
    parser.add_argument('--smote', help='True or False', default=smote)
    return parser.parse_args()


def multiclass_for_all_activitites(data):
    r = list(map(lambda x: "".join(map(str, x)), data))
    encoder = LabelEncoder()
    encoder.fit(r)
    encoded_Y = encoder.transform(r)
    # convert integers to dummy variables (i.e. one hot encoded)
    return np_utils.to_categorical(encoded_Y)
    # mlb = preprocessing.MultiLabelBinarizer()
    # r = list(map(lambda x: "".join(map(str, x)), mlb.fit_transform(data)))
    # tk = Tokenizer()
    # tk.fit_on_texts(r)
    # index_list = tk.texts_to_sequences(r)
    # return pd.DataFrame(list(itertools.chain(*pad_sequences(index_list, maxlen=1))))


def multilabel(data):
    mlb = preprocessing.MultiLabelBinarizer()
    return mlb.fit_transform(data)


def get_target_data(classification_type, data):
    if classification_type == 'binary':
        return data.apply(lambda x: 0 if x == ['none'] else 1)
    if classification_type == 'multilabel':
        return multilabel(data)
    if classification_type == 'multiclass':
        return multiclass_for_all_activitites(data)


def dnn_train_and_predict(classifier, X_train, X_test, y_train, batch_size=64, epochs=50, verbose=0,
                          validation_split=0.2):
    print("GOmdya ala re..")

    model = Sequential([
        Embedding(vocab_size, embedding_dim, input_length=X_train.shape[1]),
        Bidirectional(LSTM(embedding_dim)),
        Dense(embedding_dim, activation='relu'),
        Dense(1, activation='softmax')
    ])
    model.summary()

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    num_epochs = 10
    # history = model.fit(train_padded, training_label_seq, epochs=num_epochs,
    #                     validation_data=(validation_padded, validation_label_seq), verbose=2)

    print("GOmdya ala re..")
    print(y_train)
    print(y_train.shape)
    model.fit(x=X_train, y=y_train, batch_size=batch_size, epochs=epochs, verbose=verbose,
                   validation_split=validation_split)
    print("GOmdya ala re..")
    predicted = model.predict(X_test)
    print("GOmdya ala re..")
    predicted = list(np.round(predicted))
    predicted = list(map(lambda row: list(map(int, row)), predicted))
    return predicted, model


def dnn_classifier_for_various_states(states, values, to_predict, classfier, method=None, handleDataImbalance=False):
    mapping = {}
    print(f"states: {states}")
    print(values)
    values = values.mask(values < 0, 0)
    print(values)
    for state in tqdm(states, desc="DNN Classifiers", unit="model"):
        X_train, X_test, y_train, y_test = train_test_split(values, to_predict, random_state=state)

        if handleDataImbalance and method == 'multiclass':
            print('Original dataset shape %s' % Counter(list(itertools.chain(*y_train))))
            sm = SMOTE(random_state=1)
            X_train, y_train = sm.fit_resample(np.array(X_train), list(itertools.chain(*y_train)))
            print('Resampled dataset shape %s' % Counter(y_train))

        if handleDataImbalance and method == 'multilabel':
            print('Original dataset shape %s' % Counter(list(itertools.chain(*y_train))))
            sm = SMOTE(random_state=1)
            x_train, y_train = sm.fit_resample(np.array(X_train), list(itertools.chain(*y_train)))
            print('Resampled dataset shape %s' % Counter(y_train))

        if handleDataImbalance and method == 'binary':
            print('Original dataset shape %s' % Counter(y_train))
            sm = SMOTE(random_state=1)
            x_train, y_train = sm.fit_resample(np.array(X_train), y_train)
            print('Resampled dataset shape %s' % Counter(y_train))

        predicted, classfier = dnn_train_and_predict(classfier, X_train, X_test, y_train, batch_size=64, epochs=10,
                                                     verbose=0,
                                                     validation_split=0.2)
        scores = calculate_scores(y_test, predicted, method=method)

        mapping[state] = (scores, classfier)

    return mapping


if __name__ == "__main__":
    root, kmer, vector_size, context_size, embedding_file, output_dir, negative_data_file, positive_data_file, cores, only_positive, method, smote = get_variables(
        arg_parser())

    all_reductions_combinations = get_all_combinations(reduction_groups)

    embeddings = {}
    all_vectors = {}
    all_errors = {}
    random_states = list(range(1, 2, 1))

    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    AMP_data = positive_data
    if method == 'multilabel':
        positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
        AMP_data = positive_data_filtered

    if not only_positive:
        raw_negative_data = get_negative_amp_data(root + negative_data_file)
        negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
        AMP_data = pd.concat([AMP_data, negative_data])

    if method == "multiclass":
        activity_column = list(map(sorted, AMP_data.Activity))
        unique, counts = np.unique(list(map(lambda x: " ".join(x), activity_column)), return_counts=True)
        classes_counts = dict(zip(unique, counts))
        valid_classes = list(filter(lambda k: classes_counts[k] >= 99, classes_counts))
        multiclass_filter = AMP_data.apply(lambda row: " ".join(row[1]) in valid_classes, axis=1)
        AMP_data = AMP_data[multiclass_filter].reset_index().drop('index', axis=1)

    keys = list(np.unique(list(itertools.chain(*all_reductions_combinations))))
    keys = ['None']
    # keys = ['hydrophobicitya', 'blossom_matrix_4', 'contact_energies', 'None']
    reduced_data = reduce_amp(AMP_data, keys)

    target = get_target_data(method, reduced_data.classification)

    for red in keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    for red in keys:
        if not all_vectors.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors[red] = inputs
            all_errors[red] = errors

    # print('Original dataset shape %s' % Counter(list(itertools.chain(*target))))
    # # Original dataset shape Counter({1: 900, 0: 100})
    # sm = SMOTE(random_state=1)
    # X_res, y_res = sm.fit_resample(np.array(all_inputs), list(itertools.chain(*target)))
    # print('Resampled dataset shape %s' % Counter(y_res))
    # # Resampled dataset shape Counter({0: 900, 1: 900})

    # all_inputs = pd.DataFrame()
    # for red in keys:
    #     all_inputs = pd.concat([all_inputs, all_vectors[red]], axis=1)
    #
    # if method == 'multilabel':
    #     sm = SMOTE(random_state=1)
    #     X_train, y_train = sm.fit_resample(np.array(all_inputs), target)
    #     print('Resampled dataset shape %s' % Counter(y_train))

    random_states = list(range(1, 2, 1))

    result = list(map(
        lambda all_reductions: run_combination_of_reductions(
            all_reductions, all_vectors, random_states,
            target, cores, kmer,
            context_size, vector_size, get_model, method, smote
        ),
        [keys]
    ))

    file_name = create_file_name_using_parameters(
        root + output_dir + method,
        "all_combinations",
        kmer,
        context_size,
        vector_size
    )

    save_json(file_name + ".json", result)
