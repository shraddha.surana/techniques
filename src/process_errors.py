import itertools

import pandas as pd
import requests
from Bio import SeqIO

from src.files.utils import trim_all


def get_CAMP_errors_data(raw_data):
    return pd.read_csv(raw_data, na_filter=False)


def search_in_uniport(sequences):
    sequences_present_in_uniprot_success = list()
    sequences_present_in_uniprot_error = list()
    for uniprot_ids in sequences.iloc[:, 1]:
        ids = trim_all(uniprot_ids.split(","))
        for uniprot_id in ids:
            r = requests.get("https://www.uniprot.org/uniprot/" + str(uniprot_id) + ".txt")
            print(uniprot_id)
            print(r.status_code)
            if r.status_code == 200:
                sequences_present_in_uniprot_success.append(uniprot_id)
            else:
                sequences_present_in_uniprot_error.append(uniprot_id)


if __name__ == "__main__":
    ROOT_DIR = "/Users/in-digvijay.gunjal/e4r/life_sciences/techniques/"
    file_in = ROOT_DIR + "../data/uniprot/uniprot_sprot_standard_alphabets.fasta"
    errors_data_path = ROOT_DIR + './data/processed/errors_conformation_similarity_kmer5_context5.csv'
    errors_data_path_with_id = ROOT_DIR + 'data/processed/errors_with_id.csv'
    errors_data_path_without_id = ROOT_DIR + 'data/processed/errors_without_id.csv'

    CAMP_errors_data = get_CAMP_errors_data(errors_data_path)
    prot_id_filter = CAMP_errors_data.apply(lambda r: True if (r[1]) else False, axis=1)

    with_id = CAMP_errors_data[prot_id_filter]
    without_id = CAMP_errors_data[~prot_id_filter]

    with_id.to_csv(errors_data_path_with_id, index=False)
    without_id.to_csv(errors_data_path_without_id, index=False)

    # search_in_uniport(with_id)

    fasta = SeqIO.to_dict(SeqIO.parse(file_in, "fasta"))

    ids = set(itertools.chain.from_iterable(map(lambda r: r.split("|"), fasta.keys())))

    present_in_local_data = list()  # 125
    not_present_in_local_data = list()  # 711
    for prot_id in with_id["UniProt_id"]:  # 836
        if prot_id in ids:
            present_in_local_data.append(prot_id)
        else:
            not_present_in_local_data.append(prot_id)

    present_in_local_data_filter = with_id.apply(lambda r: True if r[1] in present_in_local_data else False, axis=1)

    sequences_present_in_local_data = with_id[present_in_local_data_filter]
    sequences_not_present_in_local_data = with_id[~present_in_local_data_filter]

    sequences_not_present_in_local_data.to_csv("sequences_not_present_in_local_data.csv", index=False)

    sequences_present_in_local_data.to_csv("sequences_present_in_local_data.csv", index=False)
