import os

import pandas as pd

from src.files.utils import load_json

file_patterns = ['rakeld_apd_train_imp-2l_test_pfeatures']

if __name__ == "__main__":
    ROOT_DIR = "./"
    directory = ROOT_DIR + "data/results/rakeld_results/"

    for file_pattern in file_patterns:
        file_out = directory + file_pattern + ".csv"
        data = pd.DataFrame()
        for root, subdirs, files in os.walk(directory):
            print(len(list(filter(lambda x: file_pattern in x, files))))
            for file in files:
                if file_pattern in file:
                    json_data = load_json(root + file)
                    result = []
                    for key in json_data.keys():
                        record = json_data[key]
                        record['combination'] = " ".join(key.split('-'))
                        result.append(record)

                    data = pd.concat([data, (pd.json_normalize(result))])

        # data['reduction'] = data['reduction'].apply(lambda r: " ".join(r))
        # data = data.sort_values('hamming_loss')
        data.to_csv(file_out, index=False)
