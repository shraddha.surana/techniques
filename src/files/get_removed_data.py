import pandas as pd

from src.files.amp import get_positive_amp_data, \
    get_negative_amp_data

root = './'

unfiltered_negative = root + "data/raw/AMP/Uniprot_AntiBP_Antviral_negative_05212020 - Uniprot_AntiBP_Antviral_negative.csv"

raw_neg_data = pd.read_csv(unfiltered_negative)
raw_neg_data['Activity'] = ['None'] * raw_neg_data.shape[0]
raw_neg_data = raw_neg_data[['Sequence', 'Activity']]
filtered_neg_data = get_negative_amp_data(root + "data/raw/AMP/negative_data_filtered.csv")
removed_neg_data = pd.concat([raw_neg_data, filtered_neg_data]).drop_duplicates(keep=False).reset_index(drop=True)
removed_neg_data.to_csv(root + "data/raw/negative_data_removed_cdhit.csv")

raw_pos_data = get_positive_amp_data(root + "data/raw/AMP/positive_data_unfiltered2807.csv")
filtered_pos_data = get_positive_amp_data(root + "data/raw/AMP/positive_data_filtered2807.csv")
removed_pos_data = pd.concat([raw_pos_data, filtered_pos_data]).drop_duplicates(keep=False).reset_index(drop=True)
removed_pos_data.to_csv(root + "data/raw/positive_data_removed_cdhit.csv")
