import itertools
import os

import numpy as np
import pandas as pd

if __name__ == "__main__":
    ROOT_DIR = "./"
    directory = ROOT_DIR + "/results/"
    file_pattern = "csv"
    file_out = directory + "/dnn_amp_tw_both_binary_kmer.csv"

    data = pd.DataFrame()
    for root, subdirs, files in os.walk(directory):
        print(files)
        print(len(list(filter(lambda x: file_pattern in x, files))))
        for file in files:
            if file_pattern in file:
                data = pd.concat([data, (pd.read_csv(root + file)[:15])])

    # Group performance
    a = list(data['reduction'].values)
    unique, counts = np.unique(a, return_counts=True)
    b = dict(zip(unique[:5], counts[:5]))

    # Individual technique
    a = list(itertools.chain(*list(map(lambda x: x.split(" "), data['reduction'].values))))
    unique, counts = np.unique(a, return_counts=True)
    b = dict(zip(unique, counts))

    # kmer, context, vector combination
    a = list(map(lambda x: str(x[6]) + str(x[7]) + str(x[8]), data.values))
    unique, counts = np.unique(a, return_counts=True)
    b = dict(zip(unique[:15], counts[:15]))

    pic = pd.DataFrame.from_dict(b, orient='index').sort_values(0, ascending=False)
    pic.plot(kind='bar', rot=45)

    data = data.reset_index()
    data = data.sort_values('hamming_loss')
    data.to_csv(file_out, index=False)
