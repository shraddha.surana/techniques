import pandas as pd

from src.files.utils import trim_all, contains, OTHER_ALPHABETS


def read_camp_data(raw_data):
    data = pd.read_csv(raw_data, sep="\t", header=0)
    return data[['Sequence', 'Activity']]


def massage_camp_data(data):
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())
    sequences['Activity'] = sequences['Activity'].apply(lambda x: trim_all(str(x).split(',')))
    # sequences['Activity'] = sequences['Activity'].apply(lambda x: x.remove(''))
    sequences = sequences.reset_index().drop('index', axis=1)
    for i in range(sequences.__len__()):
        while '' in sequences.loc[i, 'Activity']:
            sequences.loc[i, 'Activity'].remove('')
    return sequences
