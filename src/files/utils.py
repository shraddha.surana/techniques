import json

OTHER_ALPHABETS = "UOXBZJ"


def contains(other_alphabets, seq):
    for o in str(other_alphabets):
        if o in str(seq):
            return True
    return False


def replace_brackets(x):
    return str(x).replace("[", "").replace("]", "")


def trim_all(strings):
    return list(set(value.strip().strip(',').lower() for value in strings))


def save_json(file, data):
    with open(file, "w") as outfile:
        outfile.write(json.dumps(data, indent=4))


def load_json(file):
    with open(file, "r") as read_file:
        return json.load(read_file)


data_combinations_files = {
    'tw_curated_filtered_positive_negative': {
        'positive': 'positive_data_filtered2807.csv',
        'negative': 'negative_data_filtered.csv',
        'onlyPositive': False
    },
    'tw_curated_filtered_positive_only': {
        'positive': 'positive_data_filtered2807.csv',
        'negative': 'negative_data_filtered.csv',
        'onlyPositive': True
    },
    'tw_curated_unfiltered_positive_negative': {
        'positive': 'positive_data_unfiltered2807.csv',
        'negative': 'negative_data_filtered.csv',
        'onlyPositive': False
    },
    'tw_curated_unfiltered_positive_only': {
        'positive': 'positive_data_unfiltered2807.csv',
        'negative': '',
        'onlyPositive': True
    },
    'tw_curated_unfiltered_positive_only_10_60': {
        'positive': 'amp_curated_positive_unfiltered_sequence_length_10_to_60.csv',
        'negative': '',
        'onlyPositive': True
    },
    'tw_curated_unfiltered_positive_cbbio_negative_10_60': {
        'positive': 'amp_curated_positive_unfiltered_sequence_length_10_to_60.csv',
        'negative': 'amp_CBBio_negative_binary_sequence_length_10_to_60.csv',
        'onlyPositive': False
    },
}

multilabel_data_combinations_files = {
    'tw_curated_filtered_positive_negative': {
        'positive': 'positive_data_filtered2807.csv',
        'negative': 'negative_data_filtered.csv',
        'onlyPositive': False
    },
    'tw_curated_filtered_positive_only': {
        'positive': 'positive_data_filtered2807.csv',
        'negative': 'negative_data_filtered.csv',
        'onlyPositive': True
    },
    'tw_curated_unfiltered_positive_negative': {
        'positive': 'positive_data_unfiltered2807.csv',
        'negative': 'negative_data_filtered.csv',
        'onlyPositive': False
    },
    'tw_curated_unfiltered_positive_only': {
        'positive': 'positive_data_unfiltered2807.csv',
        'negative': '',
        'onlyPositive': True
    },
    'tw_curated_unfiltered_positive_only_10_60': {
        'positive': 'amp_curated_positive_unfiltered_sequence_length_10_to_60.csv',
        'negative': '',
        'onlyPositive': True
    },
    'tw_curated_filtered_positive_only_10_60': {
        'positive': 'amp_curated_positive_filtered_sequence_length_10_to_60.csv',
        'negative': '',
        'onlyPositive': True
    },
    'tw_curated_unfiltered_positive_cbbio_negative_10_60': {
        'positive': 'amp_curated_positive_unfiltered_sequence_length_10_to_60.csv',
        'negative': 'amp_CBBio_negative_binary_sequence_length_10_to_60.csv',
        'onlyPositive': False
    }
}
