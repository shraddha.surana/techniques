import numpy as np
import pandas as pd

from src.embeddings.reduce_dataset import reduce
from src.embeddings.reduction_utils import reduction_mappings
from src.files.camp import massage_camp_data
from src.files.utils import trim_all, replace_brackets, OTHER_ALPHABETS, contains

amp_positive_data_activities = ['antibacterial', 'antifungal', 'antiviral', 'anticancer']

amp_negative_data_activities = ['none']

amp_activities = amp_positive_data_activities + amp_negative_data_activities


def get_negative_amp_data(path):
    # negative_data = pd.read_csv(path, sep=",", header=0).drop(['Header'], axis=1).reset_index().drop('index', axis=1)
    negative_data = pd.read_csv(path, sep=",", header=0).reset_index().drop('index', axis=1)[['Sequence', 'Activity']]
    negative_data['Activity'] = 'None'
    return negative_data


def get_positive_amp_data(path):
    return pd.read_csv(path, sep=",", header=0).reset_index().drop('index', axis=1)[['Sequence', 'Activity']]


def massage_amp_data(data):
    data['Activity'] = data['Activity'].apply(lambda x: trim_all(replace_brackets(x).split(",")))
    data['Activity'] = data['Activity'].apply(
        lambda activities: list(filter(lambda activity: len(activity) > 0, activities)))
    return data


def massage_camp_data(data):
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())
    sequences['Activity'] = sequences['Activity'].apply(lambda x: trim_all(str(x).split(',')))
    # sequences['Activity'] = sequences['Activity'].apply(lambda x: x.remove(''))
    sequences = sequences.reset_index().drop('index', axis=1)
    for i in range(sequences.__len__()):
        while '' in sequences.loc[i, 'Activity']:
            sequences.loc[i, 'Activity'].remove('')
    return sequences


def load_amp_data(root, negative_data_file, positive_data_file):
    raw_negative_data = get_negative_amp_data(root + negative_data_file)
    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
    return pd.concat([negative_data, positive_data_filtered])


def load_data(root, positive_data_file, negative_data_file, only_positive=False):
    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
    data = positive_data_filtered

    if not only_positive:
        raw_negative_data = get_negative_amp_data(root + negative_data_file)
        negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
        data = pd.concat([positive_data_filtered, negative_data])

    return data


def reduce_amp(data, reductions):
    reduced_data = pd.DataFrame()
    for reduction in reductions:
        temp_reduce = reduce(pd.DataFrame(data), reduction_mappings[reduction])
        reduced_data[reduction] = temp_reduce[0]
        reduced_data['classification'] = temp_reduce[1]
    return reduced_data


def get_top_activities(data, total=10):
    massaged_data = massage_amp_data(massage_camp_data(data)).reset_index().drop('index', axis=1)
    concatenate = np.concatenate(np.array(massaged_data['Activity']))
    counts = {}
    for activity in concatenate:
        counts[activity] = (counts[activity] + 1) if counts.__contains__(activity) else 1

    return sorted(counts, key=counts.get, reverse=True)[:total]


def filter_unwanted_activities(activities, correct_activities):
    return list(filter(lambda y: y in correct_activities, activities))


def filter_data_for_selected_activities(data, activities):
    positive_data_filter = data.apply(lambda row: any(activity in activities for activity in row[1]), axis=1)
    positive_data_filtered = data[positive_data_filter].reset_index().drop('index', axis=1)
    positive_data_filtered.loc[:, 'Activity'] = positive_data_filtered['Activity'].apply(
        lambda x: filter_unwanted_activities(x, amp_positive_data_activities)
    )
    return positive_data_filtered


def filter_unwanted_sequences(data):
    positive_data_filter = data.apply(lambda row: not row[0].__contains__('-'), axis=1)
    return data[positive_data_filter].reset_index().drop('index', axis=1)


def filter_unwanted_sequences_by_len(data):
    positive_data_filter = data.apply(lambda row: len(row[0].strip()) > 10, axis=1)
    return data[positive_data_filter].reset_index().drop('index', axis=1)
