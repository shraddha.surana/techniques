import biovec

from src.embeddings.utils import create_file_name_using_parameters


def load_embeddings(reductions, file_path, kmer, context_size, vector_size):
    embeddings = {}
    for r in reductions:
        embeddings[r] = biovec.models.load_protvec(
            create_file_name_using_parameters(file_path, r, kmer, context_size, vector_size)
        )
    return embeddings
