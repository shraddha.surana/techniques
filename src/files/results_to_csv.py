import os

import pandas as pd

from src.files.utils import load_json

file_patterns = [
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_1_dipeptide_our_train_benchmark_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_1_dipeptide_our_train_our_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_1_tripeptide_cbbio_train_benchmark_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_1_tripeptide_our_train_ampscanner_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_1_tripeptide_our_train_benchmark_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_1_tripeptide_our_train_our_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_2_dipeptide_cbbio_train_benchmark_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_2_dipeptide_our_train_ampscanner_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_2_dipeptide_our_train_benchmark_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_2_dipeptide_our_train_our_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_2_tripeptide_our_train_ampscanner_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_2_tripeptide_our_train_benchmark_test',
    'rfc_wholebinary_kmer_3_contextWindow_10_vector_100_reduction_all_combinations_gap_2_tripeptide_our_train_our_test']

if __name__ == "__main__":
    ROOT_DIR = "./"
    directory = ROOT_DIR + "data/results/rfc/"

    for file_pattern in file_patterns:
        file_out = directory + file_pattern + ".csv"
        data = pd.DataFrame()
        for root, subdirs, files in os.walk(directory):
            print(len(list(filter(lambda x: file_pattern in x, files))))
            for file in files:
                if file_pattern in file:
                    data = pd.concat([data, (pd.json_normalize(load_json(root + file)))])

        data['reduction'] = data['reduction'].apply(lambda r: " ".join(r))
        # data = data.sort_values('hamming_loss')
        data.to_csv(file_out, index=False)
