import argparse
import itertools

import numpy as np

from src.classification.classifier_chain import run_classifier_chain_combination_of_reductions
from src.combined_reduction_all_combinations import get_all_combinations, reduction_groups
from src.dnn_classification_combinations import get_target_data
from src.embeddings.utils import create_file_name_using_parameters, create_vectors, load_all_embeddings
from src.files.amp import reduce_amp, load_data
from src.files.utils import save_json, data_combinations_files, multilabel_data_combinations_files


def get_variables(args):
    return args.root, args.data, args.kmer, args.vector_size, args.context_size, args.embedding_file, args.output_dir, args.cores


def arg_parser():
    root = "./"
    data = "data/raw/AMP/"
    kmer = 5
    context_size = 5
    vector_size = 300
    embedding_file = "data/embeddings/uniprot_"
    output_dir = "data/results/classifier_chain/"
    cores = 10

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--data', help='Data amp', default=data)
    parser.add_argument('--output-dir', help='Output directory', default=output_dir)
    parser.add_argument('--kmer', help='kmer size', default=kmer, type=int)
    parser.add_argument('--vector-size', help='vector size', default=vector_size, type=int)
    parser.add_argument('--context-size', help='context size', default=context_size, type=int)
    parser.add_argument('--embedding-file', help='Embedding file path', default=embedding_file)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


if __name__ == "__main__":
    root, data, kmer, vector_size, context_size, embedding_file, output_dir, cores = get_variables(arg_parser())

    # setup
    all_reductions_combinations = get_all_combinations(reduction_groups)
    all_reductions = list(np.unique(list(itertools.chain(*all_reductions_combinations))))
    # all_reductions = all_reductions_combinations[101]
    random_states = list(range(1, 2, 1))

    # load embeddings
    embeddings = load_all_embeddings(all_reductions, root, embedding_file, kmer, context_size, vector_size)

    for data_combination in list(multilabel_data_combinations_files.keys()):
        print("Running for data set combination : " + data_combination)
        # load data
        positive = data_combinations_files[data_combination]['positive']
        negative = data_combinations_files[data_combination]['negative']
        only_positive = data_combinations_files[data_combination]['onlyPositive']

        AMP_data = load_data(root + data, positive, negative, only_positive)

        # reduce data
        reduced_data = reduce_amp(AMP_data, all_reductions)

        # convert to target data
        target = get_target_data('multilabel', reduced_data.classification)

        # create all vectors
        all_vectors, all_errors = create_vectors(reduced_data, embeddings, kmer, all_reductions)

        # Magic happens here
        result = list(map(
            lambda reduction_combination: run_classifier_chain_combination_of_reductions(
                reduction_combination,
                all_vectors,
                random_states,
                target,
                cores,
                kmer,
                context_size,
                vector_size
            ),
            all_reductions_combinations
        ))

        # save results
        file_start = root + output_dir + 'state1_multilabel_classifier_chain_' + data_combination
        file_name = create_file_name_using_parameters(
            file_start,
            "all_combinations",
            kmer,
            context_size,
            vector_size
        )

        save_json(file_name + ".json", result)

    # # classifier chain Random forest
    # rfc = RandomForestClassifier(n_jobs=10)
    # total_chains = list(range(target.shape[1]))
    # chains = [ClassifierChain(rfc, order='random', random_state=random_states[0]) for i in total_chains]
    #
    # for i in total_chains:
    #     target_column = pd.DataFrame(pd.DataFrame(y_train)[i])
    #     print("Chain number : " + str(i))
    #     chains[i].fit(X_train, target_column)
    #
    # Y_pred_chains = np.array([chain.predict(X_test) for chain in chains])
    # y_preds = np.array((list(map(lambda x: list(itertools.chain(*x)), Y_pred_chains)))).T
    # chain_jaccard_scores = jaccard_score(y_test, y_preds >= .5, average='samples')
    #
    # Y_pred_ensemble = y_preds.mean(axis=0)
    #
    # ensemble_jaccard_score = jaccard_score(y_test,
    #                                        Y_pred_ensemble >= .5,
    #                                        average='samples')
    #
    # model_scores = [ovr_jaccard_score] + chain_jaccard_scores
    # model_scores.append(ensemble_jaccard_score)
