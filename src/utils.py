from sklearn.metrics import confusion_matrix


def print_confusion_matrix(y_true, y_predict, labels):
    conf_mat_dict = {}

    for label_col in range(len(labels)):
        y_true_label = y_true[:, label_col]
        y_pred_label = y_predict[:, label_col]
        conf_mat_dict[labels[label_col]] = confusion_matrix(y_pred=y_pred_label, y_true=y_true_label)

    for label, matrix in conf_mat_dict.items():
        print("Confusion matrix for {}:".format(label))
        print(matrix)

    return
