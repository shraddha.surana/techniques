import pandas as pd
import numpy as np
import torch
import torch.nn.functional as F
from torch.nn import Linear
from torch_geometric.data import Data, InMemoryDataset, DataLoader
from torch_geometric.nn import GCNConv, global_mean_pool

from src.classification.utils import calculate_scores
from src.combined_reduction_all_combinations_binary_whole_data import get_target_data
from src.embeddings.reduce_dataset import reduce_with_gap_dipeptide_frequency
from src.files.amp import get_positive_amp_data, filter_data_for_selected_activities, \
    amp_positive_data_activities
from src.files.amp import massage_camp_data

if __name__ == '__main__':
    root = "./"
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'


    def get_test_data(path):
        positive_test_data_file = 'data/raw/positive_data_removed_cdhit.csv'
        test_positive_data = get_positive_amp_data(path + positive_test_data_file)
        test_positive_data = massage_camp_data(test_positive_data).reset_index().drop('index', axis=1)
        positive_data_filtered = filter_data_for_selected_activities(test_positive_data, amp_positive_data_activities)
        return pd.concat([positive_data_filtered])


    def get_train_data(root, positive_data_file):
        raw_positive_data = get_positive_amp_data(root + positive_data_file)
        positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
        positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
        return pd.concat([positive_data_filtered])


    print("loading data")
    train_data = get_train_data(root, positive_data_file)
    test_data = get_test_data(root)

    print("gap dipeptide data")
    aa = reduce_with_gap_dipeptide_frequency(pd.concat([train_data, test_data]), 2)
    train_dataset = aa[:train_data.shape[0]]
    test_dataset = aa[train_data.shape[0]:]

    print("get target data")
    y_data = get_target_data("multilabel", pd.concat([train_data.Activity, test_data.Activity]))
    y_train = y_data[:train_data.shape[0]]
    y_test = y_data[train_data.shape[0]:]

    num_features = train_dataset.shape[1]
    num_classes = y_test.shape[1]


    class TrainDataset(InMemoryDataset):
        def __init__(self, root, transform=None, pre_transform=None):
            super(TrainDataset, self).__init__(root, transform, pre_transform)
            self.data, self.slices = torch.load(self.processed_paths[0])

        @property
        def raw_file_names(self):
            return []

        @property
        def processed_file_names(self):
            return ['gnn_dipeptide_train.dataset']

        def download(self):
            pass

        def process(self):
            data_list = []
            train_dataset_values = train_dataset.values
            for i in range(0, len(train_dataset)):
                group = train_dataset_values[i]
                x = torch.tensor(group, dtype=torch.float).unsqueeze(1)
                y = torch.tensor([y_train.values[i]])
                nodes = list(range(0, len(group)))

                target_nodes = nodes[1:]
                source_nodes = nodes[:-1]

                edge_index = torch.tensor([source_nodes, target_nodes], dtype=torch.long)

                data = Data(x=x, edge_index=edge_index, y=y)
                data_list.append(data)

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])


    class TestDataset(InMemoryDataset):
        def __init__(self, root, transform=None, pre_transform=None):
            super(TestDataset, self).__init__(root, transform, pre_transform)
            self.data, self.slices = torch.load(self.processed_paths[0])

        @property
        def raw_file_names(self):
            return []

        @property
        def processed_file_names(self):
            return ['gnn_dipeptide_test.dataset']

        def download(self):
            pass

        def process(self):
            data_list = []
            test_dataset_values = test_dataset.values
            for i in range(0, len(test_dataset)):
                group = test_dataset_values[i]
                x = torch.tensor(group, dtype=torch.float).unsqueeze(1)
                y = torch.tensor([y_test.values[i]])
                nodes = list(range(0, len(group)))

                target_nodes = nodes[1:]
                source_nodes = nodes[:-1]

                edge_index = torch.tensor([source_nodes, target_nodes], dtype=torch.long)

                data = Data(x=x, edge_index=edge_index, y=y)
                data_list.append(data)

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])


    print("Create Train Dataset")
    train_dataset = TrainDataset(root='./data/')
    print("Create Test Dataset")
    test_dataset = TestDataset(root='./data/')


    class GCN(torch.nn.Module):
        def __init__(self, hidden_channels):
            super(GCN, self).__init__()
            torch.manual_seed(12345)
            self.conv1 = GCNConv(train_dataset.num_node_features, hidden_channels)
            self.conv2 = GCNConv(hidden_channels, hidden_channels)
            self.conv3 = GCNConv(hidden_channels, hidden_channels)
            self.lin = Linear(hidden_channels, num_classes)

        def forward(self, x, edge_index, batch):
            x = self.conv1(x, edge_index)
            x = x.relu()
            x = self.conv2(x, edge_index)
            x = x.relu()
            x = self.conv3(x, edge_index)
            x = global_mean_pool(x, batch)
            x = F.dropout(x, p=0.5, training=self.training)
            x = self.lin(x)

            return x


    print("Create model")
    model = GCN(hidden_channels=16)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
    criterion = torch.nn.CrossEntropyLoss()

    print("Train Data loader")
    train_loader = DataLoader(train_dataset, batch_size=10, shuffle=True)
    print("Test Data loader")
    test_loader = DataLoader(test_dataset, batch_size=10, shuffle=False)


    def train():
        model.train()

        for data in train_loader:
            out = model(data.x, data.edge_index, data.batch)  # Perform a single forward pass.
            loss = criterion(out, data.y)  # Compute the loss.
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()


    def test(loader):
        model.eval()

        correct = 0
        predictions = []
        labels = []
        for data in loader:
            out = model(data.x, data.edge_index, data.batch)
            pred = out.argmax(dim=1).detach().cpu().numpy()
            label = data.y.detach().cpu().numpy()
            correct += int((pred == label).sum())
            predictions.append(pred)
            labels.append(label)

        predictions = np.hstack(predictions)
        labels = np.hstack(labels)

        return calculate_scores(labels, predictions)


    print("Start Epoch")
    for epoch in range(0, 10):
        print(f'Epoch: {epoch}')
        train()
        print(test(test_loader))


# {'accuracy_score': 0.22683885457608086, 'hamming_loss': 0.7731611454239191, 'f1_score_micro': 0.22683885457608086, 'f1_score_macro': 0.1848970251716247}