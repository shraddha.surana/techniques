import itertools
import os
import pickle
from functools import reduce as reduce_functools

import biovec
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

from src.amp_binary_classification import run_random_forest_variation
from src.oldscripts.classify_alphabet_reduced_kd_hydrophobicitya import convert_sequences_to_vectors
from src.embeddings.reduce_dataset import reduce
from src.embeddings.reduction_utils import conformation_similarity, kd_hydrophobicitya, \
    reduction_mappings
from src.embeddings.utils import words_to_vec, create_file_name_using_parameters
from src.files.utils import contains, OTHER_ALPHABETS
from src.oldscripts.main import get_negative_amp_data, get_positive_amp_data


def get_all_combinations(lst):
    return list(itertools.chain(*map(lambda x: list(itertools.combinations(lst, x)), range(1, len(lst) + 1))))


def run_combination(comb):
    global seq_vec_dict, reduced_data_dict

    embedding_file = []
    inputs = []
    for stretagy in comb:
        file_prefix = ROOT_DIR + 'data/embeddings/uniprot_'
        embedding_file = create_file_name_using_parameters(file_prefix, stretagy, 5, 5, 300)

        # ROOT_DIR + 'data/embeddings/uniprot_standard_embedding_kmer_5_contextWindow_5_vector_300_' + stretagy_file

        stretagy_reduced_data = reduced_data_dict.get(stretagy, None)
        if stretagy_reduced_data is None:
            stretagy_reduced_data = pd.DataFrame(reduce(pd.DataFrame(basic_sequences), reduction_mappings[stretagy]))
            reduced_data_dict[stretagy] = stretagy_reduced_data

        reduced_data = pd.DataFrame()
        reduced_data[stretagy] = stretagy_reduced_data[0]
        reduced_data['classification'] = stretagy_reduced_data[1]

        # --- Prot vec ----
        # Use above to classify dataset.
        stretagy_pv = biovec.models.load_protvec(embedding_file)

        # convert to protein embeddings
        inputs_stretagy = seq_vec_dict.get(stretagy, None)
        if inputs_stretagy is None:
            print("Converting sequence to vector for {}".format(stretagy))
            inputs_stretagy, _ = convert_sequences_to_vectors(reduced_data[stretagy], stretagy_pv, words_to_vec)
            seq_vec_dict[stretagy] = inputs_stretagy

        inputs.append(inputs_stretagy.values)

    inputs = reduce_functools(lambda x, y: np.append(x, y, axis=1), inputs) if len(inputs) > 1 else inputs[0]
    print(inputs.shape)
    target = reduced_data['classification']

    # ---- Classification ----

    # split train test
    X_train, X_test, Y_train, Y_test = train_test_split(inputs, target, random_state=11)

    # ---- Random Forest ----
    rfc1 = RandomForestClassifier()
    rfc100 = RandomForestClassifier(n_estimators=100, max_depth=100, n_jobs=10, verbose=True)
    return run_random_forest_variation(rfc100, X_train, X_test, Y_train, Y_test)


if __name__ == "__main__":
    seq_vec_dict = {}
    reduced_data_dict = {}
    ROOT_DIR = ""
    RESULT_DIR = ROOT_DIR + 'classification_results/binary/'
    negative_data_filtered_path = ROOT_DIR + 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_filtered_path = ROOT_DIR + 'data/raw/AMP/positive_data_filtered.csv'  # filtered 70% similarity threshold
    positive_data_unfiltered_path = ROOT_DIR + 'data/raw/AMP/positive_data_unfiltered.csv'

    # ---- Reading the negative dataaset ----
    ndata = get_negative_amp_data(negative_data_filtered_path)
    print(ndata.columns)
    ndata['Activity'] = 0

    # ---- Read the positive dataset ----
    # pdata = get_positive_amp_data(positive_data_filtered_path)
    pdata = get_positive_amp_data(positive_data_unfiltered_path)
    # pdata['Activity'] = 'antimicrobial'
    pdata['Activity'] = 1
    print(pdata.columns)

    # ---- join the data  + cleanse ----
    data = pd.concat([ndata, pdata])
    sequences = data[data.apply(lambda r: not contains(OTHER_ALPHABETS, r['Sequence']), axis=1)]
    sequences = sequences[sequences.apply(lambda r: not str(r['Sequence']) == 'nan', axis=1)]
    sequences['Sequence'] = sequences['Sequence'].apply(lambda x: x.upper())

    basic_sequences = sequences

    indexNames = basic_sequences[basic_sequences['Sequence'] == 'GWLDVAKKIGKAAFNVAKNFLFNKAVNFAAKGIKKAVDLWG '].index
    # Delete these row indexes from dataFrame
    basic_sequences.drop(indexNames, inplace=True)

    reduction_mappings = {
        'conformation_similarity': conformation_similarity,
        'kd_hydrophobicitya': kd_hydrophobicitya
    }

    reduction_stretagies = list(reduction_mappings.keys())
    if 'None' in reduction_stretagies:
        reduction_stretagies.remove('None')

    if not os.path.exists(RESULT_DIR):
        try:
            os.makedirs(RESULT_DIR)
        except FileExistsError as e:
            if e.errno != 17:
                raise OSError("Cannot create folder: {}".format(RESULT_DIR))
    with open(RESULT_DIR + "results.csv", 'w') as f:
        f.write("stretagy_comb,score,matrix\n")
        for stretagy_comb in get_all_combinations(reduction_stretagies):
            print(stretagy_comb)
            stretagy_name = "_".join([stretagy for stretagy in stretagy_comb])
            model, y_predict, score, matrix = run_combination(stretagy_comb)
            f.write("{},{},{}\n".format(stretagy_name, score, matrix))
            model_file_name = create_file_name_using_parameters("model", stretagy_name, 5, 5, 300) + ".pkl"
            with open(model_file_name, 'wb') as m:
                pickle.dump(model, m)
