import io

import PyPDF2
import pandas as pd
import pdftotext
from Bio import SeqIO
from six.moves.urllib.request import urlopen

from src.embeddings.reduce_dataset import reduce_with_gap_tripeptide_frequency, reduce_with_gap_dipeptide_frequency

if __name__ == "__main__":
    pdfFileObj = open('./data/raw/iamp2l.pdf', 'rb')

    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

    print(pdfReader.numPages)

    page_texts = []
    for num in range(0, pdfReader.numPages):
        pageObj = pdfReader.getPage(num)
        page_texts.append(pageObj.extractText())

    text = "\n".join(page_texts)
    text = " ".join(text.replace(u"\xa0", " ").strip().split())

    file1 = open(r"./data/raw/iamp2l.txt", "w+")
    file1.write(text)
    file1.close()

    pdfFileObj.close()

    url = "http://www.jci-bioinfo.cn/iAMP/Supp-S1.pdf"

    remote_file = urlopen(url).read()
    memory_file = io.BytesIO(remote_file)

    pdf = pdftotext.PDF(memory_file)

    # # Iterate over all the pages
    # for page in pdf:
    #     print(page)

    page_texts = []
    for page in pdf:
        page_texts.append(page)

    text = "\n".join(page_texts)

    file1 = open(r"./data/raw/iamp2l.txt", "w+")
    file1.write(text)
    file1.close()

    data = pd.DataFrame()

    raw_data = []
    for seq_record in SeqIO.parse(open("data/raw/iamp-2l/antiviral.fasta", mode='r'), 'fasta'):
        raw_data.append([str(seq_record.seq), "anti-viral"])

    result = pd.DataFrame(raw_data, columns=['Sequence', 'Activity'])

    result.to_csv("data/raw/iamp-2l/antiviral.csv", index=False)

    antihiv = pd.read_csv("data/raw/iamp-2l/antihiv.csv")
    antiviral = pd.read_csv("data/raw/iamp-2l/antiviral.csv")
    antibacterial = pd.read_csv("data/raw/iamp-2l/anibacterial.csv")
    antifungal = pd.read_csv("data/raw/iamp-2l/antifungal.csv")
    anticancer = pd.read_csv("data/raw/iamp-2l/anticancer.csv")

    antibacterial_sequences = list(antibacterial['Sequence'].values)
    antiviral_sequences = list(antiviral['Sequence'].values)
    antihiv_sequences = list(antihiv['Sequence'].values)
    antifungal_sequences = list(antifungal['Sequence'].values)
    anticancer_sequences = list(anticancer['Sequence'].values)

    sequences = list(set(antibacterial_sequences + antiviral_sequences + antifungal_sequences + anticancer_sequences))

    final_data = []
    for sequence in sequences:
        activity = []
        if sequence in antibacterial_sequences:
            activity.append("anti-bacterial")
        if sequence in antiviral_sequences:
            activity.append("antiviral")
        if sequence in antifungal_sequences:
            activity.append("antifungal")
        final_data.append([sequence, ", ".join(activity)])

    final = pd.DataFrame(final_data, columns=["Sequence", "Activities"])

    final.to_csv("data/raw/iamp-2l/top_3.csv", index=False)
    final['Sequence'].to_csv("data/raw/iamp-2l/top_3_plain", index=False, header=False)

    top_4_activities = ['antiviral', 'anticancer', 'antifungal', 'anti-bacterial']

    final_data_4 = []
    for sequence in sequences:
        activity = []
        if sequence in antibacterial_sequences:
            activity.append("anti-bacterial")
        if sequence in antiviral_sequences or sequence in antihiv_sequences:
            activity.append("antiviral")
        if sequence in antifungal_sequences:
            activity.append("antifungal")
        if sequence in anticancer_sequences:
            activity.append("anticancer")
        final_data_4.append([sequence, ", ".join(activity)])

    final5 = pd.DataFrame(final_data_4, columns=["Sequence", "Activities"])

    final5.to_csv("data/raw/iamp-2l/top_4.csv", index=False)
    final5['Sequence'].to_csv("data/raw/iamp-2l/top_4_plain", index=False, header=False)

    gaps = [1, 2]
    functions = {
        'tripeptide': reduce_with_gap_tripeptide_frequency,
        'dipeptide': reduce_with_gap_dipeptide_frequency
    }

    for gap in gaps:
        for peptide_type in list(functions.keys()):
            aa = functions[peptide_type](final, gap)
            aa.to_csv(f"./data/raw/iamp-2l/pfeatures/{peptide_type}_{gap}.csv", index=False)

    for gap in gaps:
        for peptide_type in list(functions.keys()):
            aa = functions[peptide_type](final, gap)
            aa.to_csv(f"./data/raw/iamp-2l/pfeatures_4/{peptide_type}_{gap}.csv", index=False)
