import argparse
import os
from itertools import combinations

import biovec
import pandas as pd
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier

from src.classification.utils import calculate_scores, classify_and_predict
from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import reduce_amp
from src.files.utils import trim_all, replace_brackets, save_json
from mlflow import log_metric, log_param, log_artifacts


def unique_combinations(keys):
    comb = []
    for n in range(1, len(keys) + 1):
        comb = comb + list(map(list, combinations(keys, n)))
    return comb


def get_variables(args):
    return args.root, args.cores


def arg_parser():
    root = "./"
    cores = 10

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--cores', help='Cores used by classifier', default=cores, type=int)
    return parser.parse_args()


if __name__ == "__main__":
    root, cores = get_variables(arg_parser())

    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"

    available_p_features = {
        "amino_acid_compositions": "amino_acid_compositions.csv",
        "auto_correlations": "auto_correlations.csv",
        "cetd": "cetd.csv",
        "DDOR": "DDOR.csv",
        "dipeptide_1": "dipeptide_1.csv",
        "dipeptide_2": "dipeptide_2.csv",
        "physico": "physico.csv",
        "residue_repeats": "residue_repeats.csv",
        "secondary_structure": "secondary_structure.csv",
        "shannon_entropy": "shannon_entropy.csv",
        "tripeptide_1": "tripeptide_1.csv",
        "tripeptide_2": "tripeptide_2.csv"
    }

    to_remove = ['ID', 'Sequence', 'aaindex', 'index']


    def read_data(file):
        data = pd.read_csv(root + "data/multilabel/apd_pfeatures_3/" + file, index_col=False)
        columns = list(filter(lambda x: to_remove.__contains__(x), list(data.columns)))
        return data.drop(columns=columns).fillna(0)

    def read_test_data(file):
        data = pd.read_csv(root + "data/raw/iamp-2l/pfeatures/" + file, index_col=False)
        columns = list(filter(lambda x: to_remove.__contains__(x), list(data.columns)))
        return data.drop(columns=columns).fillna(0)


    p_features_train = {}
    all_p_features = list(available_p_features.keys())
    for key in all_p_features:
        p_features_train[key] = {}
        p_features_train[key] = read_data(available_p_features[key])

    p_features_test = {}
    all_p_features = list(available_p_features.keys())
    for key in all_p_features:
        p_features_test[key] = {}
        p_features_test[key] = read_test_data(available_p_features[key])

    embeddings = {}
    all_vectors_train = {}
    all_vectors_test = {}
    all_errors = {}

    train_data = pd.read_csv(root + "data/raw/apd/apd_filtered_3_activities.csv")
    test_data = pd.read_csv(root + "data/raw/iamp-2l/top_3.csv")

    y_train_raw = train_data['apd_activity']
    y_train_raw = y_train_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))

    y_test_raw = test_data['Activities']
    y_test_raw = y_test_raw.apply(lambda x: trim_all(replace_brackets(x).split(",")))

    reduction_keys = ['blossom_matrix_8', 'contact_energies', 'None']
    print("reducing data")
    reduced_train_data = reduce_amp(train_data, reduction_keys)
    reduced_test_data = reduce_amp(test_data, reduction_keys)

    print("Reading embeddings")
    for red in reduction_keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    print("Creating vectors")
    for red in reduction_keys:
        if not all_vectors_train.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_train_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors_train[red] = inputs
            all_errors[red] = errors

    print("Creating vectors")
    for red in reduction_keys:
        if not all_vectors_test.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_test_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors_test[red] = inputs
            all_errors[red] = errors

    for key in reduction_keys:
        p_features_train[key] = {}
        p_features_train[key] = all_vectors_train[key].reset_index().drop(['index'], axis='columns')

        p_features_test[key] = {}
        p_features_test[key] = all_vectors_test[key].reset_index().drop(['index'], axis='columns')

    all_keys = all_p_features + reduction_keys

    print("total keys", len(all_keys))

    mlb = preprocessing.MultiLabelBinarizer()

    scores = {}

    top_combinations = [
        ["dipeptide_2", "secondary_structure"],
        ["dipeptide_1", "dipeptide_2", "physico", "secondary_structure"],
        ["dipeptide_1", "dipeptide_2", "physico", "residue_repeats", "secondary_structure"],
        ["dipeptide_1", "dipeptide_2"],
        ["DDOR", "dipeptide_1", "dipeptide_2"],
        ["amino_acid_compositions", "DDOR", "dipeptide_1", "dipeptide_2", "residue_repeats"],
        ["DDOR", "dipeptide_1", "dipeptide_2", "physico", "secondary_structure"],
        ["dipeptide_1", "dipeptide_2", "residue_repeats", "secondary_structure"],
        ["dipeptide_1", "dipeptide_2", "secondary_structure"],
        ["amino_acid_compositions", "DDOR", "dipeptide_1", "dipeptide_2", "residue_repeats", "secondary_structure"]
    ]

    # combs = top_combinations
    combs = unique_combinations(all_keys)

    print("Total combinations")
    print(len(combs))

    # for i, keys in enumerate(combs):
    # print(f"{i} : {keys}")
    keys = combs[0]
    X_train = pd.DataFrame()
    for key in keys:
        X_train = pd.concat([X_train, p_features_train[key]], axis=1)

    X_test = pd.DataFrame()
    for key in keys:
        X_test = pd.concat([X_test, p_features_test[key]], axis=1)

    y = mlb.fit_transform(pd.concat([y_train_raw, y_test_raw]))
    y_train = y[:y_train_raw.shape[0]]
    y_test = y[y_train_raw.shape[0]:]

    for state in range(1, 2):
        classifier = RandomForestClassifier(n_jobs=cores)

        predicted, model = classify_and_predict(X_train, X_test, y_train, classifier)

        combination = f"{'-'.join(keys)}-{state}"
        scores[combination] = calculate_scores(y_test, predicted, method='multilabel')
        scores[combination]["random_state"] = state
        print(scores[combination])
        for score_key in scores[combination].keys():
            log_metric(score_key, scores[combination][score_key])

    # print(scores)
    # save_json(root + "data/results/rfc_apd_imp-2l_test_pfeatures.json", scores)
