import numpy as np
import pandas as pd
import torch
import torch.nn.functional as F
from propy import PyPro
from sklearn import preprocessing
from torch.nn import Linear
from torch_geometric.data import Data, InMemoryDataset, DataLoader
from torch_geometric.nn import GCNConv, global_mean_pool

from src.classification.utils import calculate_scores
from src.combined_reduction_all_combinations_binary_whole_data import get_target_data
from src.embeddings.reduction_utils import propy_paac_map
from src.files.amp import get_positive_amp_data, filter_data_for_selected_activities, \
    amp_positive_data_activities, filter_unwanted_sequences, filter_unwanted_sequences_by_len
from src.files.amp import massage_camp_data

if __name__ == '__main__':
    root = "./"
    positive_data_file = 'data/raw/AMP/amp_curated_positive_filtered_sequence_length_10_to_60.csv'


    def get_test_data(path):
        positive_test_data_file = 'data/raw/positive_data_removed_cdhit.csv'
        test_positive_data = get_positive_amp_data(path + positive_test_data_file)
        test_positive_data = massage_camp_data(test_positive_data).reset_index().drop('index', axis=1)
        test_positive_data = filter_unwanted_sequences(test_positive_data)
        positive_data_filtered = filter_data_for_selected_activities(test_positive_data, amp_positive_data_activities)
        return pd.concat([positive_data_filtered])


    def get_train_data(root, positive_data_file):
        raw_positive_data = get_positive_amp_data(root + positive_data_file)
        positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
        positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
        return pd.concat([positive_data_filtered])


    print("loading data")
    train_data = get_train_data(root, positive_data_file)
    test_data = get_test_data(root)
    test_data = filter_unwanted_sequences_by_len(test_data)

    train_dataset = list(train_data['Sequence'].values)
    test_dataset = list(test_data['Sequence'].values)

    print("get target data")
    y_data = get_target_data("multilabel", pd.concat([train_data.Activity, test_data.Activity]))
    y_train = y_data[:train_data.shape[0]]
    y_test = y_data[train_data.shape[0]:]

    num_classes = y_test.shape[1]


    class TrainDataset(InMemoryDataset):
        def __init__(self, root, transform=None, pre_transform=None):
            super(TrainDataset, self).__init__(root, transform, pre_transform)
            self.data, self.slices = torch.load(self.processed_paths[0])

        @property
        def raw_file_names(self):
            return []

        @property
        def processed_file_names(self):
            return ['gnn_dipeptide_train_paac_lambda_uni.dataset']

        def download(self):
            pass

        def process(self):
            data_list = []
            train_dataset_values = train_dataset
            for i in range(0, len(train_dataset)):
                group = train_dataset_values[i]
                le = preprocessing.LabelEncoder()
                targets = list(le.fit_transform(list(group)))
                y = torch.FloatTensor([y_train[i]])
                unique_edges = [[targets[j], targets[j + 1]] for j in range(len(targets) - 1)]
                edges_one = torch.tensor(unique_edges, dtype=torch.long)

                unique_residues = list(set(group))
                paac = PyPro.GetProDes(group).GetPAAC(lamda=len(unique_residues))
                other = list(paac.values())[20:]
                nodes = list(map(lambda x: paac[propy_paac_map[x.upper()]], unique_residues))
                data = Data(x=torch.FloatTensor(np.vstack((other, nodes)).T), edge_index=edges_one.t().contiguous(),
                            y=y)
                data_list.append(data)

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])


    class TestDataset(InMemoryDataset):
        def __init__(self, root, transform=None, pre_transform=None):
            super(TestDataset, self).__init__(root, transform, pre_transform)
            self.data, self.slices = torch.load(self.processed_paths[0])

        @property
        def raw_file_names(self):
            return []

        @property
        def processed_file_names(self):
            return ['gnn_dipeptide_test_paac_lambda_uni.dataset']

        def download(self):
            pass

        def process(self):
            data_list = []
            test_dataset_values = test_dataset
            for i in range(0, len(test_dataset)):
                group = test_dataset_values[i].strip()
                le = preprocessing.LabelEncoder()
                targets = list(le.fit_transform(list(group)))
                y = torch.FloatTensor([y_test[i]])
                unique_edges = [[targets[j], targets[j + 1]] for j in range(len(targets) - 1)]
                edges_one = torch.tensor(unique_edges, dtype=torch.long)
                unique_residues = list(set(group))
                paac = PyPro.GetProDes(group).GetPAAC(lamda=len(unique_residues))
                other = list(paac.values())[20:]
                nodes = list(map(lambda x: paac[propy_paac_map[x.upper()]], unique_residues))
                data = Data(x=torch.FloatTensor(np.vstack((other, nodes)).T), edge_index=edges_one.t().contiguous(),
                            y=y)

                data_list.append(data)

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])


    print("Create Train Dataset")
    train_dataset_processed = TrainDataset(root='./data/')
    print("Create Test Dataset")
    test_dataset_processed = TestDataset(root='./data/')


    class GCN(torch.nn.Module):
        def __init__(self, hidden_channels):
            super(GCN, self).__init__()
            torch.manual_seed(12345)
            self.conv1 = GCNConv(train_dataset_processed.num_node_features, hidden_channels)
            self.conv2 = GCNConv(hidden_channels, hidden_channels)
            self.lin = Linear(hidden_channels, num_classes)

        def forward(self, x, edge_index, batch):
            x = self.conv1(x, edge_index)
            x = x.relu()
            x = self.conv2(x, edge_index)
            x = global_mean_pool(x, batch)
            x = self.lin(x)
            print(x)
            return x


    print("Train Data loader")
    train_loader = DataLoader(train_dataset_processed, batch_size=1, shuffle=True)
    print("Test Data loader")
    test_loader = DataLoader(train_dataset_processed, batch_size=1, shuffle=False)

    print("Create model")
    model = GCN(hidden_channels=16)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.02)
    criterion = torch.nn.BCEWithLogitsLoss()


    def train():
        model.train()
        print(len(train_loader))
        for data in train_loader:
            out = model(data.x, data.edge_index, data.batch)
            loss = criterion(out, data.y)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()


    def all_to_int(l):
        return list(map(lambda x: 1 if x >= 0.5 else 0, l))


    def test(loader):
        model.eval()

        correct = 0
        predictions = []
        labels = []
        for data in loader:
            out = model(data.x, data.edge_index, data.batch)
            pred = out.detach().cpu().numpy()
            label = data.y.detach().cpu().numpy()
            correct += int((pred == label).sum())
            predictions.append(pred)
            labels.append(label)
        preds = list(map(lambda x: all_to_int(x.flatten().tolist()), predictions))
        labs = list(map(lambda x: all_to_int(x.flatten().tolist()), labels))
        return calculate_scores(labs, preds)


    print("Start Epoch")
    for epoch in range(0, 10):
        print(f'Epoch: {epoch}')
        train()
        print(test(test_loader))

# multilabel with 1 feature same accuracy as multilabel
# {'accuracy_score': 0.5298994531663432, 'hamming_loss': 0.16828364790968425, 'f1_score_micro': 0.716282527881041, 'f1_score_macro': 0.22968720198359718}
