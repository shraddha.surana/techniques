
import requests

def map_model(data):
    metrics = {"accuracy": data['accuracy_score'], "hammings_loss": data['hamming_loss'],
               "f1_score_micro": data['f1_score_micro'], "f1_score_macro": data['f1_score_macro']}
    return {"metrics": metrics}


def map_models(scores):
    result = []
    for key in scores.keys():
        result.append(map_model(scores[key]))
    return result


def add_run(data):
    requests.post("http://localhost:5000/runs", json=data)
