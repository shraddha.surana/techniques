import pandas as pd
import pandas as pd
import torch
import torch.nn.functional as F
from sklearn import preprocessing
from torch.nn import Linear
from torch_geometric.data import Data, InMemoryDataset, DataLoader
from torch_geometric.nn import GCNConv, global_mean_pool

from src.classification.utils import calculate_scores
from src.combined_reduction_all_combinations_binary_whole_data import get_target_data
from src.files.amp import get_positive_amp_data, filter_data_for_selected_activities, \
    amp_positive_data_activities, filter_unwanted_sequences
from src.files.amp import massage_camp_data

if __name__ == '__main__':
    root = "./"
    positive_data_file = 'data/raw/AMP/positive_data_unfiltered2807.csv'


    def get_test_data(path):
        positive_test_data_file = 'data/raw/positive_data_removed_cdhit.csv'
        test_positive_data = get_positive_amp_data(path + positive_test_data_file)
        test_positive_data = massage_camp_data(test_positive_data).reset_index().drop('index', axis=1)
        test_positive_data = filter_unwanted_sequences(test_positive_data)
        positive_data_filtered = filter_data_for_selected_activities(test_positive_data, amp_positive_data_activities)
        return pd.concat([positive_data_filtered])


    def get_train_data(root, positive_data_file):
        raw_positive_data = get_positive_amp_data(root + positive_data_file)
        positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
        positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)
        return pd.concat([positive_data_filtered])


    print("loading data")
    train_data = get_train_data(root, positive_data_file)
    test_data = get_test_data(root)

    train_data['Activity'] = train_data['Activity'].apply(lambda x: ",".join(x))
    test_data['Activity'] = test_data['Activity'].apply(lambda x: ",".join(x))

    train_data['Sequence'] = train_data['Sequence'].apply(lambda x: x.strip())
    test_data['Sequence'] = test_data['Sequence'].apply(lambda x: x.strip())

    train_data.to_csv("data/intermediate/postive_unfiltered2807_4_activities.csv", index=False)
    test_data.to_csv("data/intermediate/positive_data_removed_cdhit_4_activities.csv", index=False)

    train_data['Sequence'].to_csv("data/intermediate/postive_unfiltered2807_4_activities_sequences", index=False, header=False)
    test_data['Sequence'].to_csv("data/intermediate/positive_data_removed_cdhit_4_activities_sequences", index=False, header=False)



    train_dataset = train_data['Sequence'].values
    test_dataset = test_data['Sequence'].values

    print("get target data")
    y_data = get_target_data("multilabel", pd.concat([train_data.Activity, test_data.Activity]))
    y_train = y_data[:train_data.shape[0]]
    y_test = y_data[train_data.shape[0]:]

    num_classes = y_test.shape[1]


    class TrainDataset(InMemoryDataset):
        def __init__(self, root, transform=None, pre_transform=None):
            super(TrainDataset, self).__init__(root, transform, pre_transform)
            self.data, self.slices = torch.load(self.processed_paths[0])

        @property
        def raw_file_names(self):
            return []

        @property
        def processed_file_names(self):
            return ['gnn_dipeptide_train.dataset']

        def download(self):
            pass

        def process(self):
            data_list = []
            train_dataset_values = train_dataset
            for i in range(0, len(train_dataset)):
                group = train_dataset_values[i]
                le = preprocessing.LabelEncoder()
                targets = le.fit_transform(list(group))
                y = torch.FloatTensor([y_train[i]])
                nodes = list(range(0, len(set(group))))
                unique_edges = [[targets[j], targets[j + 1]] for j in range(len(targets) - 1)]
                unique_edges_reversed = []
                for x in unique_edges:
                    unique_edges_reversed.append(x[::-1])
                edges_one = torch.tensor(unique_edges + unique_edges_reversed, dtype=torch.long)
                # nodes = list(map(lambda z: list(map(lambda x: 1 if x == z else 0, targets)), nodes))
                data = Data(x=torch.LongTensor(nodes).unsqueeze(1), edge_index=edges_one.t().contiguous(), y=y)
                data_list.append(data)

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])


    class TestDataset(InMemoryDataset):
        def __init__(self, root, transform=None, pre_transform=None):
            super(TestDataset, self).__init__(root, transform, pre_transform)
            self.data, self.slices = torch.load(self.processed_paths[0])

        @property
        def raw_file_names(self):
            return []

        @property
        def processed_file_names(self):
            return ['gnn_dipeptide_test.dataset']

        def download(self):
            pass

        def process(self):
            data_list = []
            test_dataset_values = test_dataset
            for i in range(0, len(test_dataset)):
                group = test_dataset_values[i]
                le = preprocessing.LabelEncoder()
                targets = le.fit_transform(list(group))
                y = torch.FloatTensor([y_train[i]])
                nodes = list(range(0, len(set(group))))
                unique_edges = [[targets[j], targets[j + 1]] for j in range(len(targets) - 1)]
                unique_edges_reversed = []
                for x in unique_edges:
                    unique_edges_reversed.append(x[::-1])
                edges_one = torch.tensor(unique_edges + unique_edges_reversed, dtype=torch.long)
                # nodes = list(map(lambda z: list(map(lambda x: 1 if x == z else 0, targets)), nodes))
                data = Data(x=torch.LongTensor(nodes).unsqueeze(1), edge_index=edges_one.t().contiguous(), y=y)
                data_list.append(data)

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])


    print("Create Train Dataset")
    train_dataset_processed = TrainDataset(root='./data/')
    print("Create Test Dataset")
    test_dataset_processed = TestDataset(root='./data/')


    class AtomEncoder(torch.nn.Module):
        def __init__(self, hidden_channels):
            super(AtomEncoder, self).__init__()

            self.embeddings = torch.nn.ModuleList()

            for i in range(9):
                self.embeddings.append(torch.nn.Embedding(100, hidden_channels))

        def reset_parameters(self):
            for embedding in self.embeddings:
                embedding.reset_parameters()

        def forward(self, x):
            if x.dim() == 1:
                x = x.unsqueeze(1)

            out = 0
            for i in range(x.size(1)):
                out += self.embeddings[i](x[:, i])
            return out


    class GCN(torch.nn.Module):
        def __init__(self, hidden_channels):
            super(GCN, self).__init__()
            torch.manual_seed(12345)
            self.emb = AtomEncoder(train_dataset_processed.num_node_features)
            self.conv1 = GCNConv(train_dataset_processed.num_node_features, hidden_channels)
            self.conv2 = GCNConv(hidden_channels, hidden_channels)
            self.conv3 = GCNConv(hidden_channels, hidden_channels)
            self.lin = Linear(hidden_channels, num_classes)

        def forward(self, x, edge_index, batch):
            x = self.emb(x)
            x = self.conv1(x, edge_index)
            x = x.relu()
            x = self.conv2(x, edge_index)
            x = x.relu()
            x = self.conv3(x, edge_index)
            x = global_mean_pool(x, batch)
            x = F.dropout(x, p=0.2, training=self.training)
            x = self.lin(x)

            return x


    print("Train Data loader")
    train_loader = DataLoader(train_dataset_processed, batch_size=1, shuffle=True)
    print("Test Data loader")
    test_loader = DataLoader(train_dataset_processed, batch_size=1, shuffle=False)

    print("Create model")
    model = GCN(hidden_channels=32)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
    criterion = torch.nn.BCEWithLogitsLoss()


    def train():
        model.train()
        print(len(train_loader))
        for data in train_loader:
            out = model(data.x, data.edge_index, data.batch)
            loss = criterion(out, data.y)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()


    def all_to_int(l):
        return list(map(lambda x: 1 if x >= 0.5 else 0, l))


    def test(loader):
        model.eval()

        correct = 0
        predictions = []
        labels = []
        for data in loader:
            out = model(data.x, data.edge_index, data.batch)
            pred = out.detach().cpu().numpy()
            label = data.y.detach().cpu().numpy()
            correct += int((pred == label).sum())
            predictions.append(pred)
            labels.append(label)
        preds = list(map(lambda x: all_to_int(x.flatten().tolist()), predictions))
        labs = list(map(lambda x: all_to_int(x.flatten().tolist()), labels))
        return calculate_scores(labs, preds)


    print("Start Epoch")
    for epoch in range(0, 10):
        print(f'Epoch: {epoch}')
        train()
        print(test(test_loader))

# multilabel
# {'accuracy_score': 0.5298994531663432, 'hamming_loss': 0.16828364790968425, 'f1_score_micro': 0.716282527881041, 'f1_score_macro': 0.22968720198359718}

# Binary
# {'accuracy_score': 0.22683885457608086, 'hamming_loss': 0.7731611454239191, 'f1_score_micro': 0.22683885457608086, 'f1_score_macro': 0.1848970251716247}
