from pprint import pprint

import biovec
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, hamming_loss, f1_score, classification_report, make_scorer, \
    matthews_corrcoef
from sklearn.model_selection import RandomizedSearchCV, train_test_split

from src.embeddings.utils import create_file_name_using_parameters, convert_sequences_to_vectors, words_to_vec
from src.files.amp import get_positive_amp_data, filter_data_for_selected_activities, amp_positive_data_activities, \
    get_negative_amp_data, reduce_amp
from src.files.camp import massage_camp_data
from src.files.utils import save_json


def get_target_data(classification_type, data):
    if classification_type == 'binary':
        return data.apply(lambda x: 0 if x == ['none'] else 1)


def create_random_search_grid():
    return {
        'n_estimators': (10, 50, 100, 500, 1000, 1500, 2000),
        'max_features': ['auto', 'sqrt', 'log2'],
        'max_depth': ([int(x) for x in np.linspace(20, 100, num=5)] + [None]),  # 50, 100, 250
        'min_samples_split': [2, 5, 10],
        'min_samples_leaf': [1, 2, 4],
        'bootstrap': [True, False]
    }


def create_mlknn_grid():
    return {
        'k': list(range(1, 11)),
        's': [0.5, 0.7, 1.0]
    }


def create_random_search_grid_svc():
    return {
        'probability': [True, False],
        'kernel': ['linear', 'poly', 'rbf', 'sigmoid', 'precomputed'],
        'degree': [2, 3, 4]
    }


def create_random_search_grid_sgd():
    return {
        'probability': [True, False],
        'kernel': ['linear', 'poly', 'rbf', 'sigmoid', 'precomputed'],
        'degree': [2, 3, 4]
    }


def evaluate(model, test_features, test_labels, y_test):
    predictions = model.predict(test_features)
    errors = abs(predictions - test_labels)
    div = list(map(lambda x: 0 if x == float('inf') else x, errors / y_test))
    mape = 100 * np.mean(div)
    accuracy = 100 - mape
    average_error = np.mean(errors)
    return accuracy, average_error


def custom_scorer(y_true, y_pred, actual_scorer):
    score = np.nan
    try:
        score = actual_scorer(y_true, y_pred)
    except ValueError:
        pass

    return score


def run_random_search_cv(x_train, x_test, y_train, y_test, top_activities):
    # Use the random grid to search for best hyperparameters
    # First create the base model to tune
    random_grid = create_random_search_grid()
    pprint(random_grid)
    # Random search of parameters, using 3 fold cross validation,
    # search across 100 different combinations, and use 10 available cores

    mcc = make_scorer(custom_scorer, actual_scorer=matthews_corrcoef)

    randomized_search_cv = RandomizedSearchCV(
        estimator=RandomForestClassifier(),
        param_distributions=random_grid,
        n_iter=2268,
        cv=None,
        verbose=2,
        random_state=1,
        n_jobs=50,
        scoring=mcc
    )
    # 79294
    print("Fit the random search model")
    randomized_search_cv.fit(x_train, y_train)
    print("Fit the random search model DONE!")
    return randomized_search_cv


if __name__ == "__main__":
    root = "/nvme/life_science/techniques/"
    kmer = 3
    context_size = 10
    vector_size = 100
    embedding_file = "data/embeddings/uniprot_"
    cores = 50
    method = 'binary'

    negative_data_file = 'data/raw/AMP/negative_data_filtered.csv'  # 0.4 similarity threshold with positive data
    positive_data_file = 'data/raw/AMP/positive_data_filtered2807.csv'  # filtered 70% similarity threshold

    embeddings = {}
    all_vectors = {}
    all_errors = {}
    random_states = list(range(1, 2, 1))

    raw_positive_data = get_positive_amp_data(root + positive_data_file)
    positive_data = massage_camp_data(raw_positive_data).reset_index().drop('index', axis=1)
    positive_data_filtered = filter_data_for_selected_activities(positive_data, amp_positive_data_activities)

    raw_negative_data = get_negative_amp_data(root + negative_data_file)
    negative_data = massage_camp_data(raw_negative_data).reset_index().drop('index', axis=1)
    AMP_data = pd.concat([positive_data_filtered, negative_data])

    keys = ['hydrophobicitya', 'blossom_matrix_4', 'contact_energies', 'None']  # binary

    reduced_data = reduce_amp(AMP_data, keys)

    target = get_target_data(method, reduced_data.classification)

    for red in keys:
        if not embeddings.__contains__(red):
            embeddings[red] = biovec.models.load_protvec(
                create_file_name_using_parameters(root + embedding_file, red, kmer, context_size, vector_size))

    for red in keys:
        if not all_vectors.__contains__(red):
            inputs, errors = convert_sequences_to_vectors(reduced_data[red], embeddings[red], words_to_vec, kmer)
            all_vectors[red] = inputs
            all_errors[red] = errors

    all_inputs = pd.DataFrame()
    for red in keys:
        all_inputs = pd.concat([all_inputs, all_vectors[red]], axis=1)

    x_train, x_test, y_train, y_test = train_test_split(all_inputs, target, random_state=1)

    search_cv = run_random_search_cv(x_train, x_test, y_train, y_test, amp_positive_data_activities)

    results = search_cv.cv_results_
    print(results)
    print("======================= Print the best params ==========================")
    print(search_cv.best_params_)
    best_model = search_cv.best_estimator_
    predicted = best_model.predict(x_test)
    print('Model Performance :  ')
    print('Accuracy Score    :  ', accuracy_score(y_test, predicted))
    print('HAMMING LOSS      :  ', hamming_loss(y_test, predicted))
    print('F1 SCORE MICRO    :  ', f1_score(y_test, predicted, average='micro'))
    print('F1 SCORE MACRO    :  ', f1_score(y_test, predicted, average='macro'))
    print('MCC               :  ', matthews_corrcoef(y_test, predicted))
    print('CLASSIFICATION REPORT : ')
    print(classification_report(y_test, predicted))
    save_json(root + "data/results/binary_hyperparameter_tuning_best_params", search_cv.best_params_)
    save_json(root + "data/results/binary_hyperparameter_tuning", results)
