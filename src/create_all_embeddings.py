import argparse
import os
from itertools import product

from dask import delayed
from gensim.models import word2vec

from src.combined_reduction_all_combinations import chunks
from src.embeddings.corpus import generate_corpusfile
from src.embeddings.reduction_utils import reduction_mappings
from src.embeddings.utils import create_file_name_using_parameters


def arg_parser():
    root = ""
    data_path = ""
    file_in = "data/uniprot/uniprot_sprot_standard_alphabets.fasta"
    cores = 3
    parallel_creation = 3

    parser = argparse.ArgumentParser(description='Reduce data set')
    parser.add_argument('--root', help='Path to project', default=root)
    parser.add_argument('--data-path', help='Path to data directory', default=data_path)
    parser.add_argument('--file-in', help='Fasta file', default=file_in)
    parser.add_argument('--cores', help='Cores used for creation of embedding', default=cores, type=int)
    parser.add_argument('--parallel-creation', help='embeddings to create in parallel', default=parallel_creation,
                        type=int)

    return parser.parse_args()


def get_variables(args):
    return args.root, args.data_path, args.file_in, args.cores, args.parallel_creation


combinations = [[3, 5], [5, 10, 25], [100, 200, 300], list(reduction_mappings.keys())]


def create_embedding(root, data_path, file_in, kmer, context_size, vector_size, reduction, cores):
    corpus_file = data_path + "data/processed/kmer" + str(kmer) + "_reduction_" + reduction + ".txt"
    embedding_file = create_file_name_using_parameters(data_path + "data/embeddings/uniprot_", reduction, kmer,
                                                       context_size, vector_size)

    print("These are varaibles : ")
    print(root, data_path, file_in, kmer, vector_size, context_size, reduction)
    print(corpus_file, embedding_file)
    if (os.path.isfile(corpus_file)):
        print("=== Corpus file present. Not regenerating ===")
    else:
        print("START: Generate corpus file")
        generate_corpusfile(str(root) + str(file_in), kmer, corpus_file, reduction_mappings[reduction])
        print("DONE: Generate corpus file")
    if os.path.isfile(embedding_file):
        print("=== Embedding file already present. Not regenerating ===")
    else:
        print("START: load corpus")
        corpus = word2vec.Text8Corpus(corpus_file)
        print("Done: load corpus")

        print("START: create embedding")
        sg = 1
        epochs = 5
        negative = 0
        hs = 1
        pv = word2vec.Word2Vec(
            corpus,
            size=vector_size,
            sg=sg,
            hs=hs,
            window=context_size,
            min_count=1,
            iter=epochs,
            negative=negative,
            compute_loss=True,
            workers=cores
        )
        print("DONE: create embedding")

        print("START: save embedding")
        pv.save(embedding_file)
        print("DONE: saved embedding at - ", embedding_file)


def does_embedding_file_exists(path, kmer, context_size, vector_size, reduction):
    embedding_file = create_file_name_using_parameters(path + "data/embeddings/uniprot_", reduction, kmer,
                                                       context_size, vector_size)
    return os.path.isfile(embedding_file)


if __name__ == "__main__":
    root, data_path, file_in, cores, parallel_creation = get_variables(arg_parser())

    possible_combinations = list(product(*combinations))
    all_remaining_combinations = list(
        filter(lambda c: not does_embedding_file_exists(data_path, c[0], c[1], c[2], c[3]), possible_combinations))

    all_remaining_combinations.sort(key=lambda x: x[1])
    print("Running for total combinations : ", str(len(all_remaining_combinations)))
    print("All the combinations are : ", str(all_remaining_combinations))

    for combination in chunks(all_remaining_combinations, parallel_creation):
        delayed(list)(map(
            lambda c: delayed(create_embedding)(root, data_path, file_in, c[0], c[1], c[2], c[3], cores), combination)
        ).compute()

    print("============================ All Embeddings created =================================")
