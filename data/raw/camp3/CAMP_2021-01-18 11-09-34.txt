  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ74	117413	P01518		Crabrolin	Vespa crabro	Animalia (Insects)	FLPLILRKIVTAL	13	9273892	Antibacterial		Experimentally Validated
CAMPSQ80	119391208	A3KD26		Temporin-1Oa1	 Rana ornativentris 	Animalia (Amphibians)	FLPLLASLFSRLL	13	17147973, 11892844	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ82	119391212	A3KD27		Temporin-1Oc1	 Rana ornativentris 	Animalia (Amphibians)	FLPLLASLFSRLF	13	17147973, 11892844	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ86	121956658	P84858		Temporin-GH 	Rana guentheri	Animalia (Amphibians)	FLPLLFGAISHLL	13	16979798	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ314	28201905	Q8MMJ7	1T51,1T52,1T54,1T55	Cytotoxic linear peptide IsCT 	Opisthacanthus madagascariensis 	Animalia (Arachnids)	ILGKIWEGIKSLF	13	15369808	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ315				Cytotoxic linear peptide IsCT precursor 	Synthetic construct		ILGKIAEGIKSLF	13	15369808	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ316				Cytotoxic linear peptide IsCT precursor 	Synthetic construct		ILGKILEGIKSLF	13	15369808	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ317				Cytotoxic linear peptide IsCT precursor 	Synthetic construct		ILGKIWKGIKSLF	13	15369808	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ318				Cytotoxic linear peptide IsCT precursor 	Synthetic construct		ILGKILKGIKKLF	13	15369808	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ350	32363120	P83412		Alloferon-1 	Calliphora vicina 	Animalia (Insects)	HGVSGHGQHGVHG	13	12235362	Antiviral, Anticancer		Experimentally Validated
CAMPSQ839				Temporin-1Vc	Rana catesbeiana	Animalia (Amphibians)	FLPLVTMLLGKLF	13	15996769	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ840				Temporin-1Va	Rana catesbeiana	Animalia (Amphibians)	FLSSIGKILGNLL	13	15996769	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ841				Temporin-1Vb	Rana catesbeiana	Animalia (Amphibians)	FLSIIAKVLGSLF	13	15996769	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ1093	66773949	P69844		Grammistin Pp2b	Pogonoperca punctata	Animalia (Pisces)	FIGGIISFIKKLF	13		Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1095	66773815	P69842		Grammistin Pp1	Pogonoperca punctata	Animalia (Pisces)	FIGGIISFFKRLF	13		Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2645	388423205	I3RU62		Preproalytesin-MA  	Alytes maurus  	Animalia (Amphibians)	TQWAVGHLMGKKS	13		Antimicrobial		Predicted
CAMPSQ2890				  human Histatin 7 	Homo sapiens	Animalia (Mammals)	RKFHEKHHSHRGY	13	2303595	Antimicrobial		Experimentally Validated
CAMPSQ2902				  Temporin-1Ob 	Rana ornativentris	Animalia (Amphibians)	FLPLIGKILGTIL	13	11892844	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3130				  VESP-VB1 	Vespa bicolor	Animalia (Insects)	FMPIIGRLMSGSL	13	18723059	Antimicrobial		Experimentally Validated
CAMPSQ3138				  Temporin-1PRb 	Rana pirica	Animalia (Amphibians)	ILPILGNLLNSLL	13	15003829	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3638				  Temporin-ALj 	Amolops loloensis	Animalia (Amphibians)	FFPIVGKLLFGLL	13	19843479	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3657				  Macropin 1 	Macropis fulvipes	Animalia (Insects)	GFGMALKLLKKVL	13	22100226	Antimicrobial		Experimentally Validated
CAMPSQ3675				  IsCT2 	Opisthacanthus madagascariensis	Animalia (Arachnids)	IFGAIWNGIKSLF	13	20575512	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3920		P86526		Bacteriocin rhamnosin A 	Lactobacillus rhamnosus	Bacteria	AVPAVRKTNETLD	13	19796123	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ8256				CP11CN	Synthetic construct		ILKKWPWWPWRRK-NH2	13	10898680	Antibacterial	Gram+ve	Experimentally Validated
