  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ246	22095934	P83247		Oxyopinin-1 	Oxyopes kitabensis 	Animalia (Arachnids)	FRGLAKLLKIGLKSFARVLKKVLPKAAKAGKALAKSMADENAIRQQNQ	48	11976325	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1803	71153772	Q8VBV2		Antimicrobial-like protein Bin-1b	Rattus norvegicus	Animalia (Mammals)	DIPPGIRNTVCFMQRGHCRLFMCRSGERKGDICSDPWNRCCVSSSIKNR	49		Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ2089	84028885	Q30KK4		Beta-defensin124	Pan troglodytes	Animalia (Mammals)	EFKRCWKGQGACRTYCTRQETYMHLCPDASLCCLSYALKPPPVPKHEYE	49		Antibacterial		Predicted
CAMPSQ2101	84028202	Q8NES8		Beta-defensin124	Homo sapiens 	Animalia (Mammals)	EFKRCWKGQGACQTYCTRQETYMHLCPDASLCCLSYALKPPPVPKHEYE	49		Antibacterial		Predicted
CAMPSQ2568	354991233	G8GZ64		Antifungal peptide 3  	Heliophila coronopifolia  	Viridiplantae	YCERSSGTWSGVCGNTDKCSSQCQRLEGAAHGSCNYVFPAHKCICYYPC	49	22032337	Antifungal		Experimentally Validated
CAMPSQ2569	354991229	G8GZ62		Antifungal peptide 1  	Heliophila coronopifolia  	Viridiplantae	YCERSSGTWSGVCGNSGKCSNQCQRLEGAAHGSCNYVFPAHKCICYYPC	49	22032337	Antifungal		Experimentally Validated
CAMPSQ2658	332692885	F6M2H9		Big defensin 1  	Crassostrea gigas	Animalia (Molluscs (Bivalvia))	QAQALLPIASYAGLTVSAPVFAALVTVYGAYALYRYNIRRRENSYPRIR	49	21980497	Antimicrobial		Predicted
CAMPSQ2659	332692881	F6M2H7		Big defensin 1  	Crassostrea gigas	Animalia (Molluscs (Bivalvia))	QAQALLPIASYAGLTVSAPVFAALVTVYGAYALYGYNIRRRENSYQRIR	49	21980497	Antimicrobial		Predicted
CAMPSQ2843				  Human beta defensin 4 	Homo sapiens	Animalia (Mammals)	FELDRICGYGTARCRKKCRSQEYRIGRCPNTYACCLRKWDESLLNRTKP	49	11481241	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2882		P02788	1Z6V	  Human lactoferricin 	Homo sapiens	Animalia (Mammals)	GRRRRSVQWCAVSQPEATKCFQWQRNMRKVRGPPVSCIKRDSPIQCIQA	49	11254635	Antimicrobial		Experimentally Validated
CAMPSQ3663		H1ZZ98		  Laterosporulin 	Brevibacillus sp. strain GI-9	Bacteria	ACQCPDAISGWTHTDYQCHGLENKMYRHVYAICMNGTQVYCRTEWGSSC	49	22403615	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ5171	208342477	B6DSR7		EntA (Fragment)	Enterococcus mundtii	Bacteria	VPTHSGKYYGNGVYCTKNKCTVDWAQGNYCIAGMSIGGFLGGAIPGKVQ	49		Antimicrobial		Predicted (Based on signature)
CAMPSQ6371	13605364	O85355		Butyrivibriocin	Butyrivibrio fibrisolvens	Bacteria	MNKELNALTNPIDEKELEQILGGGNGVIKTISHECHMNTWQFIFTCCS	48	10224011	Antimicrobial		Predicted (Based on signature)
CAMPSQ7511	808043092	Q40128	4UJ0	Flower-specific gamma-thionin-like protein/acidic protein (Uncharacterized protein)	Solanum lycopersicum	Viridiplantae	AQQICKAPSQTFPGLCFMDSSCRKYCIKEKFTGGHCSKLQRKCLCTKPC	49		Antimicrobial		Predicted
CAMPSQ7662		F6WQS4		Beta-defensin	Macaca mulatta 	Animalia (Mammals)	FIYIDECPSEYYNCRMKCNADEHAIRYCDDWTICCKLKNIEIEKLRRE	48		Antimicrobial		Predicted
CAMPSQ7697		W5NQ84		Beta-defensin	Ovis aries 	Animalia (Mammals)	RKSCWIIKGHCRKDCKSGEQIKKPCRNGDYCCVPSKTVSQPQRPTQTTT	49		Antimicrobial		Predicted
CAMPSQ7709		H0WKI9		Beta-defensin	Otolemur garnettii 	Animalia (Mammals)	EFKRCWKGQGICRTYCTRQESYMHLCPDASLCCLSYVFKPPRAPKNEYD	49		Antimicrobial		Predicted
CAMPSQ7767		A0A096NV12		Beta-defensin	Papio anubis 	Animalia (Mammals)	EFKRCWKGQGACRTYCTRQETYMHLCPDASLCCLSYALKPPPVPKTKYE	49		Antimicrobial		Predicted
CAMPSQ7901		Q30KS3		Beta-defensin	Canis familiaris 	Animalia (Mammals)	FFLKDTCSLEYLNCRMKCNLDEHAIKYCADWTICCKAKNTKFKRKKKW	48		Antimicrobial		Predicted
CAMPSQ7925		G1LQ67		Beta-defensin	Ailuropoda melanoleuca 	Animalia (Mammals)	KKKYPQYGSLDLRRECRKGNGRCRVECHESEIRIAFCIRPGTHCCLQK	48		Antimicrobial		Predicted
CAMPSQ7926		G3S5T2		Beta-defensin	Gorilla gorilla gorilla 	Animalia (Mammals)	RIEKCWNFRGSCCDECLKNERVYVFCVSGKLCCLKPKDQPHLPQHIKN	48		Antimicrobial		Predicted
CAMPSQ7933		G3QG52		Beta-defensin	Gorilla gorilla gorilla 	Animalia (Mammals)	KKKYPEYGSLDLRRECRMGNGRCKNQCHENEIRIAYFIRPGTHCCLQQ	48		Antimicrobial		Predicted
CAMPSQ7980		G7PGP3		Beta-defensin	Macaca fascicularis 	Animalia (Mammals)	EFKRCWKGQGACRTYCTRQETYMHLCPDASLCCLSYALKPPPVPKSKYE	49		Antimicrobial		Predicted
CAMPSQ7992		G1PYZ1		Beta-defensin	Myotis lucifugus 	Animalia (Mammals)	EFKRCWNGQGACRTYCTKYETFMHLCPDASLCCIPYGLKPAVPHKPENE	49		Antimicrobial		Predicted
CAMPSQ8018		G1SYB1		Beta-defensin	Oryctolagus cuniculus 	Animalia (Mammals)	EFKRCWQGQGACRIYCTKQESFMHLCPDASLCCLPYTLKPPLLPKVTDK	49		Antimicrobial		Predicted
