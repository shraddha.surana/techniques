  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ915	75139850	Q7M1F4, P0C8Y5		Defensin AMP1	Heuchera sanguinea	Viridiplantae	DGVKLCDVPSGTWSGHCGSSSKCSQQCKDREHFAYGGACHYQFPSVKCFCKRQC	54	7628617	Antifungal		Experimentally Validated
CAMPSQ1342	71152315	Q8R2I7		Beta-defensin 11	Mus musculus	Animalia (Mammals)	DLKHLILKAQLARCYKFGGFCYNSMCPPHTKFIGNCHPDHLHCCINMKELEGST	54		Antimicrobial		Predicted
CAMPSQ1666	56541090	Q5M9M1		Hepcidin antimicrobial peptide 2	Mus musculus	Animalia (Mammals)	QMRQTTELQPLHGEESRADIAIPMQKRRKRDINFPICRFCCQCCNKPSCGICCE	54		Antimicrobial		Predicted
CAMPSQ2080	84028898	Q30KJ4		Beta-defensin135	Pan troglodytes	Animalia (Mammals)	GPNVYIQKIFASCWRLQGTCRPKCLKNETISYFGVILYICGCVNPKYLPILTGK	54		Antibacterial		Predicted
CAMPSQ2438	326909297	F8THJ3		Hepcidin antimicrobial peptide  	Equus asinus	Animalia (Mammals)	LADLQTQDTAGMAGAVAGLMPGLHQLRRRDTHFPICTLCCGCCNKQKCGWCCKT	54		Antimicrobial		Predicted
CAMPSQ2634	307000397	E2J6L9		Hepcidin precursor	Oreochromis niloticus	Animalia (Pisces)	LEEPMSMDYPAAAHEEASVDSWKMLYNSRHKRGIKCRFCCGCCTPGICGVCCRF	54		Antimicrobial		Predicted
CAMPSQ3083		P56552	1BRZ	  Brazzein 	Pentadiplandra brazzeana	Viridiplantae	QDKCKKVYENYPVSKCQLANQCNYDCKLDKHARSGECFYDEKRNLQCICDYCEY	54	15118082	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3819				  CATH_BRALE 	Brachymystax lenok	Animalia (Pisces)	RRSKARGGSRGSKMGRKDSKGGSRGRPGSGSRPGGGSSIAGASRGDRGGTRNA	53	23390641	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ3892		P0A313		Bacteriocin lactococcin-A 	Lactococcus lactis subsp. cremoris 	Bacteria	KLTFIQSTAAGDLYYNTNTHKYVYQQTQNAFGAAANTIVNGWMGGAAGGFGLHH	54	1904860	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ4895	158324010	A8VTA8		Lactoferrin (Fragment)	Bos indicus 	Animalia (Mammals)	LCLAAPRKNVRWCTISQPEWFKCRRWQWRMKKLGAPSITCVRRAFALECIRAIA	54		Antimicrobial		Predicted (Based on signature)
CAMPSQ4931	161177137	A9QUB1		Lactoferrin (Fragment)	Bubalus bubalis 	Animalia (Mammals)	LCLAAPRKNVRWCTISQPEWLKCHRWQWRMKKLGAPSITCVRRAFVLECIRAIT	54		Antimicrobial		Predicted (Based on signature)
CAMPSQ4935	170672391	A9YTV6		Lactoferrin (Fragment)	Bos mutus grunniens 	Animalia (Mammals)	LCLAAPRKNVRWCTISQPEWFKCRRWQWRMKKLGAPSITCVRRAFALECIRAIA	54		Antimicrobial		Predicted (Based on signature)
CAMPSQ6035	339305349	G9CG71		Lactotransferrin (Fragment)	Canis familiaris 	Animalia (Mammals)	LCLAAPRKNVRWCTTSKAEAKKCSKFQVNMKKVGGPIVSCTRKASRQECIQAIK	54		Antimicrobial		Predicted (Based on signature)
CAMPSQ6284	441419565	L7T7I6		Hepcidin (Fragment)	Glossophaga soricina 	Animalia (Mammals)	TKQLADHQTQDTARASAGLMSGLQRLRRRDTHFPICIFCCGCCHKSKCGICCKT	54		Antimicrobial		Predicted (Based on signature)
CAMPSQ6288	441419567	L7T9T8		Hepcidin (Fragment)	Carollia perspicillata 	Animalia (Mammals)	TKELADLQTQDTARASAGLTSGLQRLRKRDAHFPICMFCCGCCHKSKCGICCKT	54		Antimicrobial		Predicted (Based on signature)
CAMPSQ6291		L8A924		Enterocin P	Enterococcus faecium NRRL B-2354	Bacteria	MTNFGTKVDAATRSYDNGIYCNNSKCWVNWGEAKENIAGIVISGWASGLAGMGH	54		Antimicrobial		Predicted (Based on signature)
CAMPSQ7080	283825781			Nisin U [synthetic construct]. 	Synthetic construct  		MSTKDFNLDLVSVSKKDSGASPRITSKSLCTPGCKTGILTGCPLKTATCGCHFG	54		Antimicrobial		Predicted (Based on signature)
CAMPSQ7500	808698026	A0A0F6QDU1		Cathelicidin-derived antimicrobial peptide 1d	Oncorhynchus mykiss 	Animalia (Pisces)	MKMKVQVRSLILLAVAVLQVRSRRSKVRICSRGKNCVSFNDEFIRDHSDGNRFA	54	25876762	Antimicrobial		Predicted
CAMPSQ7670		F6RBA5		Beta-defensin	Callithrix jacchus 	Animalia (Mammals)	GPNAYIQKAFASCWRLHGTCRSKCLKNEEYHILCDTTYLCCASPKQLPILTGK	53		Antimicrobial		Predicted
CAMPSQ7713		H2PCS3		Beta-defensin (Fragment)	Pongo abelii 	Animalia (Mammals)	RGKFNEISECPNGSCRDFCLKTEIHVGRCLNSRPCCLPLGHQPRIESTTPKKEG	54		Antimicrobial		Predicted
CAMPSQ7737		H2RAB6		Beta-defensin (Fragment)	Pan troglodytes 	Animalia (Mammals)	ARGKFKEICERPDGSCRDFCLETEIHVGRCLNSRPCCLPLGHQPRIESTTPKKD	54		Antimicrobial		Predicted
CAMPSQ7809		G3U1N7		Beta-defensin (Fragment)	Loxodonta africana 	Animalia (Mammals)	RSGPNIYIRRFFNTCWRTKGVCRKSCFRGELYHIFCDTSHLCCINKKFMPIQVG	54		Antimicrobial		Predicted
CAMPSQ7920		G1LI49		Beta-defensin	Ailuropoda melanoleuca 	Animalia (Mammals)	GPNMYIRRLFSTCWRTKGVCKKSCGKSEIYHIFCDSAHLCCIDKKYLPVEFGK	53		Antimicrobial		Predicted
CAMPSQ8006		G1PIK7		Beta-defensin	Myotis lucifugus 	Animalia (Mammals)	AKVMKCWRSLGKCRTTCEEGEVFYILCNAEAKCCVNPKYVPVNTKSSNSAGSLG	54		Antimicrobial		Predicted
CAMPSQ8087		G5BMG5		Beta-defensin	Heterocephalus glaber 	Animalia (Mammals)	PVRSDLEVDRICGYGTARCHRKCKSQEHKIARCPNTYACCLKTWAHSSLNIKRP	54		Antimicrobial		Predicted
