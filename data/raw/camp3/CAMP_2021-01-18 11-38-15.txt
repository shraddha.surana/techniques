  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ276	2494919	P80930		Antimicrobial peptide eNAP-1	Equus caballus 	Animalia (Mammals)	DVQCGEGHFCHDXQTCCRASQGGXACCPYSQGVCCADQRHCCPVGF	46	1639474	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ793				Salvic	Homo sapiens 	Animalia (Mammals)	MHDFWVLWVLLEYIYNSACSVLSATSSVSSRVLNRSLQVKVVKITN	46	Reference: Kim Y.S., Lee S.K., Park S.C., Chi J.G., Chung S.I. (2005) Cloning and identification of a new antimicrobial peptide, salvic, from human salivary gland. International symposium of Maxillofacial and Oral Regenerative Biology	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1473	112030734	Q0MRQ0		Milk lysozyme	Bos indicus x Bos taurus (hybrid cattle)	Animalia (Mammals)	MKALLILGLLLFSVAVQGKVFERCELARSLKRFGMDNFRGISLAN	45		Antimicrobial		Predicted
CAMPSQ1474	112030759	Q0MRP9		Milk lysozyme 	Bos indicus x Bos taurus (hybrid cattle)	Animalia (Mammals)	MKALLILGLLLFSVAVQGKVFERCELARSLKRFGMDNFRGITLAN	45		Antimicrobial		Predicted
CAMPSQ1478	114386730	A7J1K5		Hyasin 	Hyas araneus	Animalia (Crustaceans (Malacostraca))	MRLLWLLVAMVVTVLAAATPTAAWQRPLTRPRPFSRPRPYRPNYG	45		Antimicrobial		Predicted
CAMPSQ1489	1170248	P80359		Pseudo-hevein	Hevea brasiliensis	Viridiplantae	EQCGRQAGGKLCPNNLCCSQYGWCGSSDDYCSPSKNCQSNCKGGG	45		Antifungal		Predicted
CAMPSQ2068	123780119	Q32ZI0		Beta-defensin11	Rattus norvegicus	Animalia (Mammals)	FLRRSVSGFQECHSKGGYCYRYYCPRPHRRLGSCYPYAANCCRRRR	46		Antibacterial		Predicted
CAMPSQ2073	123779970	Q30KP6		Beta-defensin17	 Mus musculus	Animalia (Mammals)	KKSYPEYGSLDLRKECKMRRGHCKLQCSEKELRISFCIRPGTHCCM	46		Antibacterial		Predicted
CAMPSQ2280				Gaegurin-6-RN antimicrobial peptide	Rana nigrovittata	Animalia (Amphibians)	FPMKKSLLLIFFLGTINLSFCEEERNAEEEKRDGDDEMDVEVQKR	45	19778602	Antimicrobial		Predicted
CAMPSQ2281	160556630	C0ILJ4		Gaegurin-6-RN antimicrobial peptide	Rana nigrovittata	Animalia (Amphibians)	FTMKKSLLLIFFLGTINLSLCEEERNAEEEKRDGDDEMDVEVQKR	45	19778602	Antimicrobial		Predicted
CAMPSQ2547	310695516	E3SYM1		Esculentin-1-RA8 peptide precursor  	Odorrana andersonii	Animalia (Amphibians)	PLKKPLLLIVLFGIISLSLCEQERAADEDEGSEIKRGLFSKFAGK	45		Antimicrobial		Predicted
CAMPSQ2548	310695512	E3SYL9		Esculentin-1-RA6 peptide precursor  	Odorrana andersonii	Animalia (Amphibians)	TLKKPLLLIVLLGIISLSLCEQERAADEDEGSEIKRGLFSKFAGK	45		Antimicrobial		Predicted
CAMPSQ2771		Q28880		  Lingual antimicrobial peptide 	Bos taurus	Animalia (Mammals)	GFTQGVRNSQSCRRNKGICVPIRCPGSMRQIGTCLGAQVKCCRRK	45	7886453	Antibacterial, Antifungal		Experimentally Validated
CAMPSQ2826				  Esculentin-1PLa   	Rana palustris	Animalia (Amphibians)	GLFPKINKKKAKTGVFNIIKTVGKEAGMDLIRTGIDTIGCKIKGEC	46	11087945	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3261				  WAMP-1b 	Triticum kiharae	Viridiplantae	AQRCGDQARGAKCPNCLCCGKYGFCGSGDAYCGAGSCQSQCRGCR	45	19583772	Antifungal		Experimentally Validated
CAMPSQ5403	228736797	C2Z9I4		Lantibiotic epidermin	Bacillus cereus AH1272	Bacteria	MINEKNLFDLDVQVTTASGDVDPQITSISACTPGCGNTGSFNSFCC	46		Antimicrobial		Predicted (Based on signature)
CAMPSQ5404	228746274			Lantibiotic epidermin	Bacillus mycoides DSM 2048	Bacteria	MINEKNLFDLDVQVTTASGDVDPQITSISACTPGCGNTGSFNSFCC	46		Antimicrobial		Predicted (Based on signature)
CAMPSQ5667		D5E4G6		Antibiotic protein, putative	Bacillus megaterium (strain ATCC 12872 / QMB1551)	Bacteria	MNNVKNLFDLDVQVTTASSDVDPQITSVSLCTPGCGDTGSWNSFCC	46	21705586	Antimicrobial		Predicted (Based on signature)
CAMPSQ6422	400261049	P60057		Hellethionin-D	Helleborus purpurascens 	Viridiplantae	KSCCRNTLARNCYNACRFTGGSQPTCGILCDCIHVTTTTCPSSHPS	46		Antimicrobial		Predicted (Based on signature)
CAMPSQ6937	558693335	V5MDE9		Antibiotic protein, putative	Bacillus thuringiensis YBT-1518	Bacteria	MINEKNLFDLDVQVTTATGDVDPQITRISACTPGCGNTGSFNSFCC	46		Antimicrobial		Predicted (Based on signature)
CAMPSQ7657		I0FQI0		Beta-defensin	Macaca mulatta 	Animalia (Mammals)	QRCWNLYGKCRHRCSKKERVYVYCLNNKMCCVKPKYQPKEKWWPF	45		Antimicrobial		Predicted
CAMPSQ7665		F6PV70		Beta-defensin	Callithrix jacchus 	Animalia (Mammals)	VFDERCHRLKGTCVGYCKKNEEIIALCQKSLKCCLTIQPCGKIRE	45		Antimicrobial		Predicted
CAMPSQ7675		M3Z2J3		Beta-defensin	Mustela putorius furo 	Animalia (Mammals)	QKCWNLHGRCRQKCSKRERAYVYCTNNKLCCVKPKFQPREKLWPF	45		Antimicrobial		Predicted
CAMPSQ7868		L8I003		Beta-defensin	Bos mutus	Animalia (Mammals)	ALVDPERCSKLYGQCKKRCARYEKQIELCLSPSKICCIERAFEDD	45		Antimicrobial		Predicted
CAMPSQ7916		G1LQ69		Beta-defensin	Ailuropoda melanoleuca 	Animalia (Mammals)	SLVDPDRCTKIFGNCRRRCFKYEKQIDICFSPSKICCIERLFEED	45		Antimicrobial		Predicted
