  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ384	3913370	P81252		Caerin-1.9	Litoria chloris 	Animalia (Amphibians)	GLFGVLGSIAKHVLPHVVPVIAEKL	25	9516047	Antibacterial		Experimentally Validated
CAMPSQ1668	156530229	A7XEH6		Hepcidin	Pan troglodytes	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCKT	25		Antimicrobial		Predicted
CAMPSQ1669	156530231	A7XEH7		Hepcidin	Pongo pygmaeus	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCKT	25		Antimicrobial		Predicted
CAMPSQ1670	156530233	A7XEH8		Hepcidin	Papio papio	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCRT	25		Antimicrobial		Predicted
CAMPSQ1671	156530235	A7XEH9		Hepcidin	Presbytis obscura	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCRT	25	18321376	Antimicrobial		Predicted
CAMPSQ1672	156530237	A7XEI0		Hepcidin	Presbytis melalophos	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCRT	25	18321376	Antimicrobial		Predicted
CAMPSQ1673	156530239	A7XEI1		Hepcidin	Presbytis cristata	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCRT	25		Antimicrobial		Predicted
CAMPSQ1674	156530241	A7XEI2		Hepcidin	Macaca fuscata	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCRT	25		Antimicrobial		Predicted
CAMPSQ1675	156530243	A7XEI3		Hepcidin	Macaca fascicularis	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCRT	25	18321376	Antimicrobial		Predicted
CAMPSQ1676	156530245	A7XEI4		Hepcidin	Gorilla gorilla	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCKT	25	18321376	Antimicrobial		Predicted
CAMPSQ1677	156530247	A7XEI5		Hepcidin	Callithrix jacchus	Animalia (Mammals)	DTHFPICIFCCGCCRQSNCGMCCKT	25		Antimicrobial		Predicted
CAMPSQ1678	156530249	A7XEI6		Hepcidin	Chlorocebus aethiops	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCRT	25		Antimicrobial		Predicted
CAMPSQ1679	156530251	A7XEI7		Hepcidin	Ateles fusciceps	Animalia (Mammals)	DTHFPICIFCCGCCRQPNCGMCCKT	25		Antimicrobial		Predicted
CAMPSQ1680	156530253	A7XEI8		Hepcidin	Nomascus concolor	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCKT	25		Antimicrobial		Predicted
CAMPSQ1681	156530255	A7XEI9		Hepcidin	Hylobates lar	Animalia (Mammals)	DTHFPICIFCCGCCHRSKCGMCCKT	25		Antimicrobial		Predicted
CAMPSQ2701	301072337	E0X9N1		Hepcidin  	Ovis aries	Animalia (Mammals)	DTHFPICIFCCGCCRKGTCGICCKT	25	20723953	Antimicrobial		Predicted
CAMPSQ2947				  Plasticin-S1 	Phyllomedusa sauvagei	Animalia (Amphibians)	GLVSDLLSTVTGLLGNLGGGGLKKI	25	18644413	Antimicrobial		Predicted
CAMPSQ3216				  Ocellatin-V1 	Leptodactylus validus	Animalia (Amphibians)	GVVDILKGAGKDLLAHALSKLSEKV	25	18501993	Antimicrobial		Experimentally Validated
CAMPSQ3217				  Ocellatin-V2 	Leptodactylus validus	Animalia (Amphibians)	GVLDILKGAGKDLLAHALSKISEKV	25	18501993	Antimicrobial		Experimentally Validated
CAMPSQ3457				  Maximin 31 	Bombina maxima	Animalia (Amphibians)	GIGGALLSAGKSALKGLAKGLAEHF	25	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3460				  Maximin 41 	Bombina maxima	Animalia (Amphibians)	GIGGALLSVGKSALKGLTKGLAEHF	25	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3467				  Maximin 77 	Bombina maxima	Animalia (Amphibians)	GIGGALLSAGKSALKGLAKGLAEHL	25	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3984		P86520		Soybean toxin 27 kDa chain 	Glycine max 	Viridiplantae	ADPTFGFTPLGLSEKANLQIMKAYD	25	18328522	Antifungal		Experimentally Validated
CAMPSQ4489		P86502		Caerin-1.10	Litoria rothii	Animalia (Amphibians)	GLLSVLGSVAKHVLPHVVPVIAEKL	25	16124032, 19539637	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4491		P86503		Caerin-1.16	Litoria rothii	Animalia (Amphibians)	GLFSVLGAVAKHVLPHVVPVIAEKL	25	16124032, 19539637	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
