  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ51	115544	P06833		Caltrin precursor 	Bos taurus 	Animalia (Mammals)	SDEKASPDRHHRFSLSRYAKLANRLSKWIGNRGNRLANPKLLETFKSV	48	8485159	Antibacterial,Antifungal	Gram-ve	Experimentally Validated
CAMPSQ312	27806807	P06833		Seminalplasmin 	Bos taurus 	Animalia (Mammals)	SDEKASPDKHHRFSLSRYAKLANRLANPKLLETFLSKWIGDRGNRSVK	48	16453469	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ536	584890	P38580	1CW5,1RY3	Bacteriocin carnobacteriocin B2 	Carnobacterium maltaromaticum	Bacteria	VNYGNGVSCSKTKCSVNWGQAFQERYTAGINSFVSGVASGAGSIGRRP	48	8163526	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ1336	106880054	Q1EN14		Dermaseptin S10	Phyllomedusa sauvagii	Animalia (Amphibians)	EEEKREGENEKEQEDDNQSEEKRGLVSDLLSTVTGLLGNLGGGGLKKI	48		Antimicrobial		Predicted
CAMPSQ1654	11386627	O24331		Cysteine-rich antifungal protein 4	Raphanus sativus	Viridiplantae	QKLCERSSGTWSGVCGNNNACKNQCINLEGARHGSCNYIFPYHRCICY	48	7780308	Antifungal		Experimentally Validated
CAMPSQ1932	37076854	P59861		Beta-defensin131	Homo sapiens 	Animalia (Mammals)	FISNDECPSEYYHCRLKCNADEHAIRYCADFSICCKLKIIEIDGQKKW	48		Antibacterial		Predicted
CAMPSQ2096	84028872	Q30KL6		Beta-defensin111	Pan troglodytes	Animalia (Mammals)	KMKYPEYGSLDLRRECRMGNGRCKNQCHENEIRIAYCIRPGTHCCLQQ	48		Antibacterial		Predicted
CAMPSQ2097	84028871	Q30KQ9		Beta-defensin111	Homo sapiens 	Animalia (Mammals)	KKKYPEYGSLDLRRECRIGNGQCKNQCHENEIRIAYCIRPGTHCCLQQ	48		Antibacterial		Predicted
CAMPSQ2595	313471290	P86407		Meucin-49 	Mesobuthus eupeus	Animalia (Arachnids)	KFGSFIKRMWRSKLAKKLRAKGKELLRDYANRVLSPEEEAAAPAPVPA	48		Antimicrobial		Experimentally Validated
CAMPSQ3362				  SpStrongylocin 1 	Strongylocentrotus purpuratus	Animalia (Echinoidea)	IFNSIYHRKCVVKNRCETVSGHKTCKDLTCCRAVIFRHERPEVCRPST	48	19852980	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4906	82698056	A9CBH5		Ranatuerin 2Pb (Fragment)	Lithobates pipiens 	Animalia (Amphibians)	ADDDQGEVQQEVKRSFLTTVKKLVTNLAALAGTVIDTIKCKVTGGCRT	48	17938991	Antimicrobial		Predicted (Based on signature)
CAMPSQ7715		H2PJB8		Beta-defensin	Pongo abelii 	Animalia (Mammals)	KKKYPEYGSLDLRRECRMGNGRCKNQCHENEIRIAYCIRPGTHCCLQQ	48		Antimicrobial		Predicted
CAMPSQ7721		H2P1J5		Beta-defensin	Pongo abelii 	Animalia (Mammals)	STEKCWNFRGSCRDECLKNERFYVFCVSGKLCCLKPKDQPHLPQHTKN	48		Antimicrobial		Predicted
CAMPSQ7738		H2RII7		Beta-defensin	Pan troglodytes 	Animalia (Mammals)	FISNDECPSEYYHCRLKCNADEHAIRYCADFSICCKLKIIEIDGQKKW	48		Antimicrobial		Predicted
CAMPSQ7756		M3WTT6		Beta-defensin	Felis catus 	Animalia (Mammals)	RKKYPHYGSLDLRRECRKGNGRCKLECQENVIRIAYCMRPATHCCLQK	48		Antimicrobial		Predicted
CAMPSQ7765		A0A096NJ15		Beta-defensin	Papio anubis 	Animalia (Mammals)	KKKYPEYGSLDLRRECRMGNGRCKNQCHENEIRIAYCIRPGTHCCLQQ	48		Antimicrobial		Predicted
CAMPSQ7793		F6QHR5		Beta-defensin	Equus caballus 	Animalia (Mammals)	KKRYPQYGSLDLRRECRRGNGRCKIQCPENEIRIAYCMRPATHCCLQK	48		Antimicrobial		Predicted
CAMPSQ7806		G3SL47		Beta-defensin	Loxodonta africana 	Animalia (Mammals)	EFKRCWKGQGACRNYCTRQEAYMHLCPDASLCCLPYTLKPPPRPKGEY	48		Antimicrobial		Predicted
CAMPSQ7823		A0A0D9RKD9		Beta-defensin	Chlorocebus sabaeus 	Animalia (Mammals)	KKKYPEYGSLDLRRECRMGNGRCKNQCHENEIRIAYCIRPGTHCCLQQ	48		Antimicrobial		Predicted
CAMPSQ7826		A0A0D9RWL8		Beta-defensin	Chlorocebus sabaeus 	Animalia (Mammals)	FIYIDECPSEYYNCRMKCNADEHAIRYCDDWTICCKLKNIEIEKLKRE	48		Antimicrobial		Predicted
CAMPSQ7986		G7P4U6		Beta-defensin	Macaca fascicularis 	Animalia (Mammals)	KKKYPEYGSLDLRRECRMGNGRCKNQCHENEIRIAYCIRPGTHCCLQQ	48		Antimicrobial		Predicted
CAMPSQ7994		G1P6P1		Beta-defensin	Myotis lucifugus 	Animalia (Mammals)	RKKFPQYGSVDMRKLCKKGNGRCKVECHETEIRIAYCIRPGTHCCLQR	48		Antimicrobial		Predicted
CAMPSQ8009		G1Q8W5		Beta-defensin	Myotis lucifugus 	Animalia (Mammals)	FAFNSRCSSLYHKCRMKCNHDEYSVRYCSDSSICCRIKKVDLNKRKKW	48		Antimicrobial		Predicted
CAMPSQ8050		A0A091E1Z7		Beta-defensin	Fukomys damarensis 	Animalia (Mammals)	DTQRCWNLLGKCRHRCSKKDSVYVYCTNGKMCCVKPKYQPKPKPSWLF	48		Antimicrobial		Predicted
CAMPSQ8082		G5BMG4		Beta-defensin	Heterocephalus glaber 	Animalia (Mammals)	EFFDAKCHKLKGKCKSSCQKNEELVAFCQKSLKCCLVLQTCELNESNS	48		Antimicrobial		Predicted
