  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ679				MBMAP28	Bos taurus 	Animalia (Mammals)	GGLRSLGRKILRAWKKYGPQATPATRQ	27	8910461	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1177	10638878	Q9DET7		Bombinin-like peptide 7, BPL-7	Bombina orientalis	Animalia (Amphibians)	GIGGALLSAGKSALKGLAKGLAEHFAN	27		Antibacterial		Predicted
CAMPSQ1188	18202396	P82286		Bombinin-like peptide 2	 Bombina variegata	Animalia (Amphibians)	GIGASILSAGKSALKGFAKGLAEHFAN	27		Antimicrobial		Predicted
CAMPSQ2935				  Pilosulin 2 	Myrmecia pilosula	Animalia (Insects)	IDWKKVDWKKVSKKTCKVMLKACKFLG	27	8605256	Antimicrobial		Experimentally Validated
CAMPSQ2969				  Dermaseptin-C3 	Agalychnis litodryas	Animalia (Amphibians)	SVLSTITDMAKAAGRAALNAITGLVNQ	27	18644413	Antimicrobial		Predicted
CAMPSQ3455				  Maximin 15 	Bombina maxima	Animalia (Amphibians)	GIGTKILGGVKAALKGALKELASTYVN	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3456	169730572	C3RTJ9		  Maximin 28 	Bombina maxima	Animalia (Amphibians)	GIGTKFLGGVKTALKGALKELASTYVN	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3458				  Maximin 32 	Bombina maxima	Animalia (Amphibians)	GIGGKILGGLKTALKGAAKELASTYLH	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3459				  Maximin 39 	Bombina maxima	Animalia (Amphibians)	GIGTKFLGGVKTALKGALKELAFTYVN	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3461				  Maximin 45 	Bombina maxima	Animalia (Amphibians)	GIGGKILGGLRTALKGAAKELAATYLH	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3463				  Maximin 49 	Bombina maxima	Animalia (Amphibians)	GIGGVLLSAGKAALKGLTKVLAEKYAN	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3464				  Maximin 63 	Bombina maxima	Animalia (Amphibians)	GIGGVLLGAGKATLKGLAKVLAEKYAN	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3466				  Maximin 42 	Bombina maxima	Animalia (Amphibians)	SIGAKILGGVKTFFKGALKELAFTYLQ	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3468				  Maximin 78 	Bombina maxima	Animalia (Amphibians)	GIGGALLSVGKLALKGLANVLADKFAN	27	21338048	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4481		C5J897		Probable Antimicrobial peptide Con10	Opisthacanthus cayaporum	Animalia (Arachnids)	FWSFLVKAASKILPSLIGGGDDNKSSS	27	19379768	Antimicrobial		Predicted
CAMPSQ4995	449061927	B3EWV1		Short cationic peptide-3a	Cupiennius salei 	Animalia (Arachnids)	GFGSLFKFLGKKLLKTVAKQAAKKQME	27		Antimicrobial		Predicted (Based on signature)
CAMPSQ4996	449061928	B3EWV2		Short cationic peptide-3b	Cupiennius salei 	Animalia (Arachnids)	GFGSLFKFLGKKVLKTVAKQAAKKQME	27		Antimicrobial		Predicted (Based on signature)
CAMPSQ4997	449061929	B3EWV3		Short cationic peptide-3c	Cupiennius salei 	Animalia (Arachnids)	GFGSLFKFLGKKLAKTVAKQAAKKQME	27		Antimicrobial		Predicted (Based on signature)
CAMPSQ4998	449061930	B3EWV4		Short cationic peptide-3d	Cupiennius salei 	Animalia (Arachnids)	GFGALFKFLAKKVAKTVAKQVAKKQME	27		Antimicrobial		Predicted (Based on signature)
CAMPSQ4999	449061932	B3EWV6		Short cationic peptide-4a	Cupiennius salei 	Animalia (Arachnids)	GFGMLFKFLAKKVAKKLVSHVAQKQLE	27		Antimicrobial		Predicted (Based on signature)
CAMPSQ5000	449061933	B3EWV7		Short cationic peptide-4b	Cupiennius salei 	Animalia (Arachnids)	VYGMLFKFLAKKVAKKLISHVAKKQLQ	27		Antimicrobial		Predicted (Based on signature)
CAMPSQ5774	313585018	E5LCG3		Enterocin A (Fragment)	Enterococcus faecium 	Bacteria	TVDWAKATTCIAGMSIGGFLGGAFPGK	27		Antimicrobial		Predicted (Based on signature)
CAMPSQ6554	77819750	Q30CA4		Cyclotide A (Fragment)	Hybanthus vernonii subsp. vernonii	Viridiplantae	CGETCLFIPCLTSVFGCSCKNRGCYKI	27	16199617	Antimicrobial		Predicted (Based on signature)
CAMPSQ6558	77819742	Q30CA8		Cyclotide P (Fragment)	Hybanthus floribundus subsp. floribundus	Viridiplantae	CVWIPCISGIAGCSCKNKVCYLNSLDN	27	16199617	Antimicrobial		Predicted (Based on signature)
CAMPSQ7557		C0HJF7		Trypsin inhibitor 1 (JcTI-I)	Jatropha curcas 	Viridiplantae	VRDICKKEAERQDLSSCENYITQRRGY	27	24523715	Antibacterial	Gram+ve	Experimentally Validated
