  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ89	122056007	P84860		Brevinin-2GHa 	Rana guentheri	Animalia (Amphibians)	GFSSLFKAGAKYLLKSVGKAGAQQLACKAANNCA	34	16979798	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ308	266446	P29559	1WCO	Lantibiotic nisin-Z	Lactococcus lactis subsp. lactis 	Bacteria	ITSISLCTPGCKTGALMGCNMKTATCNCSIHVSK	34	1935953	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ332	29337160, 740957	P24302		Dermaseptin-1 	Phyllomedusa sauvagii	Animalia (Amphibians)	ALWKTMLKKLGTMALHAGKAALGAAADTISQGTQ	34	9614066 , 1909573	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ341	3024115	P56425		Antibacterial peptide BMAP-34 precursor	Bos taurus 	Animalia (Mammals)	GLFRRLRDSIRRGQQKILEKARRIGERIKDIFRG	34	9409740	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ563	6225740	P81612		Mytilin-A	Mytilus edulis 	Animalia (Molluscs (Bivalvia))	GCASRCKAKCAGRRCKGWASASFRGRCYCKCFRC	34	8702979	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ564	6225741	P81613	2EEM	Mytilin-B	Mytilus edulis 	Animalia (Molluscs (Bivalvia))	SCASRCKGHCRARRCGYYVSVLYRGRCYCKCLRC	34	8702979, 17628674	Antibacterial,Antiviral		Experimentally Validated
CAMPSQ571	66864905	Q863C8		Canine beta-defensin 	Canis lupus familiaris	Animalia (Mammals)	KCWNLRGSCREKCIKNEKLYIFCTSGKLCCLKPK	34	15845463	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ601	728981	P40839 , P84842		Brevinin-2Ec	Rana esculenta (edible frog )	Animalia (Amphibians)	GILLDKLKNFAKTAGKGVLQSLLNTASCKLSGQC	34	8163497	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ819	110832851	Q7WYZ9		Enterocin Q 	Enterococcus faecium 	Bacteria	MNFLKNGIAKWMTGAELQAYKKKYGCLPWEKISC	34	Reference: Patel S, Stott I P, Bhakoo M, Elliott P (1988) Patenting computer-designed peptides. Journal of Computer-Aided Molecular Design, 12: 543–556.


	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ1107	205829605	P82592		Cecropin-A	Aedes aegypti	Animalia (Insects)	GGLKKLGKKLEGAGKRVFNAAEKALPVVAGAKAL	34	10400619	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1621	118417	P07468		Neutrophil antibiotic peptide NP-3B	Oryctolagus cuniculus	Animalia (Mammals)	GRCVCRKQLLCSYRERRIGDCKIRGVRFPFCCPR	34		Antimicrobial		Predicted
CAMPSQ1630	62286498	Q5G862		Neutrophil defensin 4	Pan troglodytes	Animalia (Mammals)	VCSCRLVFCRRTELRVGNCLIGGVSFTYCCTRVD	34		Antimicrobial		Predicted
CAMPSQ1825	114149280	P84598		Dermaseptin-3	Phyllomedusa hypochondrialis	Animalia (Amphibians)	ALWKDVLKKIGTVALHAGKAAFGAAADTISQGGS	34		Antimicrobial		Predicted
CAMPSQ1836	209572849	P0C8A3		Defensin-A3	Ornithorhynchus anatinus	Animalia (Mammals)	RIITCSCRTFCFLGERISGRCYQSVFIYRLCCRG	34		Antimicrobial		Predicted
CAMPSQ1890	123781374	Q45VN2		Defensin-related cryptdin-20	 Mus musculus	Animalia (Mammals)	SRDLICYCRKGGCNRGEQVYGTCSGRLLYCCPRR	34		Antimicrobial		Predicted
CAMPSQ2134	46395902	Q8MUF4		Cecropin-B	Anopheles gambiae	Animalia (Insects)	APRWKFGKRLEKLGRNVFRAAKKALPVIAGYKAL	34		Antibacterial	Gram+ve, Gram-ve	Predicted
CAMPSQ2135	46395787	Q86PR6		Cecropin-A	Culex pipiens pipiens	Animalia (Insects)	GGLKKFGKKLEGVGKRVFKASEKALPVVTGFKAL	34		Antibacterial	Gram+ve, Gram-ve	Predicted
CAMPSQ2385	318055893	E7EKH9		Esculentin-2LTb-SN1 antimicrobial peptide precursor  	Hylarana spinulosa	Animalia (Amphibians)	TMKKSLLFFFFLGTISLSLCEQERGADEDDGVEE	34		Antimicrobial		Predicted
CAMPSQ2648	383465553	H9ZGM8		Prepro-beta-defensin 1  	Sus scrofa	Animalia (Mammals)	VSCLRNKGVCMPGKCAPKMKQIGTCGMPQVKCCK	34		Antimicrobial		Predicted
CAMPSQ3147				  P9 	Cervus elaphus	Animalia (Mammals)	RFIPPILRPPVRPPFRPPFRPPFRPPPIIRFFGG	34	16011891	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3161				  Defr1 	Mus musculus	Animalia (Mammals)	DPVTYIRNGGICQYRCIGLRHKIGTCGSPFKCCK	34	12226710	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3623				  Chensinin-1CEb 	Rana chensinensis	Animalia (Amphibians)	IGVIKLSLCEEERNADEEKRRDDPDEMDVEVEKR	34	21303203	Antimicrobial		Experimentally Validated
CAMPSQ3910		Q7YT39		M-theraphotoxin-Gr1a 	Grammostola rosea 	Animalia (Arachnids)	GCLEFWWKCNPNDDKCCRPKLKCSKLFKLCNFSF	34	16376854	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ6963	253723278			Chain A, Solution Structure Of Cb1a, A Novel Anticancer Peptide  Derived From Natural Antimicrobial Peptide Cecropin B. 	Hyalophora cecropia	Animalia (Insects)	KWKVFKKIEKKWKVFKKIEKAGPKWKVFKKIEKX	34		Antimicrobial		Predicted (Based on signature)
CAMPSQ7512	810515334	A6YB85	2MXQ 	Paneth cell-specific alpha-defensin 1	Equus caballus	Animalia (Mammals)	SCTCRRAWICRWGERHSGKCIDQKGSTYRLCCRR	34	17620056	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
