  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ678		P83428		Locusta migratoria 	Locusta migratoria	Animalia (Insects)	ATTGCSCPQCIIFDPICASSYKNGRRGFSSGCHMRCYNRCHGTDYFQISKGSKCI	55		Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ1239	25090979	Q962B1		Penaeidin-3m 	Litopenaeus setiferus	Animalia (Crustaceans (Malacostraca))	QGCKGPYTRPILRPYVRPVVSYNACTLSCRGITTTQARSCCTRLGRCCHVAKGYS	55		Antibacterial,Antifungal		Predicted
CAMPSQ1240	25090978	Q962B0		Penaeidin-3n 	Litopenaeus setiferus	Animalia (Crustaceans (Malacostraca))	QGYKGPYTRPILRPYVRPVVSYNACTLSCRGITTTQARSCSTRLGRCCHVAKGYS	55		Antibacterial,Antifungal		Predicted
CAMPSQ1267	25089852	O61281		CECA2_DROYA Cecropin-A2	Drosophila yakuba 	Animalia (Insects)	VFVALILAIAIGQSEAGWLKKIGKKIERVGQHTRDATIQGLGIAQQAANVAATAR	55	9553148	Antibacterial	Gram+ve, Gram-ve	Predicted
CAMPSQ1475	112030774	Q0MRP8		Milk lysozyme 	Bos indicus x Bos taurus (hybrid cattle)	Animalia (Mammals)	GCVWPDGKAITTHKLQTTMQETKALIMGYFKSIATGGAMMAKPQEQLTPVIYPAV	55		Antimicrobial		Predicted
CAMPSQ1476	112030782	Q0MRP7		Milk lysozyme	Bos indicus x Bos taurus (hybrid cattle)	Animalia (Mammals)	GCVWPDGKAITTHKLQTTMLETKALIMGYFKSIATGGAMMAKPQEQLTPVIYPAV	55		Antimicrobial		Predicted
CAMPSQ1477	112030790	Q0MRP6		Milk lysozyme	Bos indicus x Bos taurus (hybrid cattle)	Animalia (Mammals)	GCVWPDGKAITTHKLQTTMLETKALIMGYFKSIATGGAMMATQDGAVTPVIYPAV	55		Antimicrobial		Predicted
CAMPSQ1581	19856559	P17533		Defensin-related cryptdin-related sequence 1	Mus musculus	Animalia (Mammals)	RFPWCRKCRVCQKCQVCQKCPVCPTCPQCPKQPLCEERQNKTAITTQAPNTQHKGC	56		Antimicrobial		Predicted
CAMPSQ1969	25090981	Q962B2		Penaeidin-3k	Litopenaeus setiferus	Animalia (Crustaceans (Malacostraca))	QGYKGPYTRPILRPYVRPVVSYNACTLSCRGITTTQARSCCTRLGRCCHVAKGYS	55		Antibacterial,Antifungal		Predicted
CAMPSQ1971	25090975	Q962A8		Penaeidin-3l	Litopenaeus setiferus	Animalia (Crustaceans (Malacostraca))	QGYKGPYTRPILRPYVRPVVSYNVCTLSCRGITTTQARSCCTRLGRCCHVAKGYS	55		Antibacterial,Antifungal		Predicted
CAMPSQ2005	123780103	Q32ZG3		Beta-defensin 29	Rattus norvegicus	Animalia (Mammals)	GLFGLRSGKRREPWVSCELYQGSCRNACQKYEIQYLTCPKKRKCCLKFPMKITRV	55		Antibacterial		Predicted
CAMPSQ2373	306850698	E2IFI2		Hepcidin-like antimicrobial peptide precursor  	Epinephelus malabaricus	Animalia (Pisces)	LVELVSSDDPVADHQELPVELGERLFNIRKKRASPKCTPYCYPTRDGVFCGVQCDF	56		Antimicrobial		Predicted
CAMPSQ4675		A4IJZ5		Lantibiotic antimicrobial peptide	Geobacillus thermodenitrificans (strain NG80-2)	Bacteria	MAKFDDFDLDIVVKKQDDVVQPNVTSKSLCTPGCITGVLMCLTQNSCVSCNSCIRC	56		Antimicrobial		Predicted (Based on signature)
CAMPSQ5602	119524033	D2CGP0		NsuA3	Streptococcus thoraltensis	Bacteria	MNNEDFNLDLITISKENNSGASPRITSKSLCTAGCKTGILMTCPLKTATCGCHFG	55		Antimicrobial		Predicted (Based on signature)
CAMPSQ5603	130845708	D2CGP4		NsuA2	Streptococcus agalactiae	Bacteria	MNNEDFNLDLIKISKENNSGASPRVTSKSLCTPGCKTGILMTCPLKTATCGCHFG	55		Antimicrobial		Predicted (Based on signature)
CAMPSQ5604	130845732	D2CGP6		NsuA2	Streptococcus porcinus	Bacteria	MNNEDFNLDLIKISKENNSGASPRVTSKSLCTPGCKTGILMTCPLKTATCGCHFG	55		Antimicrobial		Predicted (Based on signature)
CAMPSQ5605	130845752	D2CGP8		NsuA3	Streptococcus pluranimalium	Bacteria	MNNEDFNLDLITISKENNSGASPRITSKSLCTAGCKTGILMTCPLKTATCGCHFG	55		Antimicrobial		Predicted (Based on signature)
CAMPSQ5795	319744684	E7S5A3		Lantibiotic nisin-Z	Streptococcus agalactiae ATCC 13813	Bacteria	MNNEDFNLDLVTISKENNSGASPRVTSKSLCTPGCKTGILMTCAIKTATCGCHFG	55		Antimicrobial		Predicted (Based on signature)
CAMPSQ5835	336064391	F5X6X4		Nisin U lantibiotic	Streptococcus pasteurianus (strain ATCC 43144 / JCM 5346 / CDC 1723-81)	Bacteria	MNNEDFNLDLVTISKENNSGASPRVTSKSLCTPGCKTGILMTCAIKTATCGCHFG	55		Antimicrobial		Predicted (Based on signature)
CAMPSQ6661		Q5L3A9		Lantibiotic	Geobacillus kaustophilus (strain HTA426)	Bacteria	MAKLDDFDLDIVVKKQDNIVQPNITSKSLCTPGCITGILMCLTQNSCVSCNSCIRC	56		Antimicrobial		Predicted (Based on signature)
CAMPSQ7079	446981034			MULTISPECIES: lantibiotic nisin-A [Bacilli]. 	Bacilli  	Bacteria	MNNEDFNLDLVTISKENNSGASPRVTSKSLCTPGCKTGILMTCAIKTATCGCHFG	55		Antimicrobial		Predicted (Based on signature)
CAMPSQ7106	514893299			Putative bacteriocin carnobacteriocin BM1, partial [Enterococcus  faecalis]. 	Enterococcus faecalis  	Bacteria	GFTALGTNVEAATRSYGNGVYCNKQKCWVNWNEAKQQIAGIVIGGWASSLASMGR	55		Antimicrobial		Predicted (Based on signature)
CAMPSQ7403	21666449	Q8KWU4		Prebacteriocin 	Lactobacillus sakei	Bacteria	MKNTRSLTIQEIKSITGGKYYGNGVSCNSHGCSVNWGQAWTCGVNHLANGGHGVC	55	12450870, 20152920	Antimicrobial		Predicted (Based on signature)
CAMPSQ7873		L8IZL9		Beta-defensin (Fragment)	Bos mutus	Animalia (Mammals)	EEECWMKGKCRLVCKNDEDSVTRCSNRKRCCILSRYLTIVPMTIDRILPWTTPQV	55		Antimicrobial		Predicted
CAMPSQ7943		D2H920		Beta-defensin (Fragment)	Ailuropoda melanoleuca 	Animalia (Mammals)	ILLSFFASGGTQKCWNLHGRCRQKCSRRERVYIYCTNNKLCCVKPKFQPREQLWPF	56		Antimicrobial		Predicted
