  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ230	20137310	P82390		Aurein-2.3	Litoria aurea	Animalia (Amphibians)	GLFDIVKKVVGAIGSL	16	10951191	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ232	20137313	P82393		Aurein-2.6	Litoria raniformis	Animalia (Amphibians)	GLFDIAKKVIGVIGSL	16	10951191	Antibacterial,Anticancer	Gram+ve	Experimentally Validated
CAMPSQ391	3913672	P81437		Formaecin-2	Myrmecia gulosa	Animalia (Insects)	GRPNPVNTKPTPYPRL	16	9497332	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ392	3913673	P81438		Formaecin-1	Myrmecia gulosa	Animalia (Insects)	GRPNPVNNKPTPHPRL	16	9497332	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ714		A6MBS3		Odorranain-U1 antimicrobial peptide	Odorrana grahami	Animalia (Amphibians)	GCSRWIIGIHGQICRD	16	17272268	Antibacterial,Antifungal 	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ724	913450			Antimicrobial peptide , immobilized peptide E16LKL	Synthetic construct		KGLKKLLKLLKKLLKL	16	7726486	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ727	913447			Antimicrobial peptide , immobilized peptige E16KGL	Synthetic construct		LKLLKKLLKLLKKLGK	16	7726486	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1007				Temporin-1TGb	Rana tagoi 	Animalia (Amphibians)	AVDLAKIANKVLSSLF	16	16675060	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1232	1170962	P80411		Metalnikowin-3 	Palomena prasina	Animalia (Insects)	VDKPDYRPRPWPRPNM	16		Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ1233	1170961	P80410		Metalnikowin-2B 	Palomena prasina	Animalia (Insects)	VDKPDYRPRPWPRNMI	16		Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ1946	18202395	P82240		Salmocidin-3	Oncorhynchus mykiss	Animalia (Pisces)	XXPQQLGHVKAAXSDY	16		Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ2305	332310247	P86895		Putative antimicrobial peptide 1	Lippia sidoides  	Viridiplantae	ALYNSEDLYEETSDSDD	17	21210197	Antifungal		Experimentally Validated
CAMPSQ2459	317411785	P0CI89		Antimicrobial peptide 364	Lychas mucronatus	Animalia (Arachnids)	FGLIPSMMGGLVSAFK	16	20663230	Antibacterial		Experimentally Validated
CAMPSQ2463	317411788	B9UIY3		Mucroporin	Lychas mucronatus	Animalia (Arachnids)	FGLIPSLIGGLVSAFK	16	18779362	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2475	391738011	B3EWI8		Putative antimicrobial protein 3	Cenchritis muricatus  	Animalia (Molluscs (Gastropoda))	ESILIVHQQQSRSSGS	16	22210491	Antibacterial		Experimentally Validated
CAMPSQ2705		P82389		  Aurein 2.2 	Litoria aurea	Animalia (Amphibians)	GLFDIVKKVVGALGSL	16		Antimicrobial		Experimentally Validated
CAMPSQ2706		P82391		  Aurein 2.4 	Litoria aurea	Animalia (Amphibians)	GLFDIVKKVVGTLAGL	16		Antimicrobial		Experimentally Validated
CAMPSQ3253				  Alyteserin-2a 	Alytes obstetricans	Animalia (Amphibians)	ILGKLLSTAAGLLSNL	16	19463738	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3470				  limnonectin-1Fa 	Limnonectes fujianensis	Animalia (Amphibians)	SFPFFPPGICKRLKRC	16	21396976	Antimicrobial	Gram-ve	Experimentally Validated
CAMPSQ3471				  limnonectin-1Fb 	Limnonectes fujianensis	Animalia (Amphibians)	SFHVFPPWMCKSLKKC	16	21396976	Antimicrobial	Gram-ve	Experimentally Validated
CAMPSQ3818		L0P323		Lantibiotic streptin, Senegalin 	Kassina senegalensis	Animalia (Amphibians)	FLPFLIPALTSLISSL	16	23430307	Antibacterial, Antifungal	Gram+ve	Experimentally Validated
CAMPSQ3958		P83142		Antifungal protein 2 small subunit 	Malva parviflora 	Viridiplantae	SDYSRKRDPPQKYEEE	16	11118343	Antifungal		Experimentally Validated
CAMPSQ4021				Pleurain-R1 antimicrobial peptide	Rana pleuraden 	Animalia (Amphibians)	CVHWMTNTARTACIAP	16	19028675	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4077	425872845	K7Z443		Temporin-AJ11 antimicrobial peptide precursor 	Amolops jingdongensis 	Animalia (Amphibians)	FFPIGGKLLSGLTGLL	16	22828809	Antimicrobial		Predicted
CAMPSQ4078	425872841	K7ZGS0		Temporin-AJ9 antimicrobial peptide precursor 	Amolops jingdongensis 	Animalia (Amphibians)	FLPIVGKLLSGLTGLL	16	22828809	Antimicrobial		Predicted
