  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ2182	167888496	C5H0D3		Amolopin-8a antimicrobial peptide	Amolops loloensis	Animalia (Amphibians)	MFTMKKSLLLLFFLGMISLSLCKQERDANEERRDDPDENEENGGEAKVEEIQRGARPPLRCKAALC	66		Antimicrobial		Predicted
CAMPSQ2184	167888492	C5H0D1		Amolopin-6b antimicrobial peptide	Amolops loloensis	Animalia (Amphibians)	MFTMKKSLLLLFFLGMISLSLCKQERDANEERRDDPDENEENGGEAKVEEIKRAARPPLRCKAAFC	66		Antimicrobial		Predicted
CAMPSQ2185	167888490	C5H0D0		Amolopin-6a antimicrobial peptide	Amolops loloensis	Animalia (Amphibians)	MFTMKKSLLLLFFLGMISLSLCKQERDANEERRDDPDENEENGGEAKVEEIKRAARPPLGCKAAFC	66		Antimicrobial		Predicted
CAMPSQ2191	260100777	A6BMG0		Antimicrobial peptide cecropin	Plutella xylostella	Animalia (Insects)	MKLSNIFFFVFMAFFAVASVSAAPRWKPFKKLEKVGRNIRNGIIRYNGPAVAVIGQATSIARPTGK	66		Antimicrobial		Predicted
CAMPSQ2221	169218609	C4MEB7		Lividin-7c	Rana livida	Animalia (Amphibians)	MFTMKKSLLLLFFLGIINLSLCQEERNAEEERRDEEVAKAEEIKRGILSGILGVGKKLVCGLSGLC	66		Antimicrobial		Predicted
CAMPSQ2254	90110407	Q1W5W6		Antimicrobial peptide 1	Pheretima tschiliensis	Animalia (Annelids (Clitellata))	MYSKYERQKDKRPYSERKDQYTGPQFLYPPDRIPPSKAIKWNEEGLPMYEVLPDGAGAKTAVEAAE	66		Antimicrobial		Predicted
CAMPSQ2427	397495254			Cathelicidin antimicrobial peptide  	Pan paniscus	Animalia (Mammals)	LSYKEAVLRAIDGINQRSSDANLYRLLDLDPRPTMDGDPDTPKPVSFTVKETVCPRTTQQSPEDCD	66		Antimicrobial		Predicted
CAMPSQ2430	297671435	H2PAV4		Cathelicidin antimicrobial peptide  	Pongo abelii	Animalia (Mammals)	LSYKEAVLHAIDGINQRSSDANLYRLLDLDPSLTMDGDPDTPKPVSFTVKETVCPRRTQQSPEDCD	66		Antimicrobial		Predicted
CAMPSQ4008		C0IL91		Nigroain-C antimicrobial peptide	Rana nigrovittata 	Animalia (Amphibians)	MFTMKKSLLLLFFLGVISLSLCKQKRYADEEGNEVSGGEAKVEEVKRFKTWKRPPFQTSCWGIIKE	66		Antimicrobial		Predicted
CAMPSQ4009		C0IL66		Nigroain-B antimicrobial peptide	Rana nigrovittata 	Animalia (Amphibians)	MFTMKKSLFLLFFLGTISLSLCEEERDADEEDGGEATEQEERDVQRRCVISAGWNHKIRCKLTGNC	66		Antimicrobial		Predicted
CAMPSQ4474		B6DD50		U15-lycotoxin-Ls1g	Lycosa singoriensis	Animalia (Arachnids)	DQYCPKSSITACKKMNIRNDCCKDDDCTGGSWCCATPCGNFCKYPADRPGGKRAAGGKSCKTGYVY	66	19875276	Antibacterial		Predicted
CAMPSQ5550	239618997	C5IAZ3		Ranatuerin-1Cb antimicrobial peptide	Lithobates catesbeiana 	Animalia (Amphibians)	MFTMKKSLLLLFFLGTITLSLCEQERGADEEEGNGEKEIKRSMFSVLKNLGKVGLGFVACKVNKQC	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ5551	239618999	C5IAZ4		Ranatuerin-1Cb antimicrobial peptide	Lithobates catesbeiana 	Animalia (Amphibians)	MFTLKKSLLLLFFLGTITLSLCEQERGADEEEGNGEKEIKRSMFSVLKNLGKVGLGFVACKVNKQC	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ5552	239619003	C5IAZ6		Ranatuerin-1Cc antimicrobial peptide	Lithobates catesbeiana 	Animalia (Amphibians)	MFTMKKSLLLLFFLGTITLSLCEQERGADEEEGNGEKEIKRSMFSVLKNLGKVGLGFVACKVSKQC	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ5556	238617703	C5IBS0		Ranatuerin-1	Lithobates catesbeiana 	Animalia (Amphibians)	MFTMKKSLLLLFFLGTITLSLCEQERGADEEEGSGEKEIKRSMLSVLKNLGKVGLGFVACKINKQC	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ5850	339272017	F7UI84		Phylloseptin-s1	Phyllomedusa sauvagei 	Animalia (Amphibians)	MAFLKKSLFLVLFLGLVSLSICEEEKRETEEEEHDQEEDDKSEEKRFLSLIPHIVSGVASIAKHFG	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ5851	339272019	F7UI85		Phylloseptin-s2	Phyllomedusa sauvagei 	Animalia (Amphibians)	MAFLKKSLFLVLFLGLVSLSICEEEKRETEEEEHDQEEDDKSEEKRFLSLIPHIVSGVASLAKHFG	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ5852	339272021	F7UI86		Phylloseptin-s3	Phyllomedusa sauvagei 	Animalia (Amphibians)	MAFLKKSLFLVLFLGLVSLSICEEEKRETEEEEHDQEEDDKSEEKRFLSLIPHIVSGVASLAIHFG	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ5853	339272023	F7UI87		Phylloseptin-s4	Phyllomedusa sauvagei 	Animalia (Amphibians)	MAFLKKSLFLVLFLGLVSLSICEEEKRETEEEEHDQEEDDKSEEKRFLSMIPHIVSGVAALAKHLG	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ6354	475678559	M9QVZ1		Pediocin BA 28	Pediococcus acidilactici	Bacteria	MANIIGGKYYGNGVTCGKHSCSVDWGKATTCIINNGAMAWATGGHQGNHKCEQFIKLGLLRGEFLF	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ6532	112982904	Q2WGL2		Antibacterial peptide	Bombyx mori 	Animalia (Insects)	MYFTKIVFVAIICIMIVSCASAWDFFKELEGVGQRVRDSIISAGPAIDVLQKAKGLVDSSESKEDE	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ6587	85702288	Q3V490		Beta-defensin	Mus musculus 	Animalia (Mammals)	MKTAVLTMALLLLSQVIPGTPERCWKSFGVCREECAKKESFYIFCWNGKLCCVKPKNVPLWSQNLD	66		Antimicrobial		Predicted (Based on signature)
CAMPSQ7829		A0A0D9RKD7		Beta-defensin	Chlorocebus sabaeus 	Animalia (Mammals)	GPSVPQRKTREVAERKRECQLVHGACKSECNTWEYVQYYCDVKPCCVVRDYQKPVINKITTKLYRK	66		Antimicrobial		Predicted
CAMPSQ7896		Q30KU0		Beta-defensin	Canis familiaris 	Animalia (Mammals)	GPSVSQKKTKEDAGRKRECYLVRGACKSSCNSWEYIYNYCSTEPCCVVREYQKPVSKSINFTGDIL	66		Antimicrobial		Predicted
CAMPSQ8076		I3M4H8		Beta-defensin	Spermophilus tridecemlineatus 	Animalia (Mammals)	EEECWSHGHCRLVCNDDEDHVLRCVNRKRCCVPSRYVTMKPITIDYVLDWTTPTVPTTVPTTVRKR	66		Antimicrobial		Predicted
