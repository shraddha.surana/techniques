  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ124	1345732	P36190, O17512		Ceratotoxin-A	Ceratitis capitata	Animalia (Insects)	SIGSALKKALPVAKKIGKIALPIAKAALP	29	8353519	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ400	41016801	P82234		Brevinin-2Tc	Rana temporaria	Animalia (Amphibians)	GLWETIKNFGKKFTLNILHKLKCKIGGGC	29	10333736	Antibacterial		Experimentally Validated
CAMPSQ401	41016802	P82235		Brevinin-2Td	Rana temporaria	Animalia (Amphibians)	GLWETIKNFGKKFTLNILHNLKCKIGGGC	29	10333736	Antibacterial		Experimentally Validated
CAMPSQ1452	37683075			Lacoferricin	Synthetic construct		IEGRFKCRRWQWRMKKLGAPSITCVRRAF	29		Antimicrobial		Predicted
CAMPSQ1455	50845068	Q8SQD5		EP2B protein	Macaca mulatta	Animalia (Mammals)	MKVFFLFAVLFCLVRRNSVHISHQEARGP	29		Antimicrobial		Predicted
CAMPSQ2447	312434216	E5KV54		Beta-defensin 1 antimicrobial peptide  	Coturnix japonica	Animalia (Aves)	RQKGFCAFLKCPSLTIISGKCSRFHFCCK	29		Antimicrobial		Predicted
CAMPSQ2482	310696008	E3SZB7		Odorranain-F-RA2 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	GFMDTAKNVAKNMAGNLLDNLKCKIIKPC	29		Antimicrobial		Predicted
CAMPSQ2738		P59665	3HJ2	 human neutrophil peptide-2 	Homo sapiens	Animalia (Mammals)	CYCRIPACIAGERRYGTCIYQGRLWAFCC	29	15616305	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2960				  Dermaseptin-B8 	Phyllomedusa bicolor	Animalia (Amphibians)	GLWSKIKEAGKAVLTAAGKAALGAVSDAV	29	9305726	Antimicrobial		Predicted
CAMPSQ2992	17433212	P58448		  Varv peptide C 	Viola arvensis	Viridiplantae	GVPICGETCVGGTCNTPGCSCSWPVCTRN	29	10075760	Antimicrobial		Predicted
CAMPSQ2993	17433213	P58449		  Varv peptide D 	Viola arvensis	Viridiplantae	GLPICGETCVGGSCNTPGCSCSWPVCTRN	29	10075760	Antimicrobial		Predicted
CAMPSQ3202				  Plasticin-A1 	Agalychnis annae	Animalia (Amphibians)	GLVSGLLNTAGGLLGDLLGSLGSLSGGES	29	9774745	Antimicrobial		Predicted
CAMPSQ3220				  SgI-29 	Homo sapiens	Animalia (Mammals)	HNKQEGRDHDKSKGHFHRVVIHHKGGKAH	29	18314226	Antimicrobial		Experimentally Validated
CAMPSQ3221				  SgII peptide A 	Homo sapiens	Animalia (Mammals)	KQEGRDHDKSKGHFHMIVIHHKGGQAHHG	29	18714013	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3473	269819995	D1MJ09		Cc-CATH3 	Coturnix coturnix	Animalia (Aves)	RVRRFWPLVPVAINTVAAGINLYKAIRRK	29	21375690	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3481				  GRPSp 	Scylla paramamosain	Animalia (Crustaceans (Malacostraca))	IPAMEPAARVKRSPGYGGCSPRWACGGYG	29	21220028	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3499	374110720	G3ETQ3		  Palustrin-2AJ1 	Amolops jingdongensis	Animalia (Amphibians)	GFMDTAKNVAKNVAVTLIDKLRCKVTGGC	29	21816202	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3500	374110721	G3F828		  Palustrin-2AJ2 	Amolops jingdongensis	Animalia (Amphibians)	GFMDTAKQVAKNVAVTLIDKLRCKVTGGC	29	21816202	Antimicrobial		Predicted
CAMPSQ3501				  Palustrin-2ISc 	Odorrana ishikawae	Animalia (Amphibians)	GFMDTAKNVAKNVAATLLDKLKCKITGGC	29	21867685	Antimicrobial		Experimentally Validated
CAMPSQ3504				  Vaby A 	Viola abyssinica	Viridiplantae	GLPVCGETCAGGTCNTPGCSCSWPICTRN	29	21434649	Antimicrobial		Experimentally Validated
CAMPSQ3756		P0DMI7		  HsAp	Heterometrus spinifer	Animalia (Arachnids)	SGTSEKERESGRLLGVVKRLIVCFRSPFP	29	23000095	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3757		P0DMI8		  HsAp2 	Heterometrus spinifer	Animalia (Arachnids)	SGTSEKERESERLLGVVNPLIKCFRSPCP	29	23000095	Antimicrobial		Experimentally Validated
CAMPSQ3758		P0DMI9		  HsAp3 	Heterometrus spinifer	Animalia (Arachnids)	SGTPEKERESGRLLGVVKRYIVCVRNPCP	29	23000095	Antimicrobial		Experimentally Validated
CAMPSQ3759		P0DMJ0		  HsAp4 	Heterometrus spinifer	Animalia (Arachnids)	SGTSEKERESGRLLGVVKRLIVGFRSPFR	29	23000095	Antimicrobial		Experimentally Validated
CAMPSQ4507		P84921		Dermaseptin-1	Phyllomedusa tarsius	Animalia (Amphibians)	GLWSKIKETGKEAAKAAGKAALNKIAEAV	29		Antibacterial		Experimentally Validated
