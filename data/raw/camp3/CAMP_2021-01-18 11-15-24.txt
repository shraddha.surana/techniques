  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ402	41016803	P82825		Brevinin-1La	Rana luteiventris	Animalia (Amphibians)	FLPMLAGLAASMVPKLVCLITKKC	24	10651828	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ403	41016804	P82826		Brevinin-1Lb	Rana luteiventris	Animalia (Amphibians)	FLPMLAGLAASMVPKFVCLITKKC	24	10651828	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ404	41016805	P82833		Brevinin-1Ba	Rana berlandieri	Animalia (Amphibians)	FLPFIAGMAAKFLPKIFCAISKKC	24	10651828	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ405	41016806	P82834		Brevinin-1Bb	Rana berlandieri	Animalia (Amphibians)	FLPAIAGMAAKFLPKIFCAISKKC	24	10651828	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ406	41016807	P82835		Brevinin-1Bc	Rana berlandieri	Animalia (Amphibians)	FLPFIAGVAAKFLPKIFCAISKKC	24	10651828	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ407	41016808	P82836		Brevinin-1Bd	Rana berlandieri	Animalia (Amphibians)	FLPAIAGVAAKFLPKIFCAISKKC	24	10651828	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ408	41016809	P82837		Brevinin-1Be	Rana berlandieri	Animalia (Amphibians)	FLPAIVGAAAKFLPKIFCVISKKC	24	10651828	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ409	41016810	P82838		Brevinin-1Bf	Rana berlandieri	Animalia (Amphibians)	FLPFIAGMAANFLPKIFCAISKKC	24	10651828	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ410	41016811	P82841		Brevinin-1Pa	Rana pipiens	Animalia (Amphibians)	FLPIIAGVAAKVFPKIFCAISKKC	24	10651828	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ411	41016812	P82843		Brevinin-1Pc	Rana pipiens	Animalia (Amphibians)	FLPIIASVAAKVFSKIFCAISKKC	24	10651828	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ412	41016813	P82844		Brevinin-1Pd	Rana pipiens	Animalia (Amphibians)	FLPIIASVAANVFSKIFCAISKKC	24	10651828	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ660				Brevinin-1Ec	Rana esculenta (edible frog )	Animalia (Amphibians)	FLPLLAGLAANFFPKIFCKITRKC	24	8163497	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1163	223365680		2K98	MSI-594	Synthetic construct		GIGKFLKKAKKGIGAVLKVLTTGL	24	19180607	Antimicrobial		Experimentally Validated
CAMPSQ2452	306530876	P0CH49		Antimicrobial peptide scolopin-2 	Scolopendra mutilans  	Animalia (Centipedes)	ILKKFMLHRGTKVYKMRTLSKRSH	24	19716842	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3225				  Brevinin-1BLa  	Rana blairi	Animalia (Amphibians)	FLPAIVGAAAKFLPKIFCAISKKC	24	19254736	Antimicrobial		Experimentally Validated
CAMPSQ3226				  Brevinin-1BLb  	Rana blairi	Animalia (Amphibians)	FLPIIAGVAAKVLPKIFCAISKKC	24	19254736	Antimicrobial		Experimentally Validated
CAMPSQ3227				  Brevinin-1BLc  	Rana blairi	Animalia (Amphibians)	FLPIIAGIAAKFLPKIFCTISKKC	24	19254736	Antibacterial. Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3719				  Brevinin-1CG1 	Amolops chunganensis	Animalia (Amphibians)	FLSTALKVAANVVPTLFCKITKKC	24	22951323	Antimicrobial		Experimentally Validated
CAMPSQ3720				  Brevinin-1CG2 	Amolops chunganensis	Animalia (Amphibians)	FLPIVAGLAANFLPKIVCKITKKC	24	22951323	Antimicrobial		Experimentally Validated
CAMPSQ3721				  Brevinin-1CG3 	Amolops chunganensis	Animalia (Amphibians)	FLSTLLNVASNVVPTLICKITKKC	24	22951323	Antimicrobial		Experimentally Validated
CAMPSQ3722				  Brevinin-1CG4 	Amolops chunganensis	Animalia (Amphibians)	FLSTLLNVASKVVPTLFCKITKKC	24	22951323	Antimicrobial		Experimentally Validated
CAMPSQ3723				  Brevinin-1CG5 	Amolops chunganensis	Animalia (Amphibians)	FLPMLAGLAANFLPKIVCKITKKC	24	22951323	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3993		A6MAS5		Brevinin-1E-OG5 antimicrobial peptide	Odorrana grahami 	Animalia (Amphibians)	FLPLLAGLAANFLPKIFCKITKKC	24	17272268	Antimicrobial		Predicted
CAMPSQ3994		A6MBQ0		Odorranain-P1b antimicrobial peptide	Odorrana grahami 	Animalia (Amphibians)	VIPFVASVAAETMQHVYCAASKKMLKLNWKSSDVENHLAKC	24	17272268	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3995		A6MBP6		Odorranain-M2 antimicrobial peptide	Odorrana grahami 	Animalia (Amphibians)	ATAWDFGPHGLLPIRPIRIRPLCG	24	17272268	Antimicrobial		Predicted
