  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ52	116080	P14662		Bactericidin B-2 	Manduca sexta	Animalia (Insects)	WNPFKELERAGQRVRDAVISAAPAVATVGQAAAIARG	37	3143727	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ53	116081	P14663		Bactericidin B-3 	Manduca sexta	Animalia (Insects)	WNPFKELERAGQRVRDAIISAGPAVATVGQAAAIARG	37	3143727	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ54	116083	P14664		Bactericidin B-4 	Manduca sexta	Animalia (Insects)	WNPFKELERAGQRVRDAIISAAPAVATVGQAAAIARG	37	3143727	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ55	116084	P14665		Bactericidin B-5P precursor 	Manduca sexta	Animalia (Insects)	WNPFKELERAGQRVRDAVISAAAVATVGQAAAIARGG	37	3143727	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ56	116087, 223330	P01507	1D9J,1D9L,1D9M,1D9O,1D9P,1F0D,1F0E,1F0F,1F0G,1F0H	Cecropin-A 	Hyalophora cecropia	Animalia (Insects)	KWKLFKKIEKVGQNIRDGIIKAGPAVAVVGQATQIAK	37	7140755	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ282	2506236	P80398	2G9L	Gaegurin-4	Rana rugosa 	Animalia (Amphibians)	GILDTLKQFAKGVGKDLVKGAAQGVLSTVSCKLAKTC	37	7999137	Antibacterial,Antifungal,Antiparasitic	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ554	6014948	P56686		4 kDa defensin	Androctonus australis	Animalia (Arachnids)	GFGCPFNQGACHRHCRSIRRRGGYCAGLFKQTCTCYR	37	8939880	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ560	6225249	P81610		Defensin-A	Mytilus edulis 	Animalia (Molluscs (Bivalvia))	GFGCPNDYPCHRHCKSIPGRXGGYCGGXHRLRCTCYR	37	8702979	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ570	3023392	P56386		Beta-defensin 1	Mus musculus	Animalia (Mammals)	DQYKCLQHGGFCLRSSCPSNTKLQGTCKPDKPNCCKS	37	9488417	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ791				CjaRL-37	Platyrrhini	Animalia (Mammals)	RLGDILQKAREKIEGGLKKLVQKIKDFFGKFAPRTES	37	16720578	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ803	20532149	P83002		Bacteriocin lactococcin MMFII	Lactococcus lactis subsp. lactis 	Bacteria	TSYGNGVHCNKSKCWIDVSELETYKAGTVSNPKDILW	37	11555873	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ804	175950732	B2LS02		Sakacin G immunity protein	Lactobacillus sakei 2512	Bacteria	KYYGNGVSCNSHGCSVNWGQAWTCGVNHLANGGHGVC	37	11555873	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ1076	224495924	P0C8T9		Esculentin-2HSa	Rana hosii	Animalia (Amphibians)	GIFSLIKGAAQLIGKTVAKEAGKTGLELMACKVTKQC	37	18621071	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1310	48976029	Q6QLQ9		Beta-defensin 10	Gallus gallus	Animalia (Aves)	NNEAQCEQAGGICSKDHCFHLHTRAFGHCQRGVPCRT	37	15148642	Antibacterial		Predicted
CAMPSQ2859				  Mytilus defensin 	Mytilus edulis	Animalia (Molluscs (Bivalvia))	GFGCPNDYPCHRHCKSIPGRAGGYCGGAHRLRCTCYR	37	8702979	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2862				  scorpion defensin 	Leiurus quinquestriatus	Animalia (Arachnids)	GFGCPLNQGACHRHCRSIRRRGGYCAGFFKQTCCYRN	37	8333834	Antibacterial		Experimentally Validated
CAMPSQ3355				  pBD-2 	Sus scrofa	Animalia (Mammals)	DHYICAKKGGTCNFSPCPLFNRIEGTCYSGKAKCCIR	37	17658606	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3361				  pBD-1 	Sus scrofa	Animalia (Mammals)	SVSCLRNKGVCMPGKCAPKMKQIGTCGMPQVKCCKRK	37	17168333	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3368				  Sublancin 168 	Bacillus subtilis 168	Bacteria	GLGKAQCAALWLQCASGGTIGCGGGAVACQNYRQFCR	37	9722542	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3630				  Esculentin-2-ALa 	Amolops loloensis	Animalia (Amphibians)	GIFALIKTAAKFVGKNLLKQAGKAGLEHLACKANNQC	37	19843479	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3631				  Esculentin-2-ALb 	Amolops loloensis	Animalia (Amphibians)	GIFSLIKTAAKFVGKNLLKQAGKAGVEHLACKANNQC	37	19843479	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3873		P68577		SPBc2 prophage-derived bacteriocin sublancin-168	Bacillus subtilis 	Bacteria	IGCGGGAVACQNYRQFCR	37	21196935	Antibacterial		Experimentally Validated
CAMPSQ4151	410591613	B3A0M8		Esculentin-2JDa	Odorrana jingdongensis 	Animalia (Amphibians)	GLFTLIKGAAKLIGKTVAKEAGKTGLELMACKITNQC	37	22917879	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4153	410591614	B3A0M9		Esculentin-2JDb	Odorrana jingdongensis 	Animalia (Amphibians)	GIFTLIKGAAKLIGKTVAKEAGKTGLELMACKITNQC	37	22917879	Antibacterial		Predicted
CAMPSQ7735		H2QVQ4		Beta-defensin	Pan troglodytes 	Animalia (Mammals)	SKRMEGHCEAECLTFEVKTGGCRAELAPFCCKNRKKH	37		Antimicrobial		Predicted
