  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ483	47523178	O62697		Beta-defensin 1	Sus scrofa	Animalia (Mammals)	KNIGNSVSCLRNKGVCMPGKCAPKMKQIGTCGMPQVKCCKRK	42	16552064	Antibacterial		Experimentally Validated
CAMPSQ975	145566914	P85116		Ostricacin-4	Struthio camelus	Animalia (Aves)	LPVNEAQCRQVGGYCGLRICNFPSRFLGLCTRNHPCCSRVWV	42	16459058	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1278	61971501	A0PI25		Antimicrobial protein plp	Hippocampus kuda	Animalia	MKWTAAFLVLVIVVLMAQPGECFLGLIFHGLVHAGKLIHGLI	42		Antimicrobial		Predicted
CAMPSQ1472	100170633	Q19ML1		Moricin	Antheraea pernyi	Animalia (Insects)	AKIPIKAIKTVGKAVGKGLRAINIASTANDVFNFLEPKKRKH	42		Antimicrobial		Predicted
CAMPSQ2007	123780101	Q32ZG1		Beta-defensin 33	Rattus norvegicus	Animalia (Mammals)	RKRNTKFRQCEKMGGICKYQKTHGCSILPAECKSRYKHCCRL	42		Antibacterial		Predicted
CAMPSQ2008	123780098	Q32ZF8		Beta-defensin 38	Rattus norvegicus	Animalia (Mammals)	TDQDTAKCVQKKNVCYYFECPWLSISVSTCYKGKAKCCQKRY	42		Antibacterial		Predicted
CAMPSQ2014	123779962	Q30KN3		Beta-defensin 33	 Mus musculus	Animalia (Mammals)	RKRNSKFRPCEKMGGICKSQKTHGCSILPAECKSRYKHCCRL	42		Antibacterial		Predicted
CAMPSQ2518	379067625	H9BB31		Liver-expressed antimicrobial peptide-2  	Larimichthys crocea	Animalia (Pisces)	LRRIARMTPLWRIMNSKPFGAYCQNNYECSTGLCRAGHCSTS	42		Antimicrobial		Predicted
CAMPSQ2557	310696154	E3SZJ0		OGC-RA1 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	PLKKSLLLLFFFGPISLSFCEKERKADKEENGGEVPEKEVRR	42		Antimicrobial		Predicted
CAMPSQ2740				  hBD-26 	Homo sapiens	Animalia (Mammals)	WYVKKCLNDVGICKKKCKPEEMHVKNGWAMCGKGRDCCVPAD	42	19373462	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ2818		P35618	1og7	  Sakacin P/ Sakacin 674 	Lactobacillus sake Lb674	Bacteria	KYYGNGVHCGKHSCTVDWGTAIGNIGNNAAANWATGGNAGWNK	43	8138128	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3315				  PP113 	Pteromalus puparum	Animalia (Insects)	GKWGWIYITILFADVGGFKSSRHPEERRVQERRFKRITRGPD	42	19950104	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3507				  saBD 	Sparus aurata	Animalia (Pisces)	ASFPWSCPSLSGVCRKVCLPTELFFGPLGCGKGFLCGVSHFL	42	21497909	Antimicrobial		Experimentally Validated
CAMPSQ3774				  Eurocin 	Aspergillus amstelodami	Fungi	GFGCPGDAYQCSEHCRALGGGRTGGYCAGPWYLGHPTCTCSF	42	23093408	Antimicrobial	Gram+ve	Experimentally Validated
CAMPSQ4298	296005969			Defensin 2-2, partial	Synthetic construct		FSCDVLSFQSKWVSPNHSACAVRCLAQRRKGGKCKNGDCVCR	42	20097222	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ5635	283777646	D2XR04		Abaecin (Fragment)	Bombus terrestris 	Animalia (Insects)	WILAFVPYNPPRPGQSKPFPTFPGHGPFNPKTQWPYPLPNPGH	43		Antimicrobial		Predicted (Based on signature)
CAMPSQ7620		D2H924		Beta-defensin (Fragment)	Ailuropoda melanoleuca 	Animalia (Mammals)	CGGQKSCWIIKGHCRKNCKSGEQIKKPCKNGDYCCIPSKTNS	42		Antimicrobial		Predicted
CAMPSQ7626		Q30KL0		Beta-defensin (Fragment)	Pan troglodytes troglodytes	Animalia (Mammals)	CRSQKSCWIIKGHCRKNCKPGEQVKKPCKNGDYCCIPSNTDS	42		Antimicrobial		Predicted
CAMPSQ7668		F6WQR6		Beta-defensin	Macaca mulatta 	Animalia (Mammals)	GPNVYIQKLFASCWRLRGSCRQKCLKKEEYHILCDTTRLCCV	42		Antimicrobial		Predicted
CAMPSQ7678		G3MYA8		Beta-defensin (Fragment)	Bos taurus 	Animalia (Mammals)	CRGRKSCWIIKGHCRKDCKSGEQVKKPCRNGDYCCVPSKTDS	42		Antimicrobial		Predicted
CAMPSQ7857		L8J1J9		Beta-defensin (Fragment)	Bos mutus	Animalia (Mammals)	CRGRKSCWIIKGHCRKDCKSGEQVKKPCRNGDYCCVPSKTDS	42		Antimicrobial		Predicted
CAMPSQ7890		G1S9I1		Beta-defensin (Fragment)	Nomascus leucogenys 	Animalia (Mammals)	CRSQKSCWIIKGHCRKNCKPGEQVKKPCKNGDYCCIPSNTDS	42		Antimicrobial		Predicted
CAMPSQ8104		D2WPE0		Beta-defensin	Paralichthys olivaceus 	Animalia (Pisces)	DRPKADRPKPDRPKADCSTIQGVCKDSCLSTEFSIGALGCSA	42	19900559	Antibacterial		Predicted
CAMPSQ8113		Q4S8X0, D5A7I2		Beta-defensin	Tetraodon nigroviridis 	Animalia (Pisces)	ASFPWACPSLNGVCRKVCLPTELFFGPLGCGKGFLCCVSHFL	42		Antimicrobial		Predicted
CAMPSQ8135		D5A7I4		Beta-defensin	Takifugu rubripes 	Animalia (Pisces)	ASFPWTCPSLSGVCRKVCLPTEMFFGPLGCGKGFQCCVSHFL	42		Antimicrobial		Predicted
