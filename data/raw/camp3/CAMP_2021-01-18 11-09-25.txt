  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ9	110278918	P0C1M1		Vespid chemotactic peptide 5e	Vespa magnifica 	Animalia (Insects)	FLPIIAKLLGGLL	13	16330062	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ10	110278919	P0C1M2		Vespid chemotactic peptide 5f	Vespa magnifica 	Animalia (Insects)	FLPIPRPILLGLL	13	16330062	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ11	110278920	P0C1M3		Vespid chemotactic peptide 5g	Vespa magnifica 	Animalia (Insects)	FLIIRRPIVLGLL	13	16330062	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ16	110279046	P0C1M5		Mastoparan-like peptide 12b	Vespa magnifica 	Animalia (Insects)	INWKGIAAMKKLL	13	16330062	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ267	2493350	P79874		Temporin-B	Rana temporaria	Animalia (Amphibians)	LLPIVGNLLKSLL	13	9022710	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ477	46576886	P83879		Subpeptin JM4-B	Bacillus subtilis 	Bacteria	XXKEIXHIFHDN	12	16211432	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ522	55976268	P84263		Dahlein-1.2	Litoria dahlii	Animalia (Amphibians)	GLFDIIKNIFSGL	13	11555873	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ523	55976278	P84273		Dahlein-1.1	Litoria dahlii	Animalia (Amphibians)	GLFDIIKNIVSTL	13	11555873	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ715				OdV1	Odorrana grahami	Animalia (Amphibians)	GLLSGTSVRGST	12	17272268	Antibacterial,Antifungal 	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ731	23200406		1M02	Pw2	Synthetic construct		HPLKQYWWRPSI	12	11849705	Antifungal, Antiparasitic		Experimentally Validated
CAMPSQ971	13629011	P82907		Lichenin	Bacillus licheniformis	Bacteria	ISLEICXIFHDN	12	11576300	Antibacterial		Experimentally Validated
CAMPSQ1029	223674130	P49913	2FBS	Ll-37(17-29)	Synthetic construct		FKRIVQRIKDFLR	13	16637646	Antibacterial,Anticancer		Experimentally Validated
CAMPSQ1231	46576885	P83878		Subpeptin JM4-A	Bacillus subtilis 	Bacteria	XXKEIXWIFHDN	12	16211432	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1511	1705446	P54230		Bactenecin-1	Ovis aries	Animalia (Mammals)	RICRIIFLRVCR	12		Antibacterial		Predicted
CAMPSQ2575	288561900	B3VZU2		Aponicin-1CDYa	Rana dybowskii 	Animalia (Amphibians)	FPLALLCKVFKKC	13	19539775	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2773				  Cyclic L27-11 	Synthetic construct		TWLKKRRWKKAK	12	20167788	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2790		P15516		Human Histatin 8 	Homo sapiens 	Animalia (Mammals)	KFHEKHHSHRGY	12	12711380	Antifungal		Experimentally Validated
CAMPSQ2833				  Peptide A1 	Rana esculenta	Animalia (Amphibians)	FLPAIAGILSQLF	13	2317508	Antimicrobial		Experimentally Validated
CAMPSQ3823				  Panurgine 1 	Panurgus calcaratus	Animalia (Insects)	LNWGAILKHIIK	12	23483218	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3847		R4JQZ0		  Pantinin-2 	Pandinus imperator	Animalia (Arachnids)	IFGAIWKGISSLL	13	23624072	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3848		R4JJN6		  Pantinin-3 	Pandinus imperator	Animalia (Arachnids)	FLSTIWNGIKSLL	13	23624072	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3849				  Hp1090 	Heterometrus petersii	Animalia (Arachnids)	IFKAIWSGIKSLF	13	20950663	Antiviral		Experimentally Validated
CAMPSQ8207				IN1	Synthetic construct		LLPWKWPWWKWRR	13	26046345	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ8208				IN2	Synthetic construct		RRPWRWPWWPWRR	13	26046345	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ8209				IN3	Synthetic construct		RRPWRWPRWPWRR	13	26046345	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
