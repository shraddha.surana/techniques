  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ1123	34925434	Q9JHY3		WAP four-disulfide core domain protein12	 Mus musculus	Animalia (Mammals)	GGVKGEEKRVCPPDYVRCIRQDDPQCYSDNDCGDQEICCFWQCGFKCVLPVKDNSEEQIPQSKV	64	12574366	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1618	71152320	Q8K3I8		Beta-defensin 19	Mus musculus	Animalia (Mammals)	GKNPILQCMGNRGFCRSSCKKSEQAYFYCRTFQMCCLQSYVRISLTGVDDNTNWSYEKHWPRIP	64		Antibacterial		Predicted
CAMPSQ2355	307141611	H8PHI1		Liver-expressed antimicrobial peptide 2 isoform C  	Oncorhynchus mykiss	Animalia (Pisces)	FLTLLCPIQVQTAPVPEDWTGLITRAKRSLLWRWNTLKPVGTSCREHDECGTKYCRKKICSFQV	64		Antimicrobial		Predicted
CAMPSQ2356	307141613	H8PHI2		Liver-expressed antimicrobial peptide 2 isoform C  	Salmo salar	Animalia (Pisces)	FLTLLCPIQVQTAPVPEDWTGLITRAKRSLLWRWNTLKPVGASCREHDECGTKYCRKKICSFQV	64		Antimicrobial		Predicted
CAMPSQ4113	440896194	L8HY21		Tracheal antimicrobial peptide 	Bos grunniens mutus	Animalia (Mammals)	MRLHHLLLALLFLVLSAWSGFTQGVGNPLSCGRNKGICVPIRCPGKMKQIGTCVGRAVKCCRKK	64	22751099	Antimicrobial		Predicted
CAMPSQ4114	440891937	L8HM68		Lingual antimicrobial peptide 	Bos grunniens mutus	Animalia (Mammals)	MRLHHLLLALLFLVLSAGSGFTQGVRNSQSCRRNKGICVPIRCPGSMRQIGTCLGAQVKCCRRK	64	22751099	Antimicrobial		Predicted
CAMPSQ4178	399143595	J7FJ48		Preprokukunorin-1K 	Rana kukunoris 	Animalia (Amphibians)	MFTLKKSLFLIFFLGTINLSLCEEERDADEEERRDDPEERAVEVEKRSLILKGLAGLVQKILGK	64		Antimicrobial		Predicted
CAMPSQ4893	157041465	A8SKI1		Lingual antimicrobial peptide	Bubalus bubalis 	Animalia (Mammals)	MRLHHLLLALLFLVLSAGSGFTQGVRNSQSCRRNKGICVPIRCPGSMRQIGTCLGAQVKCCRRK	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ5170	208342473	B6DSR5		EntP (Fragment)	Enterococcus faecium 	Bacteria	LALIGKLGLVVTNFGTKVDAATRSYDNGIYCNNSKCWFNWGEAKENIAGIVISGWASGLAGMGH	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ5204	217416904	B7UB88		Beta-defensin 2	Anas platyrhynchos 	Animalia (Aves)	MRILYLLFSVLFLVLQVSPGLSLPQRDMFLCRKGSCHFGRCPIHLIRVGSCFGFRSCCKSPWDV	64	19851734	Antibacterial	Gram +ve, Gram -ve	Predicted (Based on signature)
CAMPSQ5732	301350793	E2IPV0		Beta-defensin 1	Cervus nippon 	Animalia (Mammals)	MRLHHLLLALLFLVLSAGSGFTQGVRNSLSCGRNKGVCVPIRCPGHMRQIGTCLGAPVKCCRRK	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ5733	309951076	E2ITD7		Beta-defensin	Ovis aries 	Animalia (Mammals)	MRLHHLLLVLFFVVLSAGSGFTHGVTDSLSCRWKKGICVLTKCPGTMRQIGTCFGPPVKCCRLK	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ5978	357621299	G6D012		Moricin-like peptide C4	Danaus plexippus 	Animalia (Insects)	MKLFSLFLVVLSLMSLLGGALSAPQPGKISAIRKGGRVIKKGLGAIGAIGTGHEVYEHFKNRRG	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ5984	357604613	G6DQH6		Cecropin-A	Danaus plexippus 	Animalia (Insects)	MDFSKIFFFVFACFLALSNVSAAPSPKWKIFKKIEKVGRNVRDGIIKAGPAVQVVGQATSIAKG	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ6182	514783351	J7ER43		Beta-defensin 2	Anas platyrhynchos 	Animalia (Aves)	MRILYLLFSVLFLVLQVSPGLSLPQRDMFLCRKGSCHFGRCPIHLVRVGSCFGFRSCCKLPWDV	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ6496		A0A0A0UYU9		Sarcotoxin-G	Musca domestica	Animalia (Insects)	MNFNKMLVFVALVLAVFVGHSEAGWLKKIGKKIERVGQHTRDATIQAIGVAQQAANVAATLKGK	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ6641	59675953	Q5F4I4		CecI (Cecropin I)	Drosophila pseudoobscura pseudoobscura 	Animalia (Insects)	MNFYKIFVFVALILAISVGESEAGWLKKIGKKIERVGQHTRDATIQGLGVAQQAANVAATARG	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6642	59675951	Q5F4J2		Cecropin I	Drosophila subobscura 	Animalia (Insects)	MNFYTIFFFVAIILAIGVGQSDAGWLKKIGKKIERVGQHTRDATIQGLGIAQQAANVAATAKG	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6643	59675949	Q5F4J7		Cecropin II1	Drosophila subobscura 	Animalia (Insects)	MNFYKIFVFVALILAISVGQSEAGWLKKLGKRLERVGQHTRDATIQVVGIAQQAANVAATARG	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6907	449267309	R7VSG0		Gallinacin-9	Columba livia 	Animalia (Aves)	MRILFFLIAVLFFLFQAAPAYSQADADTIACRQNRGSCSYVACSGPTVDIGTCRTGKLRCCKW	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ7157	296472488			TPA: beta-defensin 2 precursor [Bos taurus]. 	Bos taurus	Animalia (Mammals)	MRLHHLLLAVLFLVLSAGSGFTQRVRNPQSCRWNMGVCIPFWCRVGMRQIGTCFGPRVPCCRR	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ7250	296472496			TPA: tracheal antimicrobial peptide precursor [Bos taurus]. 	Bos taurus	Animalia (Mammals)	MRLHHLLLALLFLVLSAWSGFTQGVGNPVSCVRNKGICVPIRCPGSMKQIGTCVGRAVKCCRKK	64		Antimicrobial		Predicted (Based on signature)
CAMPSQ7503	749206614	A0A0D3QFT4		Cecropin	Bactrocera dorsalis 	Animalia (Insects)	MNFNKVFIFLAVVIAIFAGQTEAGWLKKIGKKIERVGQHTRDAAIQGIAVAQQAANVAATARGK	64		Antimicrobial		Predicted
CAMPSQ7674		M3Z2J4		Beta-defensin	Mustela putorius furo 	Animalia (Mammals)	RHHTLRCMGNTGVCRPSCRRTEHPYLYCLNYQPCCLQSYMRISIAGREEKDDWSQENRWPKIP	63		Antimicrobial		Predicted
CAMPSQ7686		G8CY13		Beta-defensin (Fragment)	Bos taurus 	Animalia (Mammals)	ARSRKCFSTIAGYCKKKCMLGEIYDKPCTKGKLCCINESKKKTHQQAVQQPEPASKPDLKLDYV	64		Antimicrobial		Predicted
