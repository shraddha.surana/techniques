  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ1327	71152314	Q8R2I8		Beta-defensin 10	Mus musculus	Animalia (Mammals)	DLKHLILKAQLTRCYKFGGFCHYNICPGNSRFMSNCHPENLRCCKNIKQF	50		Antimicrobial		Predicted
CAMPSQ1592	75263249	Q9FZ31		Putative antifungal protein	Arabidopsis thaliana	Viridiplantae	LCKRESETWSGRCVNDYQCRDHCINNDRGNDGYCAGGYPWYRSCFCFFSC	50		Antifungal		Predicted
CAMPSQ1597	11386640	Q39313		Cysteine-rich antifungal protein 3 precursor 	Brassica napus	Viridiplantae	KLCERSSGTWSGVCGNNNACKNQCIRLEGAQHGSCNYVFPAHKCICYFPC	50		Antifungal		Predicted
CAMPSQ1869	152112320	A4H203		Beta-defensin104A	Pongo pygmaeus	Animalia (Mammals)	EFELDRICGYGTARCRKKCRSQEYRIGRCPNTYACCLRKWDESLLNRTKP	50		Antimicrobial		Predicted
CAMPSQ1870	152112319	A4H204		Beta-defensin104A	Hylobates lar	Animalia (Mammals)	EFEWDRICGYGTARCRNKCRSQEYRIGRCPNTFACCLRKWDESLLNSTKP	50		Antimicrobial		Predicted
CAMPSQ1871	152112318	A4H202		Beta-defensin104A	Gorilla gorilla gorilla	Animalia (Mammals)	EFELDRICGYGTARCRKKCRSQEYRIGRCPNTFACCLRKWDESLLNRTKP	50		Antimicrobial		Predicted
CAMPSQ2012	123779965	Q30KN8		Beta-defensin 25	 Mus musculus	Animalia (Mammals)	EFKRCWNGQGACRTFCTRQETFMHLCPDASLCCLSYSFKPSRPSRVGDV	49		Antibacterial		Predicted
CAMPSQ2041	172048421	A9Q0M7		Bacteriocin ubericin-A	Streptococcus uberis	Bacteria	KTVNYGNGLYCNQKKCWVNWSETATTIVNNSIMNGLTGGNAGWHSGGRA	49		Antibacterial	Gram+ve	Predicted
CAMPSQ2110	61212939	Q5J5Z9		Beta-defensin122	Macaca mulatta	Animalia (Mammals)	VGSIEKCWNFRGSCRDECLKNEKVYVFCMSGKLCCLKPKDQPHLPQRTKN	50		Antibacterial		Predicted
CAMPSQ2115	11386628	O24332		Cysteine-rich antifungal protein 3 precursor 	Raphanus sativus	Viridiplantae	KLCERSSGTWSGVCGNNNACKNQCIRLEGAQHGSCNYVFPAHKCICYFPC	50	7780308	Antifungal		Experimentally Validated
CAMPSQ2291	288816935	D3Y2M3		Antimicrobial peptide  	Aspergillus clavatus  	Fungi	TYDGKCYKKDNICKYKAQSGKTAICKCYVKVCPRDGAKCEFDSYKGKCY	49	20440534	Antibacterial,Antifungal	Gram+ve, Gram-ve	Predicted
CAMPSQ2566	354991235	G8GZ65		Antifungal peptide 4  	Heliophila coronopifolia  	Viridiplantae	KLCERPSGTWSGVCGNNGACRNQCIRLERARHGSCNYVFPAHKCICYFPC	50	22032337	Antifungal		Experimentally Validated
CAMPSQ2567	354991231	G8GZ63		Antifungal peptide 2  	Heliophila coronopifolia  	Viridiplantae	KLCERPSGTWSGVCGNNNACRNQCINLEKARHGSCNYVFPAHKCICYFPC	50	22032337	Antifungal		Experimentally Validated
CAMPSQ3656		H6U5Y1		  Garvieacin Q 	Lactococcus garvieae BCC 43578	Bacteria	EYHLMNGANGYLTRVNGKYVYRVTKDPVSAVFGVISNGWGSAGAGFGPQH	50	22210221	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ4568		P21776		Lysozyme C	Pseudocheirus peregrinus	Animalia (Mammals)	SKMKKCEFAKIAKEQHMDGYHGVSLADWVCLVNNESDFNTKAINRNKGI	49	2605916	Antibacterial		Predicted
CAMPSQ5169	208342469	B6DSR3		EntA (Fragment)	Enterococcus faecium 	Bacteria	FGTTHSGKYYGNGVYCTKIKCTVDWAKATTCIAGMSIGGFLGGAIPGKVQ	50		Antimicrobial		Predicted (Based on signature)
CAMPSQ5383	225353266	C0LRB2		Histatin 1	Nomascus leucogenys 	Animalia (Mammals)	MKFFVFALILALMISMTRADSHEKRHHEHRRKFHEKHHSHREYPFYGYYR	50		Antimicrobial		Predicted (Based on signature)
CAMPSQ5401	226439512	C1KGC4		Pediocin (Fragment)	Pediococcus pentosaceus	Bacteria	ANIIGGKYYGNGVTCGKHSCSVDWGKATTCIINNGAMAWATGGHQGNHKC	50	22277956	Antibacterial	Gram +ve	Predicted (Based on signature)
CAMPSQ5402	226446500	C1KGU4		Pediocin (Fragment)	Pediococcus acidilactici	Bacteria	ANIIGGKYYGNGVTCGKHSCSVDWGKATTCIINNGAMAWATGGHQGNHKC	50		Antimicrobial		Predicted (Based on signature)
CAMPSQ7242	37678091			Human beta-defensin-4 mature peptide, partial [synthetic  construct]. 	Synthetic construct  		EFELDRICGYGTARCRKKCRSQEYRIGRCPNTYACCLRKWDESLLNRTKP	50		Antimicrobial		Predicted (Based on signature)
CAMPSQ7705		W5NQA4		Beta-defensin	Ovis aries 	Animalia (Mammals)	VKLKSSRKRCWNNSGHCKKKCAADEVIRAVCKNRQSCCVSQLAVFVKETI	50		Antimicrobial		Predicted
CAMPSQ7716		H2PUJ5		Beta-defensin	Pongo abelii 	Animalia (Mammals)	ARSFISNDECPSEHYHCRLKCNADEHAIRYCADFSICCKLKIIEIDGQKK	50		Antimicrobial		Predicted
CAMPSQ7724		H2PRL7		Beta-defensin (Fragment)	Pongo abelii 	Animalia (Mammals)	LYLARTAIHRALICKRMEGHCEAECLTFEVKIGGCRAELAPFCCKNRKKH	50		Antimicrobial		Predicted
CAMPSQ7733		H2RC48		Beta-defensin	Pan troglodytes 	Animalia (Mammals)	KFKEICERPNGSCRDFCLETEIHVRRCLNSPCCLPLGHQPRIESTTPKKD	50		Antimicrobial		Predicted
CAMPSQ7739		A8CYD1		Beta-defensin	Pan troglodytes 	Animalia (Mammals)	EFELDRICGYGTARCRKKCRSQEYRIGRCPNTYACCLRKWDESLLNRTKP	50		Antimicrobial		Predicted
