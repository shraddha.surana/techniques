  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ193	1706354	P17722		Royalisin precursor 	Apis mellifera	Animalia (Insects)	VTCDLLSFKGQVNDSACAANCLSLGKAGGHCEKVGCICRKTSFKDLWDKYF	51	2358464	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ922	1703206	P30230		Cysteine-rich antifungal protein 2	Raphanus sativus	Viridiplantae	QKLCQRPSGTWSGVCGNNNACKNQCIRLEKARHGSCNYVFPAHKCICYFPC	51	7780308	Antifungal		Experimentally Validated
CAMPSQ1437	42661541	Q70KL3		Beta defensin 39	 Mus musculus	Animalia (Mammals)	DDSIQCFQKNNTCHTNQCPYFQDEIGTCYDKRGKCCQKRLLHIRVPRKKKV	51		Antimicrobial	Gram+ve, Gram-ve	Predicted
CAMPSQ1447	22748617	Q8NG35		Beta - defensin 105A 	Homo sapiens 	Animalia (Mammals)	GLDFSQPFPSGEFAVCESCKLGRGKCRKECLENEKPDGNCRLNFLCCRQRI	51	12193721	Antimicrobial		Predicted
CAMPSQ1484		P17737	1AFP	Antifungal Protein From Aspergillus Giganteus	Aspergillus giganteus	Fungi	XTYNGKCYKKDNICKYKAQSGKTAICKCYVKKCPRDGAKCEFDSYKGKCYC	51		Antifungal		Predicted
CAMPSQ1978	190359191	A8MXU0		Beta-defensin 108A , Beta-defensin 108B-like	Homo sapiens 	Animalia (Mammals)	KFKEICERPNGSCRDFCLETEIHVGRCLNSRPCCLPLGHQPRIESTTPKKD	51		Antibacterial		Predicted
CAMPSQ1981	166227868	A7X4I7		Waprin-Thr1	Thrasops jacksonii	Animalia (Reptiles)	ENEKAGSCPDVNQPIPPLGLCRNMCESDSGCPNNEKCCKNGCGFMTCSRPR	51		Antibacterial		Predicted
CAMPSQ2734		P17737	1AFP	  Antifungal protein 	Aspergillus giganteus	Fungi	ATYNGKCYKKDNICKYKAQSGKTAICKCYVKKCPRDGAKCEFDSYKGKCYC	51		Antimicrobial		Experimentally Validated
CAMPSQ2746		P17722		Royalisin 	Apis mellifera 	Animalia (Insects)	VTCDLLSFKGQVNDSACAANCLSLGKAGGHCEKVGCICRKTSFKDLWDKRF	51	 2358464	Antimicrobial	Gram+ve	Experimentally Validated
CAMPSQ3256				  AdDLP 	Anaeromyxobacter dehalogenans	Bacteria	VNPSYRLDPESRPQCEAHCGQLGMRLGAIVIMGTATGCVCEPKEAATPESR	51	19615342	Antiparasitic		Experimentally Validated
CAMPSQ3280				  NLP-29 	Caenorhabditis elegans	Animalia (Chromadorea)	QWGYGGYGRGYGGYGGYGRGMYGGYGRGMYGGYGRGMYGGYGRGMYGGWGK	51	15048112	Antifungal		Predicted
CAMPSQ3528				  Hc-AFP2 	Heliophila coronopifolia	Viridiplantae	QKLCERPSGTWSGVCGNNNACRNQCINLEKARHGSCNYVFPAHKCICYFPC	51	22032337	Antifungal		Experimentally Validated
CAMPSQ3530				  Ac-AFP4 	Heliophila coronopifolia	Viridiplantae	QKLCERPSGTWSGVCGNNGACRNQCIRLERARHGSCNYVFPAHKCICYFPC	51	22032337	Antifungal		Experimentally Validated
CAMPSQ5796	319744453	E7S5W2		Lantibiotic lacticin-481	Streptococcus agalactiae ATCC 13813	Bacteria	MEKETTIIESIQEVSLEELDQIIGAGKNGVFKTISHECHLNTWAFLATCCS	51		Antimicrobial		Predicted (Based on signature)
CAMPSQ6049		H2A7D6		Lantibiotic macedocin	Streptococcus macedonicus (strain ACA-DC 198)	Bacteria	MEKETTIIESIQEVSLEELDQIIGAGKNGVFKTISHECHLNTWAFLATCCS	51		Antimicrobial		Predicted (Based on signature)
CAMPSQ6059	297673651	H2PDH2		PREDICTED: histatin-3	Pongo abelii 	Animalia (Mammals)	MKFFIFALILALVISMTGADSHEKRHHGYRRKSHEKHHSHRGYRSNYLYDN	51		Antimicrobial		Predicted (Based on signature)
CAMPSQ6068		H2RA46		Uncharacterized protein	Pan troglodytes 	Animalia (Mammals)	MKFFVFALILALMLSMTGADSHEKRHHGYKRKFHEKHHSHRGYRSNYLYDN	51		Antimicrobial		Predicted (Based on signature)
CAMPSQ7065	446332451			MULTISPECIES: lantibiotic nukacin [Streptococcus]. 	Streptococcus  	Bacteria	MEKETTIIESIQEVSLEELDQIIGAGKNGVFKTISHECHLNTWAFLATCCS	51		Antimicrobial		Predicted (Based on signature)
CAMPSQ7614		Q30KT3		Beta-defensin	Canis familiaris 	Animalia (Mammals)	MSCWGKLGRCRATCEKNEVFHILCTNEAKCCVHPKHVPIGAGSSSAPESLG	51		Antimicrobial		Predicted
CAMPSQ7623		H2PRK0		Beta-defensin (Fragment)	Pongo abelii 	Animalia (Mammals)	KFKEICERPNGSCRDFCLETEIHVGRCLNSRPCCLPLGHQPRIESTTPKKD	51		Antimicrobial		Predicted
CAMPSQ7632		S7NAC8		Beta-defensin	Myotis brandtii 	Animalia (Mammals)	KFKEICERPNGSCQVFCLETEVQAGRCLSGQPCCLPMGHQSRIDPTIPPQD	51		Antimicrobial		Predicted
CAMPSQ7830		A0A0D9RWM7		Beta-defensin	Chlorocebus sabaeus 	Animalia (Mammals)	GLEFSEPFPSGGFAVCESCKLGRGRCRKECLENEKPDGRCRLNFLCCRQKM	51		Antimicrobial		Predicted
CAMPSQ7864		L8HM72		Beta-defensin (Fragment)	Bos mutus	Animalia (Mammals)	SFREVCERPNGSCQEFCLDSEIHSGRCLDGRPCCLPLMNVPEVDPTTPRVR	51		Antimicrobial		Predicted
CAMPSQ7886		G1RYM3		Beta-defensin	Nomascus leucogenys 	Animalia (Mammals)	KFKEICERPNGSCRDFCLETEIHVGRCLNSRPCCLPLGHQPRIESTTPKKD	51		Antimicrobial		Predicted
CAMPSQ8147		C0HJQ3		Histone H2A [Cleaved into:  Acipensin 5 (Ac5)]	Acipenser gueldenstaedtii 	Animalia (Pisces)	SGRGKTGGKARAKAKTRSSRAGLQFPVGRVHRLLRKGNYAQRVGAGAPVYL	51	25558400	Antibacterial, Antifungal		Predicted
