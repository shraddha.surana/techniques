  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ38	115056	P29002		Bombinin-like peptides 1	Bombina orientalis	Animalia (Amphibians)	GIGASILSAGKSALKGLAKGLAEHFAN	27	1744108	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ39	115057	P29003		Bombinin-like peptide 2 	Bombina orientalis	Animalia (Amphibians)	GIGSAILSAGKSALKGLAKGLAEHFAN	27	1744108	Antibacterial,Antifungal	Gram-ve	Experimentally Validated
CAMPSQ41	115060	P29006		Bombinin-like peptide 1	Bombina variegata	Animalia (Amphibians)	GIGGALLSAAKVGLKGLAKGLAEHFAN	27	1712299	Antibacterial		Experimentally Validated
CAMPSQ50	115502205	P84995		Ganodermin	Ganoderma lucidum 	Fungi	AGETHTVMINHAGRGAPKLVVGGKKLS	27	16039755	Antifungal		Experimentally Validated
CAMPSQ553	59798984	P84387		Antimicrobial peptide 1 	Xenopus tropicalis	Animalia (Amphibians)	GFLGPLLKLAAKGVAKVIPHLIPSRQQ	27	11738090	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ630	75416081	Q9KWM4		Lantibiotic nukacin precursor 	Staphylococcus warneri 	Bacteria	KKKSGVIPTVSHDCHMNSFQFVFTCCS	27	16233732 , 16957223	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ829	1536914	P71449		Bacteriocin J46	Lactococcus lactis	Bacteria	KGGSGVIHTISHEVIYNSWNFVFTCCS	27	8588891	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ1311	68052268	Q58T41 , Q58T59		Maximin-6 	Bombina maxima	Animalia (Amphibians)	GIGGALLSAGKSALKGLAKGLAEHFAN	27	11835991	Antibacterial,Antifungal		Experimentally Validated
CAMPSQ2122	124015191	P0C2D3		L-amino-acid oxidase	Eristocophis macmahoni	Animalia (Reptiles)	ADDKNPLEEAFREADYEVFLEIAKNGL	27		Antibacterial		Experimentally Validated
CAMPSQ2589	292630691	P85523		Dermaseptin-1	Phyllomedusa tomopterna	Animalia (Amphibians)	LWKDLLKNVGIAAGKAVLNKVTDMVNQ	27		Antimicrobial		Predicted
CAMPSQ2613	343952547, 343952541, 343952531	G3E7X6, G3E7X3, G3E7W8		Ranatuerin-2N protein precursor, partial  	Pelophylax nigromaculatus	Animalia (Amphibians)	LDTVKGAAKNVAGILLNKLKCKITGDC	27		Antimicrobial		Predicted
CAMPSQ2623	343952543	G3E7X4		Ranatuerin-2N protein precursor, partial  	Pelophylax nigromaculatus	Animalia (Amphibians)	LDTDKGAAKNVAGILLNKLKCKITGDC	27		Antimicrobial		Predicted
CAMPSQ2676	339958754	G1ERV9		Ranatuerin-2YJ precursor  	Rana dybowskii	Animalia (Amphibians)	LMDIFKVAVNKLLAAGMNKPRCKAAHC	27	22702542	Antibacterial,Antiviral	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2887				  human Histatin 2 	Homo sapiens	Animalia (Mammals)	RKFHEKHHSHREFPFYGDYGSNYLYDN	27	Reference: Liao W, Ma D, Wang R, Han Z, Shao T, Li H, Liu S (2009) mRNA Cloning, Evolutionary Analysis and Biological Characterization of Duck Avian Beta-defensin 10. Acta Veterinaria et Zootechnica Sinica, 2009, 40(9): 1320-1326.
	Antimicrobial		Experimentally Validated
CAMPSQ3609				  CPF-P4 	Xenopus petersii	Animalia (Amphibians)	GFGSFLGKALKAALKIGANVLGGAPEQ	27	22123629	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3610				  CPF-P5 	Xenopus petersii	Animalia (Amphibians)	GFGSFLGKALKAALKIGADVLGGAPQQ	27	22123629	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3659				  Lasiocepsin 	Lasioglossum laticeps	Animalia (Insects)	GLPRKILCAIAKKKGKCKGPLKLVCKC	27	22038181	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3670				  Geobacillin II 	Geobacillus thermodenitrificans NG80-2	Bacteria	STIVCVSLRICNWSLRFCPSFKVRCPM	27	22431611	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3681				  Enterocin NKR-5-3D 	Enterococcus faecium	Bacteria	TPGGIDFISGGPHVAQDVLNAIKNFFK	27	22738965	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3682				  CPF-SE1 	Silurana epitropicalis	Animalia (Amphibians)	GFLGPLLKLGLKGVAKVIPHLIPSRQQ	27	22800690	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3683				  CPF-SE2 	Silurana epitropicalis	Animalia (Amphibians)	GFLGPLLKLGLKGAAKLLPQLLPSRQQ	27	22800690	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3944		P83914		Dermaseptin-like peptide 	Schistosoma mansoni 	Animalia	DLWNSIKDMAAAAGRAALNAVTGMVNQ	27	16539015	Antibacterial, Antifungal	Gram+ve	Experimentally Validated
CAMPSQ5709	334350886	E0WX65		Lantibiotic nukacin	Staphylococcus simulans	Bacteria	KKKSGVIPTVSHDCHMNSFQFVFTCCS	27	20627619	Antibacterial		Predicted (Based on signature)
CAMPSQ7015	69552			Melittin, minor	Apis mellifera	Animalia (Insects)	GIGAVLKVLTTGLPALISWISRKKRQQ	27		Antimicrobial		Predicted (Based on signature)
CAMPSQ7283	247410			Zeamatin=22 kda antifungal protein [Zea mays=corn, seed, Peptide  Partial, 27 aa]. 	Zea mays	Viridiplantae	AVFTVVNQCPFTVWAASVPVGGGRQLN	27		Antimicrobial		Predicted (Based on signature)
