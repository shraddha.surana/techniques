  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ108	12644272	P46163		Beta-defensin 5 precursor 	Bos taurus 	Animalia (Mammals)	QVVRNPQSCRWNMGVCIPISCPGNMRQIGTCFGPRVPCCR	40	8454635	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ128	134863	P18312		Sarcotoxin-1D	Sarcophaga peregrina	Animalia (Insects)	GWIRDFGKRIERVGQHTRDATIQTIAVAQQAANVAATLKG	40	3182836	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ130	1352071	P46165		Beta-defensin 7 	 Bos taurus 	Animalia (Mammals)	QGVRNFVTCRINRGFCVPIRCPGHRRQIGTCLGPRIKCCR	40	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ338	298772			Beta-defensin 7 , BNBD-7	Bos taurus 	Animalia (Mammals)	EGVRNFVTCRINRGFCVPIRCPGHRRQIGTCLGPRIKCCR	40	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ339	298774			Beta-defensin 9 , BNBD-9	Bos taurus 	Animalia (Mammals)	EGVRNFVTCRINRGFCVPIRCPGHRRQIGTCLGPQIKCCR	40	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ340	298775			Beta-defensin 10 , BNBD-10	Bos taurus 	Animalia (Mammals)	EGVRSYLSCWGNRGICLLNRCPGRMRQIGTCLAPRVKCCR	40	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ397	401061	P31530		Sapecin-C	Sarcophaga peregrina	Animalia (Insects)	ATCDLLSGIGVQHSACALHCVFRGNRGGYCTGKGICVCRN	40	8471044	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ854	6647599	P82103		Myticin-A	Mytilus galloprovincialis	Animalia (Molluscs (Bivalvia))	HSHACTSYWCGKFCGTASCTHYLCRVLHPGKMCACVHCSR	40	10491159	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ855	6647598	P82102		Myticin-B	Mytilus galloprovincialis	Animalia (Molluscs (Bivalvia))	HPHVCTSYYCSKFCGTAGCTRYGCRNLHRGKLCFCLHCSR	40	10491159	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ886	116086	P14954		Cecropin-A1/A2	Drosophila melanogaster	Animalia (Insects)	GWLKKIGKKIERVGQHTRDATIQGLGIAQQAANVAATARG	40	2390977	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ894	544149	P36192		Defensin	Drosophila melanogaster	Animalia (Insects)	ATCDLLSKWNWNHTACAGHCIAKGFKGGYCNDKAVCVCRN	40	8168509	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ906	134214	P18313	1L4V	Sapecin	Sarcophaga peregrina	Animalia (Insects)	ATCDLLSGTGINHSACAAHCLLRGNRGGYCNGKAVCVCRN	40	3182836	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1139	5902771	P81591		Antimicrobial protein PN-AMP1	Ipomoea nil	Viridiplantae	QQCGRQASGRLCGNRLCCSQWGYCGSTASYCGAGCQSQCR	40	9507071	Antifungal		Experimentally Validated
CAMPSQ1400	31071995			TPA_exp: DEFB4-like protein	Papio anubis	Animalia (Mammals)	IRNPVTCIRSGAICYPRSCPGSYKQIGVCGVSVIKCCKKP	40		Antibacterial,Antifungal,Antiviral	Gram+ve	Predicted
CAMPSQ1900	71152323	Q8R2I3		Beta-defensin35	 Mus musculus	Animalia (Mammals)	EIAVCETCRLGRGKCRRACIESEKIVGWCKLNFFCCRERI	40		Antibacterial		Predicted
CAMPSQ3187				  AalDefD 	Aedes albopictus	Animalia (Insects)	ATCDLLSGFGVGDSACAAHCIARGNRGGYCNSKKVCVCPI	40	10469248	Antimicrobial		Experimentally Validated
CAMPSQ3188				  Smd2 	Stomoxys calcitrans	Animalia (Insects)	ATCDLLSMWNVNHSACAAHCLLLGKSGGRCNDDAVCVCRK	40	9326639	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ3418				  Beta-amyloid peptide(1-40) 	Homo sapiens	Animalia (Mammals)	DAEFRHDSGYEVHHQKLVFFAEDVGSNKGAIIGLMVGGVV	40	20209079	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3933		P86253		Beta-defensin 1	Emys orbicularis 	Animalia (Reptiles)	YDLSKNCRLRGGICYIGKCPRRFFRSGSCSRGNVCCLRFG	40	19253295	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4482		Q0MWV8	2LG4	Aurelin	Aurelia aurita	Animalia	AACSDRAHGHICESFKSFCKDSGRNGVKLRANCKKTCGLC	40	16890198	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ6792	21355059	Q8L0Y0		Enterocin p-like bacteriocin (Fragment)	Enterococcus faecium 	Bacteria	YDNGIYCNNSKCWVNWGEAKENIAGIVISGWASGLAGMGH	40	12859761	Antimicrobial		Predicted (Based on signature)
CAMPSQ7534				DmCecA	Aedes albopictus 	Animalia (Insects)	GWLKKIGKKIERAGQHTRDATIQGLGIAQQAANVAATARG	40	10413113	Antimicrobial		Predicted
CAMPSQ7801		Q5IAB1		Beta-defensin (Fragment)	Chlorocebus aethiops 	Animalia (Mammals)	QECIFDEKCDELKGACKKHCEKNEELTSFCQKSLKCCRTI	40		Antimicrobial		Predicted
CAMPSQ7810		G3TIW1		Beta-defensin 126	Loxodonta africana 	Animalia (Mammals)	NWYLKKCANKSGNCRKKCRNGEMQTQPSTSRCPRAKICCI	40		Antimicrobial		Predicted
CAMPSQ8071		I3M4I9		Beta-defensin	Spermophilus tridecemlineatus 	Animalia (Mammals)	EFKRCWKGQGACRTYCTRQESFMHLCPDASLCCLAYALKP	40		Antimicrobial		Predicted
