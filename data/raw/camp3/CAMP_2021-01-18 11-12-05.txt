  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ234	20138937	P82066		Maculatin-1.1 	Litoria genimaculata 	Animalia (Amphibians)	GLFGVLAKVAAHVVPAIAEHF	21	9620615	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ240	21542075	P82951	1S6W	Hepcidin	Morone chrysops x Morone saxatilis	Animalia (Pisces)	GCRFCCNCCPNMSGCGVCCRF	21	11985602	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ482	47117131	P83867		Ocellatin-3	Leptodactylus ocellatus	Animalia (Amphibians)	GVLDILKNAAKNILAHAAEQI	21	15648972	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ754		P37046	1RPB	Rp 71955	Actinomycete Sp9440	Bacteria	CLGIGSCNDFAGCGYAVVCFW	21	8286361	Antiviral		Experimentally Validated
CAMPSQ995	124007192	P85070, P0C2A5, P85071		Nigrocin-2GRc, Grahamin-2, Grahamin-1	Rana grahami	Animalia (Amphibians)	GLLSGILGAGKHIVCGLSGLC	21	16621155 , 16487561	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1273	2842667	Q64263		DFR10_MOUSE Defensin-related cryptdin, related sequence 10 precursor 	Mus musculus	Animalia (Mammals)	CPPCPSCPSCPWCPMCPRCPS	21		Antibacterial		Predicted
CAMPSQ1782	146324985	P0C2V0		Peptide2340	Lycosa singoriensis	Animalia (Arachnids)	MIASHLAFEKLSKLGSKHTML	21		Antibacterial		Predicted
CAMPSQ2284	160554470	C0ILB3		Nigroain-L antimicrobial peptide	Rana nigrovittata	Animalia (Amphibians)	FTMKKSLFLILFLGAIPLSMC	21	19778602	Antimicrobial		Predicted
CAMPSQ2285	160554464	C0ILB0		Nigroain-H antimicrobial peptide	Rana nigrovittata	Animalia (Amphibians)	FTMKKSLLFIFFLGTISLSLC	21	19778602	Antimicrobial		Predicted
CAMPSQ2286	160554462	C0ILA9		Nigroain-H antimicrobial peptide	Rana nigrovittata	Animalia (Amphibians)	FTMKKSLLLIFFLGTISLSLC	21	19778602	Antimicrobial		Predicted
CAMPSQ2287	160554448	C0ILA2		Nigroain-E antimicrobial peptide	Rana nigrovittata	Animalia (Amphibians)	FTMKKSPLLLFFLGTISLSLC	21	19778602	Antimicrobial		Predicted
CAMPSQ2289	160552404	C0IL79		Nigroain-B antimicrobial peptide	Rana nigrovittata	Animalia (Amphibians)	FTMKKSLFLLFFLGTISLSLC	21	19778602	Antimicrobial		Predicted
CAMPSQ2531	310696180	E3SZK3		Ranacyclin-RA1 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TLKKPLSLLFFLGTINLSLCQ	21		Antimicrobial		Predicted
CAMPSQ2539	310696068	E3SZE7		Odorranain-H-RA1 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TMKKPLLLLFFFGTINLSFCQ	21		Antimicrobial		Predicted
CAMPSQ2544	310695556	E3SYP1		Nigrosin-RA1 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TMKKSLLLLFFFGTINLSFCQ	21		Antimicrobial		Predicted
CAMPSQ2805				  Siamycin II 	Streptomyces strains AA3891	Bacteria	CLGIGSCNDFAGCGYAIVCFW	21	7787424	Antiviral		Experimentally Validated
CAMPSQ3298				  Scolopin 1 	Scolopendra mutilans	Animalia (Centipedes)	FLPKMSTKLRVPYRRGTKDYH	21	19716842	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3317				  Nigrocin-2LVb 	Odorrana livida	Animalia (Amphibians)	GILSGILGMGKKLVCGLSGLC	21	20158520	Antimicrobial		Experimentally Validated
CAMPSQ3318				  Nigrocin-2SCa 	Rana schmackeri	Animalia (Amphibians)	GILSGILGAGKSLVCGLSGLC	21	20158520	Antimicrobial		Experimentally Validated
CAMPSQ3319				  Nigrocin-2SCb 	Rana schmackeri	Animalia (Amphibians)	GILSGVLGMGKKIVCGLSGLC	21	20158520	Antimicrobial		Experimentally Validated
CAMPSQ3320				  Nigrocin-2SCc 	Rana schmackeri	Animalia (Amphibians)	GILSNVLGMGKKIVCGLSGLC	21	20158520	Antimicrobial		Experimentally Validated
CAMPSQ3809				  NAI-802 	Actinoplanes sp.	Bacteria	ASSGWVCTLTIECGTVICACR	21	23168402	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ4076	425872849	K7Z906		Ranacyclin-AJ antimicrobial peptide precursor 	Amolops jingdongensis 	Animalia (Amphibians)	RAAFRGCWTKSYSPKPCLGKR	21	22828809	Antimicrobial		Predicted
CAMPSQ4331				  Siamycin II	Streptomyces strain AA6532	Bacteria	CLGIGSCNDFAGCGYAIVCFW	21	8557614	Antibacterial, Antifungal	Gram+ve	Experimentally Validated
CAMPSQ4600	405944872, 405944871			Chain C, D, Co-Complex Of The Of Ns3-4a Protease With The Inhibitory  Peptide Cp5-46-A	Synthetic construct		GELGRLVYLLDGPGYDPIHCD	21	22965230	Antiviral		Experimentally Validated
