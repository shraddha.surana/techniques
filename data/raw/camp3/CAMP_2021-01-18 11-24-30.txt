  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ542	5902771	P81591		Antimicrobial protein PN-AMP1	Ipomoea nil	Viridiplantae	QQCGRQASGRLCGNRLCCSQWGYCGSTASYCGAGCQSQCRS	41	9507071	Antibacterial,Antifungal	Gram+ve	Experimentally Validated
CAMPSQ1055	25090166	Q8T0W8	2E2F	Diapause-specific peptide 	Gastrophysa atrocyanea	Animalia (Insects)	AVRIGPCDQVCPRIVPERHECCRAHGRSGYAYCSGGGMYCN	41	17994764	Antifungal		Experimentally Validated
CAMPSQ1274	145566791	A3RJ36		Lingual antimicrobial peptide 	 Bubalus bubalis	Animalia (Mammals)	VRNSQSCRRNKGICVPIRCPGSMRQIGTCLGAQVKCCRRK	40		Antibacterial		Predicted
CAMPSQ1531	75052275	O62842		Myeloid cathelicidin 3	Equus caballus	Animalia (Mammals)	KRFHSVGSLIQRHQQMIRDKSEATRHGIRIITRPKLLLAS	40		Antimicrobial		Predicted
CAMPSQ1812	82173550	Q6IV26		Gallinacin-5	Gallus gallus	Animalia (Aves)	GLPQDCERRGGFCSHKSCPPGIGRIGLCSKEDFCCRSRWYS	41		Antibacterial		Predicted
CAMPSQ1829	61239620	P0A311	2A2B	Bacteriocin curvacin-A	Lactobacillus curvatus	Bacteria	ARSYGNGVYCNNKKCWVNRGEATQSIIGGMISGWASGLAGM	41		Antibacterial	Gram+ve	Predicted
CAMPSQ2075	122138580	Q30KU3		Beta-defensin 110	Canis familiaris	Animalia (Mammals)	NFDPKYRFERCAKVKGICKTFCDDDEYDYGYCIKWRNQCCI	41		Antibacterial		Predicted
CAMPSQ2083	84028895	Q30KJ6		Beta-defensin133	Pan troglodytes	Animalia (Mammals)	VKCAVKDTYSCFIVRGKCRHECHDFEKPIGFCTKLNANCYM	41		Antibacterial		Predicted
CAMPSQ2553	310696234	E3SZN0		Andersonin-A peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	PLKKSLLLLFFFGTINLSLCQDETNPEEKKRDEEVAKMEE	40		Antimicrobial		Predicted
CAMPSQ2804				  Hinnavin I 	Artogeia rapae	Animalia (Insects)	GWKIGKKLEHHGQNIRDGLISAGPAVFAVGQAATIYAAAK	40	9339895	Antibacterial		Experimentally Validated
CAMPSQ3074				  Gallinacin 6 	Gallus gallus	Animalia (Aves)	DTLACRQSHGSCSFVACRAPSVDIGTCRGGKLKCCKWAPSS	41	17194828	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3075				  Gallinacin 8 	Gallus gallus	Animalia (Aves)	DTVACRIQGNFCRAGACPPTFTISGQCHGGLLNCCAKIPAQ	41	17483936	Antimicrobial		Experimentally Validated
CAMPSQ3085		P83597	1P9Z	  EAFP2 	Eucommia ulmoides	Viridiplantae	ETCASRCPRPCNAGLCCSIYGYCGSGAAYCGAGNCRCQCRG	41	12067732	Antimicrobial		Experimentally Validated
CAMPSQ3086		P84156	1OZZ	  ARD1 	Archaeoprepona demophoon	Animalia (Insects)	DKLIGSCVWGAVNYTSNCNAECKRRGYKGGHCGSFANVNCW	41		Antimicrobial		Experimentally Validated
CAMPSQ3087				  EAFP1 	Eucommia ulmoides	Viridiplantae	ETCASRCPRPCNAGLCCSIYGYCGSGNAYCGAGNCRCQCRG	41	12067732	Antimicrobial		Experimentally Validated
CAMPSQ3365				  Gallin 	Gallus gallus	Animalia (Aves)	LVLKYCPKIGYCSNTCSKTQIWATSHGCKMYCCLPASWKWK	41	20226050	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ3854		P86291		Bacteriocin	Lactococcus sp.	Bacteria	TSYGNGVHCNKSKCWIDVSELETYKAGTVSNPKDILWSLKE	41		Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ5659	269819039	D3UA81		Ranatuerin-2PTa (Fragment)	Rana pretiosa	Animalia (Amphibians)	DDGIEMTEEEVKRGILDLVTHVAKNLAAQLLDKLKCKMTGC	41	20179920	Antimicrobial		Predicted (Based on signature)
CAMPSQ6946	353685472	W1JF39		Plantazolicin	Bacillus pumilus ATCC 7061	Bacteria	MTKITIPTALSAKVHGEGQHLFEPMAARCTCTTIISSSSTF	41		Antimicrobial		Predicted (Based on signature)
CAMPSQ7672		Q5IAB0		Beta-defensin (Fragment)	Callithrix jacchus 	Animalia (Mammals)	KSAFFVERCQRLKGTCVGYRKKNEEIIALCQKFLKCCLTI	40		Antimicrobial		Predicted
CAMPSQ7696		W5PST9		Beta-defensin	Ovis aries 	Animalia (Mammals)	VKCAMKDTYSCFLKRGKCRHACHNFETPVGFCTKLNANCCM	41		Antimicrobial		Predicted
CAMPSQ7962		D2I429		Beta-defensin (Fragment)	Ailuropoda melanoleuca 	Animalia (Mammals)	GEFAACEPCRLSRGKCRRMCTEDEKVVGNCKMNFFCCRRKI	41		Antimicrobial		Predicted
CAMPSQ7968		G1M582		Beta-defensin	Ailuropoda melanoleuca 	Animalia (Mammals)	LSPTLGGTQKCWNLHGRCRQKCSRRERVYIYCTNNKLCCVK	41		Antimicrobial		Predicted
CAMPSQ8180	745997996	P0DKH7		Antimicrobial peptide 1 (Fa-AMP1)	Fagopyrum esculentum	Viridiplantae	AQCGAQGGGATCPGGLCCSQWGWCGSTPKYCGAGCQSNCK	40	12951494	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ8181	745997997	P0DKH8		Antimicrobial peptide 2 (Fa-AMP2)	Fagopyrum esculentum	Viridiplantae	AQCGAQGGGATCPGGLCCSQWGWCGSTPKYCGAGCQSNCR	40	12951494	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
