  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ744	75164757	Q948Z4		Snakin-1	Solanum tuberosum	Viridiplantae	GSSFCDSKCKLRCSKAGLADRCLKYCGICCEECKCVPSGTYGNKHECPCYRDKKNSKGKSKCP	63	11891250	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1960	25090711	P83419		Megourin-3	Megoura viciae	Animalia (Insects)	YLDVNQIASYLLCLGEGAVFNGRKTCQIGCRAACQQPGCGGYKECEQIPNIRLHKYRCHCISG	63		Antibacterial,Antifungal	Gram+ve	Experimentally Validated
CAMPSQ2526	357428701	G8ADN4		ASABF-related peptide  	Suberites domuncula  	Animalia (Demospongiae)	ISCKAGRVGCFASCQVQNCATGYCRGSTCVCSRCGKGTTPFNKFKIWNQLRVLVQKMVDEERA	63	22073005	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3308				  ABF-2 	Caenorhabditis elegans	Animalia (Chromadorea)	DIDFSTCARMDVPILKKAAQGLCITSCSMQNCGTGSCKKRSGRPTCVCYRCANGGGDIPLGAL	63	11772394	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3778				  Ceratoxin 	Ceratophrys calcarata	Animalia (Amphibians)	NVTPATKPTPSKPGYCRVMDELILCPDPPLSKDLCKNDSDCPGAQKCCYRTCIMQCLPPIFRE	63	23200819	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4007	160556648	C0ILK3		Brevinin-2-RN antimicrobial peptide	Rana nigrovittata 	Animalia (Amphibians)	MFPMKKSLLLLFFLGFVSLSLCEQERGADEDEGEDIEEVKRSLWETIKNAGKGFIQNILDKIR	63		Antimicrobial		Predicted
CAMPSQ4059	449269188			Liver-expressed antimicrobial peptide 2 	Columba livia 	Animalia (Aves)	MHWWKVTLVVLLCTLLLSQARAAAVPARAARERRMTPFWRGVSLRPVGASCRDNSECITMLCR	63	23371554	Antimicrobial		Predicted
CAMPSQ4300	293321607	D5GR58		Gallin protein	Gallus gallus 	Animalia (Aves)	MRFLCLVFAVLLLVSLAAPGYGLVLKYCPKIGYCSNTCSKTQIWATSHGCKMYCCLPASWKWK	63	20226050	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ4302	293321601	D5GR60		Gallin protein	Gallus gallus 	Animalia (Aves)	MRFLCLVFTVLLLVSLAAPGYGLVLKYCPKIGYCSNTCSKTQIWATSHGCKMYCCLPASWKWK	63	20226050	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ4846	765342	A7UDN2		Cecropin B	Bombyx mori 	Animalia (Insects)	MNFAKILSFVFALVLALSMTSAAPEPRWKIFKKIEKMGRNIRDGIVKAGPAIEVLGSAKAIGK	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ5050	3819134	B4R1K2		CecA1	Drosophila simulans 	Animalia (Insects)	MNFYNIFVFVALILAITIGQSEAGWLKKIGKKIERVGQHTRDATIQGLGVAQQAANVAATARG	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ5075	198130721	B5DXK3		CecII	Drosophila pseudoobscura pseudoobscura 	Animalia (Insects)	MNYYKLLVFVALHLAISLGKSEAGWLKKIGKKIERVGRHTRDATIQGLGISQQAANVEATAKG	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ5858	337255712	F8UWQ8		Enterocin A (Fragment)	Enterococcus faecalis 	Bacteria	MKHLKILSIKETQLIYGGTTHSGKYYGNGVYCTKNKCTVDWAKATTCIAGMSIGGFLGGAIPR	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6111	512906182	H9IS00		PREDICTED: cecropin-like	Bombyx mori 	Animalia (Insects)	MNFVKILCVVLALMLALSMASAALEPKRKVFKIIEKIGRNVRGGVITAGPAVVVVGQAASVGM	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6625	112984270	Q53X40		Cecropin A	Bombyx mori 	Animalia (Insects)	MNFVRILSFVFALVLALGAVSAAPEPRWKLFKKIEKVGRNVRDGLIKAGPAIAVIGQAKSLGK	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6632	61660433	Q58HM6		HacD	Helicoverpa armigera 	Animalia (Insects)	MNSKIVLFLCVCLVLVSTATAWDFFKELEGAGQRVRDAIISAGPAVDVLTKAKGLYDSSEEKD	63	17900955	Antibacterial		Predicted (Based on signature)
CAMPSQ6639	59675957	Q5F4I1		CecIII	Drosophila pseudoobscura pseudoobscura 	Animalia (Insects)	MNFYKIFIFVALILAISVGQSEAGWLKKLGKRLERVGQHTRDATIQVVGIAQQAANVAATARG	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6640	59675954	Q5F4I3		Cecropin II	Drosophila pseudoobscura pseudoobscura 	Animalia (Insects)	MNYYKLLVFVALHLTISLGTSEAGWLKKIGKKIERVGRHTRDATIQGLGIAQQAANVEATAKG	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6855	112983162	Q9GSH0		Cecropin CBM2-2	Bombyx mori 	Animalia (Insects)	MNFAKILSFVFALVLALSMTSAAPEPRWKIFKKIEKMGWNIRDGIVKAGPAIEVLGSAKAVGK	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6856	112983176	Q9GSH1		Cecropin CBM2	Bombyx mori 	Animalia (Insects)	MNFAKILSFVFALVLALSMTSAAPEPRWKIFKKIEKMGRNIRDGIVKAGPAIEVLGSAKAVGK	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ6857	112983180	Q9GSH2		Cecropin CBM1	Bombyx mori 	Animalia (Insects)	MNFVRILSFVFALVLALGAVSAAPEPRWKLFKKIEKVGRKVRDGLIKAGPAIAVIGQAKFIGK	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ7392		A9XXC6		Cecropin B	Trichoplusia ni	Animalia (Insects)	MNFSRVLLFVFACLVALCSVGAAPEPRWKVFKKIEKMGRNVRDGIIKAGPAIAVLGEAKALGK	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ7394	3929724	Q6LAH5		Cecropin B	Drosophila melanogaster	Animalia (Insects)	MNFNKIFVFVALILVISLGNSEAGWLRKLGKKIERIGQHTRDASIQVLGIAQQAANVAATARG	63		Antimicrobial		Predicted (Based on signature)
CAMPSQ7629		S7MNA1		Beta-defensin	Myotis brandtii 	Animalia (Mammals)	MCFALNLGSRAGLEYSQPFPGGEFAACEPCRLGRGKCRKVCTEDEKVVGSCKLNFFCCRRRIQ	63		Antimicrobial		Predicted
CAMPSQ7644		F7IIL6		Beta-defensin	Callithrix jacchus 	Animalia (Mammals)	KRHILLCMGNSGICRASCKKNEQPYFYCRNYQFCCLQSYMRISISGKEENTDWSYEKQWPRLP	63		Antimicrobial		Predicted
