  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ129	1352070	P46164		Beta-defensin 6 	 Bos taurus 	Animalia (Mammals)	QGVRNHVTCRIYGGFCVPIRCPGRTRQIGTCFGRPVKCCRRW	42	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ164	156630949	P85211		Lebocin-like anionic peptide 1	Galleria mellonella	Animalia (Insects)	EADEPLWLYKGDNIERAPTTADHPILPSIIDDVKLDPNRRYA	42	17194500	Antibacterial,Antifungal	Gram+ve	Experimentally Validated
CAMPSQ166	156633554	P85212		Proline-rich antimicrobial peptide 2 	Galleria mellonella	Animalia (Insects)	EIRLPEPFRFPSPTVPKPIDIDPILPHPWSPRQTYPIIARRS	42	17194500	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ645	82126776	Q6QLR3		Gallinacin-6 	Gallus gallus	Animalia (Aves)	SPIHACRYQRGVCIPGPCRWPYYRVGSCGSGLKSCCVRNRWA	42	17346671	Antibacterial		Experimentally Validated
CAMPSQ907	47117252	Q8T9R8	1ZRX	Stomoxyn	Stomoxys calcitrans	Animalia (Insects)	RGFRKHFNKLVKKVKHTISETAHVAKDTAVIAGSGAAVVAAT	42	12372834	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ929	150387844	P0C1Z8	2DCV	Tachystatin-B1	Tachypleus tridentatus 	Animalia	YVSCLFRGARCRVYSGRSCCFGYYCRRDFPGSIFGTCSRRNF	42	10473569	Antibacterial,Antifungal	Gram+ve	Experimentally Validated
CAMPSQ1657	21465926	P82818	1KV4	Moricin	Bombyx mori	Animalia (Insects)	AKIPIKAIKTVGKAVGKGLRAINIASTANDVFNFLKPKKRKA	42	11997013	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2465	298351528	P86521		Antimicrobial peptide 1a	Leymus arenarius  	Viridiplantae	QKCGEQGRGAKCPNCLCCGRYGFCGSTPDYCGVGCQSQCRGC	42		Antimicrobial		Predicted
CAMPSQ2655	332692913	F6M2J3		Big defensin 2  	Crassostrea gigas	Animalia (Molluscs (Bivalvia))	QAQALLPIASYAGLAVSPPVFAALVTAYGVYALYRYNIRREN	42	21980497	Antimicrobial		Predicted
CAMPSQ2688	343788803	G3KG76		Hepcidin  	Misgurnus mizolepis  	Animalia (Pisces)	ETTPEQSNPLALFRSKRQSHLSMCRYCCKCCRNKGCGFCCKF	42	21959039	Antibacterial	Gram-ve	Predicted
CAMPSQ2713		P46161		  Bovine Beta-defensin 3  	Bos taurus	Animalia (Mammals)	QGVRNHVTCRINRGFCVPIRCPGRTRQIGTCFGPRIKCCRSW	42	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2714		P46163		  Bovine Beta-defensin 5  	Bos taurus	Animalia (Mammals)	QVVRNPQSCRWNMGVCIPISCPGNMRQIGTCFGPRVPCCRRW	42	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3193				  Gallerimycin 	Galleria mellonella	Animalia (Insects)	GVTITVKPPFPGCVFYECIANCRSRGYKNGGYCTINGCQCLR	42	12811766	Antifungal		Experimentally Validated
CAMPSQ7074	317135484			Class IIa bacteriocin mature peptide, partial [synthetic  construct]. 	Synthetic construct  		KYYGNGVHCGKKTCYVDWGQATASIGKIIVNGWTQHGPWAHR	42		Antimicrobial		Predicted (Based on signature)
CAMPSQ7562		P01476		Myotoxin-A (Myotoxin-1)	Crotalus viridis viridis 	Animalia (Reptiles)	MKILYLLFAFLFLAFLSEPGNAYKRCHIKGGHCFPKEKICIPPSSDFGKMDCPWRRKCCKKGSGK	42		Antimicrobial		Predicted
CAMPSQ7563		P24331		Myotoxin-1 (Crotamine-1)	Crotalus durissus terrificus 	Animalia (Reptiles)	YKRCHIKGGHCFPKEKICIPPSSDFGKMDCPWRRKCCKKGSG	42		Antimicrobial		Predicted
CAMPSQ7565		P24332		Myotoxin-2 (Crotamine-2) (Fragment)	Crotalus durissus terrificus 	Animalia (Reptiles)	YKRCHIKGGHCFPKEKICIPPSSDFGKMDCPWRRKSLKKGSG	42		Antimicrobial		Predicted
CAMPSQ7568		P24333		Myotoxin-3 (Crotamine-3)	Crotalus durissus terrificus 	Animalia (Reptiles)	YKRCHIKGGHCFPKGKICIPPSSDFGKMDCPWRRKCCKKGSG	42		Antimicrobial		Predicted
CAMPSQ7569		P86194		Crotamine-IV-3	Crotalus durissus cumanensis 	Animalia (Reptiles)	YKRCHKKEGHCFPKTVICLPPSSDFGKMDCRWKWKCCKKGSVN	42	17828447	Antimicrobial		Predicted
CAMPSQ7571		P86193		Crotamine-IV-2	Crotalus durissus cumanensis 	Animalia (Reptiles)	FLSEPGNAYKQCHKKGGHCFPKEKICIPPSSDFGKMDCRWRWKCCKKRSGK	42	17828447	Antimicrobial		Predicted
CAMPSQ7573		P63327		Crotamine Ile-19 (CRO_Ile-19)	Crotalus durissus ruruima 	Animalia (Reptiles)	MKILYLLFAFLFLAFLSEPGNAYKRCHKKGGHCFPKTVICLPPSSDFGKMDCRWRWKCCKKGSVNNAISI	42		Antimicrobial		Predicted
CAMPSQ7591		Q30KQ3		Beta-defensin (Fragment)	Homo sapiens 	Animalia (Mammals)	CRSQKSCWIIKGHCRKNCKPGEQVKKPCKNGDYCCIPSNTDS	42		Antimicrobial		Predicted
CAMPSQ7797		F7CF08		Beta-defensin (Fragment)	Equus caballus 	Animalia (Mammals)	CRGRTSCWIVKGHCRKDCKFGEQVKKPCKNGDYCCIPTKTDF	42		Antimicrobial		Predicted
CAMPSQ8092		U3PYC5		Beta-defensin	Oreochromis niloticus 	Animalia (Pisces)	ASFPWSCLSLSGVCRKVCLPTELFFGPLGCGKGSLCCVSHFL	42		Antimicrobial		Predicted
CAMPSQ8094		C9WX71		Beta-defensin (omDB-4)	Oncorhynchus mykiss 	Animalia (Pisces)	FPIPWGCSNYSGVCRAVCLSAELPFGPFGCAKGFVCCVAHVF	42	19709750	Antibacterial	Gram-ve	Predicted
