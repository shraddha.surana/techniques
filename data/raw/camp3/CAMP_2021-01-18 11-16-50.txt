  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ273	2494047	Q62713		Neutrophil antibiotic peptide NP-3 precursor 	Rattus norvegicus	Animalia (Mammals)	CSCRTSSCRFGERLSGACRLNGRIYRLCC	29	2543629	Antibacterial,Antifungal,Antiviral	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ504	51701745	P84115		Ranatuerin-2BYb	Rana boylii 	Animalia (Amphibians)	GIMDSVKGLAKNLAGKLLDSLKCKITGC	28	14531844	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ514	543980	P36191		Ceratotoxin-B	Ceratitis capitata	Animalia (Insects)	SIGSAFKKALPVAKKIGKAALPIAKAALP	29	8353519	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ759	76365059	P84638		Cycloviolin-B	Leonia cymosa	Viridiplantae	GTACGESCYVLPCFTVGCTCTSSQCFKN	28	10813905	Antiviral		Experimentally Validated
CAMPSQ1294	160285698	Q17027	2E3E,2E3F,2E3G,2NY8,2NY9,2NZ3	Def-Acaa	Anopheles gambiae	Animalia (Insects)	VNHALCAAHCIARRYRGGYCNSKAVCVCR	29		Antibacterial	Gram+ve	Predicted
CAMPSQ1296	160285479	Q17027	2E3E,2E3F,2E3G,2NY8,2NY9,2NZ3	Def-Daa	Anopheles gambiae	Animalia (Insects)	WNHTLCAAHCIARRYRGGYCNSKAVCVCR	29		Antibacterial	Gram+ve	Predicted
CAMPSQ1769	119366656	Q1JS90		Ranatuerin-2Vb	Rana versabilis	Animalia (Amphibians)	GIMDTVKGVAKTVAASLLDKLKCKITGC	28		Antimicrobial		Predicted
CAMPSQ1770	119366655	Q1JS92		Ranatuerin-2Va	Rana versabilis	Animalia (Amphibians)	GLLDTIKNTAKNLAVGLLDKIKCKMTGC	28		Antimicrobial		Predicted
CAMPSQ1794	221271921	P86025		Brevinin-2Rd	Rana ridibunda	Animalia (Amphibians)	GILDSLKNLAKNAAQILLNKASCKLSGQC	29		Antimicrobial		Predicted
CAMPSQ1795	221271920	P86024		Brevinin-2Rc	Rana ridibunda	Animalia (Amphibians)	GLMSTLKGAATNVAVTLLNKLQCKLTGTC	29		Antimicrobial		Predicted
CAMPSQ1796	221271919	P86023		Brevinin-2Rb	Rana ridibunda	Animalia (Amphibians)	GLMSTLKGAATNAAVTLLNKLQCKLTGTC	29		Antimicrobial		Predicted
CAMPSQ1797	221271918	P86022		Brevinin-2Ra	Rana ridibunda	Animalia (Amphibians)	GILDSLKNFAKDAAGILLKKASCKLSGQC	29		Antimicrobial		Predicted
CAMPSQ2320	318055821, 318055819	E7EKE3, E7EKE2		Odorranain-F-HN1 antimicrobial peptide precursor  	Odorrana hainanensis  	Animalia (Amphibians)	GFMDTAKNVAKNVAVTLLDKLKCKISGGC	29		Antimicrobial		Predicted
CAMPSQ2535	310696108	E3SZG7		Odorranain-H-RA5 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TISLSLCDQERNADEEERGDEEVAKMEE	28		Antimicrobial		Predicted
CAMPSQ2793				 Kenojeinin I 	Raja kenojei	Animalia (Chondrichthyes)	GKQYFPKVGGRLSGKAPLAAKTHRRLKP	28	15752571	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2817				  human LL-29 	Homo sapiens	Animalia (Mammals)	LLGDFFRKSKEKIGKEFKRIVQRIKDFLR	29	17012259	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3049	325533386	P85130		  kalata B12 	Oldenlandia affinis DC	Viridiplantae	GSLCGDTCFVLGCNDSSCSCNYPICVKD	28	17534989	Antimicrobial		Predicted
CAMPSQ3594				  Odorranain-F-OA1 	Odorrana andersonii	Animalia (Amphibians)	GFMDTAKNVAKNMAGNLLDNLKCKITKAC	29	22029824	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3595				  Odorranain-F-OA3 	Odorrana andersonii	Animalia (Amphibians)	GFMDTAKNVAKNEAGNLLDNLKCKITKAC	29	22029824	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3596				  Odorranain-F-OA4 	Odorrana andersonii	Animalia (Amphibians)	GFMATAKNVAKNMDVTLLDNLKCKITKAC	29	22029824	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3817				Panitide L2 	Panicum laxum	Viridiplantae	QLPICGETCVLGGCYTPNCRCQYPICVR	28	23195955	Antimicrobial		Experimentally Validated
CAMPSQ4595		P0CF02		Dinoponeratoxin Da-3105	Dinoponera australis	Animalia (Insects)	GLKDWWNKHKDKIIAVAKEMGKAGLQAA	28	19879289	Antibacterial		Predicted
CAMPSQ4598		P0CF05		Dinoponeratoxin Da-3177	Dinoponera australis	Animalia (Insects)	GLKDWWNKHKDKIIDVVKEMGKAGLQAA	28	19879289	Antibacterial	Gram-ve, Gram+ve	Predicted
CAMPSQ8178		C0HJH7		Dinoponeratoxin Dq-3104	Dinoponera quadriceps 	Animalia (Insects)	GLKDWWNKHKDKIVEVVKDSGKAGLNAA	28	24157790	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ8179		C0HJH6		Dinoponeratoxin Dq-3162/Dq-3163/Dq-3178	Dinoponera quadriceps 	Animalia (Insects)	GLKDWWNKHKDKIVEVVKEMGKAGLNAA	28	24157790	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
