  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ72	1169818	P80400		Gaegurin-6	Rana rugosa 	Animalia (Amphibians)	FLPLLAGLAANFLPTIICKISYKC	24	7999137	Antibacterial,Antifungal,Antiparasitic	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ330	29336801	Q8QFQ5		Brevinin-1Pb precursor	Rana pipiens	Animalia (Amphibians)	FLPIIAGIAAKVFPKIFCAISKKC	24	10651828	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ596	728976	P40835		Brevinin-1Ea	Rana esculenta (edible frog )	Animalia (Amphibians)	FLPAIFRMAAKVVPTIICSITKKC	24	8163497	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ597	728977	P40836		Brevinin-1Eb	Rana esculenta (edible frog )	Animalia (Amphibians)	VIPFVASVAAEMMPHVYCAASRKC	24	8163497	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ598	728978	P32412 , P84839		Brevinin-1E	Rana esculenta (edible frog )	Animalia (Amphibians)	FLPLLAGLAANFLPKIFCKITRKC	24	8508915	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ831		P0CJ27		Ascaphin-3	Ascaphus truei	Animalia (Amphibians)	GFRDVLKGAAKAFVKTVAGHIANI	24	15207717	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ832				Ascaphin-5	Ascaphus truei	Animalia (Amphibians)	GIKDWIKGAAKKLIKTVASNIANQ	24	15207717	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ833		P0CJ31		Ascaphin-7	Ascaphus truei	Animalia (Amphibians)	GFKDWIKGAAKKLIKTVASSIANQ	24	15207717	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ861	14030273	Q90VX5		Winter flounder 1	Pseudopleuronectes americanus	Animalia (Pisces)	GKGRWLERIGKAGGIIIGGALDHL	24	12878506	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ864	14030279	Q90VW7		Winter flounder 3	Pseudopleuronectes americanus	Animalia (Pisces)	FLGALIKGAIHGGRFIHGMIQNHH	24	12878506	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1085	224487672	P0C8T1		Brevinin-1PTa	Rana picturata	Animalia (Amphibians)	FMGGLIKAATKIVPAAYCAITKKC	24	18621071	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1086	224487671	P0C8S8		Brevinin-1HSb	Rana hosii	Animalia (Amphibians)	FLPAVLRVAAQVVPTVFCAISKKC	24	18621071	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1087	224487670	P0C8S7		Brevinin-1HSa	Rana hosii	Animalia (Amphibians)	FLPAVLRVAAKIVPTVFCAISKKC	24	18621071	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1096	66773811	P69836		Grammistin GsG	Grammistes sexlineatus	Animalia (Pisces)	LFGFLIPLLPHIIGAIPQVIGAIR	24		Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1887	66773947	P69838		Grammistin Pp4b	Pogonoperca punctata	Animalia (Pisces)	LFGFLIPLLPHLIGAIPQVIGAIR	24		Antibacterial	Gram+ve, Gram-ve	Predicted
CAMPSQ2878				  Dermaseptin-S9 	Phyllomedusa sauvagei	Animalia (Amphibians)	GLRSKIWLWVLLMIWQESNKFKKM	24	16401077	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3378				  Microbisporicin A1 	Microbispora corallina	Bacteria	VTSWSLCTPGCTSPGGGSNCSFCC	24	18215770	Antimicrobial		Experimentally Validated
CAMPSQ3388				  WLBU2 	Synthetic construct		RRWVRRVRRWVRRVVRVVRRWVRR	24	15616311	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3407				  Brevinin-1Ed 	Rana esculenta	Animalia (Amphibians)	VIPFVASVAAEMMHHVYCAASKRC	24	15232222	Antimicrobial		Experimentally Validated
CAMPSQ3646				  Gaegurin-RN1 	Rana nigrovittata	Animalia (Amphibians)	FIGPVLKIAAGILPTAICKIFKKC	24	19778602	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3647				  Gaegurin-RN4 	Rana nigrovittata	Animalia (Amphibians)	FVGPVLKIAAGILPTAICKIYKKC	24	19778602	Antibacterial, Antifungal	Gram+ve	Experimentally Validated
CAMPSQ3648				  Gaegurin-RN5 	Rana nigrovittata	Animalia (Amphibians)	FLGPIIKIATGILPTAICKFLKKC	24	19778602	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3662	544602099	E7EKC4		Brevinin-1HN1 	Odorrana hainanensis	Animalia (Amphibians)	FLPLIASLAANFVPKIFCKITKKC	24	22450466	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ4160	403398043	J9RWT7		Brevinin-1DR precursor 	Rana draytonii 	Animalia (Amphibians)	FLPILAGLATKIVPKVFCLITKKC	24		Antimicrobial		Predicted
CAMPSQ7226	229313			Bombinin	Bombina sp.  	Animalia (Amphibians)	GIGALSAKGALKGLAKGLAZHFAN	24		Antimicrobial		Predicted (Based on signature)
