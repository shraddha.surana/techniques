  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ43	115311721	Q1ELU1	2G9P	Latarcin-2a 	Lachesana tarabaevi	Animalia (Arachnids)	GLFGKLIKKFGRKAISYAVKKARGKH	26	16735513	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ44	115311722	Q1ELU0		Latarcin-2b 	Lachesana tarabaevi	Animalia (Arachnids)	GLFGKLIKKFGRKAISYAVKKARGKN	26	16735513	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ99	122064230	P84859		NGU Guentherin 	Rana guentheri	Animalia (Amphibians)	VIDDLKKVAKKVRRELLCKKHHKKLN	26	16979798	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ311	27806725	P19661	4JWC, 4JWD	Cathelicidin antimicrobial peptide 	Bos taurus 	Animalia (Mammals)	QCVGTITLDQSDDLFDLNCNELQSVR	26	8706679	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ327	29123270	Q800R4		Dermaseptin-like PBN2	Phyllomedusa bicolor	Animalia (Amphibians)	GLVTSLIKGAGKLLGGLFGSVTGGQS	26	12709067	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ353	32396178	Q7SZH4		Pleurocidin-like peptide AP1	Hippoglossoides platessoides	Animalia (Pisces)	GWKSVFRKAKKVGKTVGGLALDHYLG	26	12878506	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ354	32396180	Q7SZH3		Pleurocidin-like peptide AP2	Hippoglossoides platessoides	Animalia (Pisces)	GWKKWFNRAKKVGKTVGGLAVDHYLG	26	12878506	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ356	32396186			Pleurocidin-like peptide GcSc4C5	Glyptocephalus cynoglossus	Animalia (Pisces)	GWGSIFKHIFKAGKFIHGAIQAHNDG	26	12878506	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ863				Winter flounder 1a1	Pseudopleuronectes americanus	Animalia (Pisces)	GRRKRKWLRRIGKGVKIIGGAALDHL	26	12878506	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ868				Witch flounder GcSc4C5	Glyptocephalus cynoglossus	Animalia (Pisces)	AGWGSIFKHIFKAGKFIHGAIQAHND	26	12878506	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1102	66773817	P69846		GrammistinGsC	Grammistes sexlineatus	Animalia (Pisces)	NWRKILGKIAKVAAGLLGSMLAGYQV	26	15777955	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1323	146324984	P0C2U8		Lycocitin-3	Lycosa singoriensis	Animalia (Arachnids)	KIKWFKTMKSLAKFLAKEQMKKHLGE	26	14991689	Antibacterial,Antifungal		Predicted
CAMPSQ1571	74795202	Q6TAX6		Defensin	Bombus ignitus	Animalia (Insects)	HSACAANCLSMGKAGGRCENGVCLCR	26		Antimicrobial		Predicted
CAMPSQ1578	2829414	P50716		Defensin-related cryptdin-related sequence 12	Mus musculus	Animalia (Mammals)	PPCPSCLSCPWCPRCLRCPMCKCNPK	26		Antimicrobial		Predicted
CAMPSQ1579	2829413	P50715		Defensin-related cryptdin-related sequence 7	Mus musculus	Animalia (Mammals)	PRCPPCPRCSWCPRCPTCPRCNCNPK	26		Antimicrobial		Predicted
CAMPSQ1584	75052273	O62840		Myeloid cathelicidin 1	Equus caballus	Animalia (Mammals)	KRFGRLAKSFLRMRILLPRRKILLAS	26		Antimicrobial		Predicted
CAMPSQ1889	190358830	P85170		Cathelicidin-3.4	Capra hircus	Animalia (Mammals)	RFRLPFRRPPIRIHPPPFYPPFRRFL	26		Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2618	343952453	G3E7S9, G3E7S2		Esculentin-2N protein precursor, partial  	Pelophylax nigromaculatus	Animalia (Amphibians)	VVAKGLGKEVGKFGLDLMACKVTNQC	26		Antimicrobial		Predicted
CAMPSQ3396				  Pc-CATH1 	Phasianus colchicus	Animalia (Aves)	RIKRFWPVVIRTVVAGYNLYRAIKKK	26	20955730	Antibacterial, Antifungal	Gram +ve, Gram -ve	Experimentally Validated
CAMPSQ5159	158514910	B6C8I8		Cecropin B (Fragment)	Drosophila sechellia 	Animalia (Insects)	QHTRDASIQVLGIAQQAANVAATARG	26		Antimicrobial		Predicted (Based on signature)
CAMPSQ6956	786336	Q9S8K5		Group 5 neutral pathogenesis-related protein (Fragment)	Nicotiana tabacum 	Viridiplantae	SGVFEVHNNXPYTVWAAATPVGGGRR	26		Antimicrobial		Predicted (Based on signature)
CAMPSQ7007	738227			Caerin	Litoria gilleni	Animalia (Amphibians)	GLLSVLLGSVAKHVLPHVVPVIAEHL	26		Antimicrobial		Predicted (Based on signature)
CAMPSQ7009	229445			Melittin 	Apis florea	Animalia (Insects)	GIGAILKVLATGLPTLISWIKNKRKQ	26		Antimicrobial		Predicted (Based on signature)
CAMPSQ8254				CP26	Synthetic construct		KWKSFIKKLTSAAKKVVTTAKPLISS	26	10898680	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ8255				CP29	Synthetic construct		KWKSFIKKLTTAVKKVLTTGLPALIS	26	10898680	Antibacterial	Gram+ve	Experimentally Validated
