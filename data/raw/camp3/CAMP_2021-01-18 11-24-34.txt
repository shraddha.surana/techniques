  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ106	12585254	P82656		Hadrurin	Hadrurus aztecus 	Animalia (Arachnids)	GILDTIKSIASKVWNSKTVQDLKRKGINWVANKLGVSPQAA	41	10931184	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ323	28849941	P46162		Neutrophil beta-defensin 4 	Bos taurus 	Animalia (Mammals)	QRVRNPQSCRWNMGVCIPFLCRVGMRQIGTCFGPRVPCCRR	41	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ335	298769			Beta-defensin 4 , BNBD-4	Bos taurus 	Animalia (Mammals)	ERVRNPQSCRWNMGVCIPFLCRVGMRQIGTCFGPRVPCCRR	41	8454635	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ351	32363160	P83596		Antifungal peptide 1 	Eucommia ulmoides 	Viridiplantae	QTCASRCPRPCNAGLCCSIYGYCGSGNAYCGAGNCRCQCRG	41	12067732	Antifungal		Experimentally Validated
CAMPSQ352	32363161	P83597	1P9G,1P9Z	Antifungal peptide 2 	Eucommia ulmoides 	Viridiplantae	QTCASRCPRPCNAGLCCSIYGYCGSGAAYCGAGNCRCQCRG	41	12067732	Antifungal		Experimentally Validated
CAMPSQ559	61239611	P0A310		Bacteriocin sakacin-A	 Lactobacillus sakei 	Bacteria	ARSYGNGVYCNNKKCWVNRGEATQSIIGGMISGWASGLAGM	41	1487735	Antibacterial		Experimentally Validated
CAMPSQ1344	18491807			Unnamed protein product	Synthetic construct		SLDKRACNFQSCWATCQAQHSIYFRRAFCDRSQCKCVFVRG	41		Antimicrobial		Predicted
CAMPSQ1608	71152311	Q9EPV9		Beta-defensin 5	Mus musculus	Animalia (Mammals)	KTINNPVSCCMIGGICRYLCKGNILQNGNCGVTSLNCCKRK	41		Antibacterial,Antifungal		Predicted
CAMPSQ1609	18202378	P82019		Beta-defensin 4	Mus musculus	Animalia (Mammals)	QIINNPITCMTNGAICWGPCPTAFRQIGNCGHFKVRCCKIR	41		Antibacterial		Predicted
CAMPSQ1610	18203569	Q9WTL0		Beta-defensin 3	Mus musculus	Animalia (Mammals)	KKINNPVSCLRKGGRCWNRCIGNTRQIGSCGVPFLKCCKRK	41		Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ1628	38503249	Q9TT12		Beta-defensin 2	Pan troglodytes	Animalia (Mammals)	GISDPVTCLKSGAICHPVFCPRRYKQIGTCGLPGTKCCKKP	41		Antibacterial		Predicted
CAMPSQ1882	2493155	P80953		Bacteriocin bavaricin-A	Lactobacillus sakei 	Bacteria	KYYGNGVHXGKHSXTVDWGTAIGNIGNNAAANXATGXNAGG	41		Antibacterial	Gram+ve	Predicted
CAMPSQ1892	123780123	Q32ZI4		Beta-defensin3	Rattus norvegicus	Animalia (Mammals)	KKVYNAVSCMTNGGICWLKCSGTFREIGSCGTRQLKCCKKK	41		Antibacterial		Predicted
CAMPSQ1903	5915769	O88514		Beta-defensin 4	Rattus norvegicus	Animalia (Mammals)	QSINNPITCLTKGGVCWGPCTGGFRQIGTCGLPRVRCCKKK	41		Antibacterial		Predicted
CAMPSQ2350	395805486	I6ZVM5		Hepcidin-like antimicrobial peptide precursor  	Channa argus (snakehead)  	Animalia (Pisces)	LEEAGSNDTPVAAHQEMSTESWMMPNHIRQKRQSHISLCRY	41		Antimicrobial		Predicted
CAMPSQ2665	324029026	F1JYN2		Hepcidin 5  	Epinephelus moara	Animalia (Pisces)	LEEAASSDTPVAAYQEMSMESRMMPDHVRQKRQSHLSMCRW	41		Antimicrobial		Predicted
CAMPSQ3399				  gcLEAP-2 	Ctenopharyngodon idella	Animalia (Pisces)	MTPLWRIMGTKPHGAYCQNHYECSTGICRKGHCSYSQPINS	41	19716607	Antimicrobial		Experimentally Validated
CAMPSQ3679				  Bacteriocin LS2 	Lactobacillus salivarius BGHO1	Bacteria	TNWKKIGKCYAGTLGSAVLGFGAMGPVGYWAGAGVGYASFC	41	22739096	Antimicrobial		Experimentally Validated
CAMPSQ3706				  MiAMP2b 	Macadamia integrifolia	Viridiplantae	DPQTECQQCQRRCRQQESGPRQQQYCQRRCKEICEEEEEYN	41	10571855	Antifungal		Experimentally Validated
CAMPSQ7732		H2PJB5		Beta-defensin	Pongo abelii 	Animalia (Mammals)	VKCPMKDNYSCFIMRGKCRHECHDFEKPIGFCTKLNANCYM	41		Antimicrobial		Predicted
CAMPSQ7762		F7G3D0		Beta-defensin	Monodelphis domestica 	Animalia (Mammals)	GLLNEQCQRQEGNCVPMCRINEELVAFCDRFKKCCKNMEPC	41		Antimicrobial		Predicted
CAMPSQ7783		Q7PCK0		Beta-defensin (Fragment)	Papio anubis 	Animalia (Mammals)	AIHRALICKRMEGHCEAECLTFEVKIGGCRAELTPYCCKKR	41		Antimicrobial		Predicted
CAMPSQ7978		G7P4U3		Beta-defensin (Fragment)	Macaca fascicularis 	Animalia (Mammals)	VKCAMKDTYSCFIIRGKCRHECRDFAKPIDFCTKLNANCYM	41		Antimicrobial		Predicted
CAMPSQ8000		G1PR29		Beta-defensin 126	Myotis lucifugus 	Animalia (Mammals)	NWYVRKCANRSGHCRSICKIGELLIDPPTGICSKEKVCCVM	41		Antimicrobial		Predicted
CAMPSQ8007		G1PYI9		Beta-defensin 126	Myotis lucifugus 	Animalia (Mammals)	NWMVKKCANKSGNCRSKCRTGELQIKPHNGMCIKEKMCCVL	41		Antimicrobial		Predicted
