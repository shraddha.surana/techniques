  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ625	74844670	Q95VE8		Md-Cec	Musca domestica (house fly )	Animalia (Insects)	GSPEFGWLKKIGKKIERVGQHTRDATIQTIGVAQQAANVAATLKG	46	15982736	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ631	7674025	P56928		Antimicrobial peptide eNAP-2	Equus caballus	Animalia (Mammals)	EVERKHPLGGSRPGRCPTVPPGTFGHCACLCTGDASEPKGQKCCSN	46	1452336	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ688	729445	P40844		Esculentin-1B precursor	Rana esculenta	Animalia (Amphibians)	GIFSKLAGKKLKNLLISGLKNVGKEVGMDVVRTGIDIAGCKIKGEC	46	8163497	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ889	3913441	O16136		Defensin-1	Stomoxys calcitrans	Animalia (Insects)	AAKPMGITCDLLSLWKVGHAACAAHCLVLGDVGGYCTKEGLCVCKE	46	9326639	Antibacterial	Gram-ve	Experimentally Validated
CAMPSQ913	6225076	P81493		Antifungal protein AX1	Beta vulgaris	Viridiplantae	AICKKPSKFFKGACGRDADCEKACDQENWPGGVCVPFLRCECQRSC	46	7655063	Antifungal		Experimentally Validated
CAMPSQ919	7993732	P82010		Antifungal protein AX2	Beta vulgaris	Viridiplantae	ATCRKPSMYFSGACFSDTNCQKACNREDWPNGKCLVGFKCECQRPC	46	7655063	Antifungal		Experimentally Validated
CAMPSQ945	114152286	P84920		Cp-thionin-2	Vigna unguiculata	Viridiplantae	KTCMTKKEGWGRCLIDTTCAHSCRKYGYMGGKCQGITRRCYCLLNC	46	16824043	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ946	2507457	P09618		Leaf-specific thionin BTH6	Hordeum vulgare	Viridiplantae	KSCCKDTLARNCYNTCRFAGGSRPVCAGACRCKIISGPKCPSDYPK	46	16453847	Antifungal		Experimentally Validated
CAMPSQ1648	147641216	P84969		Defensin Tk-AMP-D6.1	Triticum kiharae	Viridiplantae	RECRSQSKQFVGLCVSDTNCASVCLTEHFPGGKCDGYRRCFCTKDC	46		Antimicrobial		Predicted
CAMPSQ1950	209572857	P0C8B1		Defensin-BvL	Ornithorhynchus anatinus	Animalia (Mammals)	EVRRRRRRPPCEDVNGQCQPRGNPCLRLRGACPRGSRCCMPTVAAH	46		Antimicrobial		Predicted
CAMPSQ2677	384236232	I0CCB2		Defensin  	Malus x domestica  	Viridiplantae	TCEAASGKFKGMCFSSNNCANTCAREKFDGGKCKGFRRRCMCTKKC	46		Antimicrobial		Predicted
CAMPSQ3200				  DEFA1 	Equus caballus	Animalia (Mammals)	AREASKSLIGTASCTCRRAWICRWGERHSGKCIDQKGSTYRLCCRR	46	17620056	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3479	22164302	Q8MVA6		  ISAMP 	Ixodes scapularis	Animalia (Arachnids)	PDPGQPWQVKAGRPPCYSIPCRKHDECRVGSCSRCNNGLWGDRTCR	46	19852941	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3991		A6MAW1		Esculentin-1-OG4 antimicrobial peptide	Odorrana grahami 	Animalia (Amphibians)	GLFSKFAGKGIKNFLIKGVKHIGKEVGLDVIRTGIDVAGCKIKGEC	46	17272268	Antimicrobial		Predicted
CAMPSQ3997		Q1MU21		Esculentin-1S protein	Rana schmackeri	Animalia (Amphibians)	GLFSKFAGKGIKNMIIKGIKGIGKEVGMDVIRTGIDVAGCKIKGEC	46	16427248	Antimicrobial		Predicted
CAMPSQ3999		Q1MU19		Esculentin-1V protein	Odorrana versabilis 	Animalia (Amphibians)	GIFSKFAGKGIKDLIIKGVKGIAKEAGMDVIRTGIDIAGCKIKGEC	46	16427248	Antimicrobial		Predicted
CAMPSQ4000		Q1MU23		Esculentin-1P protein	Rana fukienensis 	Animalia (Amphibians)	GIFSKLAGKKIKNLLISGLKNVGKEVGMDVVRTGIDIAGCKIKGEC	46	16427248	Antimicrobial		Predicted
CAMPSQ7026	69604			Viscotoxin A3	Viscum album	Viridiplantae	KSCCPNTTGRNIYNACRLTGAPRPTCAKLSGCKIISGSTCPSYPDK	46		Antimicrobial		Predicted (Based on signature)
CAMPSQ7091	42741977			Mature divercin RV41 [synthetic construct]. 	Synthetic construct		MDPTKYYGNGVYCNSKKCWVDWGQASGCIGQTVVGGWLGGAIPGKC	46		Antimicrobial		Predicted (Based on signature)
CAMPSQ7836		A0A0D9RWM9		Beta-defensin	Chlorocebus sabaeus 	Animalia (Mammals)	AFFDEKCDELKGACKKHCEKNEELTSFCQKSLKCCRTIQTCGNTID	46		Antimicrobial		Predicted
CAMPSQ7843		K7GKA3		Beta-defensin	Sus scrofa 	Animalia (Mammals)	EFKRCWKGQGACRPYCTRHEAYMQLCPDASLCCLAYGLKMAKIENV	46		Antimicrobial		Predicted
CAMPSQ7848		F1RKV5		Beta-defensin	Sus scrofa 	Animalia (Mammals)	RYPQYGGLDLRRECRRGNGRCKLECREGEIRIAYCMRPATYCCLQK	46		Antimicrobial		Predicted
CAMPSQ8062		H0V3E7		Beta-defensin	Cavia porcellus 	Animalia (Mammals)	KKRYPEYGSLDLRKECRRSKGECRIQCLSNEIRVAFCIRPGTPCCM	46		Antimicrobial		Predicted
CAMPSQ8085		G5B147		Beta-defensin	Heterocephalus glaber 	Animalia (Mammals)	KRRDPEYGSVNLSKECRRSSGQCRAQCYDNEMRVAFCLKPGIHYCM	46		Antimicrobial		Predicted
CAMPSQ8120		U3IWP2		Beta-defensin	Anas platyrhynchos 	Animalia (Aves)	HGPDSCNHEGGLCRVGNCIPGEYLAKYCFEPVILCCKSPSTTTAKS	46		Antimicrobial		Predicted
