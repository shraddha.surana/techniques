  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ224	18202411	P82428		Ponericin-W6	Pachycondyla goeldii 	Animalia (Insects)	FIGTALGIASAIPAIVKLFK	20	11279030	Antibacterial		Experimentally Validated
CAMPSQ480	46577583	P83547		Chrysophsin-3	Pagrus major	Animalia (Pisces)	FIGLLISAGKAIHDLIRRRH	20	12581207	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ490	50401824	P83988		Arietin	Cicer arietinum	Viridiplantae	GVGYKVVVTTTAAADDDDVV	20	12084511	Antifungal		Experimentally Validated
CAMPSQ1016				CA	Synthetic construct		KWKLFKKIGIGAVLKVLTTG	20	1733777	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1773	68052841	Q58T91, Q58T89 , Q58T94		Maximin-Hv 	Bombina maxima	Animalia (Amphibians)	ILGPVLSLVGSALGGLIKKI	20		Antibacterial,Antifungal		Predicted
CAMPSQ1774	68052423	Q58T92		Maximin-Hw	Bombina maxima	Animalia (Amphibians)	ILGPVLGLVSNAIGGLIKKI	20		Antibacterial,Antifungal		Predicted
CAMPSQ1776	68052279	Q58T59		Maximin-H10	Bombina maxima	Animalia (Amphibians)	ILGPVLGLVSNALGGLLKNL	20		Antibacterial,Antifungal		Predicted
CAMPSQ1777	68052278	Q58T58		Maximin-H13	Bombina maxima	Animalia (Amphibians)	ILGPVIKTIGGVLGGLLKNL	20		Antibacterial,Antifungal		Predicted
CAMPSQ1779	68052036	Q58T72		Maximin-H6	Bombina maxima	Animalia (Amphibians)	ILGPVIGTIGNVLGGLLKNL	20		Antibacterial,Antifungal		Predicted
CAMPSQ1780	68052035	Q58T56		Maximin-H7	Bombina maxima	Animalia (Amphibians)	ILGPVIKTIGGVIGGLLKNL	20		Antibacterial,Antifungal		Predicted
CAMPSQ2532	310696172	E3SZJ9		Palustrin-RA peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TMKKPLLLLFFIGTISLSLC	20		Antimicrobial		Predicted
CAMPSQ2534	310696116	E3SZH1		Odorranain-H-RA5 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	PMKKPMLLLFFLGTISLSLC	20		Antimicrobial		Predicted
CAMPSQ2536	310696100			Odorranain-H-RA5 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TLKKPMLLLFFLGTISLSLC	20		Antimicrobial		Predicted
CAMPSQ2537	310696092, 310696086	E3SZF8, E3SZF6		Odorranain-H-RA5 peptide precursor, Odorranain-H-RA3 peptide precursor 	Odorrana andersonii	Animalia (Amphibians)	PLKKSMLLLFFLGTISLSLC	20		Antimicrobial		Predicted
CAMPSQ2538	310696088, 310696114, 310696082	E3SZF7, D2K8D7, E3SZF4		Odorranain-H-RA4 peptide precursor, Odorranain-H-RA5 peptide precursor, Odorranain-H-RA6 peptide precursor	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TMKKPMLLLFFLGTISLSLC	20		Antimicrobial		Predicted
CAMPSQ2545	310695548, 310695546	E3SYN7, E3SYN6		Lividin-14-RA1 peptide precursor, Lividin-14-RA2 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TMKKSLLLLFVLGTINLSLC	20		Antimicrobial		Predicted
CAMPSQ2549	310696314	E3SZS0		Andersonin-X peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TLKKSLLLLFFFGTISLSLC	20		Antimicrobial		Predicted
CAMPSQ2552	310696242	E3SZN4		Andersonin-D peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	TMKKSLLLLFFFGTISLSLC	20		Antimicrobial		Predicted
CAMPSQ2783				  Ranacyclin B3 	Odorrana grahami	Animalia (Amphibians)	AALKGCWTKSIPPKPCSGKR	20	21927839	Antimicrobial		Experimentally Validated
CAMPSQ2788				  D28  	Synthetic construct		FLGVVFKLASKVFPAVFGKV	20	17051220	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2789				Human MUC7 20-Mer 	Homo sapiens	Animalia (Mammals)	LAHQKPFIRKSYKCLHKRCR	20	12543672	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2794				Lunatusin 	Phaseolus lunatus L	Viridiplantae	KTCENLADTFRGPCFATSNC	20	16269344	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2796				Tu-AMP 2 	Tulipa gesneriana L.	Viridiplantae	KSCCRNTTARNCYNVCRIPG	20	15056889	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3808				  Bicarinalin 	Tetramorium bicarinatum	Animalia (Insects)	KIKIPWGKVKDFLVGGMKAV	20	22960382	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ4599	405944876, 405944875			Chain C, D, Co-Complex Structure Of Ns3-4a Protease With The Optimized  Inhibitory Peptide Cp5-46a-4d5e. 	Synthetic construct		GELDELVYLLDGPGYDPIHS	20	22965230	Antiviral		Experimentally Validated
