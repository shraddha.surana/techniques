  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ233	20137316	P82396		Aurein-3.3	Litoria raniformis	Animalia (Amphibians)	GLFDIVKKIAGHIVSSI	17	10951191	Antibacterial,Anticancer	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ481	47117121	P83719		Ranacyclin-T	Rana temporaria	Animalia (Amphibians)	GALRGCWTKSYPPKPCK	17	14636071	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ512	543819	Q06602	4E81	Apidaecin-1A 	Apis mellifera	Animalia (Insects)	GNNRPVYIPQPRPPHPRI	18	2676519	Antibacterial		Experimentally Validated
CAMPSQ532	56554823	Q5GC91, Q5GC92 , Q5GC94		Maximin S4	Bombina maxima	Animalia (Amphibians)	RSNKGFNFMVDMIQALSK	18	15649437	Antibacterial		Experimentally Validated
CAMPSQ547	59798978	P84381		Antimicrobial peptide 7 	Xenopus tropicalis	Animalia (Amphibians)	GLLGPLLKIAAKVGSNLL	18	11738090	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ723	59800386	P69135		Tachyplesin-1 	Tachypleus gigas	Animalia	KWCFRVCYRGICYRRCR	17	2229025	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ726	913448			Antimicrobial peptide , immobilized peptide E17KGG	Synthetic construct		LKLLKKLLKLLKKLGGK	17	7726486	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ752				PlnA -17	Lactobacillus plantarum	Bacteria	GATAIKQVKKLFKKWGW	17	15805109	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ1008				Temporin-1TGc	Rana tagoi 	Animalia (Amphibians)	FLPVILPVIGKLLSGIL	17	16675060	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ1236	109940031	P81904		Histone H2B 3 	Ictalurus punctatus	Animalia (Pisces)	PDPAKTAPKKKSKKAVT	17	9645227	Antibacterial,Antifungal		Experimentally Validated
CAMPSQ1258	59800387	P69136		TAC1_CARRO Tachyplesin-1 	Carcinoscorpius rotundicauda	Animalia	KWCFRVCYRGICYRRCR	17	2229025	Antibacterial	Gram+ve, Gram-ve	Predicted
CAMPSQ2026	57013024	P84334		Thaumatin-like protein	Cassia didymobotrya	Viridiplantae	ATITFTNKCTRTVWPGG	17		Antifungal		Experimentally Validated
CAMPSQ2304	332310267	P86896		Putative antimicrobial peptide 2	Lippia sidoides  	Viridiplantae	PPEAAYGPGNTNSDSGDK	18	21210197	Antifungal		Experimentally Validated
CAMPSQ2588	308197124	P86710		Phylloseptin-P1 	Phyllomedusa palliata	Animalia (Amphibians)	LSLIPHAINAVSAIAKHF	18		Antimicrobial		Predicted
CAMPSQ2786				  CPF-AM1 	Xenopus amieti	Animalia (Amphibians)	GLGSVLGKALKIGANLL	17	20226221	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2819				  Maculatin 2.1	Litoria eucnemis	Animalia (Amphibians)	GFVDFLKKVAGTIANVVT	18	15203252	Antibacterial	Gram+ve	Experimentally Validated
CAMPSQ3064				  CPF-AM4 	Xenopus amieti	Animalia (Amphibians)	GLGSLVGNALRIGAKLL	17	20226221	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3301				  Imcroporin 	Isometrus maculatus	Animalia (Arachnids)	FFSLLPSLIGGLVSAIK	17	19451300	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3330				  Dybowskin-2CDYa 	Rana dybowskii	Animalia (Amphibians)	SAVGRHGRRFGLRKHRKH	18	19539775	Antimicrobial		Experimentally Validated
CAMPSQ3567				  Andersonin-W1 	Odorrana andersonii	Animalia (Amphibians)	ATNIPFKVHFRCKAAFC	17	22029824	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3569				  Andersonin-W2 	Odorrana andersonii	Animalia (Amphibians)	AVNIPFKVHFRCKAAFC	17	22029824	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ3811				  Brevinin 1Tb  	Rana temporaria	Animalia (Amphibians)	LVPLFLSKLICFITKKC	17	23121565	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4067	425872851	K7ZGS2		Jingdongin-1 antimicrobial peptide precursor 	Amolops jingdongensis 	Animalia (Amphibians)	FLPLFLPKIICVITKKC	17	22828809	Antibacterial, Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ8162		S6D3A7		Antimicrobial peptide TsAP-2	Tityus serrulatus 	Animalia (Arachnids)	FLGMIPGLIGGLISAFK	17	23770440	Antibacterial, Antifungal, Anticancer	Gram+ve	Experimentally Validated
CAMPSQ8176		S6CWV8		Antimicrobial peptide TsAP-1	Tityus serrulatus 	Animalia (Arachnids)	FLSLIPSLVGGSISAFK	17	23770440	Antibacterial, Antifungal, Anticancer	Gram+ve, Gram-ve	Experimentally Validated
