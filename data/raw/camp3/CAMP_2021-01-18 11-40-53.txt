  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ1693	75266171	Q9SPL4		Vicin-like antimicrobial peptide 2c-3	Macadamia integrifolia 	Viridiplantae	RQRDPQQQYEQCQERCQRHETEPRHMQTCQQRCERRYEKEKRKQQKRYEEQQREDEEKYEERMKEED	67	10571855	Antibacterial,Antifungal		Predicted
CAMPSQ1699	75207035	Q9SPL3		Vicin-like antimicrobial peptide 2c-3	Macadamia integrifolia 	Viridiplantae	RQRDPQQQYEQCQKRCQRRETEPRHMQICQQRCERRYEKEKRKQQKRYEEQQREDEEKYEERMKEGD	67	10571855	Antibacterial,Antifungal		Experimentally Validated
CAMPSQ1987	152060972	A4K2V4		WAP four-disulfide core domain protein12	Pongo abelii	Animalia (Mammals)	VKKGIEKAGVCPADNVRCFKSDPPQCHTDQDCLGERKCCYLHCGFKCVIPVKELEEGGNKDEDVSRP	67		Antibacterial		Predicted
CAMPSQ1992	152060967	A4K2R5		WAP four-disulfide core domain protein12	Gorilla gorilla gorilla	Animalia (Mammals)	VKEGIEKAGVCPADNVRCFKSDPPQCHTDQDCLGERKCCYLHCGFKCVIPVKELEEGGNKDEDVSRP	67		Antibacterial		Predicted
CAMPSQ2222	256561113			Probactenecin 7 	Bubalus bubalis	Animalia (Mammals)	ALSYREAVLRAVDRINDGSSEANLYRLLELDPPPKDVEDRGARKPASFRVKETVCPRTSQQPLEQCD	67		Antimicrobial		Predicted
CAMPSQ4757	110560564	A6MBL2		Odorranain-F2 antimicrobial peptide	Odorrana grahami 	Animalia (Amphibians)	METLLLLFFLGTISLSLCQEERSADDEEGEVIEEEVKRGFMDTAKNVAKNVAVTLLDNLKCKITKAC	67	17272268	Antimicrobial		Predicted (Based on signature)
CAMPSQ5558	252971779	C5NN20		Hepcidin (Fragment)	Bos taurus 	Animalia (Mammals)	ATCLLLLVLLSLTSGSVLPPQTRQLTDLQTKDTAGAAAGLTPVLQRRRRDTHFPICIFCCGCCRKGT	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ5592	261286787	D0F1S0		Beta-defensin 9	Coturnix coturnix 	Animalia (Aves)	MRILFFLVAVLFFLFQAAPAYSQEDPDTLACRQGHGSCSFVACRAPSVDIGTCRGGKLKCCKWAPSS	67	20396878	Antibacterial		Predicted (Based on signature)
CAMPSQ5813	311274674	F1S7H7		Beta-defensin	Sus scrofa 	Animalia (Mammals)	MKLLLLTLATLLLLSQLSPGGTKKCWNLHGKCRQKCFGKERVYVYCTNNKMCCVKPKYQPRENPWKF	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ5854		F7UI88		Phylloseptin-s5	Phyllomedusa sauvagei 	Animalia (Amphibians)	MSFLKKSLFLVLFLGFVSLSICEEEKRETEEKENEQEDDREERSEEKRLLGMIPVAISAISALSKLG	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ6047		H0WKI7		Beta-defensin	Otolemur garnettii 	Animalia (Mammals)	MKLLLLTLAALLLLSQLTPGGTQRCWNLLGRCRHKCSKKERVYVYCVNSKMCCVKPRYQPKQRPWSF	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ6053	373103965	H2EST2		Leucocin C	Leuconostoc carnosum	Bacteria	MMNMKPTESYEQLDNSALEQVIGGKNYGNGVHCTKKGCSVDWGYAWTNIANNSVMNGLTGGNAGWHN	67	23053070	Antimicrobial		Predicted (Based on signature)
CAMPSQ6057	297706817	H2P1J6		Beta-defensin	Pongo abelii 	Animalia (Mammals)	MKLLLLTLTVLLLLSQLTPGGTQRCWNLYGKCRHRCSKKERVYVYCVNNKMCCVKPKYQPKERWWRF	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ6348	461495561	M4ZUG8		Moronecidin	Oplegnathus fasciatus 	Animalia (Pisces)	MKCITLFLVLSMVVLMAEPGEAFFHHIFNGLVGVGKTIHRLITGGRNQQDQKELDKRFLNQQQAAFN	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ6568	345789882	Q30KT1		Beta-defensin	Canis familiaris 	Animalia (Mammals)	MKLLWLTVAALLLLTQLTPGGTQRCWNLHGKCRQKCSRRERTYVYCTNNKLCCVKPKFQPRENLWPF	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ6570	82775365	Q32ZG0		Beta-defensin	Rattus norvegicus 	Animalia (Mammals)	MKLLLLTLAALLLLSQLTPGDAQKCWNLHGKCRHRCSRKESVYVYCTNGKMCCVKPKYQPKPKPWMF	67	16457734	Antimicrobial		Predicted (Based on signature)
CAMPSQ6571	82775363	Q32ZG5		Beta-defensin	Rattus norvegicus 	Animalia (Mammals)	MKTAVLTMVLLLLLSQVIPGSPEKCWKSFGICREECLRKEKFYIFCWDGSLCCVKPKNVPQWSQSSE	67	16457734	Antimicrobial		Predicted (Based on signature)
CAMPSQ6578	77168460	Q3HM08		Antimicrobial peptide PEN4-3	Litopenaeus vannamei 	Animalia (Crustaceans (Malacostraca))	MRLVVCLVFLASFALVCQGHSSGYTRPLPKPSRPIFIRPIGCDVCYGIPSATARLCCFRYGDCCHGG	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ6582	76786566	Q3HNH4		PEN4-1	Litopenaeus vannamei 	Animalia (Crustaceans (Malacostraca))	MRLVVCLVFLASFALVCQGHSSGYTRPLPKPSRPIFIRPIGCDVCYGIPSSTARLCCFRYGDCCHRG	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ6626	62002139	Q56H80		Antimicrobial peptide PEN4-1	Litopenaeus schmitti 	Animalia (Crustaceans (Malacostraca))	MRLVVCLVFLASFAMVCQGHSSGYTRPLPKPSRPIFIRPIGCDVCYGIPSSTARLCCFRYGDCCHLG	67	16054304	Antimicrobial		Predicted (Based on signature)
CAMPSQ6835	14495012	Q95Z19		Melittin	Polistes sp. 	Animalia (Insects)	MRKSKLYAKTFLAEATCKYLLCSKSYQYHIANCSHSVTLLGIGAVLKVLTTGLPALISWIKRKRQQG	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ7120	488225545			Bacteriocin [Enterococcus faecium]. 	Enterococcus faecium  	Bacteria	MKHCVILGILGTCLAGIGTGIDVDAATYYGNGLYCNKEKCWVNWGQSWSEGLKRWGDNLFGSFSGGR	67		Antimicrobial		Predicted (Based on signature)
CAMPSQ7594		Q5GRG0		Beta-defensin	Homo sapiens 	Animalia (Mammals)	ECWMDGHCRLLCKDGEDSIIRCRNRKRCCVPSRYLTIQPVTIHGILGWTTPQMSTTAPKMKTNITNR	67		Antimicrobial		Predicted
CAMPSQ7840		F1S7I4		Beta-defensin	Sus scrofa 	Animalia (Mammals)	GLFRSSYGKKQEEPWNPCQLYHGTCRNACRKHEILYLTCLDDQKCCLKFSKKIANANEKEDSDSDSN	67		Antimicrobial		Predicted
CAMPSQ7903		F6PM48		Beta-defensin	Canis familiaris 	Animalia (Mammals)	EECWMNGKCRLVCKSDEDSVVRCENRKRCCVPSRYLTVQPMTVERIEPETVPHTPKPVKHKHRPDSK	67		Antimicrobial		Predicted
