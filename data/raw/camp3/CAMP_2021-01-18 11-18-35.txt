  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ201	1730115	P54684		Lebocin-1/2 precursor	Bombyx mori	Animalia (Insects)	DLRFLYPRGKLPVPTPPPFNPKPIYIDMGNRY	32	7654207	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ706				OdE1	Odorrana grahami	Animalia (Amphibians)	GLGGAKKNFIIAANKTAPQSVKKTFSCKLYNG	32	17272268	Antibacterial,Antifungal 	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ969	183448055		2K10	Rantuerin-2csa	Synthetic construct		GILSSFKGVAKGVAKDLAGKLLETLKCKITGC	32	18387372	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ2248	160554562	C0ILF9		Rugosin-RN antimicrobial peptide	Rana nigrovittata	Animalia (Amphibians)	FLSKFKDIALDVAKNAGKGVLTTLACKIDGSC	32		Antimicrobial		Predicted
CAMPSQ2262	110560071	A6MAX1		Esculentin-2-OG3 antimicrobial peptide	Rana grahami	Animalia (Amphibians)	IKDAAKLIGKTVAKEAGKTGLELMACKITNQC	32		Antimicrobial		Predicted
CAMPSQ2485	310695488	E3SYK7		Brevinin-2-RA16 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	FLTSFKDMAIKVAKDAGVNILNTISCKISKTC	32		Antimicrobial		Predicted
CAMPSQ2486	310695476	E3SYK1		Brevinin-2-RA15 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	FLTSFKDMAIKVAKDAGVNILNTISCKIFKTC	32		Antimicrobial		Predicted
CAMPSQ2487	310695472	E3SYJ9		Brevinin-2-RA13 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	ILDCFKNMALNAAKSAGVSVLNALSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2488	310695468	E3SYJ7, E3SYJ5, E3SYJ6		Brevinin-2-RA11 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	LLDTFKNLALNAAKSAGVSVLNSLSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2489	310695460	E3SYJ3		Brevinin-2-RA9 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	ILDTFKNMALNAAKSAGVSVLNALSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2490	310695456	E3SYJ1		Brevinin-2-RA8 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	LLDTIKNMAINAAKGAGVSVLNALSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2491	310695452	E3SYI9, D2K8D4, E3SYI5, D2K8C6, D2K8D8		Brevinin-2-RA6 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	LLDTLKNMAINAAKGAGVSVLNALSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2492	310695436	E3SYI1		Brevinin-2-RA18 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	FLDTLKNMAINAAKGAGVSVLNALSCKLDKSC	32		Antimicrobial		Predicted
CAMPSQ2506	310695494	E3SYL0		Brevinin-2-RA19 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	FLDTLKNMAINAAKDAGVSVLNPLSCKLFKTC	32		Antimicrobial		Predicted
CAMPSQ2507	310695490	E3SYK8		Brevinin-2-RA4 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	LLDTLKNMAINAAKGAGVSVLSALSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2508	310695474	E3SYK0		Brevinin-2-RA14 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	ILDCFKNMALNAAKSAGTSVLNALSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2509	310695470	E3SYJ8		Brevinin-2-RA12 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	ILDTFKNLALNAPKSAGVSVLNALSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2510	310695462	E3SYJ4		Brevinin-2-RA10 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	ILDTFKNLALNAAKSAGVSVLNALSCKLSKTC	32		Antimicrobial		Predicted
CAMPSQ2512	310695454	E3SYJ0		Brevinin-2-RA7 peptide precursor  	Odorrana andersonii (golden crossband frog)  	Animalia (Amphibians)	LLDTFKNLALNAAKSAGVSVLNSLSCKLPKTC	32		Antimicrobial		Predicted
CAMPSQ2520	284930217	D3KDU4		Lividin-SHa antimicrobial peptides  	Odorrana schmackeri	Animalia (Amphibians)	FLDTVKNMALNAAKSAGVSVLKTLSCKLSKEC	32		Antimicrobial		Predicted
CAMPSQ2765		O18496		  Styelin E  	Styela clava 	Animalia	GWLRKAAKSVGKFYYKHKYYIKAAWKIGRHAL	32		Antimicrobial		Predicted
CAMPSQ3021				  Hyfl B 	Hybanthus floribundus E	Viridiplantae	GSPIQCAETCFIGKCYTEELGCTCTAFLCMKN	32	16199617	Antimicrobial		Predicted
CAMPSQ3022				  Hyfl C 	Hybanthus floribundus E	Viridiplantae	GSPRQCAETCFIGKCYTEELGCTCTAFLCMKN	32	16199617	Antimicrobial		Predicted
CAMPSQ3790				  Oreoch-3 	Oreochromis niloticus	Animalia (Pisces)	IWDAIFHGAKHFLHRLVNPGGKDAVKDVQQKQ	32	Reference: Acostaa J, Monterob V, Carpioa Y, Velázqueza J, Garayc HE, Reyesc O, Cabralesc A, Masforrolc Y, Moralesa A, Estrada MP (2013) Cloning and functional characterization of three novel antimicrobial peptides from tilapia (Oreochromis niloticus). A	Antibacterial, Antifungal		Experimentally Validated
CAMPSQ7102	354316			Subtilosin A. 	Bacillus subtilis  	Bacteria	GLGLWGNKGCATCSIGAACLVDGPIPDZIAGA	32		Antimicrobial		Predicted (Based on signature)
