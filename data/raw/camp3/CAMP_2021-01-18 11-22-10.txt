  Camp_ID	gi	UniProt_id	PDBID	Title	Source_Organism	Taxonomy	Seqence	Length	Pubmed_id	Activity	Gram_Nature	Validation
CAMPSQ367	34921480	P83619	2K38	Cupiennin-1	Cupiennius salei 	Animalia (Arachnids)	GFGALFKFLAKKVAKTVAKQAAKQGAKYVVNKQME	35	11792701	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ368	34921482	P83620		Cupiennin-1b	Cupiennius salei 	Animalia (Arachnids)	GFGSLFKFLAKKVAKTVAKQAAKQGAKYIANKQME	35	11792701	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ369	34921483	P83621		Cupiennin-1c	Cupiennius salei 	Animalia (Arachnids)	GFGSLFKFLAKKVAKTVAKQAAKQGAKYIANKQTE	35	11792701	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ370	34921485	P83622		Cupiennin-1d 	Cupiennius salei 	Animalia (Arachnids)	GFGSLFKFLAKKVAKTVAKQAAKQGAKYVANKHME	35	11792701	Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ371	34921485	P83622		Cupiennin-1d*	Cupiennius salei 	Animalia (Arachnids)	GFGSLFKFLAKKVAKTVAKQAAKQGAKYVANKHMQ	35		Antibacterial	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ646	87080648			Pediocin PA-1	Synthetic construct		YYGNGVTCGKHSCSVDWGKATTCIINNGAMAWATG	35	16461660	Antibacterial		Experimentally Validated
CAMPSQ1403	66968898	Q30KR2		Sperm-associated antigen 11 variant E	Canis lupus familiaris	Animalia (Mammals)	RDVICLTQHGTCRLFFCHFGERKAEICSDPWNRCC	35	16033865	Antimicrobial		Predicted
CAMPSQ1404	66968816	Q30KV2		Beta-defensin 1	Canis lupus familiaris	Animalia (Mammals)	DQYICARKGGTCNFSPCPLFTRIDGTCYRGKAKCC	35	16033865	Antimicrobial		Predicted
CAMPSQ1895	81898484	Q8C1N8		Defensin-related cryptdin-22	 Mus musculus	Animalia (Mammals)	SRDLICLCRKRRCNRGELFYGTCAGPFLRCCRRRR	35		Antimicrobial		Predicted
CAMPSQ1898	81882920	Q5G864		Defensin-related cryptdin-25	 Mus musculus	Animalia (Mammals)	CEDLICYCRTRGCKRRERLNGTCRKGHLMYMLWCC	35		Antimicrobial		Predicted
CAMPSQ2693	356483035	G5ELM2		Palustrin-2ISb precursor protein  	Odorrana ishikawae 	Animalia (Amphibians)	LWNSIKIAGKKLFVNVLDKIRCKVAGGCKTSPDVE	35	21911019	Antibacterial,Antifungal	Gram+ve, Gram-ve	Experimentally Validated
CAMPSQ4977	185487104	B2MU74		Pediocin (Fragment)	Pediococcus sp. CRA51	Bacteria	LWQHSCSVDWGKATTCIINNGAMAWATGGHQGNHK	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6009	291501840	G8ACD9		Beta-defensin 2 (Fragment)	Parus major 	Animalia (Aves)	PRREMIFCRGGSCHFGGCPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6010	291501842	G8ACE0		Beta-defensin 2 (Fragment)	Parus major 	Animalia (Aves)	PRREMIFCRGGSCHFGECPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6011	291501844	G8ACE2		Beta-defensin 2 (Fragment)	Cyanistes caeruleus 	Animalia (Aves)	PRREMIFCRGGSCHFGGCPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6012	291501848	G8ACE3		Beta-defensin 2 (Fragment)	Taeniopygia guttata 	Animalia (Aves)	PRREMFLCRGGSCHFGGCPIHLIKVGRCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6013	291501850	G8ACE4		Beta-defensin 2 (Fragment)	Hippolais icterina 	Animalia (Aves)	PRREMLFCKGGSCHFGGCPFHLIKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6014	291501852	G8ACE5		Beta-defensin 2 (Fragment)	Phylloscopus collybita 	Animalia (Aves)	PQREMLFCRGGSCHFGGCPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6015	291501854	G8ACE6		Beta-defensin 2 (Fragment)	Phylloscopus trochilus 	Animalia (Aves)	PQREMLFCRGGSCHFGGCPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6016	291501856	G8ACE7		Beta-defensin 2 (Fragment)	Phylloscopus trochilus 	Animalia (Aves)	PRREMLFCRGGSCHFGGCPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6017	291501858	G8ACE8		Beta-defensin 2 (Fragment)	Turdus merula 	Animalia (Aves)	PRREMLFCRGGSCHFGSCPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6018	291501860	G8ACE9		Beta-defensin 2 (Fragment)	Turdus iliacus	Animalia (Aves)	PRREMLFCRGGSCHFGSCPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6019	291501862	G8ACF0		Beta-defensin 2 (Fragment)	Muscicapa striata 	Animalia (Insects)	PRREMIFCRGGSCHFGACPFHLVKVGSCFGFRSCC	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6022	291502204	G8ACX1		Beta-defensin 13 (Fragment)	Acrocephalus arundinaceus 	Animalia (Aves)	GLSDSQQCRSNRGHCRRLCFHMERWEGSCSNGRLR	35		Antimicrobial		Predicted (Based on signature)
CAMPSQ6023	291502212	G8ACX5		Beta-defensin 13 (Fragment)	Hippolais icterina 	Animalia (Aves)	GLSDSQQCRSNRGHCRRLCFHMERWEGSCSNGRLR	35		Antimicrobial		Predicted (Based on signature)
